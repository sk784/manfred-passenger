package manfred.ru.manfredpassenger.keyboard;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.KeyboardView;
import android.util.Log;
import android.view.View;

import manfred.ru.manfredpassenger.R;

public class ManfredKeyboard extends InputMethodService implements KeyboardView.OnKeyboardActionListener {
    private static final String TAG = "ManfredKeyboard";

    public ManfredKeyboard() {
        super();
    }

    @Override
    public View onCreateInputView() {
        Log.d(TAG, "onCreateInputView: ");
        KeyboardView keyboardView = (KeyboardView) getLayoutInflater().inflate(R.layout.keyboard_view, null);
        keyboardView.setOnKeyboardActionListener(this);
        //Keyboard keyboard = new Keyboard(this,)
        //keyboardView.setKeyboard(mQwertyKeyboard);
        //keyboardView.setBackgroundResource(R.color.keyboard_background);
        return keyboardView;
    }

    @Override
    public void onPress(int primaryCode) {

    }

    @Override
    public void onRelease(int primaryCode) {

    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {

    }

    @Override
    public void onText(CharSequence text) {
        getCurrentInputConnection().commitText(text, 1);
    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }
}
