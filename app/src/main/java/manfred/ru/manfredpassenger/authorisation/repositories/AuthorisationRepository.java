package manfred.ru.manfredpassenger.authorisation.repositories;

import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.ManfredPassengerData;
import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.api.services.models.SmsAuthorisation;
import manfred.ru.manfredpassenger.api.services.models.UserName;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;

public interface AuthorisationRepository {
    void registerPhone(Phone phone, NetworkResponseCallback<SmsAuthorisation> callback);

    void approveSmsCode(SmsAuthorisation smsAuthorisation, NetworkResponseCallback<ManfredPassengerData> callback);

    void enterName(String token, UserName userName, AnalyticsManager analyticsManager, NetworkResponseCallback callback);
}
