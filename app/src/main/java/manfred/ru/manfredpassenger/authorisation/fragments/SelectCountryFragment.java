package manfred.ru.manfredpassenger.authorisation.fragments;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.authorisation.managers.ManfredAuthorisationManager;
import manfred.ru.manfredpassenger.utils.CountryDiffutilCallback;
import manfred.ru.manfredpassenger.utils.country_generator.CountryGenerator;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */

public class SelectCountryFragment extends Fragment implements OnBackPressed {
    private static final String TAG = "SelectCountryFragment";

    private int mColumnCount = 1;

    private OnListFragmentInteractionListener mListener;
    ManfredAuthorisationManager authorisationManager;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SelectCountryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_country_item_list, container, false);
        authorisationManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredAuthorisationManager();

        InputStream raw = getResources().openRawResource(R.raw.flags);
        Reader rd = new BufferedReader(new InputStreamReader(raw));
        final List<CountryModel> countryModels = CountryGenerator.generateCountries(rd, Resources.getSystem().getConfiguration().locale);
        RecyclerView recyclerView = view.findViewById(R.id.list);
        // Set the adapter
        final List<CountryModel> filteredCountries = new ArrayList<>(countryModels);
        final MyCountryItemRecyclerViewAdapter adapter = new MyCountryItemRecyclerViewAdapter(filteredCountries, mListener);
        Context context = view.getContext();
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        recyclerView.setAdapter(adapter);
        authorisationManager.getFilteredString().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s == null) return;
                List<CountryModel> beforeChangerCountries = new ArrayList<>(filteredCountries);
                filteredCountries.clear();
                if (s.isEmpty()) {
                    filteredCountries.addAll(countryModels);
                } else {
                    for (CountryModel p : countryModels) {
                        if (p.getCountryName().toLowerCase().contains(s.toLowerCase())) {
                            filteredCountries.add(p);
                        }
                    }
                }
                Log.d(TAG, "onChanged: after filtering we have " + filteredCountries.size() + " countries");
                CountryDiffutilCallback productDiffUtilCallback =
                        new CountryDiffutilCallback(beforeChangerCountries, filteredCountries);
                DiffUtil.DiffResult productDiffResult = DiffUtil.calculateDiff(productDiffUtilCallback);

                //adapter.setData(productList);
                productDiffResult.dispatchUpdatesTo(adapter);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onBackPress() {
        authorisationManager.getCurrentRegion().observe(this, new Observer<CountryModel>() {
            @Override
            public void onChanged(@Nullable CountryModel countryModel) {
                if(countryModel!=null) {
                    authorisationManager.enterRegionCode(countryModel);
                }else {
                    String countryName;
                    if(Resources.getSystem().getConfiguration().locale.getLanguage().equals("ru")){
                        countryName="Россия";
                    }else {
                        countryName="Russia";
                    }
                    CountryModel russiaDefault = new CountryModel("🇷🇺",countryName,"+7");
                    authorisationManager.enterRegionCode(russiaDefault);
                }
            }
        });

        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(CountryModel item);
    }


}
