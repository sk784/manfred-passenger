package manfred.ru.manfredpassenger.authorisation.managers;

import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;

/**
 * with state machine inside
 */
public interface AuthorisationManager {

    interface StateChangedCallback {
        void newState(AuthState authState);
    }

    enum AuthState {
        START,
        INPUT_PHONE,
        INPUT_SMS,
        INPUT_NAME,
        INPUT_REGION_CODE,
        FINISH,
        NEED_PERMISSION,
        ENTER_CARD
    }

    void acceptLicense();

    void enterPhone(Phone phone);

    void enterSMS(String sms);

    void enterName(String name);

    void selectRegionCode();

    void enterRegionCode(CountryModel countryModel);

    void needPermission();
}
