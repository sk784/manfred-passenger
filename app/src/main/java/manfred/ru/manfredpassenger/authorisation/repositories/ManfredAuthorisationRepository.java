package manfred.ru.manfredpassenger.authorisation.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.services.models.ManfredPassengerData;
import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.api.services.models.SmsAuthorisation;
import manfred.ru.manfredpassenger.api.services.models.UserName;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManfredAuthorisationRepository implements AuthorisationRepository {
    private static final String TAG = "ManfredAuthorisationRep";
    private final NetworkManagerProvider networkManager;

    public ManfredAuthorisationRepository(NetworkManagerProvider networkManager) {
        this.networkManager = networkManager;
    }

    @Override
    public void registerPhone(Phone phone, final NetworkResponseCallback<SmsAuthorisation> callback) {
        networkManager.addQuery(null, "user/register_phone", phone, new WebSocketAnswerCallback() {
                    @Override
                    public void messageReceived(WebSocketAnswer message) {
                        new NetworkUtils().validateSocketResponse(message, callback);
                    }

                    @Override
                    public void error(ManfredNetworkError manfredError) {
                        callback.error(manfredError);
                    }

            @Nullable
            @Override
            public Class getAnswerClass() {
                return null;
            }
        });
    }

    @Override
    public void approveSmsCode(SmsAuthorisation smsAuthorisation, final NetworkResponseCallback<ManfredPassengerData> callback) {
        networkManager.addQuery(null, "user/sms_code", smsAuthorisation, new WebSocketAnswerCallback<WebSocketAnswer<ManfredPassengerData>>() {
                    @Override
                    public void messageReceived(WebSocketAnswer<ManfredPassengerData> message) {
                        if (message.getResultCode().equals("auth_success") || message.getResultCode().equals("phone_registered")) {
                            callback.allOk(message.getPayload());
                        } else {
                            switch (message.getResultCode()) {
                                case "invalid_token":
                                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR,
                                            message.getMessage()));
                                    break;
                                case "invalid_sms_code":
                                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.WRONG_SMS_CODE,
                                            message.getMessage()));
                                    break;
                                default:
                                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER,
                                            "wrong status of server answer: " + message.getMessage()));
                                    break;
                            }
                        }
                    }

                    @Override
                    public void error(ManfredNetworkError manfredError) {

                    }

            @Override
            public Class getAnswerClass() {
                return ManfredPassengerData.class;
            }
        } );
    }

    @Override
    public void enterName(String token, UserName userName, AnalyticsManager analyticsManager, final NetworkResponseCallback callback) {
        networkManager.addQuery(token, "user/register_profile", userName, new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                if (message.getResultCode().equals("success")) {
                    analyticsManager.nameSetted(userName.getName());
                    callback.allOk(null);
                } else {
                    if (message.getResultCode().equals("invalid_token")) {
                        callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR,
                                message.getMessage()));
                    } else {
                        callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER,
                                "wrong status of server answer: " + message.getMessage()));
                    }
                }
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                callback.error(manfredError);
            }

            @Nullable
            @Override
            public Class getAnswerClass() {
                return null;
            }
        });
    }


}
