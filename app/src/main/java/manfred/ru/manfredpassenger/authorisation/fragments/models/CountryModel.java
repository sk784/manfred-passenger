package manfred.ru.manfredpassenger.authorisation.fragments.models;

public class CountryModel {
    private final String countryFlag;
    private final String countryName;
    private final String countryCode;

    public CountryModel(String countryFlag, String countryName, String countryCode) {
        this.countryFlag = countryFlag;
        this.countryName = countryName;
        this.countryCode = countryCode;
    }

    public String getCountryFlag() {
        return countryFlag;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    @Override
    public String toString() {
        return "CountryModel{" +
                "countryFlag='" + countryFlag + '\'' +
                ", countryName='" + countryName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                '}';
    }
}
