package manfred.ru.manfredpassenger.authorisation.fragments;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.authorisation.LoginActivity;
import manfred.ru.manfredpassenger.ride.OrderActivity;
import manfred.ru.manfredpassenger.ride.SelectCityActivity;
import manfred.ru.manfredpassenger.splash.RestoreStateManager;

import static manfred.ru.manfredpassenger.utils.Constants.CITY_SELECTED;


/**
 * A simple {@link Fragment} subclass.
 */
public class PermissionFragment extends Fragment {
    private static final String TAG = "PermissionFragment";


    private static final int MY_PERMISSIONS_REQUEST_GEO = 102;

    public PermissionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_permission, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        Button grantPermission = rootView.findViewById(R.id.btn_geo_preferences);
        grantPermission.setOnClickListener(v -> {
            requestGeoPermission();
        });

        Button selectCity = rootView.findViewById(R.id.btn_select_city);
        selectCity.setOnClickListener(v -> selectCity());
    }

    private void selectCity(){
        Intent intent = new Intent(getActivity(), SelectCityActivity.class);
        getActivity().startActivityForResult(intent, CITY_SELECTED);
    }

    private void requestGeoPermission(){
        Log.d(TAG, "requestGeoPermission: ");
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity(),R.style.AlertDialogCustom);
                alertBuilder.setCancelable(true);
                alertBuilder.setMessage("Мы используем местоположение, чтобы определить тарифы и подать автомобиль точно по адресу.");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_GEO);
                    }
                });
                alertBuilder.create().show();
            } else {
                // No explanation needed; request the permission
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_GEO);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_GEO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    ((ManfredPassengerApplication)getActivity().getApplication()).getAnalyticsManager().geolocationEnabled(true);
                    restoreState();
                } else {
                    ((ManfredPassengerApplication)getActivity().getApplication()).getAnalyticsManager().geolocationEnabled(false);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void restoreState() {
        RestoreStateManager restoreStateManager = new RestoreStateManager(((ManfredPassengerApplication) getActivity().getApplication()).getTokenManager(),
                ((ManfredPassengerApplication) getActivity().getApplication()).getEventManager());
        restoreStateManager.restoreState(new RestoreStateManager.RestoringCallback() {
            @Override
            public void restored() {
                //Intent intent = new Intent(SplashActivity.this, OrderActivity.class);
                Log.d(TAG, "restored: start order activity");
                ((ManfredPassengerApplication) getActivity().getApplication()).removeAutorisationManger();
                final Intent intent = new Intent(getActivity(), OrderActivity.class);
                getActivity().finishAffinity();
                startActivity(intent);
            }

            @Override
            public void failRestoring(ManfredNetworkError manfredNetworkError) {
                Toast.makeText(getActivity().getApplicationContext(), manfredNetworkError.getText(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
