package manfred.ru.manfredpassenger.authorisation.managers;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;
import android.util.Log;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.services.models.ManfredPassengerData;
import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.api.services.models.SmsAuthorisation;
import manfred.ru.manfredpassenger.api.services.models.UserName;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.authorisation.repositories.ManfredAuthorisationRepository;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;

public class ManfredAuthorisationManager implements AuthorisationManager {
    private static final String TAG = "ManfredAuthorisationMan";
    private final MutableLiveData<AuthState> authState;
    private final MutableLiveData<ManfredNetworkError> authError;
    private final MutableLiveData<Boolean> inProgress;
    //private final MutableLiveData<Boolean> needShowSearchBox;
    private final MutableLiveData<String> filteredString;
    private final MutableLiveData<CountryModel> currentRegion;
    private final ManfredAuthorisationRepository manfredAuthorisationRepository;
    private final ManfredTokenManager tokenManager;
    private final AnalyticsManager analyticsManager;
    private @Nullable Phone phone;
    private @Nullable String name;

    @Nullable
    public String getName() {
        return name;
    }

    public ManfredAuthorisationManager(ManfredTokenManager tokenManager, NetworkManagerProvider networkManager, AnalyticsManager analyticsManager) {
        this.tokenManager = tokenManager;
        this.analyticsManager = analyticsManager;
        Log.d(TAG, "ManfredAuthorisationManager: creating...");
        authState = new MutableLiveData<>();
        authState.setValue(AuthState.START);
        manfredAuthorisationRepository = new ManfredAuthorisationRepository(networkManager);
        authError = new MutableLiveData<>();
        inProgress = new MutableLiveData<>();
        inProgress.setValue(false);
        filteredString = new MutableLiveData<>();
        currentRegion = new MutableLiveData<>();
        currentRegion.setValue(new CountryModel("🇷🇺", "Russia", "+7"));
    }

    @Override
    public void acceptLicense() {
        Log.d(TAG, "acceptLicense: ");
        authState.setValue(AuthState.INPUT_PHONE);
    }

    @Override
    public void enterPhone(Phone phone) {
        this.phone = phone;
        inProgress.postValue(true);
        manfredAuthorisationRepository.registerPhone(phone, new NetworkResponseCallback<SmsAuthorisation>() {
            @Override
            public void allOk(SmsAuthorisation response) {
                inProgress.postValue(false);
                authState.setValue(AuthState.INPUT_SMS);
            }

            @Override
            public void error(ManfredNetworkError error) {
                inProgress.postValue(false);
                authError.postValue(error);
            }

        });
    }

    @Override
    public void enterSMS(String sms) {
        Log.d(TAG, "enterSMS: ");
        if (phone == null) {
            authError.postValue(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER,"неверный номер"));
            Log.d(TAG, "enterSMS: phone is null returning");
            return;
        }
        inProgress.postValue(true);
        SmsAuthorisation smsAuthorisation = new SmsAuthorisation(phone.getPhone(), sms, phone.getCountry_code());
        manfredAuthorisationRepository.approveSmsCode(smsAuthorisation, new NetworkResponseCallback<ManfredPassengerData>() {
            @Override
            public void allOk(ManfredPassengerData response) {
                Log.d(TAG, "allOk, sms approved "+response.toString());
                tokenManager.setAuthToken(response.getToken());
                inProgress.postValue(false);
                name = response.getName();
                if (response.isNew()) {
                    authState.postValue(AuthState.INPUT_NAME);
                } else {
                    authState.postValue(AuthState.FINISH);
                }
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: sms not work");
                inProgress.postValue(false);
                authError.postValue(error);
            }
        });
    }

    @Override
    public void enterName(String name) {
        Log.d(TAG, "enterName: ");
        if (name == null) {
            authError.postValue(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER,"недопустимое имя"));
            Log.d(TAG, "enterName: name is null returning");
            return;
        }
        this.name = name;
        inProgress.postValue(true);
        manfredAuthorisationRepository.enterName(tokenManager.getAuthToken(), new UserName(name), analyticsManager, new NetworkResponseCallback() {
            @Override
            public void allOk(Object response) {
                authState.postValue(AuthState.FINISH);
                inProgress.postValue(false);
            }

            @Override
            public void error(ManfredNetworkError error) {
                authError.postValue(error);
            }
        });
    }

    @Override
    public void selectRegionCode() {
        authState.postValue(AuthState.INPUT_REGION_CODE);
    }

    @Override
    public void enterRegionCode(CountryModel countryModel) {
        if (phone != null) {
            phone.setCountry_code(countryModel.getCountryCode());
        } else {
            phone = new Phone(countryModel.getCountryCode(), "");
        }
        currentRegion.postValue(countryModel);
        authState.postValue(AuthState.INPUT_PHONE);
    }

    @Override
    public void needPermission() {
        authState.postValue(AuthState.NEED_PERMISSION);
    }

    @Nullable
    public Phone getPhone() {
        return phone;
    }

    public void reSendSms() {
        inProgress.postValue(true);
        manfredAuthorisationRepository.registerPhone(phone, new NetworkResponseCallback<SmsAuthorisation>() {
            @Override
            public void allOk(SmsAuthorisation response) {
                inProgress.postValue(false);
                //authState.setValue(AuthState.INPUT_SMS);
            }

            @Override
            public void error(ManfredNetworkError error) {
                authError.postValue(error);
            }
        });
    }

    public String getToken() {
        return tokenManager.getAuthToken();
    }

    public void filterCountriesByString(String string) {
        filteredString.postValue(string);
    }

    public MutableLiveData<AuthState> getAuthState() {
        return authState;
    }

    public MutableLiveData<ManfredNetworkError> getAuthError() {
        return authError;
    }

    public MutableLiveData<Boolean> getInProgress() {
        return inProgress;
    }

    public MutableLiveData<String> getFilteredString() {
        return filteredString;
    }

    public MutableLiveData<CountryModel> getCurrentRegion() {
        return currentRegion;
    }
}
