package manfred.ru.manfredpassenger.authorisation.fragments;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import manfred.ru.manfredpassenger.authorisation.fragments.SelectCountryFragment.OnListFragmentInteractionListener;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.databinding.CountryItemBinding;

/**
 * {@link RecyclerView.Adapter} that can display a {@link CountryModel} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class MyCountryItemRecyclerViewAdapter extends RecyclerView.Adapter<MyCountryItemRecyclerViewAdapter.ViewHolder> {

    private final List<CountryModel> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyCountryItemRecyclerViewAdapter(List<CountryModel> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        CountryItemBinding itemBinding =
                CountryItemBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final CountryModel countryModel = mValues.get(position);
        holder.bind(countryModel);

        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(countryModel);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final CountryItemBinding binding;

        public ViewHolder(CountryItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(CountryModel item) {
            binding.setCountry(item);
            binding.executePendingBindings();
        }

    }
}
