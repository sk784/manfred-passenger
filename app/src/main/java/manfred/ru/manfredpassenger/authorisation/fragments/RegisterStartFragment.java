package manfred.ru.manfredpassenger.authorisation.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.authorisation.managers.ManfredAuthorisationManager;
import manfred.ru.manfredpassenger.databinding.FragmentRegisterStartBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterStartFragment extends Fragment {
    private static final String TAG = "RegisterStartFragment";
    ManfredAuthorisationManager manfredAuthorisationManager;
    OnLicenseClicked mListener;

    public RegisterStartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentRegisterStartBinding binding = FragmentRegisterStartBinding.inflate(getActivity().getLayoutInflater());
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        manfredAuthorisationManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredAuthorisationManager();
        binding.registerStartButtonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manfredAuthorisationManager.acceptLicense();
            }
        });
        binding.registerStartPolicyTextCaption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClicked("http://manfred.ru/terms/passenger/privacy-rus.html");
            }
        });
        binding.registerStartLicenseTextCaption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClicked("http://manfred.ru/terms/passenger/license-agreement.html");
            }
        });
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLicenseClicked) {
            mListener = (OnLicenseClicked) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLicenseClicked");
        }
    }


    public interface OnLicenseClicked {
        void onClicked(String url);
    }

}
