package manfred.ru.manfredpassenger.authorisation.fragments;


import android.app.Activity;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.authorisation.managers.ManfredAuthorisationManager;
import manfred.ru.manfredpassenger.databinding.FragmentRegisterEnterPhoneBinding;
import manfred.ru.manfredpassenger.utils.GraphicUtils;
import manfred.ru.manfredpassenger.utils.NumericKeyBoardTransformationMethod;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterEnterPhoneFragment extends Fragment implements LifecycleObserver, OnBackNotHideKeyboard {
    private static final String TAG = "RegisterEnterPhoneFragm";
    //private FragmentRegisterEnterPhoneBinding binding;
    ManfredAuthorisationManager manfredAuthorisationManager;

    public RegisterEnterPhoneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        FragmentRegisterEnterPhoneBinding binding = FragmentRegisterEnterPhoneBinding.inflate(getActivity().getLayoutInflater());
        manfredAuthorisationManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredAuthorisationManager();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        binding.registerPhoneButtonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code;
                if (manfredAuthorisationManager.getPhone() != null) {
                    code = manfredAuthorisationManager.getPhone().getCountry_code();
                    //Log.d(TAG, "onClick: we get phone code "+code);
                } else {
                    code = "+7";
                }
                manfredAuthorisationManager.enterPhone(new Phone(code, binding.registerPhoneNumberEdit.getText().toString()));
                binding.registerPhoneButtonContinue.setEnabled(false);
            }
        });
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        initEdit(binding,binding.registerPhoneNumberEdit);

        //showKeyboard(getActivity());
        return binding.getRoot();
    }

    private void initEdit(final FragmentRegisterEnterPhoneBinding binding, final EditText registerPhoneNumberEdit) {
        Log.d(TAG, "initEdit: ");
        registerPhoneNumberEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                try {
                    String fullPhone;
                    if (manfredAuthorisationManager.getPhone() != null) {
                        fullPhone = "+"+manfredAuthorisationManager.getPhone().getCountry_code() + registerPhoneNumberEdit.getText();
                    } else {
                        fullPhone = "+7" + registerPhoneNumberEdit.getText();
                    }
                    //Log.d(TAG, "onTextChanged: checking " + fullPhone);
                    Phonenumber.PhoneNumber phoneNumberProto = phoneUtil.parse(fullPhone, null);
                    boolean isValid = phoneUtil.isValidNumber(phoneNumberProto); // returns true if valid
                    if (isValid) {
                        binding.registerPhoneButtonContinue.setEnabled(true);
                    } else {
                        binding.registerPhoneButtonContinue.setEnabled(false);
                    }
                } catch (NumberParseException e) {
                    System.err.println("NumberParseException was thrown: " + e.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        registerPhoneNumberEdit.setTransformationMethod(new NumericKeyBoardTransformationMethod());

        binding.registerPhoneNumberContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manfredAuthorisationManager.selectRegionCode();
            }
        });

        manfredAuthorisationManager.getCurrentRegion().observe(this, new Observer<CountryModel>() {
            @Override
            public void onChanged(@Nullable CountryModel countryModel) {
                if(countryModel!=null) {
                    binding.setRegion(countryModel);
                    switch (countryModel.getCountryCode()) {
                        case "+7":
                            binding.registerPhoneNumberEdit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                            break;
                        case "+380":
                            binding.registerPhoneNumberEdit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9)});
                            break;
                        case "+77":
                            binding.registerPhoneNumberEdit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9)});
                            break;
                        default:
                            binding.registerPhoneNumberEdit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
                            break;
                    }
                }
            }
        });

        registerPhoneNumberEdit.requestFocus();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void start() {
        //initEdit(binding, binding.registerPhoneNumberEdit);
        //showKeyboard(getActivity());
    }

    @Override
    public void onResume(){
        super.onResume();
        View view = getView();
        if(view!=null) {
            EditText phoneNumber = view.findViewById(R.id.register_phone_number_edit);
            phoneNumber.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(getActivity()!=null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.showSoftInput(phoneNumber, 0);
                        }
                    }
                }
            },200);
        }
    }

}
