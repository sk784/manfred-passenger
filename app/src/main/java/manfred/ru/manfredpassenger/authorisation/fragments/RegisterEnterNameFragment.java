package manfred.ru.manfredpassenger.authorisation.fragments;


import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.authorisation.managers.ManfredAuthorisationManager;
import manfred.ru.manfredpassenger.databinding.FragmentRegisterEnterNameBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterEnterNameFragment extends Fragment {
    ManfredAuthorisationManager manfredAuthorisationManager;
    FragmentRegisterEnterNameBinding binding;

    public RegisterEnterNameFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentRegisterEnterNameBinding.inflate(getActivity().getLayoutInflater());
        manfredAuthorisationManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredAuthorisationManager();
        binding.enterNameContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manfredAuthorisationManager.enterName(binding.inputName.getText().toString());
            }
        });
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        initEdit(binding);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        View view = getView();
        if(view!=null) {
            EditText phoneNumber = view.findViewById(R.id.input_name);
            phoneNumber.requestFocus();
            phoneNumber.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(getActivity()!=null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.showSoftInput(phoneNumber, 0);
                        }
                    }
                }
            },200);
        }
    }

    private void initEdit(final FragmentRegisterEnterNameBinding binding) {
        //binding.registerPhoneFlag.setText(EmojiUtils.localeToEmoji(new Locale("ru","RU")));
        binding.inputName.requestFocus();
        binding.inputName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.inputName.getText().length() > 1) {
                    binding.enterNameContinue.setEnabled(true);
                } else {
                    binding.enterNameContinue.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }



}
