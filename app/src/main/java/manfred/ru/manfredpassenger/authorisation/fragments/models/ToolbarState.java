package manfred.ru.manfredpassenger.authorisation.fragments.models;

public enum ToolbarState {
    ORDINARY,
    SEARCH,
    WITHOUT_INTERCOM,
    CLEAR_TOOLBAR
}
