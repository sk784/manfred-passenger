package manfred.ru.manfredpassenger.authorisation.fragments.models;

public class PhoneModel {
    private final String phoneText;

    public PhoneModel(String phone) {
        this.phoneText = "Мы отправим смс с кодом на номер \n" + phone;
    }

    public String getPhoneText() {
        return phoneText;
    }
}
