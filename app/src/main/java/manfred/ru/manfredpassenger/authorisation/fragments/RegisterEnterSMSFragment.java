package manfred.ru.manfredpassenger.authorisation.fragments;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.databinding.adapters.TextViewBindingAdapter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.LinkedList;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.authorisation.fragments.models.PhoneModel;
import manfred.ru.manfredpassenger.authorisation.managers.ManfredAuthorisationManager;
import manfred.ru.manfredpassenger.databinding.FragmentRegisterEnterSmBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterEnterSMSFragment extends Fragment implements OnBackPressed {
    private static final String TAG = "RegisterEnterSMSFragmen";
    ManfredAuthorisationManager manfredAuthorisationManager;


    public RegisterEnterSMSFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final FragmentRegisterEnterSmBinding binding = FragmentRegisterEnterSmBinding.inflate(getActivity().getLayoutInflater());
        manfredAuthorisationManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredAuthorisationManager();
        if(manfredAuthorisationManager.getPhone()!=null) {
            binding.setPhone(new PhoneModel(manfredAuthorisationManager.getPhone().getCountry_code()+" "+manfredAuthorisationManager.getPhone().getPhone()));
        }

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        startTimer(binding.resendSms);
        binding.resendSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manfredAuthorisationManager.reSendSms();
                startTimer(binding.resendSms);
            }
        });

        initEditText(binding);
        manfredAuthorisationManager.getAuthError().observe(this, new Observer<ManfredNetworkError>() {
            @Override
            public void onChanged(@Nullable ManfredNetworkError networkError) {
                binding.fourDigit.setText("");
                binding.thirdDigit.setText("");
                binding.secondDigit.setText("");
                binding.firstDigit.setText("");
                binding.firstDigit.requestFocus();
            }
        });
        return binding.getRoot();
    }

    private void initEditText(FragmentRegisterEnterSmBinding binding) {
        View.OnKeyListener secondListener = (v, keyCode, event) -> {
            if(keyCode==KeyEvent.KEYCODE_DEL){
                v.setOnKeyListener(null);
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        binding.firstDigit.requestFocus();
                    }
                });

            }
            return false;
        };

        View.OnKeyListener thirdListener = (v, keyCode, event) -> {
            if(keyCode==KeyEvent.KEYCODE_DEL){
                v.setOnKeyListener(null);
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        binding.secondDigit.requestFocus();
                    }
                });
            }
            return false;
        };

        View.OnKeyListener fourListener = (v, keyCode, event) -> {
            if(keyCode==KeyEvent.KEYCODE_DEL){
                v.setOnKeyListener(null);
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        binding.thirdDigit.requestFocus();
                    }
                });
            }
            return false;
        };

        binding.firstDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count>0){
                    binding.secondDigit.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.firstDigit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    clearEdit((EditText) v);
                }
            }
        });
        binding.secondDigit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    v.setOnKeyListener(secondListener);
                    clearEdit((EditText) v);
                }
            }
        });
        binding.thirdDigit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    v.setOnKeyListener(thirdListener);
                    clearEdit((EditText) v);
                }
            }
        });
        binding.fourDigit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    v.setOnKeyListener(fourListener);
                    clearEdit((EditText) v);
                }
            }
        });

        binding.secondDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1) {
                    binding.secondDigit.setOnKeyListener(null);
                    binding.thirdDigit.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.thirdDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1) {
                    Log.d(TAG, "thirdDigit: binding.fourDigit.requestFocus()");
                    binding.fourDigit.requestFocus();
                    binding.thirdDigit.setOnKeyListener(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.fourDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1) {
                    //binding.fourDigit.addTextChangedListener(thirdWatcher);
                    String pin = binding.firstDigit.getText().toString()+
                            binding.secondDigit.getText().toString()+
                            binding.thirdDigit.getText().toString()+
                            binding.fourDigit.getText().toString();
                    if(pin.length()==4) {
                        manfredAuthorisationManager.enterSMS(pin);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void clearEdit(EditText v) {
        EditText editText = v;
        editText.setText("");
    }

    private void startTimer(final Button resendSms) {
        resendSms.setEnabled(false);
        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                resendSms.setText("Отправить снова (" + millisUntilFinished / 1000 + " сек)");
            }

            public void onFinish() {
                resendSms.setText("Отправить снова");
                resendSms.setEnabled(true);
            }
        }.start();
    }

    @Override
    public boolean onBackPress() {
        //manfredAuthorisationManager.acceptLicense();
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        View view = getView();
        if(view!=null) {
            EditText phoneNumber = view.findViewById(R.id.first_digit);
            phoneNumber.requestFocus();
            phoneNumber.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(getActivity()!=null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.showSoftInput(phoneNumber, 0);
                        }
                    }
                }
            },200);
        }
    }


}
