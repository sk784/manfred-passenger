package manfred.ru.manfredpassenger.authorisation.fragments;

/**
 * using for handle back pressing in fragments
 */
public interface OnBackPressed {
    boolean onBackPress();
}
