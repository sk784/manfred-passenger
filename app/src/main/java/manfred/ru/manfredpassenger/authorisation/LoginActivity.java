package manfred.ru.manfredpassenger.authorisation;

import android.Manifest;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Objects;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.authorisation.fragments.OnBackNotHideKeyboard;
import manfred.ru.manfredpassenger.authorisation.fragments.OnBackPressed;
import manfred.ru.manfredpassenger.authorisation.fragments.PermissionFragment;
import manfred.ru.manfredpassenger.authorisation.fragments.RegisterEnterNameFragment;
import manfred.ru.manfredpassenger.authorisation.fragments.RegisterEnterPhoneFragment;
import manfred.ru.manfredpassenger.authorisation.fragments.RegisterEnterSMSFragment;
import manfred.ru.manfredpassenger.authorisation.fragments.RegisterStartFragment;
import manfred.ru.manfredpassenger.authorisation.fragments.SelectCountryFragment;
import manfred.ru.manfredpassenger.authorisation.fragments.WebViewFragment;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.authorisation.fragments.models.ToolbarState;
import manfred.ru.manfredpassenger.authorisation.managers.AuthorisationManager;
import manfred.ru.manfredpassenger.authorisation.managers.ManfredAuthorisationManager;
import manfred.ru.manfredpassenger.common.repositories.FirebaseTokenRepository;
import manfred.ru.manfredpassenger.databinding.ActivityLoginBinding;
import manfred.ru.manfredpassenger.ride.OrderActivity;
import manfred.ru.manfredpassenger.splash.RestoreStateManager;
import manfred.ru.manfredpassenger.utils.Constants;

import static manfred.ru.manfredpassenger.utils.Constants.CITY_SELECTED;

public class LoginActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener,
        SelectCountryFragment.OnListFragmentInteractionListener,
        RegisterStartFragment.OnLicenseClicked {
    private static final String TAG = "LoginActivity";
    private VideoView videoView;
    ManfredAuthorisationManager manfredAuthorisationManager;
    private ActivityLoginBinding binding;
    final MutableLiveData<ToolbarState> toolbarState = new MutableLiveData<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        videoView = findViewById(R.id.login_videoview);
        startVideo();
        manfredAuthorisationManager = ((ManfredPassengerApplication)getApplication()).getManfredAuthorisationManager();
        LiveData<AuthorisationManager.AuthState> authStateLiveData = manfredAuthorisationManager.getAuthState();
        LiveData<ManfredNetworkError> errorLiveDate = manfredAuthorisationManager.getAuthError();
        LiveData<Boolean> inProgress = manfredAuthorisationManager.getInProgress();

        final Toolbar toolbar = findViewById(R.id.auth_bar);
        final ProgressBar progressBar = binding.toolbarProgressBar;
        final EditText searchEdit = toolbar.findViewById(R.id.auth_bar_search_country);
        initEdit(searchEdit);
        setSupportActionBar(toolbar);


        authStateLiveData.observe(this, new Observer<AuthorisationManager.AuthState>() {
            @Override
            public void onChanged(@Nullable AuthorisationManager.AuthState authState) {
                if (authState == null) return;
                Log.d(TAG, "onChanged: " + authState);
                changeFragmentContainerState(authState, toolbarState);
            }
        });

        //for test entersms
        //changeFragmentContainerState(AuthorisationManager.AuthState.INPUT_SMS,toolbarState);

        errorLiveDate.observe(this, s -> {
            if(s!=null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this,R.style.AlertDialogCustom);
                if(s.getType().equals(ManfredNetworkError.ErrorType.WRONG_SMS_CODE)) {
                    builder.setTitle(s.getText());
                    builder.setMessage("Проверьте код и повторите попытку");
                }else {
                    builder.setTitle("Ошибка");
                    builder.setMessage(s.getText());
                }
                builder.setPositiveButton("OK",null);
                builder.create().show();
            }
        });

        inProgress.observe(this, aBoolean -> {
            if (aBoolean != null) {
                if (aBoolean) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

        toolbarState.observe(this, toolbarState -> {
            if (toolbarState != null) {
                switch (toolbarState) {
                    case SEARCH:
                        showSearchToolbar();
                        break;
                    case ORDINARY:
                        showOrdinaryToolbar("");
                        break;
                    case WITHOUT_INTERCOM:
                        showWithoutIntercomToolbar("Лицензионный договор");
                        break;
                    case CLEAR_TOOLBAR:
                        showClearToolbar();
                        break;
                }
            }
        });

        //Listen for changes in the back stack
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        //because elevation=0(if elevation!=0 we have shadows)
        findViewById(R.id.auth_app_bar_layout).bringToFront();
        //because if use fitsSystemWindows status bar lose transparency
        findViewById(R.id.auth_app_bar_layout).setPadding(0, getStatusBarHeight(), 0, 0);
        // set an exit transition
        getWindow().setExitTransition(new Slide());

        initNoInetObserver();
    }

    private void showClearToolbar() {
        binding.registerPhoneIntercomButton.setVisibility(View.GONE);
        binding.authBarSearchCountry.setVisibility(View.GONE);
        binding.authBarTitle.setVisibility(View.GONE);
        binding.authAppBarLayout.setBackgroundColor(getResources().getColor(R.color.full_transparent));
        binding.authAppBarLayout.setElevation(0);
    }

    private void showWithoutIntercomToolbar(String title) {
        binding.registerPhoneIntercomButton.setVisibility(View.GONE);
        binding.authBarSearchCountry.setVisibility(View.GONE);
        if (!title.isEmpty()) {
            binding.authBarTitle.setVisibility(View.VISIBLE);
            binding.authBarTitle.setText(title);
        }
        binding.authAppBarLayout.setBackgroundColor(getResources().getColor(R.color.select_country_background));
        binding.authAppBarLayout.setElevation(16);
    }

    private void showOrdinaryToolbar(String title) {
        Log.d(TAG, "showOrdinaryToolbar: ");
        binding.registerPhoneIntercomButton.setVisibility(View.VISIBLE);
        binding.registerPhoneIntercomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d(TAG, "onClick: ");
                Intercom.client().displayMessenger();
            }
        });
        binding.authBarSearchCountry.setVisibility(View.GONE);
        if (!title.isEmpty()) {
            binding.authBarTitle.setVisibility(View.VISIBLE);
            binding.authBarTitle.setText(title);
        } else {
            binding.authBarTitle.setVisibility(View.GONE);
        }
        binding.authAppBarLayout.setBackgroundColor(getResources().getColor(R.color.full_transparent));
        binding.authAppBarLayout.setElevation(0);
    }

    private void showSearchToolbar() {
        binding.registerPhoneIntercomButton.setVisibility(View.GONE);
        binding.authBarSearchCountry.setVisibility(View.VISIBLE);
        binding.authAppBarLayout.setBackgroundColor(getResources().getColor(R.color.select_country_background));
        binding.authAppBarLayout.setElevation(16);
        //binding.authBarSearchCountry.requestFocus();
        binding.authBarSearchCountry.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Log.d(TAG, "onFocusChange: need show keyboard");
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
                        }
                    });
                }
            }
        });
    }

    private void initEdit(final EditText searchEdit) {
        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                manfredAuthorisationManager.filterCountriesByString(searchEdit.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void changeFragmentContainerState(AuthorisationManager.AuthState authState, MutableLiveData<ToolbarState> toolbarState) {
        Log.d(TAG, "changeFragmentContainerState: state is " + authState);
        //ContentFrameLayout contentFrameLayout = findViewById(R.id.fragment_container);
        View opacityView = findViewById(R.id.login_opacity_view);
        switch (authState) {
            case START:
                swapFragment(new RegisterStartFragment(), false);
                opacityView.setAlpha(0.24f);
                toolbarState.setValue(ToolbarState.CLEAR_TOOLBAR);
                break;
            case INPUT_PHONE:
                swapFragment(new RegisterEnterPhoneFragment(), false);
                opacityView.setAlpha(0.5f);
                toolbarState.setValue(ToolbarState.ORDINARY);
                break;
            case INPUT_REGION_CODE:
                swapFragment(new SelectCountryFragment(), true);
                toolbarState.setValue(ToolbarState.SEARCH);
                break;
            case INPUT_SMS:
                if (manfredAuthorisationManager.getPhone() != null) {
                    Registration registration = Registration.create()
                            .withUserId(manfredAuthorisationManager.getPhone().getFullNumber());
                    Intercom.client().registerIdentifiedUser(registration);
                }
                swapFragment(new RegisterEnterSMSFragment(), true);
                opacityView.setAlpha(0.72f);
                toolbarState.setValue(ToolbarState.ORDINARY);
                break;
            case INPUT_NAME:
                swapFragment(new RegisterEnterNameFragment(), false);
                opacityView.setAlpha(0.72f);
                toolbarState.setValue(ToolbarState.ORDINARY);
                ((ManfredPassengerApplication)getApplication()).setNewUser(true);
                break;
            case FINISH:
                authFinished();
                break;
            case NEED_PERMISSION:
                swapFragment(new PermissionFragment(),false);
                break;
        }
    }

    private void authFinished() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                FirebaseTokenRepository firebaseTokenRepository =
                        new FirebaseTokenRepository(((ManfredPassengerApplication)getApplication()).getTokenManager());
                final NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
                firebaseTokenRepository.setFirebaseToken(instanceIdResult.getToken(), notificationManagerCompat.areNotificationsEnabled());
            }
        });
        ((ManfredPassengerApplication)getApplication()).getRegionManager().refreshRegions();
        Log.d(TAG, "authFinished: ");
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_FILE_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Constants.PREFS_TOKEN_FIELD, manfredAuthorisationManager.getToken());
        if (manfredAuthorisationManager.getPhone() != null) {
            editor.putString(Constants.PREFS_PHONE_NUMBER_FIELD, manfredAuthorisationManager.getPhone().getPhone());
        }
        if(manfredAuthorisationManager.getName()!=null){
            editor.putString(Constants.PREFS_USERNAME_FIELD,manfredAuthorisationManager.getName());
        }
        if(manfredAuthorisationManager.getPhone().getCountry_code()!=null){
            editor.putString(Constants.PREFS_PHONE_CODE_FIELD,manfredAuthorisationManager.getPhone().getCountry_code());
        }
        editor.apply();
        //((ManfredPassengerApplication)getApplication()).getTokenManager().setAuthToken(manfredAuthorisationManager.getToken());
        ((ManfredPassengerApplication)getApplication()).refreshManagersData();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            manfredAuthorisationManager.needPermission();
        }else {
            restoreState();
        }

    }

    private void restoreState() {
        RestoreStateManager restoreStateManager = new RestoreStateManager(((ManfredPassengerApplication) getApplication()).getTokenManager(),
                ((ManfredPassengerApplication) getApplication()).getEventManager());
        restoreStateManager.restoreState(new RestoreStateManager.RestoringCallback() {
            @Override
            public void restored() {
                //Intent intent = new Intent(SplashActivity.this, OrderActivity.class);
                Log.d(TAG, "restored: start order activity");
                ((ManfredPassengerApplication) getApplication()).removeAutorisationManger();
                ((ManfredPassengerApplication) getApplication()).getAnalyticsManager().registered();
                final Intent intent = new Intent(LoginActivity.this, OrderActivity.class);
                finishAffinity();
                startActivity(intent);
            }

            @Override
            public void failRestoring(ManfredNetworkError manfredNetworkError) {
                Toast.makeText(getApplicationContext(), manfredNetworkError.getText(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void swapFragment(Fragment fragment, boolean needBackStacked) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left);
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        if (needBackStacked) {
            fragmentTransaction.addToBackStack(null);
        } else {
            while (fragmentManager.getBackStackEntryCount() > 0) {
                fragmentManager.popBackStackImmediate();
            }
        }
        fragmentTransaction.commit();
    }

    @Deprecated
    private void hideSearchString() {
        binding.registerPhoneIntercomButton.setVisibility(View.VISIBLE);
        binding.authBarSearchCountry.setVisibility(View.GONE);
        binding.authAppBarLayout.setBackgroundColor(getResources().getColor(R.color.quad_transparent));
        binding.authAppBarLayout.setElevation(0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CITY_SELECTED:
                restoreState();
        }
    }

    void initNoInetObserver(){
        ((ManfredPassengerApplication)getApplication()).getConnectManager().getIsConnected().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    ConstraintLayout noInetLayout = findViewById(R.id.cl_no_inet);
                    if(aBoolean){
                        noInetLayout.setVisibility(View.GONE);
                    }else {
                        noInetLayout.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
    }

    private void startVideo() {
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.reg_video);
        videoView.setVideoURI(uri);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        // to restart the video after coming from other activity like Sing up
        videoView.start();
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getSupportFragmentManager().getBackStackEntryCount() > 0;
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(canback);
    }

    @Override
    public void onBackPressed() {
        // here remove code for your last fragment
        Log.d(TAG, "onBackPressed: ");
        if(!((ManfredPassengerApplication)getApplication()).getConnectManager().isConnected())return;
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if ((fragment instanceof OnBackPressed) && ((OnBackPressed) fragment).onBackPress()) {
            Log.d(TAG, "onBackPressed: OnBackPressed");
            super.onBackPressed();
            toolbarState.setValue(ToolbarState.ORDINARY);
        } else if (fragment instanceof OnBackNotHideKeyboard) {
            Log.d(TAG, "onBackPressed: OnBackNotHideKeyboard");
        } else {
            Log.d(TAG, "onBackPressed: just go back");
            toolbarState.postValue(ToolbarState.CLEAR_TOOLBAR);
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onListFragmentInteraction(CountryModel item) {
        manfredAuthorisationManager.enterRegionCode(item);
    }

    @Override
    public void onClicked(String url) {
        swapFragment(WebViewFragment.newInstance(url), true);
        toolbarState.setValue(ToolbarState.WITHOUT_INTERCOM);
    }
}
