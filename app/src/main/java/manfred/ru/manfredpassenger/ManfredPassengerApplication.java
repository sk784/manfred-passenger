package manfred.ru.manfredpassenger;

import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.arch.persistence.room.Room;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import io.fabric.sdk.android.Fabric;
import io.intercom.android.sdk.Intercom;
import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.authorisation.managers.ManfredAuthorisationManager;
import manfred.ru.manfredpassenger.cards.managers.ManfredCreditCardManager;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.common.managers.ConnectManager;
import manfred.ru.manfredpassenger.common.managers.ManfredAnalyticsManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.ManfredAppStatusManager;
import manfred.ru.manfredpassenger.common.managers.events.EventServiceManager;
import manfred.ru.manfredpassenger.common.managers.events.ManfredEventManager;
import manfred.ru.manfredpassenger.common.managers.regions.ManfredRegionManager;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.history.managers.ManfredHistoryManager;
import manfred.ru.manfredpassenger.history.repositories.WebsocketHistoryRepository;
import manfred.ru.manfredpassenger.profile.managers.ManfredProfileManager;
import manfred.ru.manfredpassenger.profile.models.Profile;
import manfred.ru.manfredpassenger.promocodes.manager.ManfredPromoCodeManager;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;
import manfred.ru.manfredpassenger.ride.database.ManfredPassengerDatabase;
import manfred.ru.manfredpassenger.ride.managers.DebtManager;
import manfred.ru.manfredpassenger.ride.managers.FreeCarsManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredDebtManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredFreeCarsManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredOrderManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredOrderManagerBuilder;
import manfred.ru.manfredpassenger.ride.managers.ManfredPreOrderManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredPushMessageManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredTariffManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredTimeToDriverManagerBuilder;
import manfred.ru.manfredpassenger.ride.managers.PushMessageManager;
import manfred.ru.manfredpassenger.ride.managers.TariffManager;
import manfred.ru.manfredpassenger.ride.managers.TimeToDriverManager;
import manfred.ru.manfredpassenger.ride.repositories.PlacesDatabaseRepository;
import manfred.ru.manfredpassenger.utils.Constants;
import manfred.ru.manfredpassenger.utils.NetworkChangeReceiver;

public class ManfredPassengerApplication extends Application implements LifecycleObserver {
    private static final String TAG = "ManfredPassengerApplica";

    private static ManfredPassengerApplication inst;

    public static ManfredPassengerApplication instance() {
        return inst;
    }

    private String getTokenFromPreferences(){
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_FILE_NAME, 0);
        String token = settings.getString(Constants.PREFS_TOKEN_FIELD, "");
        //Log.d(TAG, "getTokenFromPreferences: token is "+token);
        return token;
    }

    private ManfredTokenManager tokenManager;
    public ManfredTokenManager getTokenManager() {
        if(tokenManager==null){
            tokenManager = new ManfredTokenManager();
        }
        if(tokenManager.getAuthToken()==null){
            Log.d(TAG, "getTokenManager: token is null");
            tokenManager.setAuthToken(getTokenFromPreferences());
        }
        return tokenManager;
    }

    private DebtManager debtManager;
    public DebtManager getDebtManager() {
        if(debtManager==null){
            debtManager = new ManfredDebtManager(getTokenManager(), getAppStatusManager(),getNetworkManager());
        }
        return debtManager;
    }

    private ManfredOrderManager manfredOrderManager;
    public ManfredOrderManager getManfredOrderManager() {
        if (manfredOrderManager == null) {
            manfredOrderManager = new ManfredOrderManagerBuilder()
                    .setTimeToDriverManager(getTimeToDriverManager())
                    .setManfredTokenManager(getTokenManager())
                    .setPreOrderManager(getPreOrderManager())
                    .setPromoCodeManager(getPromoCodeManager())
                    .setPlacesDatabaseRepository(getPlacesDatabaseRepository())
                    .setAppStatusManager(getAppStatusManager())
                    .setTariffManager(getTariffManager())
                    .setNetworkManager(getNetworkManager())
                    .setAnalyticsManager(getAnalyticsManager())
                    .setHistoryManager(getHistoryManager())
                    .createManfredOrderManager();
        }
        return manfredOrderManager;
    }
    public void removeOrderManager(){
        manfredOrderManager=null;
    }

    private TimeToDriverManager timeToDriverManager;

    public TimeToDriverManager getTimeToDriverManager() {
        if(timeToDriverManager==null){
            timeToDriverManager = new ManfredTimeToDriverManagerBuilder()
                    .setAppStatusManager(getAppStatusManager())
                    .setNetworkManager(getNetworkManager())
                    .setRegionManager(getRegionManager())
                    .setTariffManager(getTariffManager())
                    .setTokenManager(getTokenManager())
                    .createManfredTimeToDriverManager();
        }
        return timeToDriverManager;
    }

    private AppStatusManager appStatusManager;

    public AppStatusManager getAppStatusManager() {
        if(appStatusManager==null){
            appStatusManager = new ManfredAppStatusManager();
        }
        return appStatusManager;
    }

    private ManfredPreOrderManager preOrderManager;
    public ManfredPreOrderManager getPreOrderManager() {
        if(preOrderManager == null){
            preOrderManager = new ManfredPreOrderManager(getTokenManager(), getNetworkManager());
        }
        return preOrderManager;
    }

    public void killAllManagers(){
        Log.d(TAG, "killAllManagers: ");
        tokenManager=null;
        preOrderManager=null;
        tariffManager=null;
        debtManager=null;
        historyManager=null;
        promoCodeManager=null;
        eventManager = null;
        manfredCreditCardManager=null;
        regionManager = null;
        removeAutorisationManger();
        removeOrderManager();
    }

    private EventServiceManager serviceManager;
    public EventServiceManager getServiceManager() {
        if(serviceManager==null){
            serviceManager = new EventServiceManager();
        }
        return serviceManager;
    }

    //but it will be singleton not necessary
    private ManfredEventManager eventManager;
    public ManfredEventManager getEventManager() {
        if (eventManager == null) {
            eventManager = new ManfredEventManager(getManfredOrderManager(), getHistoryManager(), getDebtManager(),
                    getPreOrderManager(), getPromoCodeManager(), getAnalyticsManager());
        }
        return eventManager;
    }

    private ManfredCreditCardManager manfredCreditCardManager;
    public ManfredCreditCardManager getManfredCreditCardManager() {
        if(manfredCreditCardManager==null){
            manfredCreditCardManager = buildCreditCardManager();
        }
        return manfredCreditCardManager;
    }

    private NetworkManagerProvider networkManager;

    public NetworkManagerProvider getNetworkManager() {
        if(networkManager==null){
            networkManager=new NetworkManagerProvider(getApplicationContext());
        }
        return networkManager;
    }

    private ManfredAuthorisationManager manfredAuthorisationManager;
    public ManfredAuthorisationManager getManfredAuthorisationManager() {
        if(manfredAuthorisationManager==null){
            manfredAuthorisationManager = new ManfredAuthorisationManager(getTokenManager(),getNetworkManager(), getAnalyticsManager());
        }
        return manfredAuthorisationManager;
    }
    public void removeAutorisationManger(){
        manfredAuthorisationManager=null;
    }

    private ManfredHistoryManager historyManager;
    public ManfredHistoryManager getHistoryManager() {
        if(historyManager==null){
            historyManager=new ManfredHistoryManager(getTokenManager(), new WebsocketHistoryRepository(getNetworkManager(),getTokenManager()));
        }
        return historyManager;
    }

    private ManfredProfileManager manfredProfileManager;
    public ManfredProfileManager getManfredProfileManager() {
        if(manfredProfileManager==null){
            SharedPreferences settings = getSharedPreferences(Constants.PREFS_FILE_NAME, 0);
            String username = settings.getString(Constants.PREFS_USERNAME_FIELD,"");
            String userPhoneCode = settings.getString(Constants.PREFS_PHONE_CODE_FIELD,"+7");
            String userPhone = settings.getString(Constants.PREFS_PHONE_NUMBER_FIELD, "");
            Profile profile = new Profile(new Phone(userPhoneCode,userPhone),username);
            manfredProfileManager = new ManfredProfileManager(getTokenManager(),profile,settings,getNetworkManager(), getAnalyticsManager());
        }
        return manfredProfileManager;
    }

    private ManfredPassengerDatabase manfredPassengerDatabase;
    private ManfredPassengerDatabase getManfredPassengerDatabase() {
        if(manfredPassengerDatabase==null){
            synchronized (ManfredPassengerDatabase.class) {
                manfredPassengerDatabase = Room.databaseBuilder(getApplicationContext(), ManfredPassengerDatabase.class, "passenger_database")
                        .fallbackToDestructiveMigration()
                        .build();
            }
        }
        return manfredPassengerDatabase;
    }

    private PlacesDatabaseRepository placesDatabaseRepository;
    public PlacesDatabaseRepository getPlacesDatabaseRepository(){
        if(placesDatabaseRepository==null) {
            placesDatabaseRepository= new PlacesDatabaseRepository(getManfredPassengerDatabase());
        }
        return placesDatabaseRepository;
    }

    private RegionManager regionManager;
    public RegionManager getRegionManager() {
        if(regionManager==null){
            regionManager = new ManfredRegionManager(getTokenManager(),
                    getTariffManager(),
                    getAppStatusManager(),
                    getPromoCodeManager(),
                    getManfredCreditCardManager(),
                    getNetworkManager());
        }
        return regionManager;
    }

    private TariffManager tariffManager;
    public TariffManager getTariffManager() {
        if(tariffManager==null){
            tariffManager = new ManfredTariffManager(getTokenManager(),getAppStatusManager(), getCarsManager(), getNetworkManager());
        }
        return tariffManager;
    }

    private PromoCodeManager promoCodeManager;
    public PromoCodeManager getPromoCodeManager() {
        if(promoCodeManager==null){
            promoCodeManager = new ManfredPromoCodeManager(getTokenManager(),getNetworkManager());
        }
        return promoCodeManager;
    }

    private FreeCarsManager carsManager;
    public FreeCarsManager getCarsManager() {
        if(carsManager==null){
            carsManager = new ManfredFreeCarsManager();
        }
        return carsManager;
    }

    private ConnectManager connectManager;
    public ConnectManager getConnectManager() {
        if(connectManager==null){
            connectManager = new ConnectManager();
        }
        return connectManager;
    }

    private AnalyticsManager analyticsManager;
    public AnalyticsManager getAnalyticsManager(){
        if(analyticsManager==null){
            analyticsManager=new ManfredAnalyticsManager(AppEventsLogger.newLogger(getApplicationContext()));
        }
        return analyticsManager;
    }


    private PushMessageManager pushMessageManager;

    public PushMessageManager getPushMessageManager() {
        if (pushMessageManager == null) {
            pushMessageManager = new ManfredPushMessageManager(getTokenManager(), getNetworkManager());
        }
        return pushMessageManager;
    }

    public void refreshManagersData(){
        getTariffManager().refreshTariffs();
        getManfredCreditCardManager().refreshCards();
        getPromoCodeManager().refreshPromoCodes();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ManfredPassengerApplication.inst = this;
        Intercom.initialize(this, "android_sdk-54fea269e641ae1e1331049336f8ba3eb518cf9a", "joe69oki");
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        buildNetworkStatusObserver();
        //init debt
        getDebtManager();
        configureCrashReporting();
        initilizeAppMetrica();
    }

    private void initilizeAppMetrica(){
        // Создание расширенной конфигурации библиотеки.
        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder("2614e0af-aa18-4f2e-bd41-810aa7c1bdc5").build();
        // Инициализация AppMetrica SDK.
        YandexMetrica.activate(getApplicationContext(), config);
        // Отслеживание активности пользователей.
        YandexMetrica.enableActivityAutoTracking(this);
    }

    private void buildNetworkStatusObserver() {
        Log.d(TAG, "buildNetworkStatusObserver: ");
        NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver(getConnectManager());
        IntentFilter intentFilterNetw = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        this.registerReceiver(networkChangeReceiver, intentFilterNetw);
    }

    private ManfredCreditCardManager buildCreditCardManager() {
        return new ManfredCreditCardManager(getTokenManager(), getPreOrderManager(),getNetworkManager(), getAnalyticsManager());
    }

    private boolean isNewUser = false;
    public boolean isNewUser() {
        return isNewUser;
    }

    public void setNewUser(boolean newUser) {
        Log.d(TAG, "setNewUser: "+newUser);
        isNewUser = newUser;
    }

    private void configureCrashReporting() {
        CrashlyticsCore crashlyticsCore = new CrashlyticsCore.Builder()
                .disabled(BuildConfig.DEBUG)
                .build();
        Fabric.with(this, new Crashlytics.Builder().core(crashlyticsCore).build());
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void onAppBackgrounded() {
        Log.d(TAG, "App in background");
        getServiceManager().goToForeground();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void onAppForegrounded() {
        Log.d(TAG, "App in foreground");
        getServiceManager().goToBackground();
    }

}
