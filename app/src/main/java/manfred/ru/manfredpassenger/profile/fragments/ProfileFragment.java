package manfred.ru.manfredpassenger.profile.fragments;


import android.arch.lifecycle.Observer;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.databinding.FragmentProfileBinding;
import manfred.ru.manfredpassenger.profile.managers.ManfredProfileManager;
import manfred.ru.manfredpassenger.profile.models.Profile;
import manfred.ru.manfredpassenger.utils.country_generator.CountryGenerator;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    private static final String TAG = "ProfileFragment";

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentProfileBinding binding = FragmentProfileBinding.inflate(getLayoutInflater());
        init(binding);
        return binding.getRoot();
        //return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    private void init(FragmentProfileBinding binding) {
        InputStream raw = getResources().openRawResource(R.raw.flags);
        Reader rd = new BufferedReader(new InputStreamReader(raw));
        final List<CountryModel> countryModels = CountryGenerator.generateCountries(rd, Resources.getSystem().getConfiguration().locale);

        ManfredProfileManager profileManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredProfileManager();
        profileManager.getProfile().observe(this, new Observer<Profile>() {
            @Override
            public void onChanged(@Nullable Profile profile) {
                binding.setProfile(profile);
                binding.setRegion(getCountryModelByCode(profile.getPhone().getCountry_code(),countryModels));
            }
        });

        binding.tvName.setOnClickListener(v -> profileManager.editName());
        binding.registerPhoneNumberContainer.setOnClickListener(v -> profileManager.editNumber());

    }

    @Nullable
    private CountryModel getCountryModelByCode(String code,List<CountryModel> countryModels){
        for (CountryModel countryModel : countryModels){
            if(countryModel.getCountryCode().equals("+"+code)){
                return countryModel;
            }
        }
        return null;
    }

}
