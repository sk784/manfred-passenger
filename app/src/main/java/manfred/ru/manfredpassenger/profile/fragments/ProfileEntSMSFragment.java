package manfred.ru.manfredpassenger.profile.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.authorisation.fragments.models.PhoneModel;
import manfred.ru.manfredpassenger.databinding.FragmentProfileEntSmBinding;
import manfred.ru.manfredpassenger.profile.managers.ManfredProfileManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileEntSMSFragment extends Fragment {


    public ProfileEntSMSFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Подтверждение");
        FragmentProfileEntSmBinding binding = FragmentProfileEntSmBinding.inflate(getLayoutInflater());
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentProfileEntSmBinding binding) {
        ManfredProfileManager profileManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredProfileManager();
        if(profileManager.getProfile().getValue().getPhone()!=null) {
            binding.setPhone(new PhoneModel(profileManager.getProfile().getValue().getPhone().getCountry_code()+" "
                    +profileManager.getProfile().getValue().getPhone().getPhone()));
        }
        initEditText(binding,profileManager);
    }

    private void initEditText(FragmentProfileEntSmBinding binding, ManfredProfileManager profileManager) {
        binding.firstDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count>0){
                    binding.secondDigit.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.secondDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count<1){
                    binding.firstDigit.requestFocus();
                }else {
                    binding.thirdDigit.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.thirdDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count<1){
                    binding.secondDigit.requestFocus();
                }else {
                    binding.fourDigit.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.fourDigit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count<1){
                    binding.thirdDigit.requestFocus();
                }else {
                    //Log.d(TAG, "onTextChanged: we have full code, sending");
                    String pin = binding.firstDigit.getText().toString()+
                            binding.secondDigit.getText().toString()+
                            binding.thirdDigit.getText().toString()+
                            binding.fourDigit.getText().toString();
                    profileManager.enterSMS(pin);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
