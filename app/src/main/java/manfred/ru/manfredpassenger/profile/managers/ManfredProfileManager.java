package manfred.ru.manfredpassenger.profile.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import android.util.Log;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.services.models.ManfredPassengerData;
import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.api.services.models.SmsAuthorisation;
import manfred.ru.manfredpassenger.api.services.models.UserName;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.authorisation.repositories.ManfredAuthorisationRepository;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.profile.models.Profile;
import manfred.ru.manfredpassenger.profile.repositories.ProfileRepository;
import manfred.ru.manfredpassenger.utils.Constants;
import manfred.ru.manfredpassenger.utils.SingleLiveEvent;

public class ManfredProfileManager implements ProfileManager {
    private static final String TAG = "ManfredProfileManager";
    public enum ProfileEditState{
        VIEW_PROFILE,
        CHANGE_NUMBER,
        CHANGE_NAME,
        SELECT_REGION,
        ENTER_SMS
    }

    private MutableLiveData<ProfileEditState> profileEditState;
    private ManfredTokenManager tokenManager;
    private NetworkManagerProvider networkManager;
    private MutableLiveData<Profile> profile;
    private SingleLiveEvent<ManfredNetworkError>errors;
    private AnalyticsManager analyticsManager;

    public LiveData<ManfredNetworkError> getErrors() {
        return errors;
    }

    private SharedPreferences settings;

    public ManfredProfileManager(ManfredTokenManager tokenManager, Profile profile, SharedPreferences settings, NetworkManagerProvider networkManager, AnalyticsManager analyticsManager) {
        this.tokenManager = tokenManager;
        this.networkManager=networkManager;
        this.analyticsManager = analyticsManager;
        this.profile = new MutableLiveData<>();
        this.profile.postValue(profile);
        this.profileEditState = new MutableLiveData<>();
        this.errors = new SingleLiveEvent<>();
        this.profileEditState.postValue(ProfileEditState.VIEW_PROFILE);
        this.settings = settings;
    }

    private void setAndSaveProfile(Profile profile) {
        this.profile.postValue(profile);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Constants.PREFS_TOKEN_FIELD, tokenManager.getAuthToken());
        editor.putString(Constants.PREFS_PHONE_NUMBER_FIELD, profile.getPhone().getPhone());
        editor.putString(Constants.PREFS_PHONE_CODE_FIELD, profile.getPhone().getCountry_code());
        editor.putString(Constants.PREFS_USERNAME_FIELD,profile.getName());
        editor.apply();
    }

    @Override
    public void editName() {
        profileEditState.postValue(ProfileEditState.CHANGE_NAME);
    }

    @Override
    public void editNumber() {
        profileEditState.postValue(ProfileEditState.CHANGE_NUMBER);
    }

    @Override
    public void editRegion() {
        profileEditState.postValue(ProfileEditState.SELECT_REGION);
    }

    @Override
    public void enterSMS(String sms) {
        Log.d(TAG, "enterSMS: ");
        Phone phone;
        if (profile.getValue() == null) {
            //authError.postValue("enterSMS: phone is null returning");
            Log.d(TAG, "enterSMS: phone is null returning");
            return;
        }else {
            phone = profile.getValue().getPhone();
        }
        //inProgress.postValue(true);
        ManfredAuthorisationRepository manfredAuthorisationRepository = new ManfredAuthorisationRepository(networkManager);
        SmsAuthorisation smsAuthorisation = new SmsAuthorisation(phone.getPhone(), sms, phone.getCountry_code());
        manfredAuthorisationRepository.approveSmsCode(smsAuthorisation, new NetworkResponseCallback<ManfredPassengerData>() {
            @Override
            public void allOk(ManfredPassengerData response) {
                Log.d(TAG, "allOk, sms approved "+response.toString());
                tokenManager.setAuthToken(response.getToken());
                profileEditState.postValue(ProfileEditState.VIEW_PROFILE);
                setAndSaveProfile(profile.getValue());
                //inProgress.postValue(false);
/*                name = response.getName();
                if (response.isNew()) {
                    authState.postValue(AuthState.INPUT_NAME);
                } else {
                    authState.postValue(AuthState.FINISH);
                }*/
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: sms not work");
                errors.postValue(error);
            }
        });
    }

    @Override
    public void selectRegion(CountryModel countryModel) {
        Phone phone = new Phone(countryModel.getCountryCode(),"");
        Profile prevProfile=profile.getValue();
        Profile newProfile=new Profile(phone, prevProfile.getName());
        profile.postValue(newProfile);
        profileEditState.postValue(ProfileEditState.CHANGE_NUMBER);
    }

    @Override
    public void changePhoneNumber(Phone phone) {
        ProfileRepository profileRepository = new ProfileRepository(tokenManager);
        profileRepository.changePhone(phone, new NetworkResponseCallback() {
            @Override
            public void allOk(Object response) {
                Profile prevProfile=profile.getValue();
                Profile newProfile=new Profile(phone, prevProfile.getName());
                profile.postValue(newProfile);
                profileEditState.postValue(ProfileEditState.ENTER_SMS);
                //setAndSaveProfile(newProfile);
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: "+error.toString());
            }
        });

    }

    @Override
    public void changeName(String name) {
        ManfredAuthorisationRepository repository = new ManfredAuthorisationRepository(networkManager);
        repository.enterName(tokenManager.getAuthToken(), new UserName(name), analyticsManager, new NetworkResponseCallback() {
            @Override
            public void allOk(Object response) {
                Profile prevProfile=profile.getValue();
                Profile newProfile=new Profile(prevProfile.getPhone(), name);
                profile.postValue(newProfile);
                profileEditState.postValue(ProfileEditState.VIEW_PROFILE);
                setAndSaveProfile(newProfile);
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: "+error.toString());
            }
        });
    }

    public void clearState(){
        profileEditState.postValue(ProfileEditState.VIEW_PROFILE);
    }

    public LiveData<ProfileEditState> getProfileEditState() {
        return profileEditState;
    }

    @Override
    public LiveData<Profile> getProfile() {
        return profile;
    }
}
