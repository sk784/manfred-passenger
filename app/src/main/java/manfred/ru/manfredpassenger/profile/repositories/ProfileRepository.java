package manfred.ru.manfredpassenger.profile.repositories;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.ManfredPassengerData;
import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileRepository {
    private ManfredTokenManager manfredTokenManager;

    public ProfileRepository(ManfredTokenManager manfredTokenManager) {
        this.manfredTokenManager = manfredTokenManager;
    }

    public void changePhone(Phone phone, NetworkResponseCallback callback){
        APIClient.getUserService().changePhoneNumber(manfredTokenManager.getAuthToken(),phone).enqueue(new Callback<ManfredResponse<ManfredPassengerData>>() {
            @Override
            public void onResponse(Call<ManfredResponse<ManfredPassengerData>> call, Response<ManfredResponse<ManfredPassengerData>> response) {
                new NetworkUtils().validateResponse(response, callback);
            }

            @Override
            public void onFailure(Call<ManfredResponse<ManfredPassengerData>> call, Throwable t) {
                new NetworkUtils().processFailureResponse(t, callback);
            }
        });
    }

}
