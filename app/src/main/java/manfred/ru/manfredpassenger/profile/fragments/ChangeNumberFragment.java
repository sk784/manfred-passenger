package manfred.ru.manfredpassenger.profile.fragments;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.databinding.FragmentChangeNameBinding;
import manfred.ru.manfredpassenger.databinding.FragmentChangeNumberBinding;
import manfred.ru.manfredpassenger.profile.managers.ManfredProfileManager;
import manfred.ru.manfredpassenger.profile.models.Profile;
import manfred.ru.manfredpassenger.utils.NumericKeyBoardTransformationMethod;
import manfred.ru.manfredpassenger.utils.country_generator.CountryGenerator;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangeNumberFragment extends Fragment {
    private static final String TAG = "ChangeNumberFragment";

    public ChangeNumberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentChangeNumberBinding binding = FragmentChangeNumberBinding.inflate(getLayoutInflater());
        init(binding);
        getActivity().setTitle("Изменить номер телефона");
        return binding.getRoot();
    }

    private void init(FragmentChangeNumberBinding binding) {
        ManfredProfileManager profileManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredProfileManager();
        InputStream raw = getResources().openRawResource(R.raw.flags);
        Reader rd = new BufferedReader(new InputStreamReader(raw));
        final List<CountryModel> countryModels = CountryGenerator.generateCountries(rd, Resources.getSystem().getConfiguration().locale);

        binding.changeRegionContainer.setOnClickListener(v -> profileManager.editRegion());

        profileManager.getProfile().observe(this, new Observer<Profile>() {
            @Override
            public void onChanged(@Nullable Profile profile) {
                binding.setRegion(getCountryModelByCode(profile.getPhone().getCountry_code(),countryModels));
                binding.setProfile(profile);
                binding.registerPhoneNumberEdit.append(profile.getPhone().getPhone());
            }
        });

        binding.btnChangeNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Phone phone = new Phone(profileManager.getProfile().getValue().getPhone().getCountry_code(),
                        binding.registerPhoneNumberEdit.getText().toString());
                profileManager.changePhoneNumber(phone);
            }
        });

        initEdit(binding.btnChangeNumber,binding.registerPhoneNumberEdit,profileManager);

    }

    private void initEdit(final Button nextButton, final EditText registerPhoneNumberEdit, ManfredProfileManager profileManager) {
        registerPhoneNumberEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                try {
                    String fullPhone;
                    if (profileManager.getProfile().getValue().getPhone() != null) {
                        fullPhone = "+" +profileManager.getProfile().getValue().getPhone().getCountry_code() + registerPhoneNumberEdit.getText();
                    } else {
                        fullPhone = "+7" + registerPhoneNumberEdit.getText();
                    }
                    Log.d(TAG, "onTextChanged: checking " + fullPhone);
                    Phonenumber.PhoneNumber phoneNumberProto = phoneUtil.parse(fullPhone, null);
                    boolean isValid = phoneUtil.isValidNumber(phoneNumberProto); // returns true if valid
                    if (isValid) {
                        nextButton.setEnabled(true);
                    } else {
                        nextButton.setEnabled(false);
                    }
                } catch (NumberParseException e) {
                    System.err.println("NumberParseException was thrown: " + e.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        registerPhoneNumberEdit.setTransformationMethod(new NumericKeyBoardTransformationMethod());

        registerPhoneNumberEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //Log.d(TAG, "onFocusChange: "+hasFocus);
                if (hasFocus) {
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
                        }
                    });
                }
            }
        });

        registerPhoneNumberEdit.requestFocus();
    }

    @Nullable
    private CountryModel getCountryModelByCode(String code, List<CountryModel> countryModels){
        for (CountryModel countryModel : countryModels){
            if(countryModel.getCountryCode().equals("+"+code)){
                return countryModel;
            }
        }
        return null;
    }

}
