package manfred.ru.manfredpassenger.profile.models;

import manfred.ru.manfredpassenger.api.services.models.Phone;

public class Profile {
    private final Phone phone;
    private final String name;

    public Profile(Phone phone, String name) {
        this.phone = phone;
        this.name = name;
    }

    public Phone getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }
}
