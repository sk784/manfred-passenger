package manfred.ru.manfredpassenger.profile.fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.authorisation.fragments.MyCountryItemRecyclerViewAdapter;
import manfred.ru.manfredpassenger.authorisation.fragments.SelectCountryFragment;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.profile.ProfileVM;
import manfred.ru.manfredpassenger.profile.managers.ManfredProfileManager;
import manfred.ru.manfredpassenger.utils.CountryDiffutilCallback;
import manfred.ru.manfredpassenger.utils.country_generator.CountryGenerator;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSelCountryFragment extends Fragment {


    public ProfileSelCountryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_country, container, false);

        InputStream raw = getResources().openRawResource(R.raw.flags);
        Reader rd = new BufferedReader(new InputStreamReader(raw));
        final List<CountryModel> countryModels = CountryGenerator.generateCountries(rd, Resources.getSystem().getConfiguration().locale);

        ManfredProfileManager profileManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredProfileManager();
        ProfileVM profileVM = ViewModelProviders.of(getActivity()).get(ProfileVM.class);

        final List<CountryModel> filteredCountries = new ArrayList<>(countryModels);
        final MyCountryItemRecyclerViewAdapter adapter = new MyCountryItemRecyclerViewAdapter(filteredCountries, new SelectCountryFragment.OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(CountryModel item) {
                profileManager.selectRegion(item);
            }
        });

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);
        }

        profileVM.getCountryName().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s == null) return;
                List<CountryModel> beforeChangerCountries = new ArrayList<>(filteredCountries);
                filteredCountries.clear();
                if (s.isEmpty()) {
                    filteredCountries.addAll(countryModels);
                } else {
                    for (CountryModel p : countryModels) {
                        if (p.getCountryName().toLowerCase().contains(s.toLowerCase())) {
                            filteredCountries.add(p);
                        }
                    }
                }
                //Log.d(TAG, "onChanged: after filtering we have " + filteredCountries.size() + " countries");
                CountryDiffutilCallback productDiffUtilCallback =
                        new CountryDiffutilCallback(beforeChangerCountries, filteredCountries);
                DiffUtil.DiffResult productDiffResult = DiffUtil.calculateDiff(productDiffUtilCallback);

                //adapter.setData(productList);
                productDiffResult.dispatchUpdatesTo(adapter);
            }
        });

        return view;
    }

}
