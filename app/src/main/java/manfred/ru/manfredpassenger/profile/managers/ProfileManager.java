package manfred.ru.manfredpassenger.profile.managers;

import android.arch.lifecycle.LiveData;

import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.authorisation.fragments.models.CountryModel;
import manfred.ru.manfredpassenger.profile.models.Profile;

public interface ProfileManager {
    void editName();
    void editNumber();
    void editRegion();
    void enterSMS(String sms);
    void selectRegion(CountryModel countryModel);
    void changePhoneNumber(Phone phone);
    void changeName(String name);

    public LiveData<Profile> getProfile();
}
