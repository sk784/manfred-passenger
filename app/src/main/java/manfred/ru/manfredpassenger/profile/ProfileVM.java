package manfred.ru.manfredpassenger.profile;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class ProfileVM extends ViewModel {
    private MutableLiveData<String> countryName = new MutableLiveData<>();

    void setCountryName(String string){
        countryName.postValue(string);
    }

    public LiveData<String> getCountryName() {
        return countryName;
    }
}
