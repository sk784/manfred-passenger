package manfred.ru.manfredpassenger.profile.fragments;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.databinding.FragmentChangeNameBinding;
import manfred.ru.manfredpassenger.profile.managers.ManfredProfileManager;
import manfred.ru.manfredpassenger.profile.models.Profile;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangeNameFragment extends Fragment {
    private static final String TAG = "ChangeNameFragment";

    public ChangeNameFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentChangeNameBinding binding = FragmentChangeNameBinding.inflate(getLayoutInflater());
        init(binding);
        getActivity().setTitle("Изменить имя");
        return binding.getRoot();
    }

    private void init(FragmentChangeNameBinding binding) {
        ManfredProfileManager profileManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredProfileManager();
        initEdit(binding.editText,binding.btnChangeName);
        binding.btnChangeName.setOnClickListener(v -> profileManager.changeName(binding.editText.getText().toString()));
        profileManager.getProfile().observe(this, new Observer<Profile>() {
            @Override
            public void onChanged(@Nullable Profile profile) {
                if (profile != null) {
                    binding.editText.append(profile.getName());
                }
            }
        });
    }

    private void initEdit(final EditText editText, final Button btnNext) {

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editText.getText().length() > 1) {
                    btnNext.setEnabled(true);
                } else {
                    btnNext.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d(TAG, "onFocusChange: "+hasFocus);
                if (hasFocus) {
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
                        }
                    });
                }
            }
        });

        editText.requestFocus();
    }

}
