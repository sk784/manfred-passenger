package manfred.ru.manfredpassenger.profile;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.authorisation.LoginActivity;
import manfred.ru.manfredpassenger.databinding.ActivityProfileBinding;
import manfred.ru.manfredpassenger.profile.fragments.ChangeNameFragment;
import manfred.ru.manfredpassenger.profile.fragments.ChangeNumberFragment;
import manfred.ru.manfredpassenger.profile.fragments.ProfileEntSMSFragment;
import manfred.ru.manfredpassenger.profile.fragments.ProfileFragment;
import manfred.ru.manfredpassenger.profile.fragments.ProfileSelCountryFragment;
import manfred.ru.manfredpassenger.profile.managers.ManfredProfileManager;

public class ProfileActivity extends AppCompatActivity {
    private static final String TAG = "ProfileActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.animator.enter_from_right,R.animator.exit_to_left);
        ActivityProfileBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_profile);
        ManfredProfileManager profileManager = ((ManfredPassengerApplication)getApplication()).getManfredProfileManager();
        ProfileVM profileVM = ViewModelProviders.of(this).get(ProfileVM.class);

        Toolbar toolbar = findViewById(R.id.profile_toolbar);
        EditText searchEdit = findViewById(R.id.et_profile_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        setTitle("Редактировать профиль");
        initEdit(searchEdit,profileVM);

        //setting statusBar color
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.black));

        profileManager.getErrors().observe(this, new Observer<ManfredNetworkError>() {
            @Override
            public void onChanged(@Nullable ManfredNetworkError manfredNetworkError) {
                if(manfredNetworkError!=null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this,R.style.AlertDialogCustom);
                    if(manfredNetworkError.getType().equals(ManfredNetworkError.ErrorType.WRONG_SMS_CODE)) {
                        builder.setTitle(manfredNetworkError.getText());
                        builder.setMessage("Проверьте код и повторите попытку");
                    }else {
                        builder.setTitle("Ошибка");
                        builder.setMessage(manfredNetworkError.getText());
                    }
                    builder.setPositiveButton("OK",null);
                    builder.create().show();
                }
            }
        });

        profileManager.getProfileEditState().observe(this, new Observer<ManfredProfileManager.ProfileEditState>() {
            @Override
            public void onChanged(@Nullable ManfredProfileManager.ProfileEditState profileEditState) {
                if (profileEditState != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    switch (profileEditState){
                        case VIEW_PROFILE:
                            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            fragmentTransaction.replace(binding.fragmentContainer.getId(), new ProfileFragment());
                            searchEdit.setVisibility(View.GONE);
                            break;
                        case CHANGE_NUMBER:
                            fragmentTransaction.replace(binding.fragmentContainer.getId(),new ChangeNumberFragment());
                            fragmentTransaction.addToBackStack("changingNumber");
                            searchEdit.setVisibility(View.GONE);
                            break;
                        case CHANGE_NAME:
                            fragmentTransaction.replace(binding.fragmentContainer.getId(),new ChangeNameFragment());
                            fragmentTransaction.addToBackStack("changingName");
                            searchEdit.setVisibility(View.GONE);
                            break;
                        case SELECT_REGION:
                            fragmentTransaction.replace(binding.fragmentContainer.getId(),new ProfileSelCountryFragment());
                            fragmentTransaction.addToBackStack("changingRegion");
                            searchEdit.setVisibility(View.VISIBLE);
                            break;
                        case ENTER_SMS:
                            fragmentTransaction.replace(binding.fragmentContainer.getId(),new ProfileEntSMSFragment());
                            fragmentTransaction.addToBackStack("enteringSms");
                            searchEdit.setVisibility(View.GONE);
                            break;
                    }
                    fragmentTransaction.commit();
                }
            }
        });

        initNoInetObserver();
    }

    private void initEdit(final EditText searchEdit, ProfileVM profileVM) {
        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                profileVM.setCountryName(searchEdit.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: "+getFragmentManager().getBackStackEntryCount());
        //getFragmentManager().popBackStack();

        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
            //finish();
            ((ManfredPassengerApplication)getApplication()).getManfredProfileManager().clearState();
            setTitle("Редактировать профиль");
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Log.d(TAG, "onSupportNavigateUp: backStack is " + getFragmentManager().getBackStackEntryCount());
        //getFragmentManager().popBackStack();
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
            ((ManfredPassengerApplication)getApplication()).getManfredProfileManager().clearState();
            setTitle("Редактировать профиль");
        } else {
            getFragmentManager().popBackStack();
        }
        return false;
    }

    void initNoInetObserver(){
        ((ManfredPassengerApplication)getApplication()).getConnectManager().getIsConnected().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    ConstraintLayout noInetLayout = findViewById(R.id.cl_no_inet);
                    if(aBoolean){
                        noInetLayout.setVisibility(View.GONE);
                    }else {
                        noInetLayout.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
    }

}
