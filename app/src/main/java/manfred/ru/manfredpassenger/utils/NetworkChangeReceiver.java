package manfred.ru.manfredpassenger.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import manfred.ru.manfredpassenger.common.managers.ConnectManager;

/**
 * Created by begemot on 07.09.17.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    private static final String TAG = "NetworkChangeReceiver";
    private final ConnectManager connectManager;

    public NetworkChangeReceiver(ConnectManager connectManager) {
        Log.d(TAG, "NetworkChangeReceiver: creating");
        this.connectManager = connectManager;
    }

    @Override
    public void onReceive(final Context context, final Intent intent1) {
        int status = NetworkUtil.getConnectivityStatus(context);
        Log.d(TAG, "onReceive: "+status);
        if (status == NetworkUtil.TYPE_NOT_CONNECTED) {
            connectManager.noInet();
            Log.d(TAG, "onReceive: not connected");
            Log.d(TAG, "onReceive: from " + context.getPackageCodePath() + ", " + intent1);
        } else {
            connectManager.haveInet();
        }
    }
}
