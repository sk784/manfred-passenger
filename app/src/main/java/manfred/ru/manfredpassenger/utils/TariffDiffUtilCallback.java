package manfred.ru.manfredpassenger.utils;

import android.support.v7.util.DiffUtil;

import java.util.List;

import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;

public class TariffDiffUtilCallback extends DiffUtil.Callback {
    private List<TariffViewModel> oldTariffs;
    private List<TariffViewModel> newTariffs;

    public TariffDiffUtilCallback(List<TariffViewModel> oldTariffs, List<TariffViewModel> newTariffs) {
        this.oldTariffs = oldTariffs;
        this.newTariffs = newTariffs;
    }

    @Override
    public int getOldListSize() {
        return oldTariffs.size();
    }

    @Override
    public int getNewListSize() {
        return newTariffs.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        TariffViewModel oldTariff = oldTariffs.get(oldItemPosition);
        TariffViewModel newTariff = newTariffs.get(newItemPosition);
        return oldTariff.equals(newTariff);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        TariffViewModel oldTariff = oldTariffs.get(oldItemPosition);
        TariffViewModel newTariff = newTariffs.get(newItemPosition);
        return oldTariff.equals(newTariff);
    }
}
