package manfred.ru.manfredpassenger.utils.country_generator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlagModel {

    @SerializedName("flags")
    @Expose
    public Flags flags;

}
