package manfred.ru.manfredpassenger.utils;

public class Constants {
    public static final String PREFS_FILE_NAME = "ManfredPrefsFile";
    public static final String PREFS_PHONE_NUMBER_FIELD = "phoneNumber";
    public static final String PREFS_PHONE_CODE_FIELD = "phoneCode";
    public static final String PREFS_TOKEN_FIELD = "token";
    public static final String PREFS_USERNAME_FIELD = "username";
    public static final String PLACE_NAME = "placeName";
    public static final String FINISH_FIELD = "isFinish";
    public static final String IS_FROM_MENU_START = "isFromMenu";
    public static final String IS_WANT_SHARE_PROMO = "isWantShare";
    public static final long LOCATION_REQUEST_INTERVAL = 10000;
    public static final int CITY_SELECTED = 300;
    public static final String MERCHANT_PUBLIC_ID = "pk_f05cd7ed8e95464466cfb8cbffc8c";
    public static final String TERM_URL = "";
}
