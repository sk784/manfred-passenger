package manfred.ru.manfredpassenger.utils;

public class CostUtils {
    private CostUtils(){}

    public static String removeAfterDotsIfNeed(String cost){
        if(cost.length()<3){
            return cost;
        }else {
            String afterDotsString = cost.substring(cost.length()-3);
            if(afterDotsString.equals(".00")){
                return cost.substring(0,cost.length()-3);
            }else {
                return cost;
            }
        }
    }
}
