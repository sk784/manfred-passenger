package manfred.ru.manfredpassenger.utils.country_generator;

import com.google.gson.annotations.SerializedName;

public class Flag {

    @SerializedName("code")
    private String code;

    @SerializedName("flag")
    private String flag;

    @SerializedName("dial_code")
    private String dialCode;

    @SerializedName("name")
    private String name;

    @SerializedName("ru_name")
    private String russianName;

    public String getRussianName() {
        return russianName;
    }

    public String getCode() {
        return code;
    }

    public String getFlag() {
        return flag;
    }

    public String getDialCode() {
        return dialCode;
    }

    public String getName() {
        return name;
    }

}
