package manfred.ru.manfredpassenger.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.common.models.PushMessage;

public class SharedData {
	private static final String TAG = "SharedData";
	private static Long messageId;
	private static ArrayList<PushMessage> pushMessages = null;


	public static Long getMessageId() {
		SharedPreferences  preferences = (ManfredPassengerApplication.instance()).getSharedPreferences("push_messages", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		int value = preferences.getInt("push_message_id", 0);
		if(value == 0) {
			return null;
		}
		return new Long(value);
	}

	public static void setMessageId(Long messageId) {
		Log.d(TAG, "set messageId: " + messageId);
		SharedPreferences  preferences = (ManfredPassengerApplication.instance()).getSharedPreferences("push_messages", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		if(messageId == null) {
			editor.remove("push_message_id");
		} else {
			editor.putInt("push_message_id", messageId.intValue());
		}
		editor.apply();
	}


	public static PushMessage getPushMessage() {
		if(pushMessages == null) {
			return null;
		}
		if(pushMessages.isEmpty()) {
			return null;
		}
		PushMessage pushMessage = pushMessages.remove(0);
		return pushMessage;
	}


	public static void addPushMessage(PushMessage pushMessage) {
		if(pushMessages == null) {
			pushMessages = new ArrayList<PushMessage>();
		}
		pushMessages.add(pushMessage);
	}

}
