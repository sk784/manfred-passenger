package manfred.ru.manfredpassenger.utils;

import android.support.v7.util.DiffUtil;

import java.util.List;

import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.ride.viewmodels.CardType;
import manfred.ru.manfredpassenger.ride.viewmodels.CreditCardItem;

public class CardsDiffUtilCallback extends DiffUtil.Callback {
    private List<CardType> oldCards;
    private List<CardType> newCards;

    public CardsDiffUtilCallback(List<CardType> oldCards, List<CardType> newCards) {
        this.oldCards = oldCards;
        this.newCards = newCards;
    }

    @Override
    public int getOldListSize() {
        return oldCards.size();
    }

    @Override
    public int getNewListSize() {
        return newCards.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        CardType oldCreditCard = oldCards.get(oldItemPosition);
        CardType newCreditCard = newCards.get(newItemPosition);
        if(oldCreditCard.getType()== CardType.ItemType.CREDIT_CARD && newCreditCard.getType()== CardType.ItemType.CREDIT_CARD){
            return ((CreditCardItem)oldCreditCard).getCreditCard().equals(((CreditCardItem)newCreditCard).getCreditCard());
        }else {
            return false;
        }
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return false;
    }
}
