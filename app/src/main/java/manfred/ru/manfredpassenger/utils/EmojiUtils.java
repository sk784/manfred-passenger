package manfred.ru.manfredpassenger.utils;

import android.util.Log;

import java.util.Locale;

public class EmojiUtils {
    private static final String TAG = "EmojiUtils";

    public static String localeToEmoji(Locale locale) {
        String countryCode = locale.getCountry();
        Log.d(TAG, "localeToEmoji: " + countryCode);
        int firstLetter = Character.codePointAt(countryCode, 0) - 0x41 + 0x1F1E6;
        int secondLetter = Character.codePointAt(countryCode, 1) - 0x41 + 0x1F1E6;
        return new String(Character.toChars(firstLetter)) + new String(Character.toChars(secondLetter));
    }

    public static char[] getSymbol(int codepoint) {
        return Character.toChars(codepoint);
    }
}
