package manfred.ru.manfredpassenger.utils;

import java.net.SocketTimeoutException;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import retrofit2.Response;

public class NetworkUtils {
    //<editor-fold desc="Response class">
    public class ResponseStatus {
        private final boolean isSuccess;
        private final String statusString;

        ResponseStatus(boolean isSuccess, String statusString) {
            this.isSuccess = isSuccess;
            this.statusString = statusString;
        }

        public boolean isSuccess() {
            return isSuccess;
        }

        public String getStatusString() {
            return statusString;
        }
    }
    //</editor-fold>

    public ResponseStatus checkOnResponse(Response<ManfredResponse> response) {
        ManfredResponse manfredResponse = response.body();

        if (manfredResponse != null) {
            if (manfredResponse.getResult_code().equals("success")) {
                return new ResponseStatus(true, "all nice");
            } else {
                return new ResponseStatus(false, manfredResponse.getResult_code());
            }
        } else {
            return new ResponseStatus(false, "answer is null");
        }
    }

    @SuppressWarnings("unchecked")
    public <T extends ManfredResponse, L extends NetworkResponseCallback> void validateResponse(Response<T> response, L callback) {
        T manfredResponse = response.body();
        if (manfredResponse != null) {
            switch (manfredResponse.getResult_code()) {
                case "success":
                    callback.allOk(manfredResponse.getData());
                    break;
                case "invalid_token":
                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR, manfredResponse.getMessage()));
                    break;
                case "no_free_drivers":
                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.NO_FREE_DRIVERS, manfredResponse.getMessage()));
                    break;
                case "passenger_out_region":
                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OUT_REGION,manfredResponse.getMessage()));
                    break;
                case "invalid_passenger_region":
                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OUT_REGION,manfredResponse.getMessage()));
                    break;
                default:
                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, manfredResponse.getMessage()));
                    break;
            }
        } else {
            callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, "сервер вернул пустой ответ"));
        }
    }

    public <L extends NetworkResponseCallback> void validateSocketResponse(WebSocketAnswer response, L callback){
        if(response!=null){
            switch (response.getResultCode()){
                case "success":
                    callback.allOk(response.getPayload());
                    break;
                case "invalid_token":
                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR, response.getMessage()));
                    break;
                case "no_free_drivers":
                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.NO_FREE_DRIVERS, response.getMessage()));
                    break;
                case "passenger_out_region":
                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OUT_REGION,response.getMessage()));
                    break;
                default:
                    callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, response.getMessage()));
                    break;
            }
        }else {
            callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, "сервер вернул пустой ответ"));
        }
    }

    public void processFailureResponse(Throwable t, NetworkResponseCallback callback) {
        if (t instanceof SocketTimeoutException || t instanceof java.net.UnknownHostException) {
            callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.SERVER_UNAVAILABLE, "сервер недоступен"));
        } else {
            callback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, t.getMessage()));
        }
    }
}
