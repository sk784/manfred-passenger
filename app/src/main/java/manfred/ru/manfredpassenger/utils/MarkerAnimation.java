package manfred.ru.manfredpassenger.utils;
import android.graphics.Point;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
/**
 * Created by Vinil Chandran on 7/6/18.
 * https://stackoverflow.com/questions/28967821/animate-the-rotation-of-the-marker-in-google-map-v2
 */

public class MarkerAnimation {
    private static final String TAG = "MarkerAnimation";
    private static Location fromPosition;
    private static float angle = 0;
    public static void move(GoogleMap mMap, final Marker marker, final Location toPosition) {
        //Log.d(TAG, "move: ");

        if (fromPosition != null && marker != null && toPosition != null) {
            if (fromPosition.distanceTo(toPosition)<1.0){
                return;
            }
            final Handler handlerRotation = new Handler();
            final long startAngle = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long durationRotation = 300;
            final Interpolator interpolatorRotation = new LinearInterpolator();
            float bearing = fromPosition.bearingTo(toPosition);
            //Print.e("Bearing:" + bearing);
            angle = bearing<0?(360+bearing):bearing;
            angle = angle%360f;
            //Print.e("Angle:" + angle);
            handlerRotation.post(new Runnable() {
                @Override
                public void run() {
                    long elapsed = SystemClock.uptimeMillis() - startAngle;
                    float t = interpolatorRotation.getInterpolation((float) elapsed / durationRotation);
                    float rot = t * angle + (1 - t) * startRotation;
                    float mAngle = -rot > 180 ? rot / 2 : rot;
                    marker.setRotation(mAngle);

                    if (t < 1.0) {
                        handlerRotation.postDelayed(this, 16);
                    } else {
                        final Handler handler = new Handler();
                        final long start = SystemClock.uptimeMillis();
                        Projection projection = mMap.getProjection();
                        Point startPoint = projection.toScreenLocation(marker.getPosition());
                        final LatLng startLatLng = projection.fromScreenLocation(startPoint);
                        final long duration = Constants.LOCATION_REQUEST_INTERVAL;
                        final Interpolator interpolator = new LinearInterpolator();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                long elapsed = SystemClock.uptimeMillis() - start;
                                float t = interpolator.getInterpolation((float) elapsed
                                        / duration);
                                double lng = t * toPosition.getLongitude() + (1 - t)
                                        * startLatLng.longitude;
                                double lat = t * toPosition.getLatitude() + (1 - t)
                                        * startLatLng.latitude;
                                marker.setPosition(new LatLng(lat, lng));
                                if (t < 1.0) {
                                    // Post again 16ms later.
                                    handler.postDelayed(this, 16);
                                }
                            }
                        });
                    }
                }
            });
        }
        fromPosition = toPosition;
    }
}