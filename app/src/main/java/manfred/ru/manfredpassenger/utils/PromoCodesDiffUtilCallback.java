package manfred.ru.manfredpassenger.utils;

import android.support.v7.util.DiffUtil;
import android.util.Log;

import java.util.List;

import manfred.ru.manfredpassenger.promocodes.models.PromoCodeVM;

public class PromoCodesDiffUtilCallback extends DiffUtil.Callback {
    private static final String TAG = "PromoCodesDiffUtilCallb";
    private List<PromoCodeVM> oldPromoCodes;
    private List<PromoCodeVM> newPromoCodes;

    public PromoCodesDiffUtilCallback(List<PromoCodeVM> oldPromoCodes, List<PromoCodeVM> newPromoCodes) {
        Log.d(TAG, "PromoCodesDiffUtilCallback: "+oldPromoCodes+", "+newPromoCodes);
        this.oldPromoCodes = oldPromoCodes;
        this.newPromoCodes = newPromoCodes;
    }

    @Override
    public int getOldListSize() {
        return oldPromoCodes.size();
    }

    @Override
    public int getNewListSize() {
        return newPromoCodes.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        PromoCodeVM oldPromo = oldPromoCodes.get(oldItemPosition);
        PromoCodeVM newPromo = newPromoCodes.get(newItemPosition);
        return oldPromo.getId()==newPromo.getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        PromoCodeVM oldPromo = oldPromoCodes.get(oldItemPosition);
        PromoCodeVM newPromo = newPromoCodes.get(newItemPosition);
        boolean isSame = oldPromo.equals(newPromo);
/*        if(!isSame){
            Log.d(TAG, "areContentsTheSame: not same"+oldPromo.toString()+", "+newPromo.toString());
        }else {
            Log.d(TAG, "areContentsTheSame: "+oldPromo.toString()+", "+newPromo.toString());
        }*/
        return isSame;
    }
}
