package manfred.ru.manfredpassenger.splash;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.services.models.event.Event;
import manfred.ru.manfredpassenger.api.services.models.event.RestoreResponse;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.common.managers.events.ManfredEventManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static manfred.ru.manfredpassenger.api.ManfredNetworkError.ErrorType.OTHER;

public class RestoreStateManager {
    private static final String TAG = "RestoreStateManager";
    private ManfredTokenManager tokenManager;
    private ManfredEventManager manfredEventManager;

    public RestoreStateManager(ManfredTokenManager tokenManager, ManfredEventManager manfredEventManager) {
        this.tokenManager = tokenManager;
        this.manfredEventManager = manfredEventManager;
    }

    public interface RestoringCallback {
        void restored();

        void failRestoring(ManfredNetworkError manfredNetworkError);
    }
    //private MutableLiveData<Event>currentStatus;
    //private MutableLiveData<ManfredNetworkError>errors;

    public void restoreState(RestoringCallback restoringCallback) {
        Log.d(TAG, "initState: ");
        APIClient.getUserService().getState(tokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse<RestoreResponse>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<RestoreResponse>> call, @NonNull Response<ManfredResponse<RestoreResponse>> response) {
                ManfredResponse<RestoreResponse> manfredResponse = response.body();
                if (manfredResponse != null) {
                    if(!manfredResponse.getResult_code().equals("success")){
                        restoringCallback.failRestoring(new ManfredNetworkError(OTHER, manfredResponse.getMessage()));
                        return;
                    }
                    List<Event> events = new ArrayList<>();
                    if(manfredResponse.getData()!=null) {
                        Event restoringOrder = manfredResponse.getData().getOrder();
                        if (restoringOrder != null) {
                            Log.d(TAG, "onResponse: restoring order is " + restoringOrder.getId());
                            events.add(restoringOrder);
                            manfredEventManager.newEvents(events);
                        }
                    }
                    restoringCallback.restored();
                } else {
                    restoringCallback.failRestoring(new ManfredNetworkError(OTHER, "Пустой ответ"));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<RestoreResponse>> call, @NonNull Throwable t) {
                Log.e(TAG,"restoring error", t);
                restoringCallback.failRestoring(new ManfredNetworkError(OTHER, t.getMessage()));
            }
        });
    }

}
