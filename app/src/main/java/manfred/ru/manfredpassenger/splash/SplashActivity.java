package manfred.ru.manfredpassenger.splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.authorisation.LoginActivity;
import manfred.ru.manfredpassenger.ride.OrderActivity;
import manfred.ru.manfredpassenger.utils.Constants;
import manfred.ru.manfredpassenger.utils.SharedData;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_FILE_NAME, 0);
        String userPhoneCode = settings.getString(Constants.PREFS_PHONE_CODE_FIELD,null);
        String token = settings.getString(Constants.PREFS_TOKEN_FIELD, null);
        String phoneNumber = settings.getString(Constants.PREFS_PHONE_NUMBER_FIELD, null);
        //Log.d(TAG, "onCreate: token is "+token);
        if (token == null || phoneNumber == null || userPhoneCode==null) {
            //mb it deprecated because we have it in profileM
            newUser();
        } else {
            String fullPhoneNumber = userPhoneCode+phoneNumber;
            restoreState(fullPhoneNumber);
        }
        Intent intent = getIntent();
        if(intent.hasExtra("message_id")) {
            Log.d(TAG, "messageId: got from splash intent");
            Log.d("messageId", intent.toUri(0));
            SharedData.setMessageId(new Long(intent.getStringExtra("message_id")));
        } else {
            Log.d(TAG, "messageId: no message id in splash intent");
        }
    }

    private void restoreState(String phoneNumber) {
        Log.d(TAG, "restoreState: ");
        RestoreStateManager restoreStateManager = new RestoreStateManager(((ManfredPassengerApplication)getApplication()).getTokenManager(),
                ((ManfredPassengerApplication)getApplication()).getEventManager());
        restoreStateManager.restoreState(new RestoreStateManager.RestoringCallback() {
            @Override
            public void restored() {
                Log.d(TAG, "restored: ");
                Intent intent = new Intent(SplashActivity.this, OrderActivity.class);
                Registration registration = Registration.create().withUserId(phoneNumber);
                Intercom.client().registerIdentifiedUser(registration);
                startActivity(intent);
            }

            @Override
            public void failRestoring(ManfredNetworkError manfredNetworkError) {
                Log.d(TAG, "failRestoring: "+manfredNetworkError.toString());
                Toast.makeText(getApplicationContext(), manfredNetworkError.getText(), Toast.LENGTH_LONG).show();
                removeCredentials();
            }
        });
    }

    private void removeCredentials() {
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_FILE_NAME, 0);
        settings.edit().clear().apply();
        ((ManfredPassengerApplication) getApplication()).killAllManagers();
        newUser();
    }

    private void newUser() {
        Log.d(TAG, "onCreate: token is null, starting registration");
        Intercom.client().registerUnidentifiedUser();
        Intent intent = new Intent(this, LoginActivity.class);
        ((ManfredPassengerApplication)getApplication()).getTokenManager().setAuthToken(null);
        startActivity(intent);
    }
}
