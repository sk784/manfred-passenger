package manfred.ru.manfredpassenger.promocodes.repositories;

import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.promocodes.models.PromoCode;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodesAnswer;
import manfred.ru.manfredpassenger.promocodes.models.ReferalAnswer;

public interface PromoCodesRepository {
    void getPromoCodes(NetworkResponseCallback<PromoCodesAnswer> responseCallback);

    void addPromoCode(String promocode, NetworkResponseCallback<PromoCode> responseCallback);

    void getMyRefferal(NetworkResponseCallback<ReferalAnswer> responseCallback);
}
