package manfred.ru.manfredpassenger.promocodes.manager;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodeVM;
import manfred.ru.manfredpassenger.ride.models.Referal;

public interface PromoCodeManager {
    interface WorkDoneCallback{
        void done();
        void error(String text);
    }

    void selectPromoCode(int id);
    void addPromoCode(String code,WorkDoneCallback workDoneCallback);
    void unSelectPromoCode();
    void refreshPromoCodes();
    LiveData<List<PromoCodeVM>> getPromoCodes();
    MutableLiveData<PromoCodeVM> getSelectedPromoCode();
    MutableLiveData<Referal> getMyReferral();
    MutableLiveData<ManfredNetworkError>getError();
}
