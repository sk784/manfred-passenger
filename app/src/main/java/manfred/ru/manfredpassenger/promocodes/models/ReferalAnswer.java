package manfred.ru.manfredpassenger.promocodes.models;

import com.google.gson.annotations.SerializedName;

import manfred.ru.manfredpassenger.ride.models.Referal;

public class ReferalAnswer {
    @SerializedName("referal")
    private final Referal referal;

    public ReferalAnswer(Referal referal) {
        this.referal = referal;
    }

    public Referal getReferal() {
        return referal;
    }
}
