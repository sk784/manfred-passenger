package manfred.ru.manfredpassenger.promocodes.models;

import android.support.annotation.IdRes;
import android.util.Log;

import java.text.ChoiceFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import manfred.ru.manfredpassenger.R;


public class PromoCodeVM {
    private static final String TAG = "PromoCodeVM";
    private final String dateExpiring;
    private final String sizeOfDiscount;
    @IdRes private final int discountSymbol;
    private final String maxOrders;
    private final String maxSum;
    private final String promoCodeCaption;
    private final int id;
    private boolean isSelected;

    @Override
    public String toString() {
        return "PromoCodeVM{" +
                "dateExpiring='" + dateExpiring + '\'' +
                ", sizeOfDiscount='" + sizeOfDiscount + '\'' +
                ", id=" + id +
                ", isSelected=" + isSelected +
                '}';
    }

    public PromoCodeVM(PromoCode promoCode, boolean isSelected) {
        this.isSelected = isSelected;
        if(promoCode.getExpireDate()!=null && !promoCode.getExpireDate().isEmpty()) {
            Date expireDate = new Date(Long.parseLong(promoCode.getExpireDate())*1000);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            dateExpiring = simpleDateFormat.format(expireDate);
        }else {
            dateExpiring = "";
        }

        switch (promoCode.getType()){
            case "SUM":
                if(promoCode.getRegion().getPaymentSystem().getCurrency().equals("KZT")){
                    discountSymbol = R.drawable.ic_tenge;
                }else {
                    discountSymbol = R.drawable.ic_rouble;
                }
                sizeOfDiscount = promoCode.getDiscountSum();
                promoCodeCaption = "Скидка - "+promoCode.getDiscountSum()+" "+promoCode.getRegion().getPaymentSystem().getCurrencySymbol();
                break;
            default:
                discountSymbol = R.drawable.ic_procent;
                sizeOfDiscount = String.valueOf(promoCode.getDiscountPercent());
                promoCodeCaption = "Скидка - "+sizeOfDiscount+" %";
                break;
        }
        MessageFormat form = new MessageFormat("{0}");
        //int count = 2;
        Object[] testArgs = {promoCode.getMaxOrders()};
        double[] filelimits = {0,1,2,5};
        String[] filepart = {"{0} поездок","{0} поездка","{0} поездки","{0} поездок"};
        ChoiceFormat fileform = new ChoiceFormat(filelimits, filepart);
        form.setFormatByArgumentIndex(0, fileform);
        maxOrders = form.format(testArgs);

        if(promoCode.getMaxDiscountSum()!=null && !promoCode.getMaxDiscountSum().isEmpty()
                && !promoCode.getMaxDiscountSum().equals("0")) {
            maxSum = "Максимум " + promoCode.getMaxDiscountSum()+" "+promoCode.getRegion().getPaymentSystem().getCurrencySymbol();
        }else {
            maxSum = "";
        }
        id = promoCode.getId();
    }

    public String getDateExpiring() {
        return dateExpiring;
    }

    public String getSizeOfDiscount() {
        return sizeOfDiscount;
    }

    public int getDiscountSymbol() {
        return discountSymbol;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public String getMaxOrders() {
        return maxOrders;
    }

    public String getMaxSum() {
        return maxSum;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getId() {
        return id;
    }

    public String getPromoCodeCaption() {
        return promoCodeCaption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final PromoCodeVM that = (PromoCodeVM) o;

        if (discountSymbol != that.discountSymbol) return false;
        if (id != that.id) return false;
        if (isSelected != that.isSelected) return false;
        if (dateExpiring != null ? !dateExpiring.equals(that.dateExpiring) : that.dateExpiring != null)
            return false;
        if (sizeOfDiscount != null ? !sizeOfDiscount.equals(that.sizeOfDiscount) : that.sizeOfDiscount != null)
            return false;
        if (maxOrders != null ? !maxOrders.equals(that.maxOrders) : that.maxOrders != null)
            return false;
        return maxSum != null ? maxSum.equals(that.maxSum) : that.maxSum == null;
    }

    @Override
    public int hashCode() {
        int result = dateExpiring != null ? dateExpiring.hashCode() : 0;
        result = 31 * result + (sizeOfDiscount != null ? sizeOfDiscount.hashCode() : 0);
        result = 31 * result + discountSymbol;
        result = 31 * result + (maxOrders != null ? maxOrders.hashCode() : 0);
        result = 31 * result + (maxSum != null ? maxSum.hashCode() : 0);
        result = 31 * result + id;
        result = 31 * result + (isSelected ? 1 : 0);
        return result;
    }
}
