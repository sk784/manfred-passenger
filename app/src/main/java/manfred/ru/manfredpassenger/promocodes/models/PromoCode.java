package manfred.ru.manfredpassenger.promocodes.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import manfred.ru.manfredpassenger.common.models.Region;

@Generated("com.robohorse.robopojogenerator")
public class PromoCode{

	@SerializedName("activation_date")
	private String activationDate;

	@SerializedName("created")
	private String created;

	@SerializedName("referal")
	private boolean referal;

	@SerializedName("discount_sum")
	private String discountSum;

	@SerializedName("type")
	private String type;

	@SerializedName("max_orders")
	private int maxOrders;

	@SerializedName("used_orders")
	private int usedOrders;

	@SerializedName("expire_date")
	private String expireDate;

	@SerializedName("max_discount_sum")
	private String maxDiscountSum;

	@SerializedName("common")
	private boolean common;

	@SerializedName("name")
	private String name;

	@SerializedName("phone_number")
	private Object phoneNumber;

	@SerializedName("id")
	private int id;

	@SerializedName("discount_percent")
	private int discountPercent;

	@SerializedName("status")
	private String status;

	@SerializedName("use_date")
	private String useDate;

	@SerializedName("region")
	private Region region;

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public void setActivationDate(String activationDate){
		this.activationDate = activationDate;
	}

	public String getActivationDate(){
		return activationDate;
	}

	public void setCreated(String created){
		this.created = created;
	}

	public String getCreated(){
		return created;
	}

	public void setReferal(boolean referal){
		this.referal = referal;
	}

	public boolean isReferal(){
		return referal;
	}

	public void setDiscountSum(String discountSum){
		this.discountSum = discountSum;
	}

	public String getDiscountSum(){
		return discountSum;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setMaxOrders(int maxOrders){
		this.maxOrders = maxOrders;
	}

	public int getMaxOrders(){
		return maxOrders;
	}

	public void setUsedOrders(int usedOrders){
		this.usedOrders = usedOrders;
	}

	public int getUsedOrders(){
		return usedOrders;
	}

	public void setExpireDate(String expireDate){
		this.expireDate = expireDate;
	}

	public String getExpireDate(){
		return expireDate;
	}

	public void setMaxDiscountSum(String maxDiscountSum){
		this.maxDiscountSum = maxDiscountSum;
	}

	public String getMaxDiscountSum(){
		return maxDiscountSum;
	}

	public void setCommon(boolean common){
		this.common = common;
	}

	public boolean isCommon(){
		return common;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setPhoneNumber(Object phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public Object getPhoneNumber(){
		return phoneNumber;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setDiscountPercent(int discountPercent){
		this.discountPercent = discountPercent;
	}

	public int getDiscountPercent(){
		return discountPercent;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setUseDate(String useDate){
		this.useDate = useDate;
	}

	public String getUseDate(){
		return useDate;
	}

	@Override
 	public String toString(){
		return 
			"PromoCode{" + 
			"activation_date = '" + activationDate + '\'' + 
			",created = '" + created + '\'' + 
			",referal = '" + referal + '\'' + 
			",discount_sum = '" + discountSum + '\'' + 
			",type = '" + type + '\'' + 
			",max_orders = '" + maxOrders + '\'' + 
			",used_orders = '" + usedOrders + '\'' + 
			",expire_date = '" + expireDate + '\'' + 
			",max_discount_sum = '" + maxDiscountSum + '\'' + 
			",common = '" + common + '\'' + 
			",name = '" + name + '\'' + 
			",phone_number = '" + phoneNumber + '\'' + 
			",id = '" + id + '\'' + 
			",discount_percent = '" + discountPercent + '\'' + 
			",status = '" + status + '\'' + 
			",use_date = '" + useDate + '\'' + 
			"}";
		}
}