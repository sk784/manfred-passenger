package manfred.ru.manfredpassenger.promocodes.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PromoCodesAnswer {
    @SerializedName("promocodes")
    private final List<PromoCode>promoCodes;

    public PromoCodesAnswer(List<PromoCode> promoCodes) {
        this.promoCodes = promoCodes;
    }

    public List<PromoCode> getPromoCodes() {
        return promoCodes;
    }
}
