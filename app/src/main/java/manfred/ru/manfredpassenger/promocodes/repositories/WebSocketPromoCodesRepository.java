package manfred.ru.manfredpassenger.promocodes.repositories;

import android.support.annotation.Nullable;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.promocodes.models.PromoCode;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodeRequest;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodesAnswer;
import manfred.ru.manfredpassenger.promocodes.models.ReferalAnswer;
import manfred.ru.manfredpassenger.utils.NetworkUtils;

public class WebSocketPromoCodesRepository implements PromoCodesRepository {
    private final TokenManager manfredTokenManager;
    private final NetworkManagerProvider networkManager;

    public WebSocketPromoCodesRepository(TokenManager manfredTokenManager, NetworkManagerProvider networkManager) {
        this.manfredTokenManager = manfredTokenManager;
        this.networkManager = networkManager;
    }

    @Override
    public void getPromoCodes(NetworkResponseCallback<PromoCodesAnswer> responseCallback) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(),"order/promocodes_list",new WebSocketAnswerCallback<WebSocketAnswer<PromoCodesAnswer>>(){
            @Override
            public void messageReceived(WebSocketAnswer<PromoCodesAnswer> message) {
                new NetworkUtils().validateSocketResponse(message, responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return PromoCodesAnswer.class;
            }
        });
    }

    @Override
    public void addPromoCode(String promocode, NetworkResponseCallback<PromoCode> responseCallback) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(), "order/add_promocode", new PromoCodeRequest(promocode), new WebSocketAnswerCallback<WebSocketAnswer<PromoCode>>() {
            @Override
            public void messageReceived(WebSocketAnswer<PromoCode> message) {
                new NetworkUtils().validateSocketResponse(message, responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return PromoCode.class;
            }
        });

    }

    @Override
    public void getMyRefferal(NetworkResponseCallback<ReferalAnswer> responseCallback) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(),"user/referal", new WebSocketAnswerCallback<WebSocketAnswer<ReferalAnswer>>() {
            @Override
            public void messageReceived(WebSocketAnswer<ReferalAnswer> message) {
                new NetworkUtils().validateSocketResponse(message, responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return ReferalAnswer.class;
            }
        });
    }
}
