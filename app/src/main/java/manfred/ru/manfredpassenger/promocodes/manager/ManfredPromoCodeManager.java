package manfred.ru.manfredpassenger.promocodes.manager;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.promocodes.models.PromoCode;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodeVM;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodesAnswer;
import manfred.ru.manfredpassenger.promocodes.models.ReferalAnswer;
import manfred.ru.manfredpassenger.promocodes.repositories.PromoCodesRepository;
import manfred.ru.manfredpassenger.promocodes.repositories.WebSocketPromoCodesRepository;
import manfred.ru.manfredpassenger.ride.models.Referal;

public class ManfredPromoCodeManager implements PromoCodeManager {
    private static final String TAG = "ManfredPromoCodeManager";

    private final PromoCodesRepository promoCodesRepository;
    private final MutableLiveData<List<PromoCodeVM>> promoCodes;
    private final MutableLiveData<ManfredNetworkError>errors;
    private final MutableLiveData<PromoCodeVM> selectedPromoCode;
    private final MutableLiveData<Referal>referal;

    public ManfredPromoCodeManager(TokenManager tokenManager, NetworkManagerProvider networkManager) {
        this.promoCodesRepository = new WebSocketPromoCodesRepository(tokenManager, networkManager);
        this.promoCodes = new MutableLiveData<>();
        this.errors = new MutableLiveData<>();
        this.selectedPromoCode = new MutableLiveData<>();
        this.referal = new MutableLiveData<>();
        init();
        initUserRefferal();

    }

    public void initUserRefferal() {
        Log.d(TAG, "initUserRefferal: ");
        promoCodesRepository.getMyRefferal(new NetworkResponseCallback<ReferalAnswer>() {
            @Override
            public void allOk(ReferalAnswer response) {
                referal.postValue(response.getReferal());
                Log.d(TAG, "initUserRefferal allOk: referal is "+response.getReferal());
                errors.postValue(null);
            }

            @Override
            public void error(ManfredNetworkError error) {
                errors.postValue(error);
                Log.d(TAG, "initUserRefferal error: "+error);
            }
        });
    }

    private void init() {
        promoCodesRepository.getPromoCodes(new NetworkResponseCallback<PromoCodesAnswer>() {
            @Override
            public void allOk(PromoCodesAnswer response) {
                promoCodes.postValue(toVMPromoCode(response.getPromoCodes()));
                errors.postValue(null);
            }

            @Override
            public void error(ManfredNetworkError error) {
                errors.postValue(error);
                Log.d(TAG, "error: "+error);
            }
        });
    }

    private List<PromoCodeVM>toVMPromoCode(List<PromoCode>promoCodes){
        List<PromoCodeVM>promoCodeVMS = new ArrayList<>();
        for (PromoCode promoCode : promoCodes){
            promoCodeVMS.add(new PromoCodeVM(promoCode,false));
        }
        return promoCodeVMS;
    }

    public MutableLiveData<ManfredNetworkError> getErrors() {
        return errors;
    }

    @Override
    public void selectPromoCode(int id) {
        Log.d(TAG, "selectPromoCode: "+id);
        List<PromoCodeVM>promoCodeVMS = promoCodes.getValue();
        Log.d(TAG, "selectPromoCode: old list is "+promoCodeVMS);
        PromoCodeVM itemToMove=null;
        if(promoCodeVMS!=null){
            for (PromoCodeVM promoCodeVM : promoCodeVMS){
                if(promoCodeVM.getId()==id){
                    promoCodeVM.setSelected(true);
                    itemToMove = promoCodeVM;
                }else {
                    promoCodeVM.setSelected(false);
                }
            }
        }

        if(promoCodeVMS!=null) {
            promoCodeVMS.remove(itemToMove);
            promoCodeVMS.add(0, itemToMove);
        }

        Log.d(TAG, "selectPromoCode: new list is "+promoCodeVMS);
        promoCodes.postValue(promoCodeVMS);
        selectedPromoCode.postValue(itemToMove);

    }

    @Override
    public void addPromoCode(String code, WorkDoneCallback workDoneCallback) {
        promoCodesRepository.addPromoCode(code, new NetworkResponseCallback<PromoCode>() {
            @Override
            public void allOk(PromoCode response) {
                init();
                workDoneCallback.done();
                errors.postValue(null);
            }

            @Override
            public void error(ManfredNetworkError error) {
                errors.postValue(error);
                workDoneCallback.error(error.getText());
            }
        });
    }

    @Override
    public void unSelectPromoCode() {
        List<PromoCodeVM>promoCodeVMS = promoCodes.getValue();
        if(promoCodeVMS!=null){
            for (PromoCodeVM promoCodeVM : promoCodeVMS){
                promoCodeVM.setSelected(false);
                selectedPromoCode.postValue(null);
            }
            promoCodes.postValue(promoCodeVMS);
        }
    }

    @Override
    public void refreshPromoCodes() {
        init();
    }

    @Override
    public LiveData<List<PromoCodeVM>> getPromoCodes() {
        return promoCodes;
    }

    @Override
    public MutableLiveData<PromoCodeVM> getSelectedPromoCode() {
        return selectedPromoCode;
    }

    @Override
    public MutableLiveData<Referal> getMyReferral() {
        return referal;
    }

    @Override
    public MutableLiveData<ManfredNetworkError> getError() {
        return errors;
    }
}
