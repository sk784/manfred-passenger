package manfred.ru.manfredpassenger.promocodes;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.promocodes.fragments.AddPromoCodeFragment;
import manfred.ru.manfredpassenger.promocodes.fragments.ListPromoCodesFragment;
import manfred.ru.manfredpassenger.promocodes.fragments.SharePromocodeFragment;

import static manfred.ru.manfredpassenger.utils.Constants.IS_FROM_MENU_START;
import static manfred.ru.manfredpassenger.utils.Constants.IS_WANT_SHARE_PROMO;

public class PromoCodeActivity extends AppCompatActivity implements ListPromoCodesFragment.OnAddPromoClick{
    private static final String TAG = "PromoCodeActivity";
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.animator.enter_from_right,R.animator.exit_to_left);
        setContentView(R.layout.activity_promo_code);

        toolbar = findViewById(R.id.promo_codes_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        setTitle("Промокод");

        Bundle bundle = getIntent().getExtras();
        boolean fromMenu = false;
        boolean wantShare = false;
        if(bundle!=null){
            fromMenu = bundle.getBoolean(IS_FROM_MENU_START);
            wantShare = bundle.getBoolean(IS_WANT_SHARE_PROMO);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(!wantShare) {
            transaction.replace(R.id.fragment_promo_codes_container, ListPromoCodesFragment.newInstance(fromMenu));
        }else {
            transaction.replace(R.id.fragment_promo_codes_container, new SharePromocodeFragment());
        }
        transaction.commit();

        ((ManfredPassengerApplication)getApplication()).getPromoCodeManager().getError().observe(this, new Observer<ManfredNetworkError>() {
            @Override
            public void onChanged(@Nullable ManfredNetworkError manfredNetworkError) {
                if (manfredNetworkError != null) {
                    Toast.makeText(getApplicationContext(),manfredNetworkError.getText(),Toast.LENGTH_LONG).show();
                }
            }
        });
        initNoInetObserver();
    }

    void initNoInetObserver(){
        ((ManfredPassengerApplication)getApplication()).getConnectManager().getIsConnected().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    ConstraintLayout noInetLayout = findViewById(R.id.cl_no_inet);
                    if(aBoolean){
                        noInetLayout.setVisibility(View.GONE);
                        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
                    }else {
                        noInetLayout.setVisibility(View.VISIBLE);
                        toolbar.setTitleTextColor(getResources().getColor(R.color.half_transparent));
                    }

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(!((ManfredPassengerApplication)getApplication()).getConnectManager().isConnected())return;
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        if(((ManfredPassengerApplication)getApplication()).getConnectManager().isConnected()) {
            super.onBackPressed();
        }
        return false;
    }

    @Override
    public void addPromoCode() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_promo_codes_container, new AddPromoCodeFragment());
        transaction.commit();
    }

    @Override
    public void shareMyRefferalCode() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_promo_codes_container, new SharePromocodeFragment());
        transaction.commit();
    }
}
