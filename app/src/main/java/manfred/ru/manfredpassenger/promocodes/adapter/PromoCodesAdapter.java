package manfred.ru.manfredpassenger.promocodes.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import manfred.ru.manfredpassenger.databinding.PromocodeItemBinding;
import manfred.ru.manfredpassenger.promocodes.fragments.ListPromoCodesFragment;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;
import manfred.ru.manfredpassenger.promocodes.models.PromoCode;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodeVM;

public class PromoCodesAdapter extends RecyclerView.Adapter<PromoCodesAdapter.ViewHolder>{
    private List<PromoCodeVM>promoCodes;
    private final PromoCodeManager promoCodeManager;
    private static final String TAG = "PromoCodesAdapter";
    private final OnPromoClick onPromoClick;
    public interface OnPromoClick{
        void click(int promoId);
    }

    public PromoCodesAdapter(List<PromoCodeVM> promoCodes, PromoCodeManager promoCodeManager, OnPromoClick onPromoClick) {
        this.promoCodes = promoCodes;
        this.promoCodeManager = promoCodeManager;
        this.onPromoClick = onPromoClick;
    }

    public List<PromoCodeVM> getPromoCodes() {
        return promoCodes;
    }

    public void setPromoCodes(List<PromoCodeVM> promoCodes) {
        this.promoCodes = promoCodes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        PromocodeItemBinding itemBinding =
                PromocodeItemBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PromoCodeVM promoCodeVM = promoCodes.get(position);
        holder.bind(promoCodeVM);
        holder.itemView.setOnClickListener(v->{
            promoCodeManager.selectPromoCode(promoCodeVM.getId());
            Log.d(TAG, "onBindViewHolder: click");
            onPromoClick.click(promoCodeVM.getId());
        });

    }

    @Override
    public int getItemCount() {
        if(promoCodes==null)return 0;
        return promoCodes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final PromocodeItemBinding binding;
        public ViewHolder(PromocodeItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        public void bind(PromoCodeVM promoCode){
            binding.setPromocode(promoCode);
            binding.executePendingBindings();
            Log.d(TAG, "bind: "+promoCode.getId());

        }
    }
}
