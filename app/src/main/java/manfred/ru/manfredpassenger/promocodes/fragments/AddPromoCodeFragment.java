package manfred.ru.manfredpassenger.promocodes.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddPromoCodeFragment extends Fragment {

    public AddPromoCodeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_add_promo_code, container, false);
        EditText promoCode = root.findViewById(R.id.et_promo);
        Button addPromoCode = root.findViewById(R.id.btn_add_entered_promo);
        Button clearText = root.findViewById(R.id.clearText);
        PromoCodeManager promoCodeManager = ((ManfredPassengerApplication)getActivity().getApplication()).getPromoCodeManager();
        promoCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count>0){
                    addPromoCode.setVisibility(View.VISIBLE);
                    clearText.setVisibility(View.VISIBLE);
                }else {
                    addPromoCode.setVisibility(View.GONE);
                    clearText.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        clearText.setOnClickListener(v -> {
            promoCode.setText("");
            clearText.setVisibility(View.GONE);
        });

        addPromoCode.setOnClickListener(v -> {
            addPromoCode.setEnabled(false);
            promoCodeManager.addPromoCode(promoCode.getText().toString(), new PromoCodeManager.WorkDoneCallback() {
                @Override
                public void done() {
                    getActivity().finish();
                }

                @Override
                public void error(String text) {
                    addPromoCode.setEnabled(true);
                    Toast.makeText(getActivity(),text,Toast.LENGTH_LONG).show();
                }
            });
        });
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        View view = getView();
        if(view!=null) {
            EditText phoneNumber = view.findViewById(R.id.et_promo);
            phoneNumber.requestFocus();
            phoneNumber.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(getActivity()!=null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.showSoftInput(phoneNumber, 0);
                        }
                    }
                }
            },200);
        }
    }


}
