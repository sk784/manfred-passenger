package manfred.ru.manfredpassenger.promocodes.repositories;

import android.support.annotation.NonNull;
import android.util.Log;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.promocodes.models.PromoCode;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodeRequest;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodesAnswer;
import manfred.ru.manfredpassenger.promocodes.models.ReferalAnswer;
import manfred.ru.manfredpassenger.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HTTPPromoCodesRepository implements PromoCodesRepository {
    private static final String TAG = "PromoCodesRepository";
    private TokenManager manfredTokenManager;

    public HTTPPromoCodesRepository(TokenManager manfredTokenManager) {
        this.manfredTokenManager = manfredTokenManager;
    }

    @Override
    public void getPromoCodes(NetworkResponseCallback<PromoCodesAnswer> responseCallback){
        Log.d(TAG, "getPromoCodes: with token"+manfredTokenManager.getAuthToken());
        APIClient.getOrderService().getPromocodes(manfredTokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse<PromoCodesAnswer>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<PromoCodesAnswer>> call, @NonNull Response<ManfredResponse<PromoCodesAnswer>> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<PromoCodesAnswer>> call, @NonNull Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void addPromoCode(String promocode, NetworkResponseCallback<PromoCode> responseCallback){
        APIClient.getOrderService().addPromocode(manfredTokenManager.getAuthToken(),new PromoCodeRequest(promocode)).enqueue(new Callback<ManfredResponse<PromoCode>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<PromoCode>> call, @NonNull Response<ManfredResponse<PromoCode>> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<PromoCode>> call, @NonNull Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void getMyRefferal(NetworkResponseCallback<ReferalAnswer> responseCallback){
        //Log.d(TAG, "getMyRefferal: ");
        APIClient.getUserService().getMyReferral(manfredTokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse<ReferalAnswer>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<ReferalAnswer>> call, @NonNull Response<ManfredResponse<ReferalAnswer>> response) {
                //Log.d(TAG, "onResponse: "+response.raw().message());
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<ReferalAnswer>> call, @NonNull Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
                //Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }

}
