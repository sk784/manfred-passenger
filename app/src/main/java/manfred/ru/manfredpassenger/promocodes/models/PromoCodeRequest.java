package manfred.ru.manfredpassenger.promocodes.models;

public class PromoCodeRequest {
    private final String promocode;

    public PromoCodeRequest(String promocode) {
        this.promocode = promocode;
    }

    public String getPromocode() {
        return promocode;
    }
}
