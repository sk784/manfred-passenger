package manfred.ru.manfredpassenger.promocodes.fragments;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.promocodes.manager.ManfredPromoCodeManager;
import manfred.ru.manfredpassenger.ride.models.Referal;


/**
 * A simple {@link Fragment} subclass.
 */
public class SharePromocodeFragment extends Fragment {


    public SharePromocodeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_share_promocode, container, false);
        TextView description = rootView.findViewById(R.id.tv_description);
        TextView promoValue = rootView.findViewById(R.id.tv_my_promo);
        ImageButton sharePromo = rootView.findViewById(R.id.forward_button);
        //перечитаем настройки реферальной программы с сервера
        ((ManfredPromoCodeManager)((ManfredPassengerApplication)getActivity().getApplication()).getPromoCodeManager()).initUserRefferal();

        ((ManfredPassengerApplication)getActivity().getApplication()).getPromoCodeManager().getMyReferral().observe(this, new Observer<Referal>() {
            @Override
            public void onChanged(@Nullable Referal referal) {
                if (referal != null) {
                    String referalText="Я пользуюсь сервисом деловых поездок Manfred и рекомендую его попробовать. Установите приложение и получите скидку 20% на первую поездку по моему промокоду "+referal.getName()+". https://manfred.ru/app";
                    description.setText(referal.getDesc());
                    promoValue.setText(referal.getName());
                    sharePromo.setOnClickListener(v -> {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, referalText);
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                    });
                }
            }
        });
        return rootView;
    }

}
