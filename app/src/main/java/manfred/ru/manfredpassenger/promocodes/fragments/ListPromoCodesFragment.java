package manfred.ru.manfredpassenger.promocodes.fragments;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.databinding.FragmentListPromoCodesBinding;
import manfred.ru.manfredpassenger.promocodes.adapter.PromoCodesAdapter;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodeVM;
import manfred.ru.manfredpassenger.utils.PromoCodesDiffUtilCallback;

import static manfred.ru.manfredpassenger.utils.Constants.IS_FROM_MENU_START;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListPromoCodesFragment extends Fragment {
    private static final String TAG = "ListPromoCodesFragment";
    OnAddPromoClick onAddPromoClick;
    public interface OnAddPromoClick{
        void addPromoCode();
        void shareMyRefferalCode();
    }


    public static ListPromoCodesFragment newInstance(boolean fromMenu){
        ListPromoCodesFragment fragment = new ListPromoCodesFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_FROM_MENU_START,fromMenu);
        fragment.setArguments(args);
        return fragment;
    }

    public ListPromoCodesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            onAddPromoClick = (OnAddPromoClick)context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString()
                    + " must implement OnAddPromoClick");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FragmentListPromoCodesBinding binding = FragmentListPromoCodesBinding.inflate(getLayoutInflater());
        Bundle args = getArguments();
        boolean isFromMenu = false;
        if (args != null) {
            isFromMenu = args.getBoolean(IS_FROM_MENU_START, false);
        }
        init(binding,isFromMenu);

        return binding.getRoot();
    }

    private void init(FragmentListPromoCodesBinding binding, boolean isFromMenu) {
        RecyclerView recyclerView = binding.rvPromoCodes;
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.onSaveInstanceState();
        recyclerView.setLayoutManager(mLayoutManager);

        //List<PromoCodeVM> promoCodes = new ArrayList<>();
        PromoCodeManager promoCodeManager = ((ManfredPassengerApplication)getActivity().getApplication()).getPromoCodeManager();
        PromoCodesAdapter promoCodesAdapter = new PromoCodesAdapter(new ArrayList<>(), promoCodeManager, promoId -> {
            if(!isFromMenu) {
                getActivity().finish();
            }
        });
        recyclerView.setAdapter(promoCodesAdapter);
        promoCodeManager.getPromoCodes().observe(this, new Observer<List<PromoCodeVM>>() {
            @Override
            public void onChanged(@Nullable List<PromoCodeVM> promoCodeVMS) {
                //Log.d(TAG, "onChanged: promoCodes="+promoCodes+"promoCodeVMS(new) = "+promoCodeVMS);
                Log.d(TAG, "onChanged: in adapter "+promoCodesAdapter.getPromoCodes());
                Log.d(TAG, "onChanged: incoming "+promoCodeVMS);
                PromoCodesDiffUtilCallback promoCodesDiffUtilCallback =
                        new PromoCodesDiffUtilCallback(promoCodesAdapter.getPromoCodes(), promoCodeVMS);
                DiffUtil.DiffResult tariffResult = DiffUtil.calculateDiff(promoCodesDiffUtilCallback);
                promoCodesAdapter.setPromoCodes(promoCodeVMS);
                tariffResult.dispatchUpdatesTo(promoCodesAdapter);
                promoCodesAdapter.notifyDataSetChanged();

                /*
                promoCodes.clear();
                if(promoCodeVMS!=null) {
                    //Log.d(TAG, "onChanged: "+promoCodeVMS.get(0).toString());
                    promoCodes.addAll(promoCodeVMS);
                }*/
            }
        });
        if(isFromMenu){
            binding.btnUnselectPromo.setVisibility(View.GONE);
            binding.btnAddPromo.setVisibility(View.VISIBLE);
            binding.btnAddPromo.setOnClickListener(v -> onAddPromoClick.addPromoCode());
            binding.btnDiscount.setVisibility(View.VISIBLE);
            binding.btnDiscount.setOnClickListener(v -> onAddPromoClick.shareMyRefferalCode());
        }else {
            binding.btnUnselectPromo.setVisibility(View.VISIBLE);
            binding.btnUnselectPromo.setOnClickListener(v -> {
                promoCodeManager.unSelectPromoCode();
                getActivity().finish();
            });
            binding.btnAddPromo.setVisibility(View.GONE);
            binding.btnDiscount.setVisibility(View.GONE);
        }
    }


}
