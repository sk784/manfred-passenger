package manfred.ru.manfredpassenger.history;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.authorisation.LoginActivity;
import manfred.ru.manfredpassenger.authorisation.managers.AuthorisationManager;
import manfred.ru.manfredpassenger.common.fragments.NoNetFragmentForRelative;
import manfred.ru.manfredpassenger.history.fragments.HistoryDetailFragment;
import manfred.ru.manfredpassenger.history.fragments.TabbedHistoryFragment;
import manfred.ru.manfredpassenger.history.viewmodel.HistoryVM;
import manfred.ru.manfredpassenger.history.viewmodel.HistoryViewModelFactory;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.utils.Constants;

public class HistoryActivity extends AppCompatActivity {
    private static final String TAG = "HistoryActivity";
    HistoryVM historyVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.animator.enter_from_right,R.animator.exit_to_left);
        setContentView(R.layout.activity_history);

        Toolbar toolbar = findViewById(R.id.history_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        setTitle("Поездки");

        HistoryViewModelFactory historyViewModelFactory = new HistoryViewModelFactory(((ManfredPassengerApplication)getApplication()).getHistoryManager(),
                ((ManfredPassengerApplication)getApplication()).getPreOrderManager());
        historyVM = ViewModelProviders.of(this, historyViewModelFactory).get(HistoryVM.class);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_history_container, new TabbedHistoryFragment());
        fragmentTransaction.commit();

        historyVM.getHistoryDetail().observe(this, new Observer<OrderVM>() {
            @Override
            public void onChanged(@Nullable OrderVM orderVM) {
                if(orderVM!=null){
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_history_container, new HistoryDetailFragment());
                    fragmentTransaction.addToBackStack("details");
                    fragmentTransaction.commit();
                    getSupportActionBar().setElevation(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                }
            }
        });

        historyVM.getErrors().observe(this, manfredNetworkError -> {
            if (manfredNetworkError != null) {
                Toast.makeText(getApplicationContext(), manfredNetworkError.getText(), Toast.LENGTH_LONG).show();
                if(manfredNetworkError.getType()== ManfredNetworkError.ErrorType.TOKEN_ERROR){
                    Log.d(TAG, "getErrors from historyVM: token error, current token is "+
                            ((ManfredPassengerApplication)getApplication()).getTokenManager().getAuthToken());
                    final Intent intent = new Intent(this, LoginActivity.class);
                    SharedPreferences settings = getSharedPreferences(Constants.PREFS_FILE_NAME, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.clear().apply();
                    ((ManfredPassengerApplication)getApplication()).killAllManagers();
                    startActivity(intent);
                }
            }
        });
        initNoInetObserver();

    }

    void initNoInetObserver(){
        ((ManfredPassengerApplication)getApplication()).getConnectManager().getIsConnected().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    ConstraintLayout noInetLayout = findViewById(R.id.cl_no_inet);
                    if(aBoolean){
                        noInetLayout.setVisibility(View.GONE);
                    }else {
                        noInetLayout.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(!((ManfredPassengerApplication)getApplication()).getConnectManager().isConnected())return;
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setElevation(0);
        }
        super.onBackPressed();
        getFragmentManager().popBackStack();
        historyVM.showHistoryDetail(null);
        setTitle("Поездки");
    }

    @Override
    public boolean onSupportNavigateUp() {
        if(((ManfredPassengerApplication)getApplication()).getConnectManager().isConnected()) {
            super.onBackPressed();
            if (getSupportActionBar() != null) {
                getSupportActionBar().setElevation(0);
            }
            getFragmentManager().popBackStack();
            historyVM.showHistoryDetail(null);
            setTitle("Поездки");
        }
        return false;
    }
}
