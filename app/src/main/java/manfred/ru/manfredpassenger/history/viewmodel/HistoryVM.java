package manfred.ru.manfredpassenger.history.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.history.managers.ManfredHistoryManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredPreOrderManager;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public class HistoryVM extends ViewModel{
    private ManfredHistoryManager historyManager;
    private ManfredPreOrderManager preOrderManager;
    //private MutableLiveData<OrderVM> selectedOrder;

    public HistoryVM(ManfredHistoryManager historyManager, ManfredPreOrderManager preOrderManager) {
        this.historyManager = historyManager;
        this.preOrderManager = preOrderManager;
    }

    public void showHistoryDetail(OrderVM orderVM){
        historyManager.showHistoryDetail(orderVM);
    }

    public LiveData<List<OrderVM>>getHistory(){
        return historyManager.getHistory();
    }

    public LiveData<OrderVM>getHistoryDetail(){
        return historyManager.getOrderDetails();
    }

    public LiveData<List<OrderVM>>getPreorders(){
        return preOrderManager.getPreOrders();
    }

    public LiveData<ManfredNetworkError>getErrors(){
        return historyManager.getError();
    }

}
