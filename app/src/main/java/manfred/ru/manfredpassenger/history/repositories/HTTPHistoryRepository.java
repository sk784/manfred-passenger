package manfred.ru.manfredpassenger.history.repositories;

import android.support.annotation.NonNull;
import android.util.Log;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.HistoryResponce;
import manfred.ru.manfredpassenger.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HTTPHistoryRepository implements HistoryRepository {
    private static final String TAG = "HistoryRepository";
    @Override
    public void getHistory(String token, NetworkResponseCallback<HistoryResponce> callback){
        //Log.d(TAG, "getHistory: with token "+token);
        APIClient.getUserService().getHistory(token).enqueue(new Callback<ManfredResponse<HistoryResponce>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<HistoryResponce>> call, @NonNull Response<ManfredResponse<HistoryResponce>> response) {
                Log.d(TAG, "onResponse: "+response.body());
                new NetworkUtils().validateResponse(response, callback);
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<HistoryResponce>> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                new NetworkUtils().processFailureResponse(t, callback);
            }
        });
    }
}
