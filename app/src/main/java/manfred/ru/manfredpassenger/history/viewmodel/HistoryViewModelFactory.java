package manfred.ru.manfredpassenger.history.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import manfred.ru.manfredpassenger.history.managers.ManfredHistoryManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredPreOrderManager;

public class HistoryViewModelFactory implements ViewModelProvider.Factory {
    private ManfredHistoryManager historyManager;
    private ManfredPreOrderManager preOrderManager;
    public HistoryViewModelFactory(ManfredHistoryManager manfredHistoryManager,ManfredPreOrderManager preOrderManager) {
        this.historyManager = manfredHistoryManager;
        this.preOrderManager = preOrderManager;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(HistoryVM.class)) {
            return (T) new HistoryVM(historyManager, preOrderManager);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
