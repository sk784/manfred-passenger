package manfred.ru.manfredpassenger.history.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.HistoryResponce;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.history.repositories.HTTPHistoryRepository;
import manfred.ru.manfredpassenger.history.repositories.HistoryRepository;
import manfred.ru.manfredpassenger.history.repositories.WebsocketHistoryRepository;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.utils.SingleLiveEvent;

public class ManfredHistoryManager implements HistoryManager {
    private static final String TAG = "ManfredHistoryManager";
    private final TokenManager tokenManager;
    private MutableLiveData<List<OrderVM>>orders;
    private MutableLiveData<OrderVM>orderDetails;
    private SingleLiveEvent<ManfredNetworkError> netErrorState;
    private final HistoryRepository historyRepository;

    public ManfredHistoryManager(TokenManager tokenManager, HistoryRepository historyRepository) {
        this.tokenManager = tokenManager;
        this.historyRepository=historyRepository;
        orders = new MutableLiveData<>();
        orderDetails = new MutableLiveData<>();
        netErrorState = new SingleLiveEvent<>();
        refreshHistory();
    }

    @Override
    public LiveData<List<OrderVM>> getHistory() {
        return orders;
    }

    @Override
    public void refreshHistory() {
        historyRepository.getHistory(tokenManager.getAuthToken(), new NetworkResponseCallback<HistoryResponce>() {
            @Override
            public void allOk(HistoryResponce response) {
                netErrorState.postValue(null);
                orders.postValue(response.getOrders());
            }

            @Override
            public void error(ManfredNetworkError error) {
                netErrorState.postValue(error);
                orders.postValue(new ArrayList<>());
                Log.d(TAG, "error: "+error.toString());
            }
        });
    }

    @Override
    public void showHistoryDetail(OrderVM orderVM) {
        orderDetails.postValue(orderVM);
    }

    public LiveData<OrderVM> getOrderDetails() {
        return orderDetails;
    }

    public LiveData<ManfredNetworkError> getError() {
        return netErrorState;
    }
}
