package manfred.ru.manfredpassenger.history.managers;

import android.arch.lifecycle.LiveData;

import java.util.List;

import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public interface HistoryManager {
    LiveData<List<OrderVM>> getHistory();
    void refreshHistory();
    void showHistoryDetail(OrderVM orderVM);
}
