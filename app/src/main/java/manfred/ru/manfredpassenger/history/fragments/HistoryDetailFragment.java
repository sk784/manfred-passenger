package manfred.ru.manfredpassenger.history.fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import io.intercom.android.sdk.Intercom;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.databinding.FragmentHistoryDetailBinding;
import manfred.ru.manfredpassenger.history.viewmodel.HistoryVM;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryDetailFragment extends Fragment {
    private static final String TAG = "HistoryDetailFragment";

    public HistoryDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentHistoryDetailBinding binding = FragmentHistoryDetailBinding.inflate(getLayoutInflater());
        HistoryVM historyVM = ViewModelProviders.of(getActivity()).get(HistoryVM.class);
        historyVM.getHistoryDetail().observe(this, new Observer<OrderVM>() {
            @Override
            public void onChanged(@Nullable OrderVM orderVM) {
                binding.setHistoryOrder(orderVM);
                if(orderVM!=null) {
                    Log.d(TAG, "onChanged: showing "+orderVM.toString());
                    Glide
                            .with(HistoryDetailFragment.this)
                            .load(orderVM.getDriverAvatarUrl())
                            .apply(new RequestOptions().centerCrop().circleCrop())
                            .into(binding.ivAvatar);

                    String title = DateFormat.format("dd MMMM ", (orderVM.getOrderStartTime()*1000)) + "в" +
                            DateFormat.format(" HH:mm", orderVM.getOrderStartTime());
                    getActivity().setTitle(title);
                }
            }
        });

        binding.btnHistorySupport.setOnClickListener(v -> Intercom.client().displayMessenger());
        return binding.getRoot();
    }

}
