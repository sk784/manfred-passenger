package manfred.ru.manfredpassenger.history.repositories;

import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.HistoryResponce;

public interface HistoryRepository {
    void getHistory(String token, NetworkResponseCallback<HistoryResponce> callback);
}
