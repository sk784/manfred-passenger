package manfred.ru.manfredpassenger.history.fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;

import java.util.List;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.databinding.FragmentTabbedHistoryBinding;
import manfred.ru.manfredpassenger.databinding.HistoryOrderItemBinding;
import manfred.ru.manfredpassenger.databinding.PreOrderItemBinding;
import manfred.ru.manfredpassenger.history.viewmodel.HistoryVM;
import manfred.ru.manfredpassenger.ride.managers.ManfredPreCalculateCostManager;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabbedHistoryFragment extends Fragment {
    private static final String TAG = "TabbedHistoryFragment";

    public TabbedHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentTabbedHistoryBinding binding = FragmentTabbedHistoryBinding.inflate(getLayoutInflater());
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

        binding.container.setAdapter(mSectionsPagerAdapter);

        binding.container.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabs));
        binding.tabs.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(binding.container));
        HistoryVM historyVM = ViewModelProviders.of(getActivity()).get(HistoryVM.class);
        historyVM.getPreorders().observe(this, new Observer<List<OrderVM>>() {
            @Override
            public void onChanged(@Nullable List<OrderVM> orderVMS) {
                if(orderVMS==null || orderVMS.isEmpty()){
                    TabLayout.Tab tab = binding.tabs.getTabAt(1);
                    Log.d(TAG, "no preOrders, need switch to preOrders");
                    if (tab != null) {
                        tab.select();
                    }
                }
            }
        });
        return binding.getRoot();
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        public static final int PRE_ORDER = 1;
        private static final String TAG = "PlaceholderFragment";
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        //2 - history, 1 - preOrders
        private int fragmentType;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Log.d(TAG, "onCreateView: ");
            View rootView = inflater.inflate(R.layout.fragment_history, container, false);
            HistoryVM historyVM = ViewModelProviders.of(getActivity()).get(HistoryVM.class);
            if(getArguments()!=null && getArguments().containsKey(ARG_SECTION_NUMBER)){
                fragmentType = getArguments().getInt(ARG_SECTION_NUMBER);
            }else {
                throw new IllegalArgumentException("Must be created through newInstance(...)");
            }

            RecyclerView recyclerView = rootView.findViewById(R.id.history_recycler);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mLayoutManager.onSaveInstanceState();
            recyclerView.setLayoutManager(mLayoutManager);

            ProgressBar progressBar = rootView.findViewById(R.id.history_progress);

            OrderViewModelFactory orderViewModelFactory =
                    new OrderViewModelFactory(((ManfredPassengerApplication)getActivity().getApplication()).getManfredOrderManager(),
                    ((ManfredPassengerApplication)getActivity().getApplication()).getPreOrderManager(),
                    ((ManfredPassengerApplication)getActivity().getApplication()).getAppStatusManager(),
                    ((ManfredPassengerApplication)getActivity().getApplication()).getTariffManager(),
                            ((ManfredPassengerApplication)getActivity().getApplication()).getRegionManager(),
                            ((ManfredPassengerApplication)getActivity().getApplication()).getTimeToDriverManager(),
                            new ManfredPreCalculateCostManager(((ManfredPassengerApplication) getActivity().getApplication()).getTokenManager(),
                                    ((ManfredPassengerApplication) getActivity().getApplication()).getNetworkManager()),
                            ((ManfredPassengerApplication) getActivity().getApplication()).getPromoCodeManager(),
                            ((ManfredPassengerApplication) getActivity().getApplication()).getAnalyticsManager());
            OrderViewModel orderViewModel = ViewModelProviders.of(this, orderViewModelFactory).get(OrderViewModel.class);

            historyVM.getErrors().observe(this, new Observer<ManfredNetworkError>() {
                @Override
                public void onChanged(@Nullable ManfredNetworkError manfredNetworkError) {
                    if (manfredNetworkError != null) {
                        Toast.makeText(getContext(),manfredNetworkError.toString(),Toast.LENGTH_LONG).show();
                    }
                }
            });

            if(fragmentType== PRE_ORDER){
                historyVM.getPreorders().observe(this, new Observer<List<OrderVM>>() {
                    @Override
                    public void onChanged(@Nullable List<OrderVM> orderVMS) {
                        progressBar.setVisibility(View.GONE);
                        if (orderVMS != null) {
                            ItemType<PreOrderItemBinding>preOrderItem = new ItemType<PreOrderItemBinding>(R.layout.pre_order_item){
                                @Override
                                public void onCreate(Holder<PreOrderItemBinding> holder) {
                                    super.onCreate(holder);
                                    holder.itemView.setOnClickListener(v -> {
                                        orderViewModel.showPreOrderDetail(holder.getBinding().getPreOrder());
                                        getActivity().finish();
                                    });
                                }
                            };
                            LastAdapter adapter = new LastAdapter(orderVMS, BR.preOrder)
                                    .map(OrderVM.class,preOrderItem)
                                    .into(recyclerView);
                            Log.d(TAG, "we have preOrders: "+orderVMS.size());
                        }
                    }
                });
            }else {
                historyVM.getHistory().observe(this, new Observer<List<OrderVM>>() {
                    @Override
                    public void onChanged(@Nullable List<OrderVM> orderVMS) {
                        //orders.clear();
                        ItemType<HistoryOrderItemBinding>historyOrderItemType = new ItemType<HistoryOrderItemBinding>(R.layout.history_order_item){
                            @Override
                            public void onCreate(Holder<HistoryOrderItemBinding> holder) {
                                super.onCreate(holder);
                                holder.itemView.setOnClickListener(v -> historyVM.showHistoryDetail(holder.getBinding().getHistoryOrder()));
                            }
                        };
                        progressBar.setVisibility(View.GONE);
                        if(orderVMS!=null){
                            LastAdapter adapter = new LastAdapter(orderVMS, BR.historyOrder)
                                    .map(OrderVM.class,historyOrderItemType)
                                    .into(recyclerView);
/*                            Log.d(TAG, "we have history: "+orderVMS.size());
                            OrderVM orderVM = orderVMS.get(0);
                            Log.d(TAG, "first element is: "+orderVM.toString());*/
                        }
                    }
                });
            }
            return rootView;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
