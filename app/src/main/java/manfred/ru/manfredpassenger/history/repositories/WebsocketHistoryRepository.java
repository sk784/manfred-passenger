package manfred.ru.manfredpassenger.history.repositories;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.model.NeedDebt;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.services.models.HistoryResponce;
import manfred.ru.manfredpassenger.api.services.models.RegionAnswer;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.utils.NetworkUtils;

public class WebsocketHistoryRepository implements HistoryRepository {
    private final NetworkManagerProvider networkManager;
    private final TokenManager tokenManager;

    public WebsocketHistoryRepository(NetworkManagerProvider networkManager, TokenManager tokenManager) {
        this.networkManager = networkManager;
        this.tokenManager = tokenManager;
    }

    @Override
    public void getHistory(String token, NetworkResponseCallback<HistoryResponce> callback) {
        NeedDebt needDebt = new NeedDebt(false);
        networkManager.addQuery(tokenManager.getAuthToken(),"order/list", needDebt, new WebSocketAnswerCallback<WebSocketAnswer<HistoryResponce>>() {
            @Override
            public void messageReceived(WebSocketAnswer<HistoryResponce> message) {
                new NetworkUtils().validateSocketResponse(message,callback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                callback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return HistoryResponce.class;
            }
        });
    }
}
