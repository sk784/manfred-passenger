package manfred.ru.manfredpassenger.chats;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.chats.models.Chat;
import manfred.ru.manfredpassenger.databinding.ActivityChatBinding;


public class ChatActivity extends AppCompatActivity {

    ActivityChatBinding binding;

    private static final int IMAGE_GALLERY_REQUEST = 1;

    private LinearLayoutManager mLinearLayoutManager;

    private Chat chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);

        final MessageAdapter messageAdapter = new MessageAdapter();
        messageAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = messageAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    binding.chatRecyclerView.scrollToPosition(positionStart);
                }
            }
        });
        binding.chatRecyclerView.setLayoutManager(mLinearLayoutManager);
        binding.chatRecyclerView.setAdapter(messageAdapter);

        binding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = binding.textSend.getText().toString();
                if(!msg.equals("")){
                    Toast.makeText(ChatActivity.this, "Сообщение отправлено!",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChatActivity.this, "Вы не можете отправить пустое сообщение!",Toast.LENGTH_SHORT).show();
                }
                binding.textSend.setText("");
            }
        });

        binding.btnPhotoSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoGalleryIntent();
            }
        });

        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
                    // send file
                    Log.d("sk7847",selectedImageUri.toString());
                }
            }
        }
    }


    private void photoGalleryIntent(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "GET PHOTO FROM"), IMAGE_GALLERY_REQUEST);
    }

}

