package manfred.ru.manfredpassenger.chats.models;

public class Chat {

    private String id;
    private String userId;
    private String userName;
    private String message;
    private FileModel file;


    public Chat() {
    }

    public Chat(String userName, String message, FileModel file) {
        this.userName = userName;
        this.message = message;
        this.file = file;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FileModel getFile() {
        return file;
    }

    public void setFile(FileModel file) {
        this.file = file;
    }
}

