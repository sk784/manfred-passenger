package manfred.ru.manfredpassenger.chats;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.chats.models.Chat;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    public static final int MSG_TYPE_LEFT = 0;
    public static final int MSG_TYPE_RIGHT = 1;
    public static final int MSG_TYPE_RIGHT_IMG = 2;

    private String nameUser;

    private List<Chat> chat = new ArrayList<>();

    public MessageAdapter() {

    }


    @NonNull
    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MSG_TYPE_LEFT) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_left, parent, false);
            return new ViewHolder(view);
        } else if (viewType == MSG_TYPE_RIGHT) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_right, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_right_img, parent, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.ViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {

        if (chat.get(position).getFile() != null) {
            if (chat.get(position).getFile().getType().equals("img") && chat.get(position).getUserName().equals(nameUser)) {
                return MSG_TYPE_RIGHT_IMG;
            } else {
                return 0;
            }
        } else if (chat.get(position).getUserName().equals(nameUser)) {
            return MSG_TYPE_RIGHT;
        } else {
            return MSG_TYPE_LEFT;
        }
    }

    @Override
    public int getItemCount() {
        return chat == null ? 0 : chat.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView show_message;
        public ImageView show_img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            show_message = itemView.findViewById(R.id.show_message);
            show_img = itemView.findViewById(R.id.show_img);
        }


        void onBind(int position) {
            show_message.setText(chat.get(position).getMessage());

            if (chat.get(position).getFile() != null) {
                Glide.with(show_img.getContext()).load(chat.get(position).getFile().getUrl_file())
                        .into(show_img);
            }
        }
    }
}
