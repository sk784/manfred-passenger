package manfred.ru.manfredpassenger.api.services;

import android.support.annotation.Keep;

import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.services.models.CardIdRequest;
import manfred.ru.manfredpassenger.api.services.models.Cards;
import manfred.ru.manfredpassenger.api.services.models.CreditCardForServer;
import manfred.ru.manfredpassenger.api.services.models.HistoryResponce;
import manfred.ru.manfredpassenger.api.services.models.ManfredPassengerData;
import manfred.ru.manfredpassenger.api.services.models.Phone;
import manfred.ru.manfredpassenger.api.services.models.PushTokenRequest;
import manfred.ru.manfredpassenger.api.services.models.SmsAuthorisation;
import manfred.ru.manfredpassenger.api.services.models.UserName;
import manfred.ru.manfredpassenger.api.services.models.event.RestoreResponse;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.promocodes.models.ReferalAnswer;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
@Keep
public interface UserService {
    @POST("v1.0/passenger/user/register_phone/")
    Call<ManfredResponse> registerPhone(@Body Phone data);

    @POST("v1.0/passenger/user/change_phone/")
    Call<ManfredResponse<ManfredPassengerData>> changePhoneNumber(@Query("token") String token, @Body Phone phone);

    @POST("v1.0/passenger/user/sms_code/")
    Call<ManfredResponse<ManfredPassengerData>> sendAuthorisationSms(@Body SmsAuthorisation smsAuthorisation);

    @POST("v1.0/passenger/user/register_profile/")
    Call<ManfredResponse> enterName(@Query("token") String token, @Body UserName userName);

    @GET("v1.0/passenger/user/cards/")
    Call<ManfredResponse<Cards>> getCards(@Query("token") String token);

    @POST("v1.0/passenger/user/add_card/")
    Call<ManfredResponse<CreditCard>> addCard(@Query("token") String token, @Body CreditCardForServer creditCardForServer);

    @GET("v1.0/passenger/user/state/")
    Call<ManfredResponse<RestoreResponse>> getState(@Query("token") String token);

    @POST("v1.0/passenger/user/delete_card/")
    Call<ManfredResponse> removeCreditCard(@Query("token") String token, @Body CardIdRequest cardIdRequest);

    @GET("v1.0/passenger/order/list/")
    Call<ManfredResponse<HistoryResponce>>getHistory(@Query("token") String token);

    @GET("v1.0/passenger/order/list/")
    Call<ManfredResponse<HistoryResponce>>getHistoryWithDebt(@Query("token") String token, @Query("have_debt") boolean needDebt);

    @POST("v1.0/passenger/user/set_active_card/")
    Call<ManfredResponse<CreditCard>> selectCard(@Query("token") String token, @Body CardIdRequest cardIdRequest);

    @GET("v1.0/passenger/user/referal/")
    Call<ManfredResponse<ReferalAnswer>> getMyReferral(@Query("token") String token);

    @POST("v1.0/passenger/user/set_push_token/")
    Call<ManfredResponse>setPushToken(@Query("token") String token,@Body PushTokenRequest cardIdRequest);

}
