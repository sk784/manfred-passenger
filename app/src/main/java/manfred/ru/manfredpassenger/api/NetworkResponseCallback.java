package manfred.ru.manfredpassenger.api;

public interface NetworkResponseCallback<T> {
    void allOk(T response);

    void error(ManfredNetworkError error);
}
