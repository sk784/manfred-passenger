package manfred.ru.manfredpassenger.api.services.models.event;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Tariff {

    @SerializedName("ride_to_passenger_price")
    private String rideToPassengerPrice;

    @SerializedName("icon")
    private String icon;

    @SerializedName("title")
    private String title;

    @SerializedName("cancel_ride_price_assistant")
    private String cancelRidePriceAssistant;

    @SerializedName("car_type")
    private CarType carType;

    @SerializedName("ride_km_price_out_mkad")
    private String rideKmPriceOutMkad;

    @SerializedName("car_image")
    private String carImage;

    @SerializedName("avance_minutes")
    private int avanceMinutes;

    @SerializedName("assistant_price")
    private String assistantPrice;

    @SerializedName("ride_km_price")
    private String rideKmPrice;

    @SerializedName("modified")
    private String modified;

    @SerializedName("prepayment_price")
    private String prepaymentPrice;

    @SerializedName("free_cancel_time")
    private int freeCancelTime;

    @SerializedName("allow_assistant")
    private boolean isAssistantAllowed;

    @SerializedName("id")
    private int id;

    @SerializedName("prepayment_price_assistant")
    private String prepaymentPriceAssistant;

    @SerializedName("icon_map_name")
    private String iconMapName;

    @SerializedName("ride_to_passenger_km_price_out_mkad")
    private String rideToPassengerKmPriceOutMkad;

    @SerializedName("airport_prepayment_price_assistant")
    private String airportPrepaymentPriceAssistant;

    @SerializedName("min_preorder_time_assistant")
    private int minPreorderTimeAssistant;

    @SerializedName("ride_minute_price")
    private String rideMinutePrice;

    @SerializedName("ride_km_price_assistant")
    private String rideKmPriceAssistant;

    @SerializedName("airport_prepayment_price")
    private String airportPrepaymentPrice;

    @SerializedName("min_preorder_time")
    private int minPreorderTime;

    @SerializedName("ride_to_passenger_price_assistant")
    private String rideToPassengerPriceAssistant;

    @SerializedName("cancel_ride_price")
    private String cancelRidePrice;

    @SerializedName("ride_km_price_out_mkad_assistant")
    private String rideKmPriceOutMkadAssistant;

    @SerializedName("ride_to_passenger_km_price_out_mkad_assistant")
    private String rideToPassengerKmPriceOutMkadAssistant;

    @SerializedName("free_wait_time")
    private int freeWaitTime;

    @SerializedName("icon_pressed")
    private String iconPressed;

    @SerializedName("ride_minute_price_assistant")
    private String rideMinutePriceAssistant;

    @SerializedName("formatted_description")
    private String formattedDescription;

    public String getFormattedDescription() {
        return formattedDescription;
    }

    public String getFormattedDescriptionAssistent() {
        return formattedDescriptionAssistent;
    }

    @SerializedName("formatted_description_assist")
    private String formattedDescriptionAssistent;

    @SerializedName("desc")
    private String desc;

    @SerializedName("max_baggage")
    private int maximumBaggage;

    @SerializedName("max_passengers")
    private int maximumPassengers;

    @SerializedName("currency_symbol")
    private String currencySymbol;

    public boolean isAssistantAllowed() {
        return isAssistantAllowed;
    }

    public void setAssistantAllowed(boolean assistantAllowed) {
        isAssistantAllowed = assistantAllowed;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public void setRideToPassengerPrice(String rideToPassengerPrice) {
        this.rideToPassengerPrice = rideToPassengerPrice;
    }

    public String getRideToPassengerPrice() {
        return rideToPassengerPrice;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return capitalizeFirstLetter(title);
    }

    private String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1).toLowerCase();
    }

    public void setCancelRidePriceAssistant(String cancelRidePriceAssistant) {
        this.cancelRidePriceAssistant = cancelRidePriceAssistant;
    }

    public String getCancelRidePriceAssistant() {
        return cancelRidePriceAssistant;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setRideKmPriceOutMkad(String rideKmPriceOutMkad) {
        this.rideKmPriceOutMkad = rideKmPriceOutMkad;
    }

    public String getRideKmPriceOutMkad() {
        return rideKmPriceOutMkad;
    }

    public void setCarImage(String carImage) {
        this.carImage = carImage;
    }

    public String getCarImage() {
        return carImage;
    }

    public void setAvanceMinutes(int avanceMinutes) {
        this.avanceMinutes = avanceMinutes;
    }

    public int getAvanceMinutes() {
        return avanceMinutes;
    }

    public void setAssistantPrice(String assistantPrice) {
        this.assistantPrice = assistantPrice;
    }

    public String getAssistantPrice() {
        return assistantPrice;
    }

    public void setRideKmPrice(String rideKmPrice) {
        this.rideKmPrice = rideKmPrice;
    }

    public String getRideKmPrice() {
        return rideKmPrice;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getModified() {
        return modified;
    }

    public void setPrepaymentPrice(String prepaymentPrice) {
        this.prepaymentPrice = prepaymentPrice;
    }

    public String getPrepaymentPrice() {
        return prepaymentPrice;
    }

    public void setFreeCancelTime(int freeCancelTime) {
        this.freeCancelTime = freeCancelTime;
    }

    public int getFreeCancelTime() {
        return freeCancelTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setPrepaymentPriceAssistant(String prepaymentPriceAssistant) {
        this.prepaymentPriceAssistant = prepaymentPriceAssistant;
    }

    public String getPrepaymentPriceAssistant() {
        return prepaymentPriceAssistant;
    }

    public void setIconMapName(String iconMapName) {
        this.iconMapName = iconMapName;
    }

    @Nullable public String getIconMapName() {
        return iconMapName;
    }

    public void setRideToPassengerKmPriceOutMkad(String rideToPassengerKmPriceOutMkad) {
        this.rideToPassengerKmPriceOutMkad = rideToPassengerKmPriceOutMkad;
    }

    public String getRideToPassengerKmPriceOutMkad() {
        return rideToPassengerKmPriceOutMkad;
    }

    public void setAirportPrepaymentPriceAssistant(String airportPrepaymentPriceAssistant) {
        this.airportPrepaymentPriceAssistant = airportPrepaymentPriceAssistant;
    }

    public String getAirportPrepaymentPriceAssistant() {
        return airportPrepaymentPriceAssistant;
    }

    public void setMinPreorderTimeAssistant(int minPreorderTimeAssistant) {
        this.minPreorderTimeAssistant = minPreorderTimeAssistant;
    }

    public int getMinPreorderTimeAssistant() {
        return minPreorderTimeAssistant;
    }

    public void setRideMinutePrice(String rideMinutePrice) {
        this.rideMinutePrice = rideMinutePrice;
    }

    public String getRideMinutePrice() {
        return rideMinutePrice;
    }

    public void setRideKmPriceAssistant(String rideKmPriceAssistant) {
        this.rideKmPriceAssistant = rideKmPriceAssistant;
    }

    public String getRideKmPriceAssistant() {
        return rideKmPriceAssistant;
    }

    public void setAirportPrepaymentPrice(String airportPrepaymentPrice) {
        this.airportPrepaymentPrice = airportPrepaymentPrice;
    }

    public String getAirportPrepaymentPrice() {
        return airportPrepaymentPrice;
    }

    public void setMinPreorderTime(int minPreorderTime) {
        this.minPreorderTime = minPreorderTime;
    }

    public int getMinPreorderTime() {
        return minPreorderTime;
    }

    public void setRideToPassengerPriceAssistant(String rideToPassengerPriceAssistant) {
        this.rideToPassengerPriceAssistant = rideToPassengerPriceAssistant;
    }

    public String getRideToPassengerPriceAssistant() {
        return rideToPassengerPriceAssistant;
    }

    public void setCancelRidePrice(String cancelRidePrice) {
        this.cancelRidePrice = cancelRidePrice;
    }

    public String getCancelRidePrice() {
        return cancelRidePrice;
    }

    public void setRideKmPriceOutMkadAssistant(String rideKmPriceOutMkadAssistant) {
        this.rideKmPriceOutMkadAssistant = rideKmPriceOutMkadAssistant;
    }

    public String getRideKmPriceOutMkadAssistant() {
        return rideKmPriceOutMkadAssistant;
    }

    public void setRideToPassengerKmPriceOutMkadAssistant(String rideToPassengerKmPriceOutMkadAssistant) {
        this.rideToPassengerKmPriceOutMkadAssistant = rideToPassengerKmPriceOutMkadAssistant;
    }

    public String getRideToPassengerKmPriceOutMkadAssistant() {
        return rideToPassengerKmPriceOutMkadAssistant;
    }

    public void setFreeWaitTime(int freeWaitTime) {
        this.freeWaitTime = freeWaitTime;
    }

    public int getFreeWaitTime() {
        return freeWaitTime;
    }

    public void setIconPressed(String iconPressed) {
        this.iconPressed = iconPressed;
    }

    public String getIconPressed() {
        return iconPressed;
    }

    public void setRideMinutePriceAssistant(String rideMinutePriceAssistant) {
        this.rideMinutePriceAssistant = rideMinutePriceAssistant;
    }

    public String getRideMinutePriceAssistant() {
        return rideMinutePriceAssistant;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public int getMaximumBaggage() {
        return maximumBaggage;
    }

    public int getMaximumPassengers() {
        return maximumPassengers;
    }

    @Override
    public String toString() {
        return
                "Tariff{" +
                        "ride_to_passenger_price = '" + rideToPassengerPrice + '\'' +
                        ",icon = '" + icon + '\'' +
                        ",title = '" + title + '\'' +
                        ",cancel_ride_price_assistant = '" + cancelRidePriceAssistant + '\'' +
                        ",car_type = '" + carType + '\'' +
                        ",ride_km_price_out_mkad = '" + rideKmPriceOutMkad + '\'' +
                        ",car_image = '" + carImage + '\'' +
                        ",avance_minutes = '" + avanceMinutes + '\'' +
                        ",assistant_price = '" + assistantPrice + '\'' +
                        ",ride_km_price = '" + rideKmPrice + '\'' +
                        ",modified = '" + modified + '\'' +
                        ",prepayment_price = '" + prepaymentPrice + '\'' +
                        ",free_cancel_time = '" + freeCancelTime + '\'' +
                        ",id = '" + id + '\'' +
                        ",prepayment_price_assistant = '" + prepaymentPriceAssistant + '\'' +
                        ",icon_map_name = '" + iconMapName + '\'' +
                        ",ride_to_passenger_km_price_out_mkad = '" + rideToPassengerKmPriceOutMkad + '\'' +
                        ",airport_prepayment_price_assistant = '" + airportPrepaymentPriceAssistant + '\'' +
                        ",min_preorder_time_assistant = '" + minPreorderTimeAssistant + '\'' +
                        ",ride_minute_price = '" + rideMinutePrice + '\'' +
                        ",ride_km_price_assistant = '" + rideKmPriceAssistant + '\'' +
                        ",airport_prepayment_price = '" + airportPrepaymentPrice + '\'' +
                        ",min_preorder_time = '" + minPreorderTime + '\'' +
                        ",ride_to_passenger_price_assistant = '" + rideToPassengerPriceAssistant + '\'' +
                        ",cancel_ride_price = '" + cancelRidePrice + '\'' +
                        ",ride_km_price_out_mkad_assistant = '" + rideKmPriceOutMkadAssistant + '\'' +
                        ",ride_to_passenger_km_price_out_mkad_assistant = '" + rideToPassengerKmPriceOutMkadAssistant + '\'' +
                        ",free_wait_time = '" + freeWaitTime + '\'' +
                        ",icon_pressed = '" + iconPressed + '\'' +
                        ",ride_minute_price_assistant = '" + rideMinutePriceAssistant + '\'' +
                        ",desc = '" + desc + '\'' +
                        "}";
    }
}