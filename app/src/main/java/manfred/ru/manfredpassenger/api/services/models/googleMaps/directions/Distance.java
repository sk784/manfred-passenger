package manfred.ru.manfredpassenger.api.services.models.googleMaps.directions;

/**
 * Created by begemot on 11.12.17.
 */

public class Distance {
    //in meters
    private int value;
    private String text;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
