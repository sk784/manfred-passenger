package manfred.ru.manfredpassenger.api.services.models;

public class CardIdRequest {
    private final int id;

    public CardIdRequest(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
