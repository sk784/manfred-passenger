package manfred.ru.manfredpassenger.api.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import manfred.ru.manfredpassenger.BuildConfig;
import manfred.ru.manfredpassenger.api.model.EventProcessedAnswer;
import manfred.ru.manfredpassenger.api.model.ManfredEventNetworkReceiver;
import manfred.ru.manfredpassenger.api.model.ManfredNetworkReceiver;
import manfred.ru.manfredpassenger.api.model.NetworkQuery;
import manfred.ru.manfredpassenger.api.model.NetworkReceiver;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.model.WebSocketRequest;
import manfred.ru.manfredpassenger.api.websocket.OKHttpWebSocketClient;

public class ManfredNetworkManager implements NetworkManager {
    private static final int TIMEOUT_OF_REQUEST_IN_SECONDS = 30;
    private List<NetworkReceiver>receivers = new ArrayList<>();
    private ManfredEventNetworkReceiver eventNetworkReceiver;
    private final OKHttpWebSocketClient webSocketClient;
    private MutableLiveData<Boolean> isConnected=new MutableLiveData<>();
    private static final String TAG = "ManfredNetworkManager";
    private LinkedBlockingQueue<Runnable> processingEventQueue = new LinkedBlockingQueue<>();
    private ThreadPoolExecutor eventExecutor = new ThreadPoolExecutor(1, 1,
            0L, TimeUnit.MILLISECONDS, processingEventQueue);

    public ManfredNetworkManager() {
        Log.d(TAG, "ManfredNetworkManager: creating");
        OKHttpWebSocketClient.MessageListener messageListener = createMessageListener();
        webSocketClient = new OKHttpWebSocketClient(messageListener,BuildConfig.SOCKET_URL);
        webSocketClient.connect();
        webSocketClient.setSocketConnectionListener(createSocketConnectionListener());
        final ScheduledExecutorService timeOutChecker = Executors.newScheduledThreadPool(1);
        timeOutChecker.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if(receivers.isEmpty())return;
                //Log.d(TAG, "run: checking for timed out request");
                for (NetworkReceiver networkReceiver : new ArrayList<>(receivers)){
                    if(networkReceiver instanceof ManfredNetworkReceiver){
                        Date dateOfRequest = ((ManfredNetworkReceiver) networkReceiver).getDateOfSendingRequest();
                        if(Math.abs(new Date().getTime()-dateOfRequest.getTime())/1000> TIMEOUT_OF_REQUEST_IN_SECONDS){
                            networkReceiver.errorWithReceive("превышено время ожидания ответа");
                            receivers.remove(networkReceiver);
                            Log.d(TAG, "run: "+networkReceiver.getTypeOfRequest()
                                    +" with date of creation "+dateOfRequest+" is timed out");
                        }
                    }
                }
            }
        },30,30,TimeUnit.SECONDS);
    }

    @NonNull
    private OKHttpWebSocketClient.SocketConnectionListener createSocketConnectionListener() {
        return new OKHttpWebSocketClient.SocketConnectionListener() {
            @Override
            public void onConnectionChange(boolean isConnect) {
                if(isConnected.getValue()!=null) {
                    Log.d(TAG, "onConnectionChange: "+isConnect);
                    if (isConnect != isConnected.getValue()) {
                        isConnected.postValue(isConnect);
                        }
                    if(!isConnect){
                        sendAllReceiversLinkToServerAborted();
                    }
                }
            }
        };
    }

    @NonNull
    private OKHttpWebSocketClient.MessageListener createMessageListener() {
        return new OKHttpWebSocketClient.MessageListener() {
            @Override
            public void onSocketMessage(String answer) {
                //Log.d(TAG, "onSocketMessage: "+answer);
                //Log.d(TAG, "onSocketMessage: in thread "+Thread.currentThread().getId());
                //Log.d(TAG, "onSocketMessage: size of queue now "+processingEventQueue.size()+" adding new task to queue");
                eventExecutor.submit(new Runnable() {
                    @Override
                    public void run() {
                        //Log.d(TAG, "onSocketMessage: processing answer in thread"+Thread.currentThread().getId());
                        processMessage(answer);
                    }
                });
            }
        };
    }

    private void sendAllReceiversLinkToServerAborted() {
        for (NetworkReceiver networkReceiver : new ArrayList<>(receivers)){
            networkReceiver.errorWithReceive("Связь с сервером не установлена");
            if(networkReceiver instanceof ManfredNetworkReceiver){
                receivers.remove(networkReceiver);
            }
        }

    }

    public LiveData<Boolean> getIsConnected() {
        return isConnected;
    }

    private void processMessage(String answer) {
        Gson gson = new GsonBuilder().create();
        WebSocketAnswer socketAnswer = gson.fromJson(answer,WebSocketAnswer.class);
        if(socketAnswer.getType().equals("new_event")){
            //Log.d(TAG, "processMessage: it is new event "+socketAnswer.getId()+"event receiver is "+eventNetworkReceiver);
            eventNetworkReceiver.receiveResponse(answer);
        }else {
            //Log.d(TAG, "processMessage: processing "+socketAnswer.getType()+" with id "+socketAnswer.getId()+", have "+receivers);
            for (NetworkReceiver networkReceiver : new ArrayList<>(receivers)) {
            /*
            Log.d(TAG, "onSocketMessage: we try receiver "+networkReceiver.getTypeOfRequest()+"for message "+socketAnswer.getId()+
                    " with type "+socketAnswer.getType());*/
                if (networkReceiver.getTypeOfRequest().equals(socketAnswer.getType())) {
                    noticeReceiver(answer, networkReceiver);
                    //need remove receiver if it single-shot request
                    removeReceiver(socketAnswer, networkReceiver);
                    break;
                }
            }
        }
    }

    private void noticeReceiver(String answer, NetworkReceiver networkReceiver) {
        //Log.d(TAG, "onSocketMessage: sending to "+networkReceiver.getTypeOfRequest());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                networkReceiver.receiveResponse(answer);
            }
        });
    }

    private void removeReceiver(WebSocketAnswer socketAnswer, NetworkReceiver networkReceiver) {
        if(networkReceiver instanceof ManfredNetworkReceiver) {
            Long requestId = ((ManfredNetworkReceiver)networkReceiver).getIdOfRequest();
            if (requestId != null) {
                if (requestId == socketAnswer.getIdOfRequest()) {
/*                    Log.d(TAG, "removeReceiver: >-------"+Thread.currentThread().getId());
                    Log.d(TAG, "removeReceiver: we have receivers "+receivers);
                    Log.d(TAG, "removeReceiver: id of answer " + socketAnswer.getIdOfRequest() +
                     " , answer id is " + requestId + ", removing listener "+networkReceiver);*/
                    receivers.remove(networkReceiver);
                }
            }
        }
/*        Log.d(TAG, "removeReceiver: after removing receivers is "+receivers);
        Log.d(TAG, "removeReceiver: ------->");*/
    }

    private void runOnUiThread(Runnable action) {
        new Handler(Looper.getMainLooper()).post(action);
    }

    @Override
    public void sendRequest(WebSocketRequest request) {
        //Log.d(TAG, "sendRequest: "+request);
        webSocketClient.sendRequest(request);
    }

    @Override
    public void sendEventAcceptedRequest(EventProcessedAnswer eventProcessedAnswer) {
        //Log.d(TAG, "sendEventAcceptedRequest: "+eventProcessedAnswer.toString());
        webSocketClient.sendEventReceived(eventProcessedAnswer);
    }

    @Override
    public void addRequestReceiver(NetworkReceiver networkReceiver) {
        receivers.add((ManfredNetworkReceiver) networkReceiver);
        //Log.d(TAG, "addRequestReceiver: "+networkReceiver+", receivers is "+receivers);
    }

    @Override
    public void addEventReceiver(ManfredEventNetworkReceiver eventNetworkReceiver) {
        //Log.d(TAG, "addEventReceiver: "+eventNetworkReceiver);
        this.eventNetworkReceiver=eventNetworkReceiver;
    }

    @Override
    public void addQuery(NetworkQuery networkQuery) {
        NetworkReceiver addingReceiver = networkQuery.getNetworkReceiver();
        ((ManfredNetworkReceiver) addingReceiver).setDateOfSendingRequest(new Date());
        receivers.add((ManfredNetworkReceiver)addingReceiver);
        sendRequest(networkQuery.getWebSocketRequest());
    }

    @Override
    public <V>void addQuery(@Nullable String token, String path, WebSocketAnswerCallback callback) {
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<Void,V>(path)
                .setCallback(callback,callback.getAnswerClass())
                .setToken(token)
                .buildOneRequest();
        NetworkReceiver addingReceiver = networkQuery.getNetworkReceiver();
        ((ManfredNetworkReceiver) addingReceiver).setDateOfSendingRequest(new Date());
        receivers.add((ManfredNetworkReceiver)addingReceiver);
        sendRequest(networkQuery.getWebSocketRequest());
    }

    @Override
    public <T, V> void addQuery(@Nullable String token, String path, @NonNull T payload, WebSocketAnswerCallback callback) {
        NetworkQuery networkQuery = new NetworkQuery.OneRequestBuilder<T,V>(path)
                .setCallback(callback,callback.getAnswerClass())
                .setDataForTransmit(payload)
                .setToken(token)
                .buildOneRequest();
        NetworkReceiver addingReceiver = networkQuery.getNetworkReceiver();
        ((ManfredNetworkReceiver) addingReceiver).setDateOfSendingRequest(new Date());
        receivers.add((ManfredNetworkReceiver)addingReceiver);
        sendRequest(networkQuery.getWebSocketRequest());
    }

    @Override
    public void removeRequestReceiver(NetworkReceiver networkReceiver) {
        Log.d(TAG, "removeRequestReceiver: "+networkReceiver);
        receivers.remove(networkReceiver);
    }

    @Override
    public void internetOff() {
        sendAllReceiversLinkToServerAborted();
        webSocketClient.inetOff();
    }

    @Override
    public void internetOn() {
        webSocketClient.inetOn();
    }

    @Override
    public void disconnect() {
        Log.d(TAG, "disconnect: ");
        webSocketClient.close();
    }
}
