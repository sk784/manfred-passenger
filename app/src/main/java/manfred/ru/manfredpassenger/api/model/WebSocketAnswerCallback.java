package manfred.ru.manfredpassenger.api.model;

import android.support.annotation.Nullable;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;

public interface WebSocketAnswerCallback<V extends WebSocketAnswer>{
    void messageReceived(V message);
    void error(ManfredNetworkError manfredError);
    @Nullable Class getAnswerClass();
}