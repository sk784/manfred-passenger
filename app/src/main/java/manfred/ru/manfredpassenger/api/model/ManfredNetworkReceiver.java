package manfred.ru.manfredpassenger.api.model;

import android.support.annotation.Nullable;

import com.google.gson.Gson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;

public class ManfredNetworkReceiver<T> implements NetworkReceiver {
    private static final String TAG = "ManfredNetworkReceiver";
    private WebSocketAnswerCallback<WebSocketAnswer<T>> webSocketAnswerCallback;
    @Nullable private final Class<T> payloadType;
    private String typeOfRequest;
    @Nullable private final Long idOfRequest;

    private Date dateOfSendingRequest;

    @Nullable
    public Long getIdOfRequest() {
        return idOfRequest;
    }

    public ManfredNetworkReceiver(WebSocketAnswerCallback<WebSocketAnswer<T>> webSocketAnswerCallback,
                                  String typeOfRequest,
                                  @Nullable Class<T> payloadType,
                                  @Nullable Long idOfRequest) {
        this.webSocketAnswerCallback = webSocketAnswerCallback;
        this.payloadType = payloadType;
        this.typeOfRequest = typeOfRequest;
        this.idOfRequest = idOfRequest;
    }

    public ManfredNetworkReceiver(WebSocketAnswerCallback<WebSocketAnswer<T>> webSocketAnswerCallback,
                                  String typeOfRequest, @Nullable Class<T> payloadType) {
        this.webSocketAnswerCallback = webSocketAnswerCallback;
        this.typeOfRequest = typeOfRequest;
        this.payloadType=payloadType;
        this.idOfRequest=null;
    }

    public void setWebSocketAnswerCallback(WebSocketAnswerCallback<WebSocketAnswer<T>> webSocketAnswerCallback) {
        this.webSocketAnswerCallback = webSocketAnswerCallback;
    }

    @Override
    public String getTypeOfRequest(){
        return typeOfRequest;
    }

    public void setTypeOfRequest(String typeOfRequest) {
        this.typeOfRequest = typeOfRequest;
    }

    @Override
    public void receiveResponse(String response) {
        Gson gson = new Gson();
        if(payloadType!=null) {
            //Log.d(TAG, "receiveResponse: payloadType is "+payloadType.getName()+", request is "+typeOfRequest);
            WebSocketAnswer<T> answer = gson.fromJson(response, getType(WebSocketAnswer.class, payloadType));
            //Log.d(TAG, "receiveResponse: answer is "+answer.toString());
            webSocketAnswerCallback.messageReceived(answer);
        }else {
            WebSocketAnswer answer = gson.fromJson(response,WebSocketAnswer.class);
            //Log.d(TAG, "receiveResponse: answer is "+answer.toString());
            webSocketAnswerCallback.messageReceived(answer);
        }
    }

    public Date getDateOfSendingRequest() {
        return dateOfSendingRequest;
    }

    public void setDateOfSendingRequest(Date dateOfSendingRequest) {
        this.dateOfSendingRequest = dateOfSendingRequest;
    }

    @Override
    public void errorWithReceive(String message) {
        webSocketAnswerCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, message));
    }
    private Type getType(final Class<?> rawClass, final Class<?> parameterClass) {
        return new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[]{parameterClass};
            }

            @Override
            public Type getRawType() {
                return rawClass;
            }

            @Override
            public Type getOwnerType() {
                return null;
            }

        };
    }

    @Override
    public String toString() {
        return "ManfredNetworkReceiver{" +
                typeOfRequest +
                '}';
    }
}
