package manfred.ru.manfredpassenger.api.services.models;

import com.google.gson.annotations.SerializedName;

public class AddCardResponse {

    @SerializedName("card_number")
    private final String cardNumber;

    @SerializedName("active")
    private final boolean active;

    @SerializedName("id")
    private final int id;

    @SerializedName("card_type")
    private final String cardType;

    public AddCardResponse(String cardNumber, boolean active, int id, String cardType) {
        this.cardNumber = cardNumber;
        this.active = active;
        this.id = id;
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public boolean isActive() {
        return active;
    }

    public int getId() {
        return id;
    }

    public String getCardType() {
        return cardType;
    }
}