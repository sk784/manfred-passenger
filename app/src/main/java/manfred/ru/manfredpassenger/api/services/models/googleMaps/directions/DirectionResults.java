package manfred.ru.manfredpassenger.api.services.models.googleMaps.directions;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by begemot on 18.07.17.
 * https://stackoverflow.com/questions/30163699/google-map-direction-api-using-retrofit
 */

public class DirectionResults {
    @SerializedName("routes")
    private List<Route> routes;

    public List<Route> getRoutes() {
        return routes;
    }
}

