package manfred.ru.manfredpassenger.api.services.models;

import com.google.gson.annotations.SerializedName;

public class CoordForAverageTime {

    @SerializedName("tariff_id")
    private final String tariffId;

    @SerializedName("location")
    private final LatLngLocation latLngLocation;

    public CoordForAverageTime(String tariffId, LatLngLocation latLngLocation) {
        this.tariffId = tariffId;
        this.latLngLocation = latLngLocation;
    }

    public String getTariffId() {
        return tariffId;
    }

    public LatLngLocation getLatLngLocation() {
        return latLngLocation;
    }

    @Override
    public String toString() {
        return "CoordForAverageTime{" +
                "tariffId='" + tariffId + '\'' +
                ", latLngLocation=" + latLngLocation +
                '}';
    }
}