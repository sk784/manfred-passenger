package manfred.ru.manfredpassenger.api.services.models;

public class TariffChangedResponce {
    private boolean modified;

    public TariffChangedResponce(boolean modified) {
        this.modified = modified;
    }

    public boolean isModified() {
        return modified;
    }
}
