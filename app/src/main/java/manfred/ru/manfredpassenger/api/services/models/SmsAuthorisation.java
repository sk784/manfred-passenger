package manfred.ru.manfredpassenger.api.services.models;

public class SmsAuthorisation {
    private final String phone;
    private final String sms_code;
    private final String country_code;

    public SmsAuthorisation(String phone, String sms_code, String country_code) {
        this.phone = phone;
        this.sms_code = sms_code;
        this.country_code = country_code;
    }

    public String getPhone() {
        return phone;
    }

    public String getSms_code() {
        return sms_code;
    }

    public String getCountry_code() {
        return country_code;
    }
}
