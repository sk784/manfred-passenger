package manfred.ru.manfredpassenger.api.services.models;

import android.location.Location;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class LatLngLocation {
    private static final String TAG = "LatLngLocation";

    @SerializedName("latitude")
    private final String latitude;

    @SerializedName("longitude")
    private final String longitude;

    public LatLngLocation(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "LatLngLocation{" +
                "latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }

    public LatLngLocation(Location location) {
        this.latitude = String.valueOf(location.getLatitude());
        this.longitude = String.valueOf(location.getLongitude());
        Log.d(TAG, "LatLngLocation: create from "+location+", we have "+toString());
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}