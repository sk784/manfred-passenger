package manfred.ru.manfredpassenger.api.token;

public interface TokenManager {
    void setAuthToken(String token);
    String getAuthToken();
}
