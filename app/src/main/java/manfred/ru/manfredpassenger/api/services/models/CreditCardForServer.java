package manfred.ru.manfredpassenger.api.services.models;

import manfred.ru.manfredpassenger.cards.managers.CreditCard;

public class CreditCardForServer {
    private final String card_number;
    private final String card_token;
    private final int card_type;

    public CreditCardForServer(CreditCard creditCard, String cardToken) {
        this.card_number = creditCard.getCardNumber();
        this.card_token = cardToken;
        card_type = creditCard.getCardType();
    }

    public CreditCardForServer(String card_number, String card_token, int card_type) {
        this.card_number = card_number;
        this.card_token = card_token;
        this.card_type = card_type;
    }

    public String getCard_number() {
        return card_number;
    }

    public String getCard_token() {
        return card_token;
    }

    public int getCard_type() {
        return card_type;
    }
}
