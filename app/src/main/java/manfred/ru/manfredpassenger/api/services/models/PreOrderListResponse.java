package manfred.ru.manfredpassenger.api.services.models;

import java.util.List;

import manfred.ru.manfredpassenger.api.services.models.event.Event;

public class PreOrderListResponse {
    private final List<Event>orders;

    public PreOrderListResponse(List<Event> orders) {
        this.orders = orders;
    }

    public List<Event> getOrders() {
        return orders;
    }
}
