package manfred.ru.manfredpassenger.api.services.models;

public class Phone {
    private String country_code;
    private final String phone;

    public Phone(String country_code, String phone) {
        this.country_code = country_code.replaceAll("[^0-9]+", "");
        this.phone = phone;
    }

    public String getCountry_code() {
        return country_code;
    }

    public String getPhone() {
        return phone.replaceAll("[^0-9]+", "");
    }

    public String getFullNumber() {
        return country_code + phone;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }
}
