package manfred.ru.manfredpassenger.api.model;

import android.util.Log;

import com.google.gson.Gson;

public class ManfredEventNetworkReceiver implements NetworkReceiver {
    private static final String TAG = "ManfredEventNetworkRece";
    public interface NewEventCallback{
        void newEvent(NewEventAnswer event);
    }
    private NewEventCallback newEventCallback;

    public ManfredEventNetworkReceiver(NewEventCallback newEventCallback) {
        this.newEventCallback = newEventCallback;
    }

    @Override
    public String getTypeOfRequest() {
        return "new_event";
    }

    @Override
    public void receiveResponse(String response) {
        //Log.d(TAG, "receiveResponse: ");
        Gson gson = new Gson();
        NewEventAnswer event = gson.fromJson(response,NewEventAnswer.class);
        //Log.d(TAG, "receiveResponse: order is "+event.getPayload());
        newEventCallback.newEvent(event);
    }

    @Override
    public void errorWithReceive(String message) {

    }

    @Override
    public String toString() {
        return "ManfredEventNetworkReceiver{"+
                "request type = new_event}";
    }
}
