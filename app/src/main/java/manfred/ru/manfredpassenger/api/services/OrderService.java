package manfred.ru.manfredpassenger.api.services;

import android.support.annotation.Keep;

import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.services.models.NewOrderResponse;
import manfred.ru.manfredpassenger.api.services.models.IdQuery;
import manfred.ru.manfredpassenger.api.services.models.PreOrderListResponse;
import manfred.ru.manfredpassenger.api.services.models.TariffChangedResponce;
import manfred.ru.manfredpassenger.api.services.models.VoteOrderQuery;
import manfred.ru.manfredpassenger.api.services.models.event.EventsResponse;
import manfred.ru.manfredpassenger.promocodes.models.PromoCode;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodeRequest;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodesAnswer;
import manfred.ru.manfredpassenger.ride.models.NewOrder;
import manfred.ru.manfredpassenger.ride.models.Tariffs;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
@Keep
public interface OrderService {
    @GET("v1.0/passenger/order/tariffs/")
    Call<ManfredResponse<Tariffs>> getTariffs(@Query("token") String token);

    @POST("v1.0/passenger/order/request_journey/")
    Call<ManfredResponse<NewOrderResponse>> requestJourney(@Query("token") String token, @Body NewOrder newOrder);

    @POST("v1.0/passenger/order/cancel/")
    Call<ManfredResponse> cancelRide(@Query("token") String token, @Body NewOrderResponse currOrder);

    @GET("v1.0/passenger/events/list/")
    Call<ManfredResponse<EventsResponse>> getEvents(@Query("token") String token);

    @POST("v1.0/passenger/order/vote/")
    Call<ManfredResponse> voteRide(@Body VoteOrderQuery voteOrderQuery,@Query("token") String token);

    @GET("v1.0/passenger/order/preorders/")
    Call<ManfredResponse<PreOrderListResponse>>getPreOrders(@Query("token") String token);

    @POST("v1.0/passenger/order/force_payment/")
    Call<ManfredResponse<PreOrderListResponse>>tryPayment(@Query("token") String token,@Body IdQuery idQuery);

    @GET("v1.0/passenger/order/promocodes_list/")
    Call<ManfredResponse<PromoCodesAnswer>>getPromocodes(@Query("token") String token);

    @POST("v1.0/passenger/order/add_promocode/")
    Call<ManfredResponse<PromoCode>>addPromocode(@Query("token") String token, @Body PromoCodeRequest promocode);

    @GET("v1.0/passenger/order/tariffs/modified/")
    Call<ManfredResponse<TariffChangedResponce>>checkTariffForModification(@Query("token") String token, @Query("timestamp") long timestamp);

}
