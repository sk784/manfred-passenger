package manfred.ru.manfredpassenger.api.services.models;

import java.util.List;

import manfred.ru.manfredpassenger.cards.managers.CreditCard;

public class Cards {
    private final List<CreditCard> cards;

    public Cards(List<CreditCard> cards) {
        this.cards = cards;
    }

    public List<CreditCard> getCards() {
        return cards;
    }
}
