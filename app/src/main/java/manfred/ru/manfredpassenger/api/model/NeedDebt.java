package manfred.ru.manfredpassenger.api.model;

import com.google.gson.annotations.SerializedName;

public class NeedDebt {
    @SerializedName("have_debt")
    private final boolean withDebt;

    public NeedDebt(boolean withDebt) {
        this.withDebt = withDebt;
    }

    public boolean isWithDebt() {
        return withDebt;
    }
}
