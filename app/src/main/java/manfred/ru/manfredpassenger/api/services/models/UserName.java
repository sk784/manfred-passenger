package manfred.ru.manfredpassenger.api.services.models;

public class UserName {
    private final String name;

    public UserName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
