package manfred.ru.manfredpassenger.api.model;

import com.google.gson.annotations.SerializedName;

public class WebSocketAnswer<T> {
    @SerializedName("id")
    private final long id;
    @SerializedName("id_of_request")
    private final long idOfRequest;
    @SerializedName("time")
    private final long time;
    @SerializedName("type")
    private final String type;
    @SerializedName("message")
    private final String message;
    @SerializedName("result_code")
    private final String resultCode;
    @SerializedName("status")
    private final String status;
    @SerializedName("payload")
    private final T payload;

    public WebSocketAnswer(long id, long idOfRequest, long time, String type, String message, String resultCode, String status, T payload) {
        this.id = id;
        this.idOfRequest = idOfRequest;
        this.time = time;
        this.type = type;
        this.message = message;
        this.resultCode = resultCode;
        this.status = status;
        this.payload = payload;
    }

    public long getId() {
        return id;
    }

    public long getIdOfRequest() {
        return idOfRequest;
    }

    public long getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String getResultCode() {
        return resultCode;
    }

    public T getPayload() {
        return payload;

    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "WebSocketAnswer{" +
                "id=" + id +
                ", idOfRequest=" + idOfRequest +
                ", time=" + time +
                ", type='" + type + '\'' +
                ", message='" + message + '\'' +
                ", resultCode='" + resultCode + '\'' +
                ", status='" + status + '\'' +
                ", payload=" + payload +
                '}';
    }
}
