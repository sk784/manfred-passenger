package manfred.ru.manfredpassenger.api.services.models;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.api.services.models.event.Event;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public class HistoryResponce {
    private final List<Event> orders;

    public HistoryResponce(List<Event> orders) {
        this.orders = orders;
    }

    public List<OrderVM> getOrders() {
        List<OrderVM>orderVMS = new ArrayList<>();
        for (Event event : orders){
            orderVMS.add(new OrderVM(event));
        }
        return orderVMS;
    }
}
