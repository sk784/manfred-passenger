package manfred.ru.manfredpassenger.api.managers;

import manfred.ru.manfredpassenger.api.model.EventProcessedAnswer;
import manfred.ru.manfredpassenger.api.model.ManfredEventNetworkReceiver;
import manfred.ru.manfredpassenger.api.model.NetworkQuery;
import manfred.ru.manfredpassenger.api.model.NetworkReceiver;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.model.WebSocketRequest;

public interface NetworkManager {
    void sendRequest(WebSocketRequest request);
    void sendEventAcceptedRequest(EventProcessedAnswer eventProcessedAnswer);
    void addRequestReceiver(NetworkReceiver networkReceiver);
    void addEventReceiver(ManfredEventNetworkReceiver eventNetworkReceiver);
    void addQuery(NetworkQuery networkQuery);
    <V>void addQuery(String token, String path, WebSocketAnswerCallback callback);
    <T,V>void addQuery(String token, String path, T payload, WebSocketAnswerCallback callback);
    void removeRequestReceiver(NetworkReceiver networkReceiver);
    void internetOff();
    void internetOn();
    void disconnect();
}
