package manfred.ru.manfredpassenger.api.services.models;

import com.google.gson.annotations.SerializedName;

public class AverageWaitTime {
    @SerializedName("average_wait_time")
    private final int averageWaitTime;

    public AverageWaitTime(int averageWaitTime) {
        this.averageWaitTime = averageWaitTime;
    }

    public int getAverageWaitTime() {
        return averageWaitTime;
    }
}
