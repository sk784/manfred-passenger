package manfred.ru.manfredpassenger.api.services.models.event;

import com.google.gson.annotations.SerializedName;

public class Driver {

    @SerializedName("driver_name")
    private String driverName;

    @SerializedName("image")
    private String image;

    @SerializedName("car_number")
    private String carNumber;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("car_color")
    private String carColor;

    @SerializedName("id")
    private int id;

    @SerializedName("driver_phone")
    private String driverPhone;

    @SerializedName("car_type")
    private String carType;

    @SerializedName("longitude")
    private String longitude;

    public CarType getCarTypeObj() {
        return carTypeObj;
    }

    public void setCarTypeObj(CarType carTypeObj) {
        this.carTypeObj = carTypeObj;
    }

    @SerializedName("car_type_object")
    private CarType carTypeObj;

    public String getDriverName() {
        return driverName;
    }

    public String getImage() {
        return image;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getCarColor() {
        return carColor;
    }

    public int getId() {
        return id;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public String getCarType() {
        return carType;
    }

    public String getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return
                "Driver{" +
                        "driver_name = '" + driverName + '\'' +
                        ",image = '" + image + '\'' +
                        ",car_number = '" + carNumber + '\'' +
                        ",latitude = '" + latitude + '\'' +
                        ",car_color = '" + carColor + '\'' +
                        ",id = '" + id + '\'' +
                        ",driver_phone = '" + driverPhone + '\'' +
                        ",car_type = '" + carType + '\'' +
                        ",longitude = '" + longitude + '\'' +
                        "}";
    }
}