package manfred.ru.manfredpassenger.api.services.models.googleMaps.directions;

public class Steps {
    private Location start_location;
    private Location end_location;
    private OverviewPolyLine polyline;

    private Distance distance;

    public Location getStart_location() {
        return start_location;
    }

    public Location getEnd_location() {
        return end_location;
    }

    public OverviewPolyLine getPolyline() {
        return polyline;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }
}
