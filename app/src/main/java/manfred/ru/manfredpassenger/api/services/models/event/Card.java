package manfred.ru.manfredpassenger.api.services.models.event;

import com.google.gson.annotations.SerializedName;

@Deprecated
public class Card {

    @SerializedName("card_number")
    private String cardNumber;

    @SerializedName("active")
    private boolean active;

    @SerializedName("id")
    private int id;

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return
                "Card{" +
                        "card_number = '" + cardNumber + '\'' +
                        ",active = '" + active + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}