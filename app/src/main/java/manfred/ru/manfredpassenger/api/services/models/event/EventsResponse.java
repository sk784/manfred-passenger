package manfred.ru.manfredpassenger.api.services.models.event;

import java.util.List;

public class EventsResponse {
    private final String server_time;
    private final List<Event> events;

    public EventsResponse(String server_time, List<Event> events) {
        this.server_time = server_time;
        this.events = events;
    }

    public String getServer_time() {
        return server_time;
    }

    public List<Event> getEvents() {
        return events;
    }
}
