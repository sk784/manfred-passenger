package manfred.ru.manfredpassenger.api.services.models.event;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.promocodes.models.PromoCode;
import manfred.ru.manfredpassenger.ride.models.ManfredDTOLocation;

public class Event {

    @SerializedName("total_cost")
    private String totalCost;

    public String getTotalCostWithoutPromocode() {
        return totalCostWithoutPromocode;
    }

    public void setTotalCostWithoutPromocode(String totalCostWithoutPromocode) {
        this.totalCostWithoutPromocode = totalCostWithoutPromocode;
    }

    @SerializedName("total_cost_without_promocode")
    private String totalCostWithoutPromocode;

    @SerializedName("destination")
    private ManfredDTOLocation destination;

    @SerializedName("type")
    private String type;

    @SerializedName("needAssistant")
    private boolean needAssistant;

    @SerializedName("accept_order_time")
    private String acceptOrderTime;

    @SerializedName("holded_amount")
    private String holdedAmount;

    @SerializedName("avance_minutes")
    private int avanceMinutes;

    @SerializedName("car_need_time")
    private String carNeedTime;

    @SerializedName("initial_car_need_time")
    private String initialCarNeedTime;

    public String getInitialCarNeedTime() {
        return initialCarNeedTime;
    }

    public void setInitialCarNeedTime(String initialCarNeedTime) {
        this.initialCarNeedTime = initialCarNeedTime;
    }

    @SerializedName("final_location")
    private ManfredDTOLocation finalLocation;

    @SerializedName("prepayment_price")
    private String prepaymentPrice;

    @SerializedName("tariff")
    private Tariff tariff;

    @SerializedName("free_cancel_time")
    private int freeCancelTime;

    @SerializedName("id")
    private int id;

    @SerializedName("to_airport")
    private boolean toAirport;

    @SerializedName("cancel_time")
    private String cancelTime;

    @SerializedName("comments")
    private String comments;

    @SerializedName("timeToCostumer")
    private int timeToCostumer;

    @SerializedName("to_airport_cancelled")
    private boolean toAirportCancelled;

    @SerializedName("created")
    private Long created;

    @SerializedName("ride_minute_price")
    private String rideMinutePrice;

    @SerializedName("airport_prepayment_price")
    private String airportPrepaymentPrice;

    @SerializedName("customerNumber")
    private String customerNumber;

    @SerializedName("customerName")
    private String customerName;

    @SerializedName("delay")
    private int delay;

    @Nullable
    @SerializedName("driver")
    private Driver driver;

    @SerializedName("cancel_ride_price")
    private String cancelRidePrice;

    @SerializedName("wait_passenger_time")
    private String waitPassengerTime;

    @SerializedName("free_wait_time")
    private int freeWaitTime;

    @SerializedName("assist")
    private Object assist;

    @SerializedName("ride_finish_time")
    private String rideFinishTime;

    @SerializedName("ride_start_time")
    private String rideStartTime;

    @SerializedName("preorder_start_time")
    private String preorderStartTime;

    @SerializedName("location")
    private ManfredDTOLocation location;

    @SerializedName("debt")
    private float debt;

    @SerializedName("card")
    private CreditCard card;

    @SerializedName("status")
    private String status;

    @Nullable
    @SerializedName("promocode")
    private PromoCode promoCode;

    @SerializedName("cash_payment")
    private boolean cashPayment;

    @Nullable
    public PromoCode getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(@Nullable PromoCode promoCode) {
        this.promoCode = promoCode;
    }

    public int getPassengerVote() {
        return passengerVote;
    }

    public void setPassengerVote(int passengerVote) {
        this.passengerVote = passengerVote;
    }

    @SerializedName("passenger_vote")
    private int passengerVote;

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setDestination(ManfredDTOLocation destination) {
        this.destination = destination;
    }

    public ManfredDTOLocation getDestination() {
        return destination;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setNeedAssistant(boolean needAssistant) {
        this.needAssistant = needAssistant;
    }

    public boolean isNeedAssistant() {
        return needAssistant;
    }

    public void setAcceptOrderTime(String acceptOrderTime) {
        this.acceptOrderTime = acceptOrderTime;
    }

    public String getAcceptOrderTime() {
        return acceptOrderTime;
    }

    public void setHoldedAmount(String holdedAmount) {
        this.holdedAmount = holdedAmount;
    }

    public String getHoldedAmount() {
        return holdedAmount;
    }

    public void setAvanceMinutes(int avanceMinutes) {
        this.avanceMinutes = avanceMinutes;
    }

    public int getAvanceMinutes() {
        return avanceMinutes;
    }

    public void setCarNeedTime(String carNeedTime) {
        this.carNeedTime = carNeedTime;
    }

    public String getCarNeedTime() {
        return carNeedTime;
    }

    public void setFinalLocation(ManfredDTOLocation finalLocation) {
        this.finalLocation = finalLocation;
    }

    public ManfredDTOLocation getFinalLocation() {
        return finalLocation;
    }

    public void setPrepaymentPrice(String prepaymentPrice) {
        this.prepaymentPrice = prepaymentPrice;
    }

    public String getPrepaymentPrice() {
        return prepaymentPrice;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setFreeCancelTime(int freeCancelTime) {
        this.freeCancelTime = freeCancelTime;
    }

    public int getFreeCancelTime() {
        return freeCancelTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setToAirport(boolean toAirport) {
        this.toAirport = toAirport;
    }

    public boolean isToAirport() {
        return toAirport;
    }

    public void setCancelTime(String cancelTime) {
        this.cancelTime = cancelTime;
    }

    public String getCancelTime() {
        return cancelTime;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Nullable
    public String getComments() {
        return comments;
    }

    public void setTimeToCostumer(int timeToCostumer) {
        this.timeToCostumer = timeToCostumer;
    }

    public int getTimeToCostumer() {
        return timeToCostumer;
    }

    public void setToAirportCancelled(boolean toAirportCancelled) {
        this.toAirportCancelled = toAirportCancelled;
    }

    public boolean isToAirportCancelled() {
        return toAirportCancelled;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getCreated() {
        return created;
    }

    public void setRideMinutePrice(String rideMinutePrice) {
        this.rideMinutePrice = rideMinutePrice;
    }

    public String getRideMinutePrice() {
        return rideMinutePrice;
    }

    public void setAirportPrepaymentPrice(String airportPrepaymentPrice) {
        this.airportPrepaymentPrice = airportPrepaymentPrice;
    }

    public String getAirportPrepaymentPrice() {
        return airportPrepaymentPrice;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getDelay() {
        return delay;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    @Nullable
    public Driver getDriver() {
        return driver;
    }

    public void setCancelRidePrice(String cancelRidePrice) {
        this.cancelRidePrice = cancelRidePrice;
    }

    public String getCancelRidePrice() {
        return cancelRidePrice;
    }

    public void setWaitPassengerTime(String waitPassengerTime) {
        this.waitPassengerTime = waitPassengerTime;
    }

    public String getWaitPassengerTime() {
        return waitPassengerTime;
    }

    public void setFreeWaitTime(int freeWaitTime) {
        this.freeWaitTime = freeWaitTime;
    }

    public int getFreeWaitTime() {
        return freeWaitTime;
    }

    public void setAssist(Object assist) {
        this.assist = assist;
    }

    public Object getAssist() {
        return assist;
    }

    public void setRideFinishTime(String rideFinishTime) {
        this.rideFinishTime = rideFinishTime;
    }

    public String getRideFinishTime() {
        return rideFinishTime;
    }

    public void setRideStartTime(String rideStartTime) {
        this.rideStartTime = rideStartTime;
    }

    public String getRideStartTime() {
        return rideStartTime;
    }

    public void setPreorderStartTime(String preorderStartTime) {
        this.preorderStartTime = preorderStartTime;
    }

    public String getPreorderStartTime() {
        return preorderStartTime;
    }

    public void setLocation(ManfredDTOLocation location) {
        this.location = location;
    }

    public ManfredDTOLocation getLocation() {
        return location;
    }

    public void setDebt(int debt) {
        this.debt = debt;
    }

    public float getDebt() {
        return debt;
    }

    public void setCard(CreditCard card) {
        this.card = card;
    }

    public CreditCard getCard() {
        return card;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public boolean isCashPayment() {
        return cashPayment;
    }

    public void setCashPayment(boolean cashPayment) {
        this.cashPayment = cashPayment;
    }

    @Override
    public String toString() {
        return
                "Event{" +
                        "total_cost = '" + totalCost + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",type = '" + type + '\'' +
                        ",needAssistant = '" + needAssistant + '\'' +
                        ",accept_order_time = '" + acceptOrderTime + '\'' +
                        ",holded_amount = '" + holdedAmount + '\'' +
                        ",avance_minutes = '" + avanceMinutes + '\'' +
                        ",car_need_time = '" + carNeedTime + '\'' +
                        ",final_location = '" + finalLocation + '\'' +
                        ",prepayment_price = '" + prepaymentPrice + '\'' +
                        ",tariff = '" + tariff + '\'' +
                        ",free_cancel_time = '" + freeCancelTime + '\'' +
                        ",id = '" + id + '\'' +
                        ",to_airport = '" + toAirport + '\'' +
                        ",cancel_time = '" + cancelTime + '\'' +
                        ",comments = '" + comments + '\'' +
                        ",timeToCostumer = '" + timeToCostumer + '\'' +
                        ",to_airport_cancelled = '" + toAirportCancelled + '\'' +
                        ",created = '" + created + '\'' +
                        ",ride_minute_price = '" + rideMinutePrice + '\'' +
                        ",airport_prepayment_price = '" + airportPrepaymentPrice + '\'' +
                        ",customerNumber = '" + customerNumber + '\'' +
                        ",customerName = '" + customerName + '\'' +
                        ",delay = '" + delay + '\'' +
                        ",driver = '" + driver + '\'' +
                        ",cancel_ride_price = '" + cancelRidePrice + '\'' +
                        ",wait_passenger_time = '" + waitPassengerTime + '\'' +
                        ",free_wait_time = '" + freeWaitTime + '\'' +
                        ",assist = '" + assist + '\'' +
                        ",ride_finish_time = '" + rideFinishTime + '\'' +
                        ",ride_start_time = '" + rideStartTime + '\'' +
                        ",preorder_start_time = '" + preorderStartTime + '\'' +
                        ",location = '" + location + '\'' +
                        ",debt = '" + debt + '\'' +
                        ",card = '" + card + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}