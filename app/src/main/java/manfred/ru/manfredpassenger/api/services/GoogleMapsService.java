package manfred.ru.manfredpassenger.api.services;

import manfred.ru.manfredpassenger.api.services.models.googleMaps.directions.DirectionResults;
import manfred.ru.manfredpassenger.api.services.models.googleMaps.geocode.GeocodeResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleMapsService {
    @GET("/maps/api/geocode/json")
    Call<GeocodeResult> getLocationByLatLng(@Query("latlng") String latlng, @Query("language") String language, @Query("key") String key);

    @GET("/maps/api/directions/json?departure_time=now")
    Call<DirectionResults> getDirection(@Query("origin") String origin, @Query("destination") String destination, @Query("key") String key);
}
