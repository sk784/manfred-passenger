package manfred.ru.manfredpassenger.api.services.models;

public class NewOrderResponse {
    private final int id;

    public NewOrderResponse(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
