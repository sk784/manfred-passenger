package manfred.ru.manfredpassenger.api.services.models.event;

public class RestoreResponse {
    private final Event order;

    public RestoreResponse(Event order) {
        this.order = order;
    }

    public Event getOrder() {
        return order;
    }
}
