package manfred.ru.manfredpassenger.api.services.models;

public class IdQuery {
    final int id;

    public IdQuery(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
