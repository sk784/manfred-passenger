package manfred.ru.manfredpassenger.api.services.models;


import com.google.gson.annotations.SerializedName;

public class ManfredPassengerData {

    @SerializedName("country_code")
    private String countryCode;

    @SerializedName("phone")
    private String phone;

    @SerializedName("is_new")
    private boolean isNew;

    @SerializedName("name")
    private String name;

    @SerializedName("country_name")
    private String countryName;

    @SerializedName("id")
    private int id;

    @SerializedName("token")
    private String token;

    public String getCountryCode() {
        return countryCode;
    }

    public String getPhone() {
        return phone;
    }

    public boolean isNew() {
        return isNew;
    }

    public String getName() {
        return name;
    }

    public String getCountryName() {
        return countryName;
    }

    public int getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return
                "ManfredPassengerData{" +
                        "country_code = '" + countryCode + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",is_new = '" + isNew + '\'' +
                        ",name = '" + name + '\'' +
                        ",country_name = '" + countryName + '\'' +
                        ",id = '" + id + '\'' +
                        ",token = '" + token + '\'' +
                        "}";
    }
}