package manfred.ru.manfredpassenger.api.services.models.googleMaps.directions;

import java.util.List;

public class Legs {
    private List<Steps> steps;
    private Duration duration;
    private Distance distance;

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public List<Steps> getSteps() {
        return steps;
    }
}
