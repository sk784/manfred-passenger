package manfred.ru.manfredpassenger.api.services.models.event;

import com.google.gson.annotations.SerializedName;

public class CarType {

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    @SerializedName("brand")
    private String brand;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return
                "CarType{" +
                        "name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        ",brand = '" + brand + '\'' +
                        "}";
    }
}