package manfred.ru.manfredpassenger.api.services.models.googleMaps.directions;

public class Location {
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
