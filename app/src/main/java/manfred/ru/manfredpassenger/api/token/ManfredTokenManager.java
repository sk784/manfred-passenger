package manfred.ru.manfredpassenger.api.token;

import android.support.annotation.Nullable;
import android.util.Log;


public class ManfredTokenManager implements TokenManager {
    private static final String TAG = "ManfredTokenManager";
    @Nullable
    private String token;

    public ManfredTokenManager() {
        Log.d(TAG, "ManfredTokenManager: creating...");
    }

    @Override
    public void setAuthToken(String token) {
        //Log.d(TAG, "setAuthToken: "+token);
        this.token = token;
    }

    @Nullable
    @Override
    public String getAuthToken() {
        //Log.d(TAG, "getAuthToken: "+token);
        return token;
    }
}
