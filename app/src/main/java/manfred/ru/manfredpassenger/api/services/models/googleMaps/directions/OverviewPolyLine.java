package manfred.ru.manfredpassenger.api.services.models.googleMaps.directions;

import com.google.gson.annotations.SerializedName;

public class OverviewPolyLine {

    @SerializedName("points")
    public String points;

    public String getPoints() {
        return points;
    }
}
