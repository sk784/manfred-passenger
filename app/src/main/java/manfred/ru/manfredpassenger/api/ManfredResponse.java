package manfred.ru.manfredpassenger.api;

import android.support.annotation.Keep;
import android.support.annotation.Nullable;
@Keep
public class ManfredResponse<T> {
    private final String message;
    private final String status;
    private final T data;
    private final String result_code;

    public ManfredResponse(String message, String status, T data, String result_code) {
        this.message = message;
        this.status = status;
        this.data = data;
        this.result_code = result_code;
    }

    public String getMessage() {
        return message;
    }

    @Nullable
    public T getData() {
        return (T) data;
    }

    public String getResult_code() {
        return result_code;
    }

    public String getStatus() {
        return status;
    }
}
