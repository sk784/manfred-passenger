package manfred.ru.manfredpassenger.api.services;

import android.support.annotation.Keep;

import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.services.models.AverageWaitTime;
import manfred.ru.manfredpassenger.api.services.models.CoordForAverageTime;
import manfred.ru.manfredpassenger.api.services.models.DriverLocationResponse;
import manfred.ru.manfredpassenger.ride.models.CarsAnswer;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
@Keep
public interface DriverService {
    @POST("v1.0/passenger/driver/average_wait_time/")
    Call<ManfredResponse<Object>> getAverageTime(@Query("token") String token, @Body CoordForAverageTime CoordForAverageTime);

    @GET("v1.0/passenger/driver_location/")
    Call<ManfredResponse<DriverLocationResponse>> getDriverLocation(@Query("token") String token);

    @GET("v1.0/passenger/car/list/free/")
    Call<ManfredResponse<CarsAnswer>>getFreeDrivers(@Query("token") String token);
}
