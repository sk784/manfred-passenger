package manfred.ru.manfredpassenger.api.services.models;

public class VoteOrderQuery {
    private final int id;
    /**
     * 1 - negative vote
     * 5 - positive vote
     */
    private final int vote;

    public VoteOrderQuery(int id, int vote) {
        this.id = id;
        this.vote = vote;
    }

    public int getId() {
        return id;
    }

    public int getVote() {
        return vote;
    }
}
