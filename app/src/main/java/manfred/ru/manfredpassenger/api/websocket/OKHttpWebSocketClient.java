package manfred.ru.manfredpassenger.api.websocket;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import manfred.ru.manfredpassenger.api.model.EventProcessedAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketRequest;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class OKHttpWebSocketClient {
    private static final int NORMAL_CLOSURE_STATUS = 1000;
    private static final String TAG = "OKHttpWebSocketClient";
    private static final long RECONNECT_DELAY_MS = 3000;
    private final MessageListener listener;
    private final String host;
    @Nullable
    private okhttp3.WebSocket webSocket =null;
    @Nullable private SocketConnectionListener socketConnectionListener;

    public OKHttpWebSocketClient(MessageListener listener, String host) {
        this.listener = listener;
        this.host = host;
    }

    private WebSocketListener createMessagingListener() {
        Log.d(TAG, "createMessagingListener: ");
        return new WebSocketListener() {
            @Override
            public void onOpen(okhttp3.WebSocket webSocket, Response response) {
                super.onOpen(webSocket, response);
                Log.d(TAG, "onOpen: ");
                if (socketConnectionListener != null) {
                    socketConnectionListener.onConnectionChange(true);
                }
            }

            @Override
            public void onMessage(okhttp3.WebSocket webSocket, String text) {
                super.onMessage(webSocket, text);
                listener.onSocketMessage(text);
            }

            @Override
            public void onMessage(okhttp3.WebSocket webSocket, ByteString bytes) {
                super.onMessage(webSocket, bytes);
            }

            @Override
            public void onClosing(okhttp3.WebSocket webSocket, int code, String reason) {
                super.onClosing(webSocket, code, reason);
                close();
                Log.d(TAG, "onClosing: because "+reason+", code is "+code);
            }

            @Override
            public void onClosed(okhttp3.WebSocket webSocket, int code, String reason) {
                super.onClosed(webSocket, code, reason);
                Log.d(TAG, "onClosed: "+reason);
                if (socketConnectionListener != null) {
                    socketConnectionListener.onConnectionChange(false);
                }
                socketDisconnected();
            }

            @Override
            public void onFailure(okhttp3.WebSocket webSocket, Throwable t, @Nullable Response response) {
                super.onFailure(webSocket, t, response);
                Log.d(TAG, "onFailure: "+t.getMessage());
                if (socketConnectionListener != null) {
                    Log.d(TAG, "onFailure: changing connect state");
                    socketConnectionListener.onConnectionChange(false);
                }
                socketDisconnected();
                schedulleReconnect();
            }
        };
    }

    private AtomicBoolean isReconnectingTimed=new AtomicBoolean(false);
    private void schedulleReconnect(){
        if(isReconnectingTimed.get()){
            Log.d(TAG, "reconnect: is scheduled, ignoring");
            return;
        }
        isReconnectingTimed.set(true);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                connect();
                isReconnectingTimed.set(false);
            }
        }, RECONNECT_DELAY_MS);
    }

    private void socketDisconnected(){
        this.webSocket=null;
    }

    public void setSocketConnectionListener(@NonNull SocketConnectionListener socketConnectionListener) {
        this.socketConnectionListener=socketConnectionListener;
    }

    public void removeConnectionListener() {
        this.socketConnectionListener=null;
    }

    public void connect() {
        Log.d(TAG, "connect: ");
        OkHttpClient client = new OkHttpClient.Builder()
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(host)
                .build();
        this.webSocket = client.newWebSocket(request, createMessagingListener());
    }

    public void sendRequest(WebSocketRequest request) {
        //Log.d(TAG, "sendRequest: "+request.getType());
        Gson gson = new Gson();
        String stringRequest = gson.toJson(request);
        if (webSocket != null) {
            boolean sended = webSocket.send(stringRequest);
            if(!sended){
                listener.onSocketMessage(createErrorAnswer(request));
                Log.d(TAG, "sendRequest: not sended");
            }else {
                //Log.d(TAG, "sendRequest: sended");
            }
        }else {
            Log.d(TAG, "sendRequest: websocket is null, recreating");
            connect();
            webSocket.send(stringRequest);
        }
    }

    private String createErrorAnswer(WebSocketRequest request){
        final WebSocketAnswer webSocketAnswer = new WebSocketAnswer<Void>(new Random().nextLong(), request.getId(),
                (new Date().getTime() / 1000), request.getType(), "Связь не установлена", "sending_error", "error", null);
        Gson gson = new GsonBuilder().create();
        return gson.toJson(webSocketAnswer);
    }

    public void sendEventReceived(EventProcessedAnswer eventProcessedAnswer) {
        if(webSocket!=null) {
            Log.d(TAG, "event "+eventProcessedAnswer.getIdOfRequest()+" processed, sending answer ");
            Gson gson = new Gson();
            webSocket.send((gson.toJson(eventProcessedAnswer)));
        }
    }

    public void close() {
        Log.d(TAG, "close: ");
        if (webSocket != null) {
            webSocket.close(NORMAL_CLOSURE_STATUS,null);
        }
    }

    public void inetOff() {
        Log.d(TAG, "inetOff: ");
        //close();
    }

    public void inetOn() {
        Log.d(TAG, "inetOn: ");
        //connect();
    }

    public interface MessageListener {
        void onSocketMessage(String answer);
    }

    public interface SocketConnectionListener {
        void onConnectionChange(boolean isConnected);
    }
}