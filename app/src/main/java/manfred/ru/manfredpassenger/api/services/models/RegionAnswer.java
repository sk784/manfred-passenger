package manfred.ru.manfredpassenger.api.services.models;

import manfred.ru.manfredpassenger.common.models.Region;

public class RegionAnswer {
    private final Region region;

    public RegionAnswer(Region region) {
        this.region = region;
    }

    public Region getRegion() {
        return region;
    }
}
