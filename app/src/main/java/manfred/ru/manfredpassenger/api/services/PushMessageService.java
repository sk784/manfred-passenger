package manfred.ru.manfredpassenger.api.services;

import android.support.annotation.Keep;

import com.google.android.gms.maps.model.LatLng;

import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.services.models.PushMessageAnswer;
import manfred.ru.manfredpassenger.api.services.models.RegionAnswer;
import manfred.ru.manfredpassenger.common.models.ListRegionAnswer;
import manfred.ru.manfredpassenger.common.models.PushMessageIdQuery;
import manfred.ru.manfredpassenger.common.models.RegionIdQuery;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

@Keep
public interface PushMessageService {
    @GET("v1.0/passenger/mailing/get_message/")
    Call<ManfredResponse<PushMessageAnswer>> getPushMessage(@Query("token") String token);

    @POST("v1.0/passenger/mailing/set_message_read/")
    Call<ManfredResponse>setMessageRead(@Query("token") String token, @Body PushMessageIdQuery idQuery);

}
