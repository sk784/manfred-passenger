package manfred.ru.manfredpassenger.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class EventProcessedAnswer {
    @SerializedName("id_of_request")
    private final long idOfRequest;
    @SerializedName("time")
    private final long time;
    @SerializedName("type")
    private final String type;

    public EventProcessedAnswer(long idOfRequest) {
        this.idOfRequest = idOfRequest;
        this.time=new Date().getTime();
        this.type="new_event";
    }

    public long getIdOfRequest() {
        return idOfRequest;
    }

    public long getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "EventProcessedAnswer{" +
                "idOfRequest=" + idOfRequest +
                ", time=" + time +
                ", type='" + type + '\'' +
                '}';
    }
}
