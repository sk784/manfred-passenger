package manfred.ru.manfredpassenger.api.repositories;

import android.location.Location;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.services.models.googleMaps.geocode.AddressComponent;
import manfred.ru.manfredpassenger.api.services.models.googleMaps.geocode.GeocodeResult;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;

public class GeocodingRepository {
    private final static String API_KEY = "AIzaSyCAjroGjHf3UPpNGGmEeS3vJJPu-HiMu6I";
    private static String TAG = "GeocodingRepository";
    private final TokenManager tokenManager;
    private final NetworkManagerProvider networkManager;

    public GeocodingRepository(TokenManager tokenManager, NetworkManagerProvider networkManager) {
        this.tokenManager = tokenManager;
        this.networkManager = networkManager;
    }

    public void getLocationNameByLatLng(final Location location, final NetworkResponseCallback<ManfredLocation> responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(), "region/get_address",
                new LocationRequestModel(location.getLatitude(), location.getLongitude()), new WebSocketAnswerCallback<WebSocketAnswer<LocationResponseModel>>() {
                    @Override
                    public void messageReceived(WebSocketAnswer<LocationResponseModel> message) {
                        switch (message.getResultCode()){
                            case "success":
                                final ManfredLocation checkingLocation = new ManfredLocation(location);
                                checkingLocation.setCity(message.getPayload().getCity());
                                checkingLocation.setStreet(message.getPayload().getAddress());
                                responseCallback.allOk(checkingLocation);
                                break;
                            case "invalid_token":
                                responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR, message.getMessage()));
                                break;
                            case "no_free_drivers":
                                responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.NO_FREE_DRIVERS, message.getMessage()));
                                break;
                            case "passenger_out_region":
                                responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OUT_REGION,message.getMessage()));
                                break;
                            default:
                                responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, message.getMessage()));
                                break;
                        }
                    }

                    @Override
                    public void error(ManfredNetworkError manfredError) {
                        responseCallback.error(manfredError);
                    }

                    @Override
                    public Class getAnswerClass() {
                        return LocationResponseModel.class;
                    }
                });

/*        APIClient.getMapService().getLocationByLatLng(latlng, "RU", API_KEY)
                .enqueue(new Callback<GeocodeResult>() {
                    @Override
                    public void onResponse(@NonNull Call<GeocodeResult> call, @NonNull Response<GeocodeResult> response) {
                        //Log.d(TAG, "testCheckAdress onResponse: "+response.body());
                        GeocodeResult geocodeResult = response.body();
                        //Log.d(TAG, "testCheckAdress onResponse: adress is "+geocodeResult.getResults().get(0).getFormattedAddress());
                        if (geocodeResult != null) {
                            ManfredLocation manfredLocation = getAddress(location, geocodeResult);
                            responseCallback.allOk(manfredLocation);
                        } else {
                            Log.d(TAG, "onResponse: "+response.message());
                            responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, "Ошибка определения адреса"));
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GeocodeResult> call, @NonNull Throwable t) {
                        Log.d(TAG, "testCheckAdress onFailure: " + t.getMessage());
                        new NetworkUtils().processFailureResponse(t, responseCallback);
                    }
                });*/
    }

    private @Nullable
    ManfredLocation getAddress(Location location, GeocodeResult geocodeResult) {
        if (geocodeResult.getResults().isEmpty()) return null;
        List<AddressComponent> addressComponents = geocodeResult.getResults().get(0).getAddressComponents();
        String streetNumber = "";
        String route = "";
        String city = "";
        for (AddressComponent addressComponent : addressComponents) {
            if (addressComponent.getTypes().contains("street_number")) {
                streetNumber = addressComponent.getLongName();
            }
            if (addressComponent.getTypes().contains("locality")) {
                city = addressComponent.getShortName();
            }
            if (addressComponent.getTypes().contains("route")) {
                route = addressComponent.getShortName();
            }
        }
        return new ManfredLocation(location.getLatitude(), location.getLongitude(), route + " " + streetNumber, city);
    }

    private class LocationRequestModel {
        @SerializedName("latitude")
        private final double latitude;
        @SerializedName("longitude")
        private final double longitude;

        public LocationRequestModel(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }

    private class LocationResponseModel {
        @SerializedName("address")
        private final String address;
        @SerializedName("city")
        private final String city;
        //it is for moving pin
        @SerializedName("latitude")
        private final double latitude;
        @SerializedName("longitude")
        private final double longitude;

        public LocationResponseModel(String address, String city, double latitude, double longitude) {
            this.address = address;
            this.city = city;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public String getAddress() {
            return address;
        }

        public String getCity() {
            return city;
        }
    }
}
