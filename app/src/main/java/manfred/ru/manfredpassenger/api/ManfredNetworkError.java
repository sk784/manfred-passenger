package manfred.ru.manfredpassenger.api;

import android.support.annotation.Keep;

@Keep
public class ManfredNetworkError {
    public enum ErrorType {
        SERVER_UNAVAILABLE,
        TOKEN_ERROR,
        NO_FREE_DRIVERS,
        WRONG_SMS_CODE,
        OUT_REGION,
        CANT_SET_REGION,
        OTHER
    }

    private final ErrorType type;
    private final String text;

    public ManfredNetworkError(ErrorType type, String text) {
        this.type = type;
        this.text = text;
    }

    public ErrorType getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "ManfredNetworkError{" +
                "type=" + type +
                ", text='" + text + '\'' +
                '}';
    }
}
