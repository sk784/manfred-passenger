package manfred.ru.manfredpassenger.api.services;

import android.support.annotation.Keep;


import com.google.android.gms.maps.model.LatLng;

import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.services.models.RegionAnswer;
import manfred.ru.manfredpassenger.common.models.ListRegionAnswer;
import manfred.ru.manfredpassenger.common.models.RegionIdQuery;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
@Keep
public interface RegionService {
    @GET("v1.0/passenger/region/list/")
    Call<ManfredResponse<ListRegionAnswer>> getRegions(@Query("token") String token);

    @POST("v1.0/passenger/region/set/")
    Call<ManfredResponse<RegionAnswer>>setCurrentRegionById(@Query("token") String token, @Body RegionIdQuery idQuery);

    @POST("v1.0/passenger/region/set_by_geo/")
    Call<ManfredResponse<RegionAnswer>>setCurrentRegionByGeoLocation(@Query("token") String token, @Body LatLng location);
}
