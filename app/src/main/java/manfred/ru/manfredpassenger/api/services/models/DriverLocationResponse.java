package manfred.ru.manfredpassenger.api.services.models;

import com.google.gson.annotations.SerializedName;

import manfred.ru.manfredpassenger.api.services.models.event.Driver;

public class DriverLocationResponse {
    private final Driver driver;

    @SerializedName("id")
    private final int orderId;

    public DriverLocationResponse(Driver driver, int orderId) {
        this.driver = driver;
        this.orderId = orderId;
    }

    public Driver getDriver() {
        return driver;
    }

    public int getOrderId() {
        return orderId;
    }
}
