package manfred.ru.manfredpassenger.api.model;

import com.google.gson.annotations.SerializedName;

import manfred.ru.manfredpassenger.api.services.models.event.Event;

public class NewEventAnswer {
    @SerializedName("id")
    private final long id;
    @SerializedName("type")
    private final String type;
    @SerializedName("time")
    private final long time;
    @SerializedName("payload")
    private final Event payload;

    public NewEventAnswer(long id, String type, long time, Event payload) {
        this.id = id;
        this.type = type;
        this.time = time;
        this.payload = payload;
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public long getTime() {
        return time;
    }

    public Event getPayload() {
        return payload;
    }
}
