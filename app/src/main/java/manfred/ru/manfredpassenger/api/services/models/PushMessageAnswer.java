package manfred.ru.manfredpassenger.api.services.models;

import manfred.ru.manfredpassenger.common.models.PushMessage;

public class PushMessageAnswer {
	private final PushMessage message;

	public PushMessageAnswer(PushMessage message) {
		this.message = message;
	}

	public PushMessage getMessage() {
		return message;
	}
}
