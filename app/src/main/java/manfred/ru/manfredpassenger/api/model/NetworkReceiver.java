package manfred.ru.manfredpassenger.api.model;

public interface NetworkReceiver {
    String getTypeOfRequest();
    void receiveResponse(String response);
    void errorWithReceive(String message);
}
