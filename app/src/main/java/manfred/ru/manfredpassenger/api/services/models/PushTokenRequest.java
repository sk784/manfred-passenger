package manfred.ru.manfredpassenger.api.services.models;

public class PushTokenRequest {
    private final boolean push_enabled;
    private final String push_token;

    public PushTokenRequest(String push_token, boolean push_enabled) {
        this.push_token = push_token;
        this.push_enabled = push_enabled;
    }

    public boolean isPush_enabled() {
        return push_enabled;
    }

    public String getPush_token() {
        return push_token;
    }
}
