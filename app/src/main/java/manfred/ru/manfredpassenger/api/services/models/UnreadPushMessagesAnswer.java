package manfred.ru.manfredpassenger.api.services.models;

import java.util.List;

import manfred.ru.manfredpassenger.common.models.PushMessage;

public class UnreadPushMessagesAnswer {
	private final List<PushMessage> messages;

	public UnreadPushMessagesAnswer(List<PushMessage> pushMessages) {
		this.messages = pushMessages;
	}

	public List<PushMessage> getPushmessages() {
		return messages;
	}
}
