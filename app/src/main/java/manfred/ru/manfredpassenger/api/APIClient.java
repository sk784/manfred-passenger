package manfred.ru.manfredpassenger.api;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import manfred.ru.manfredpassenger.BuildConfig;
import manfred.ru.manfredpassenger.api.services.DriverService;
import manfred.ru.manfredpassenger.api.services.GoogleMapsService;
import manfred.ru.manfredpassenger.api.services.OrderService;
import manfred.ru.manfredpassenger.api.services.PushMessageService;
import manfred.ru.manfredpassenger.api.services.RegionService;
import manfred.ru.manfredpassenger.api.services.UserService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static volatile OkHttpClient.Builder CLIENT = new OkHttpClient.Builder();
    private final static String BASE_URL = BuildConfig.BASE_URL;

    public static UserService getUserService() {
        return getRetrofit(BASE_URL).create(UserService.class);
    }

    public static DriverService getDriverService() {
        return getRetrofit(BASE_URL).create(DriverService.class);
    }

    public static OrderService getOrderService() {
        return getRetrofit(BASE_URL).create(OrderService.class);
    }

    public static RegionService getRegionService(){
        return getRetrofit(BASE_URL).create(RegionService.class);
    }

    public static PushMessageService getPushMessageService(){
        return getRetrofit(BASE_URL).create(PushMessageService.class);
    }

    @NonNull
    public static GoogleMapsService getMapService() {
        return getRetrofit("https://maps.googleapis.com/").create(GoogleMapsService.class);
    }

    @NonNull
    private static Retrofit getRetrofit(String baseUrl) {
        CLIENT.readTimeout(30, TimeUnit.SECONDS);
        CLIENT.connectTimeout(30, TimeUnit.SECONDS);

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .create();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(CLIENT.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
}
