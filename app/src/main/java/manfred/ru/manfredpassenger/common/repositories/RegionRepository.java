package manfred.ru.manfredpassenger.common.repositories;

import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.RegionAnswer;
import manfred.ru.manfredpassenger.common.models.ListRegionAnswer;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;

public interface RegionRepository {
    void getRegions(NetworkResponseCallback<ListRegionAnswer> responseCallback);

    void setRegionById(int id, NetworkResponseCallback<RegionAnswer> responseCallback);

    void setRegionByCords(ManfredLocation location, NetworkResponseCallback<RegionAnswer> responseCallback);
}
