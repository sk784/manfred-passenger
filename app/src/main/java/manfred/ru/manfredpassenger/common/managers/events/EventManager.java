package manfred.ru.manfredpassenger.common.managers.events;

import java.util.List;

import manfred.ru.manfredpassenger.api.services.models.event.Event;

public interface EventManager {
    void newEvents(List<Event> events);
    void newEvent(Event event);
}
