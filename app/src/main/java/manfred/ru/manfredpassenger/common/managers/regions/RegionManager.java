package manfred.ru.manfredpassenger.common.managers.regions;

import android.arch.lifecycle.LiveData;
import android.support.annotation.Nullable;

import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.common.models.PaymentSystem;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.utils.SingleLiveEvent;

public interface RegionManager {
    enum PaymentSystemType {
        FONDY_MSK,
        FONDY_KZ,
        CLOUDPAYMENTS_MSK,
        CLOUDPAYMENTS_KZ,
        UNKNOWN
    }
    interface WorkDoneCallback{
        void done();
        void error(String text);
    }
    void setRegionAndZoomToCenter(int id, WorkDoneCallback done);
    void setRegionById(int id);
    void setRegion(ManfredLocation manfredLocation);

    void refreshRegions();

    LiveData<List<Region>>getRegions();
    LiveData<Region>getCurrentRegion();
    LiveData<ManfredNetworkError> getRegionError();
    SingleLiveEvent<ManfredLocation> getCenterOfCurrentRegion();
    PaymentSystemType getCurrentPaymentSystemType();
    @Nullable PaymentSystem getCurrentPaymentSystem();
}
