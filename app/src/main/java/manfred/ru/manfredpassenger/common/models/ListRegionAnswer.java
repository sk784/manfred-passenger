package manfred.ru.manfredpassenger.common.models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListRegionAnswer {
    @SerializedName("regions")
    private final List<Region>regions;

    @SerializedName("current_region_id")
    @Nullable private final Integer currRegionId;

    public ListRegionAnswer(List<Region> regions, int currRegionId) {
        this.regions = regions;
        this.currRegionId = currRegionId;
    }

    public List<Region> getRegions() {
        return regions;
    }

    @Nullable public Integer getCurrRegionId() {
        return currRegionId;
    }
}
