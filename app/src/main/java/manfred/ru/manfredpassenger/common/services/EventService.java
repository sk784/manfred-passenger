package manfred.ru.manfredpassenger.common.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.arch.lifecycle.LifecycleService;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.concurrent.ScheduledExecutorService;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.managers.ManfredNetworkManager;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.model.EventProcessedAnswer;
import manfred.ru.manfredpassenger.api.model.ManfredEventNetworkReceiver;
import manfred.ru.manfredpassenger.api.model.NewEventAnswer;
import manfred.ru.manfredpassenger.common.managers.events.EventServiceManager;
import manfred.ru.manfredpassenger.common.managers.events.ManfredEventManager;

public class EventService extends LifecycleService {
    private ScheduledExecutorService checkForEventsTask;
    private NetworkManager networkManager;
    private final IBinder binder = new LocalBinder();
    private static final String TAG = "EventService";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ManfredPassengerApplication manfredPassengerApplication = ((ManfredPassengerApplication)getApplication());
        initTask(manfredPassengerApplication.getEventManager(),
                manfredPassengerApplication.getServiceManager());
        //networkManager = new ManfredNetworkManager();
        //toForeground();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        stopTasks();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        super.onBind(intent);
        return binder;
    }

    public NetworkManager getNetworkManager(){
        if(networkManager==null){
            networkManager=new ManfredNetworkManager();
        }
        return networkManager;
    }

    private void stopTasks() {
        if (checkForEventsTask != null) {
            checkForEventsTask.shutdown();
            checkForEventsTask = null;
        }
    }

    public class LocalBinder extends Binder{
        EventService getService(){
            return EventService.this;
        }
    }

    private void toForeground() {
        String channelId;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel();
        } else {
            // If earlier version channel ID is not used
            // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
            channelId = "";
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher_round))
                .setSmallIcon(R.drawable.ic_status_bar_icon)
                .setContentTitle("ManfredPassenger")
                .setContentText("Сервис получения событий активен");
        //.setContentIntent(pendingIntent);
        Notification notification = notificationBuilder.build();
        startForeground(6060, notification);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel() {
        String channelId = "passenger_event_service";
        String channelName = "Manfred passenger";
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (service != null) {
            service.createNotificationChannel(chan);
        }
        return channelId;
    }

    private ManfredEventNetworkReceiver createEventListener(ManfredEventManager manfredEventManager) {
        Log.d(TAG, "createEventListener: ");
        return new ManfredEventNetworkReceiver(new ManfredEventNetworkReceiver.NewEventCallback() {
            @Override
            public void newEvent(NewEventAnswer event) {
                Log.d(TAG, "eventListener: id of event " + event.getId());
                getNetworkManager().sendEventAcceptedRequest(new EventProcessedAnswer(event.getId()));
                Log.d(TAG, "newEvent: sendedAccept");
                manfredEventManager.newEvent(event.getPayload());
                Log.d(TAG, "newEvent: complete processing");

            }
        });
    }

    private void initTask(ManfredEventManager eventManager, EventServiceManager serviceManager) {
        Log.d(TAG, "initTask: ");
        getNetworkManager().addEventReceiver(createEventListener(eventManager));

        serviceManager.getIsForeground().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    if(aBoolean){
                        if(((ManfredPassengerApplication) getApplication()).getAppStatusManager().isRidingNow()) {
                            toForeground();
                        }else {
                            ((ManfredPassengerApplication) getApplication()).getNetworkManager().clearQueue();
                            networkManager.disconnect();
                        }
                    }else {
                        stopForeground(true);
                    }
                }
            }
        });
    }

}
