package manfred.ru.manfredpassenger.common.managers.regions;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.RegionAnswer;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.managers.CreditCardManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.models.ListRegionAnswer;
import manfred.ru.manfredpassenger.common.models.PaymentSystem;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.common.repositories.RegionRepository;
import manfred.ru.manfredpassenger.common.repositories.WebsocketRegionRepository;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;
import manfred.ru.manfredpassenger.ride.managers.TariffManager;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.utils.SingleLiveEvent;

public class ManfredRegionManager implements RegionManager {
    private static final String TAG = "ManfredRegionManager";
    private final TariffManager tariffManager;
    private final AppStatusManager appStatusManager;
    private final PromoCodeManager promoCodeManager;
    private final CreditCardManager creditCardManager;
    private final RegionRepository regionRepository;
    //private final OrderManager orderManager;

    private MutableLiveData<ManfredNetworkError> regionError;
    private MutableLiveData<List<Region>>regions;
    private MutableLiveData<Region>currRegion;
    private final SingleLiveEvent<ManfredLocation>centerOfSelectedRegion;
    private PaymentSystemType paymentSystemType;
    private PaymentSystem currPaymentSystem;

    public ManfredRegionManager(TokenManager tokenManager,
                                TariffManager tariffManager,
                                AppStatusManager appStatusManager,
                                PromoCodeManager promoCodeManager,
                                CreditCardManager creditCardManager,
                                NetworkManagerProvider networkManager) {
        this.regionRepository = new WebsocketRegionRepository(networkManager,tokenManager);
        this.tariffManager = tariffManager;
        this.appStatusManager = appStatusManager;
        this.promoCodeManager = promoCodeManager;
        this.creditCardManager = creditCardManager;
        this.regionError = new MutableLiveData<>();
        this.regions = new MutableLiveData<>();
        this.currRegion = new MutableLiveData<>();
        centerOfSelectedRegion = new SingleLiveEvent<>();
        refreshRegions();
        initCardsObserver();
    }

    private void initCardsObserver(){
        creditCardManager.cards().observeForever(new Observer<List<CreditCard>>() {
            @Override
            public void onChanged(@Nullable List<CreditCard> creditCards) {
                if(isNoCardsInRegion(currPaymentSystem, creditCards)){
                    Log.d(TAG, "setCurrentPaymentSystem: no cards in region");
                    appStatusManager.needCardInRegion();
                }else {
                    appStatusManager.swithToReadyIfNotOnTrip();
                }
            }
        });
    }

    @Override
    public void setRegionAndZoomToCenter(int id, WorkDoneCallback done) {
        Log.d(TAG, "setRegionAndZoomToCenter: ");
        regionRepository.setRegionById(id, new NetworkResponseCallback<RegionAnswer>() {
            @Override
            public void allOk(RegionAnswer response) {
                reInitRegions(response);
                centerOfSelectedRegion.postValue(new ManfredLocation(Double.parseDouble(response.getRegion().getCenterLatitude()),
                        Double.parseDouble(response.getRegion().getCenterLongitude())));
                done.done();
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "setRegionAndZoomToCenter error: "+error.toString());
                regionError.postValue(error);
                done.error(error.getText());
            }
        });
    }

    @Override
    public void setRegionById(int id) {
        Log.d(TAG, "setRegionById: ");
        regionRepository.setRegionById(id, new NetworkResponseCallback<RegionAnswer>() {
            @Override
            public void allOk(RegionAnswer response) {
                reInitRegions(response);
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: "+error.toString());
                regionError.postValue(error);
            }
        });
    }

    private void reInitRegions(RegionAnswer response) {
        Log.d(TAG, "reInitRegions: ");
        tariffManager.refreshTariffs();
        promoCodeManager.refreshPromoCodes();
        currRegion.postValue(response.getRegion());
        setCurrentPaymentSystem(response.getRegion());
    }

    /**
     * @return true if have cards, but out of region
     */
    private boolean isNoCardsInRegion(PaymentSystem paymentSystem, List<CreditCard>creditCards){
        //isAvailable = currentPaymentSystem.getId() == creditCard.getPaymentSystem().getId();
        //List<CreditCard>creditCards = creditCardManager.cards().getValue();
        if(creditCards==null || creditCards.isEmpty())return true;
        if(paymentSystem==null)return false;
        for (CreditCard creditCard : creditCards){
            if(creditCard.getPaymentSystem().getId()==paymentSystem.getId()){
                return false;
            }
        }
        return true;
    }

    @Override
    public void setRegion(ManfredLocation manfredLocation) {
        Log.d(TAG, "setRegion: by location "+ manfredLocation);
        regionRepository.setRegionByCords(manfredLocation, new NetworkResponseCallback<RegionAnswer>() {
            @Override
            public void allOk(RegionAnswer response) {
                reInitRegions(response);
            }

            @Override
            public void error(ManfredNetworkError error) {
                regionError.postValue(new ManfredNetworkError(ManfredNetworkError.ErrorType.CANT_SET_REGION,error.getText()));
            }
        });
    }

    private void setCurrentRegion(int regionId, List<Region> regions){
        Log.d(TAG, "setCurrentRegion: "+regionId);
        if(regions==null){
            //Log.d(TAG, "setCurrentRegion: regions is null");
            return;
        }
        for (Region localRegion : regions){
            if(localRegion.getId()==regionId){
                Log.d(TAG, "setCurrentRegion: setted to "+localRegion);
                currRegion.postValue(localRegion);
                setCurrentPaymentSystem(localRegion);
            }
        }
    }

    private void setCurrentPaymentSystem(Region localRegion) {
        Log.d(TAG, "setCurrentPaymentSystem: "+localRegion);
        if(localRegion.getPaymentSystem()==null){
            paymentSystemType= PaymentSystemType.UNKNOWN;
            currPaymentSystem=null;
            return;
        }
        switch (localRegion.getPaymentSystem().getId()){
            case 1:
                paymentSystemType= PaymentSystemType.FONDY_MSK;
                break;
            case 2:
                paymentSystemType= PaymentSystemType.CLOUDPAYMENTS_MSK;
                break;
            case 3:
                paymentSystemType= PaymentSystemType.FONDY_KZ;
                break;
            case 4:
                paymentSystemType= PaymentSystemType.CLOUDPAYMENTS_KZ;
                break;
        }
        currPaymentSystem=localRegion.getPaymentSystem();
        creditCardManager.refreshCards();
    }

    @Override
    public void refreshRegions(){
        Log.d(TAG, "refreshRegions: ");
        regionRepository.getRegions(new NetworkResponseCallback<ListRegionAnswer>() {
            @Override
            public void allOk(ListRegionAnswer response) {
                Log.d(TAG, "getRegions allOk: current region is "+response.getCurrRegionId()+", available regions is "+response.getRegions());
                regions.postValue(response.getRegions());
                if(response.getCurrRegionId()==null){ //it is new user, initialising
                    regionError.postValue(new ManfredNetworkError(ManfredNetworkError.ErrorType.CANT_SET_REGION,"регион не задан"));
                }else {
                    setCurrentRegion(response.getCurrRegionId(), response.getRegions());
                }
                creditCardManager.refreshCards();
            }

            @Override
            public void error(ManfredNetworkError error) {
                regionError.postValue(new ManfredNetworkError(ManfredNetworkError.ErrorType.CANT_SET_REGION,error.getText()));
            }
        });
    }

    @Override
    public LiveData<List<Region>> getRegions() {
        return regions;
    }

    @Override
    public LiveData<Region> getCurrentRegion() {
        return currRegion;
    }

    @Override
    public LiveData<ManfredNetworkError> getRegionError() {
        return regionError;
    }

    @Override
    public SingleLiveEvent<ManfredLocation> getCenterOfCurrentRegion() {
        return centerOfSelectedRegion;
    }

    @Override
    public PaymentSystemType getCurrentPaymentSystemType() {
        return paymentSystemType;
    }

    @Nullable
    @Override
    public PaymentSystem getCurrentPaymentSystem() {
        return currPaymentSystem;
    }
}
