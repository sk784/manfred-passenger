package manfred.ru.manfredpassenger.common.managers.events;

import android.util.Log;

import java.util.Date;
import java.util.List;

import manfred.ru.manfredpassenger.api.services.models.event.Event;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.history.managers.ManfredHistoryManager;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;
import manfred.ru.manfredpassenger.ride.managers.DebtManager;
import manfred.ru.manfredpassenger.ride.managers.OrderManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatus;
import manfred.ru.manfredpassenger.ride.managers.PreOrderManager;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public class ManfredEventManager implements EventManager {
    private final OrderManager orderManager;
    private final ManfredHistoryManager historyManager;
    private final DebtManager debtManager;
    private final PreOrderManager preOrderManager;
    private final PromoCodeManager promoCodeManager;
    private final AnalyticsManager analyticsManager;
    private static final String TAG = "ManfredEventManager";

    public ManfredEventManager(OrderManager orderManager, ManfredHistoryManager historyManager,
                               DebtManager debtManager, PreOrderManager preOrderManager, PromoCodeManager promoCodeManager, AnalyticsManager analyticsManager) {
        this.orderManager = orderManager;
        this.historyManager = historyManager;
        this.debtManager = debtManager;
        this.preOrderManager = preOrderManager;
        this.promoCodeManager = promoCodeManager;
        this.analyticsManager = analyticsManager;
    }

    @Override
    public void newEvents(List<Event> events) {
        if (events.isEmpty()) return;

        /*
        Event currEvent=events.get(events.size()-1);
        processEvent(currEvent);*/
        for (Event event : events){
            processEvent(event);
        }

    }

    @Override
    public void newEvent(Event event) {
        Log.d(TAG, "newEvent: "+event);
        processEvent(event);
    }

    private void processEvent(Event event) {
        if (event.getType() != null) {
            Log.d("sk7847", event.getType());
            Log.d(TAG, "processEvent: id of order is "+event.getId()+", type is "+event.getType());
            switch (event.getType()) {
                case "driver_found":
                    analyticsManager.searchCar(event,"success");
                    orderManager.carRideToPassenger(new OrderVM(event));
                    break;
                case "no_free_drivers":
                    if(event.getInitialCarNeedTime()!=null && !event.getInitialCarNeedTime().isEmpty()) {
                        preOrderManager.preOrderTimeOut(new OrderVM(event));
                    }
                    orderManager.noFreeDrivers(new OrderVM(event));
                    analyticsManager.searchCar(event,"fail");
                    break;
                case "initial_car_need_time": //it is for preOrder with assistant
                    preOrderManager.preOrderTimeOut(new OrderVM(event));
                    break;
                case "accept_pre_order_timeout":
                    //preOrderManager.preOrderTimeOut(new OrderVM(event));
                    break;
                case "driver_ready":
                    orderManager.carOnPassenger(new OrderVM(event));
                    break;
                case "start_ride":
                    orderManager.riding(new OrderVM(event));
                    break;
                case "order_completed":
                    orderManager.completed(new OrderVM(event));
                    historyManager.refreshHistory();
                    debtManager.refreshDebts();
                    preOrderManager.refreshPreOrders();
                    promoCodeManager.refreshPromoCodes();
                    analyticsManager.rideSuccess(new Date(), event);
                    break;
                case "order_completed_with_debt":
                    orderManager.orderCanceledPrePaymentError(event.getId(), "Ошибка оплаты заказа, поездка отменена");
                    debtManager.refreshDebts();
                    break;
                case "next_order_payment_error":
                    orderManager.currTripPaymentError(event);
                    break;
                case "driver_move_to_passenger_delay":
                    orderManager.driverDelay(new OrderVM(event));
                    break;
                case "prepayment_error":
                    orderManager.orderCanceledPrePaymentError(event.getId(), "Ошибка оплаты заказа, поездка отменена");
                    debtManager.refreshDebts();
                    break;
            }
        }else {
            Log.d("sk7847", event.getStatus());
            switch (event.getStatus()) {
                case "SEARCH_DRIVER":
                    if(event.getInitialCarNeedTime()==null || event.getInitialCarNeedTime().isEmpty()) {
                        orderManager.searchingCar(new OrderVM(event));
                    }
                    break;
                case "RIDE_TO_PASSENGER":
                    analyticsManager.searchCar(event,"success");
                    orderManager.carRideToPassenger(new OrderVM(event));
                    break;
                case "WAIT_PASSENGER":
                    orderManager.carOnPassenger(new OrderVM(event));
                    break;
                case "RIDE":
                    orderManager.riding(new OrderVM(event));
                    break;
                case "PREPAYMENT_ERROR":
                    orderManager.orderCanceledPrePaymentError(event.getId(), "Ошибка оплаты заказа, поездка отменена");
                    debtManager.refreshDebts();
                    break;
                case "NEXT_ORDER_PAYMENT_ERROR":
                    orderManager.currTripPaymentError(event);
                    break;
            }
        }
    }

    private AppStatus mapStatus(Event event) {
        String status = event.getStatus();
        AppStatus appStatus = null;
        if (status.equals("RIDE_TO_PASSENGER")) {
            appStatus = AppStatus.CAR_RIDE_TO_PASSENGER;
        }
        return appStatus;
    }
}
