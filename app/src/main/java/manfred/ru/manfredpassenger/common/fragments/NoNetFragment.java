package manfred.ru.manfredpassenger.common.fragments;


import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoNetFragment extends Fragment {
    private ViewGroup.LayoutParams prevParentParams;

    public NoNetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        View rootView = inflater.inflate(R.layout.fragment_no_net, container, false);
        rootView.setOnClickListener(null);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getView()!=null) {
            ViewGroup container = ((ViewGroup) getView().getParent());
            container.setLayoutParams(prevParentParams);
        }
    }

}
