package manfred.ru.manfredpassenger.common.models;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PushMessage {
	@SerializedName("id")
	private int id;

	@SerializedName("caption")
	private String caption;

	@SerializedName("short_description")
	private String shortDescription;

	@SerializedName("content")
	private String content;

	@SerializedName("is_read")
	private Boolean isRead;

	public int getId() {
		return id;
	}

	public String getCaption() {
		return caption;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public String getContent() {
		return content;
	}

	public boolean isRead() {
		return isRead != null && isRead.booleanValue();
	}
}
