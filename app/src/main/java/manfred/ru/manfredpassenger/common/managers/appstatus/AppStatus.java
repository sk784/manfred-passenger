package manfred.ru.manfredpassenger.common.managers.appstatus;

public enum AppStatus {
    NEED_CARD,
    OUT_OF_SERVICE,
    LOCATION_PERMISSION_DENIED,
    SHOWING_TARIFF_DETAILS,
    READY_TO_ORDER,
    MAKING_ORDER,
    ADDING_COMMENT,
    MAKING_PRE_ORDER,
    //MAKING_PREORDER_TIME_SETTED,
    SELECTING_TIME_FOR_PREORDER,
    SELECTING_CARD_FOR_REPAY_LAST_DEBT,
    SELECTING_CARD_FOR_REPAY_CURRENT_ORDER,
    PAYMENT_ERROR,
    HAVE_DEBT,
    SELECT_CARDS,
    PICKING_CARDS,
    SEARCH_CAR,
    SEARCH_CAR_TIME_OUT,
    CAR_ON_PASSENGER,
    CAR_RIDE_TO_PASSENGER,
    RIDING,
    SHOWING_PREORDER_DETAILS,
    COMPLETED
}
