package manfred.ru.manfredpassenger.common.models;

import com.google.gson.annotations.SerializedName;

public class RegionIdQuery {
    @SerializedName("region_id")
    private final int regionId;

    public RegionIdQuery(int regionId) {
        this.regionId = regionId;
    }

    public int getRegionId() {
        return regionId;
    }
}
