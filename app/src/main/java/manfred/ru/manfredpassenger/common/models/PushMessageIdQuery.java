package manfred.ru.manfredpassenger.common.models;

import com.google.gson.annotations.SerializedName;

public class PushMessageIdQuery {
    @SerializedName("id")
    private final int messageId;

    public PushMessageIdQuery(int messageId) {
        this.messageId = messageId;
    }

    public int getMessageId() {
        return messageId;
    }
}
