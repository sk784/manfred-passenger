package manfred.ru.manfredpassenger.common.services;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;

public class NetworkManagerProvider {
    private static final String TAG = "NetworkManagerProvider";
    private EventService eventService;
    private boolean bound = false;
    private List<Query> queryQueue = new ArrayList<>();

    public NetworkManagerProvider(Context context) {
        Log.d(TAG, "NetworkManagerProvider: creating...");
        Intent startServiceIntent = new Intent(context,EventService.class);
        context.bindService(startServiceIntent,serviceConnection,Context.BIND_AUTO_CREATE);
    }

    public <T, V> void addQuery(@Nullable String token, String path, @NonNull T payload, WebSocketAnswerCallback callback){
        if(bound){
            //Log.d(TAG, "connected to service, sending transparent to network manager ");
            eventService.getNetworkManager().addQuery(token,path,payload,callback);
        }else {
            //Log.d(TAG, "addQuery: to queue "+path);
            queryQueue.add(new Query<T,V>(token,path,payload,callback));
        }
    }

    public <V>void addQuery(@Nullable String token, String path, WebSocketAnswerCallback callback){
        if(bound){
            //Log.d(TAG, "connected to service, sending transparent to network manager ");
            eventService.getNetworkManager().addQuery(token,path,callback);
        }else {
            //Log.d(TAG, "addQuery: to queue "+path);
            queryQueue.add(new Query<Void,V>(token,path,null,callback));
        }
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            if(service instanceof EventService.LocalBinder) {
                EventService.LocalBinder binder = (EventService.LocalBinder) service;
                connected(binder.getService());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;
        }
    };

    private void connected(EventService eventService){
        Log.d(TAG, "connected: ");
        this.eventService=eventService;
        bound = true;
        for (Query query : queryQueue){
            Log.d(TAG, "connected: send query from queue "+query.path);
            query.sendQueryToManager(eventService.getNetworkManager());
        }
        queryQueue.clear();
        Log.d(TAG, "connected: queue cleared");
    }

    private class Query<T,V>{
        private final String token;
        private final String path;
        private final T payload;
        private final WebSocketAnswerCallback callback;

        public Query(String token, String path, T payload, WebSocketAnswerCallback callback) {
            this.token = token;
            this.path = path;
            this.payload = payload;
            this.callback = callback;
        }

        public Query(String token, String path, WebSocketAnswerCallback callback) {
            this.token = token;
            this.path = path;
            this.callback = callback;
            payload = null;
        }

        public void sendQueryToManager(NetworkManager networkManager){
            if(payload==null){
                networkManager.addQuery(token,path,callback);
            }else {
                networkManager.addQuery(token,path,payload,callback);
            }
        }
    }

    void clearQueue(){
        queryQueue.clear();
    }
}
