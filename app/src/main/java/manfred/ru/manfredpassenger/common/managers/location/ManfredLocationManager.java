package manfred.ru.manfredpassenger.common.managers.location;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.location.LocationListener;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class ManfredLocationManager {
    private static final long MIN_UPDATE_INTERVAL = 1000;
    private static final long MAX_UPDATE_INTERVAL = 3000;


    public static void bindLocationListenerIn(LifecycleOwner lifecycleOwner,
                                              LocationListener listener, Context context) {
        new BoundLocationListener(lifecycleOwner, listener, context);
    }

    @SuppressWarnings("MissingPermission")
    static class BoundLocationListener implements LifecycleObserver {
        private final Context mContext;
        //private LocationManager mLocationManager;
        private final LocationListener mListener;
        FusedLocationProviderClient locationProviderClient;
        LocationCallback locationCallback;

        public BoundLocationListener(LifecycleOwner lifecycleOwner,
                                     LocationListener listener, Context context) {
            mContext = context;
            mListener = listener;
            lifecycleOwner.getLifecycle().addObserver(this);
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        void addLocationListener() {
            Log.d("BoundLocationMgr", "Listener added");
            locationProviderClient = new FusedLocationProviderClient(mContext);
            locationProviderClient.getLastLocation().addOnSuccessListener(location -> {
                if(location!=null) {
                    mListener.onLocationChanged(location);
                }
            });
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setInterval(MAX_UPDATE_INTERVAL);
            locationRequest.setFastestInterval(MIN_UPDATE_INTERVAL);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            Looper looper = Looper.myLooper();
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    //Log.d(TAG, "onLocationResult: location updated to "+locationResult);
                    mListener.onLocationChanged(locationResult.getLastLocation());
                }
            };
            locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, looper);
        }


        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        void removeLocationListener() {
            if (locationProviderClient == null) {
                return;
            }
            locationProviderClient.removeLocationUpdates(locationCallback);
            locationProviderClient = null;

            Log.d("BoundLocationMgr", "Listener removed");
        }
    }
}
