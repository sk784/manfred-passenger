package manfred.ru.manfredpassenger.common.repositories;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.services.models.RegionAnswer;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.models.ListRegionAnswer;
import manfred.ru.manfredpassenger.common.models.RegionIdQuery;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.utils.NetworkUtils;

public class WebsocketRegionRepository implements RegionRepository {
    private final NetworkManagerProvider networkManager;
    private final TokenManager tokenManager;

    public WebsocketRegionRepository(NetworkManagerProvider networkManager, TokenManager tokenManager) {
        this.networkManager = networkManager;
        this.tokenManager = tokenManager;
    }

    @Override
    public void getRegions(NetworkResponseCallback<ListRegionAnswer> responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(),"region/list", new WebSocketAnswerCallback<WebSocketAnswer<ListRegionAnswer>>() {
            @Override
            public void messageReceived(WebSocketAnswer<ListRegionAnswer> message) {
                new NetworkUtils().validateSocketResponse(message,responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return ListRegionAnswer.class;
            }
        });
    }

    @Override
    public void setRegionById(int id, NetworkResponseCallback<RegionAnswer> responseCallback) {

        networkManager.addQuery(tokenManager.getAuthToken(),"region/set",new RegionIdQuery(id), new WebSocketAnswerCallback<WebSocketAnswer<RegionAnswer>>() {
            @Override
            public void messageReceived(WebSocketAnswer<RegionAnswer> message) {
                new NetworkUtils().validateSocketResponse(message,responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return RegionAnswer.class;
            }
        });
    }

    @Override
    public void setRegionByCords(ManfredLocation location, NetworkResponseCallback<RegionAnswer> responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(),"region/set_by_geo",location.toLatLng(), new WebSocketAnswerCallback<WebSocketAnswer<RegionAnswer>>() {
            @Override
            public void messageReceived(WebSocketAnswer<RegionAnswer> message) {
                new NetworkUtils().validateSocketResponse(message,responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return RegionAnswer.class;
            }
        });
    }
}
