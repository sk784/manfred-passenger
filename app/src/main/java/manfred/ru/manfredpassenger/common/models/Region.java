package manfred.ru.manfredpassenger.common.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Region{

	@SerializedName("center_latitude")
	private String centerLatitude;

	@SerializedName("country")
	private String country;

	@SerializedName("center_longitude")
	private String centerLongitude;

	@SerializedName("payment_system")
	private PaymentSystem paymentSystem;

	@SerializedName("support_phone")
	private String supportPhone;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	public String getCenterLatitude(){
		return centerLatitude;
	}

	public String getCountry(){
		return country;
	}

	public String getCenterLongitude(){
		return centerLongitude;
	}

	public PaymentSystem getPaymentSystem(){
		return paymentSystem;
	}

	public Object getSupportPhone(){
		return supportPhone;
	}

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}

	public void setCenterLatitude(String centerLatitude) {
		this.centerLatitude = centerLatitude;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setCenterLongitude(String centerLongitude) {
		this.centerLongitude = centerLongitude;
	}

	public void setPaymentSystem(PaymentSystem paymentSystem) {
		this.paymentSystem = paymentSystem;
	}

	public void setSupportPhone(String supportPhone) {
		this.supportPhone = supportPhone;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
 	public String toString(){
		return 
			"Region{" + 
			"center_latitude = '" + centerLatitude + '\'' + 
			",country = '" + country + '\'' + 
			",center_longitude = '" + centerLongitude + '\'' + 
			",payment_system = '" + paymentSystem + '\'' + 
			",support_phone = '" + supportPhone + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}