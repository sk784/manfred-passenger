package manfred.ru.manfredpassenger.common.managers.appstatus;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.LinkedList;

public class ManfredAppStatusManager implements AppStatusManager {
    private final MutableLiveData<AppStatus> appStatus;
    private final LinkedList<AppStatus> statusHistory;
    @Nullable private AppStatus statusBeforePayInTripError;
    private static final String TAG = "ManfredAppStatusManager";
    private boolean isNewUserNeedCard=false;

    public ManfredAppStatusManager() {
        Log.d(TAG, "ManfredAppStatusManager: creating");
        this.appStatus = new MutableLiveData<>();
        this.appStatus.setValue(AppStatus.READY_TO_ORDER);
        this.statusHistory = new LinkedList<>();
        this.statusHistory.add(AppStatus.READY_TO_ORDER);
    }

    @Override
    public LiveData<AppStatus> getCurrentStatus() {
        return appStatus;
    }

    @Override
    public void readyToOrder() {
        Log.d(TAG, "readyToOrder: ");
        changeStateAndClearHistory(AppStatus.READY_TO_ORDER);
    }

    @Override
    public void makingOrder() {
        changeStateWithHistory(AppStatus.MAKING_ORDER);
    }

    @Override
    public void makingPreOrder() {
        changeStateWithHistory(AppStatus.MAKING_PRE_ORDER);
    }

    @Override
    public void showCardList() {
        changeStateWithHistory(AppStatus.SELECT_CARDS);
    }

    @Override
    public void selectActiveCard() {
        changeStateWithHistory(AppStatus.PICKING_CARDS);
    }

    @Override
    public void selectCardForRePayLastDebt() {
        changeStateWithHistory(AppStatus.SELECTING_CARD_FOR_REPAY_LAST_DEBT);
    }

    @Override
    public void addComment() {
        changeStateWithHistory(AppStatus.ADDING_COMMENT);
    }

    @Override
    public void searchingCar() {
        changeStateAndClearHistory(AppStatus.SEARCH_CAR);
    }

    @Override
    public void carRideToPassenger() {
        changeStateAndClearHistory(AppStatus.CAR_RIDE_TO_PASSENGER);
    }

    @Override
    public void carOnPassenger() {
        changeStateAndClearHistory(AppStatus.CAR_ON_PASSENGER);
    }

    @Override
    public void riding() {
        changeStateAndClearHistory(AppStatus.RIDING);
    }

    @Override
    public void completed() {
        changeStateAndClearHistory(AppStatus.COMPLETED);
    }

    @Override
    public void searchCarTimeOut() {
        changeStateAndClearHistory(AppStatus.SEARCH_CAR_TIME_OUT);
    }

    @Override
    public void showTariffDetails() {
        changeStateWithHistory(AppStatus.SHOWING_TARIFF_DETAILS);
    }

    @Override
    public void selectTimeForPreOrder() {
        changeStateWithHistory(AppStatus.SELECTING_TIME_FOR_PREORDER);
    }

    @Override
    public void showPreOrderDetails() {
        changeStateWithHistory(AppStatus.SHOWING_PREORDER_DETAILS);
    }

    @Override
    public void haveDebt() {
        changeStateAndClearHistory(AppStatus.HAVE_DEBT);
    }

    @Override
    public boolean isRidingNow() {
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus == AppStatus.SEARCH_CAR |
                currAppStatus == AppStatus.CAR_RIDE_TO_PASSENGER |
                currAppStatus == AppStatus.CAR_ON_PASSENGER |
                currAppStatus == AppStatus.RIDING |
                currAppStatus == AppStatus.PAYMENT_ERROR;
    }

    @Override
    public boolean isOrderingNow() {
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus == AppStatus.MAKING_ORDER |
                currAppStatus == AppStatus.MAKING_PRE_ORDER;
    }

    @Override
    public boolean isReadyToOrder(){
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus == AppStatus.READY_TO_ORDER;
    }

    @Override
    public boolean isNeedRefreshMarkerState() {
        AppStatus currAppStatus = appStatus.getValue();
        boolean isNeedRefresh = currAppStatus == AppStatus.READY_TO_ORDER | currAppStatus == AppStatus.OUT_OF_SERVICE;
        //Log.d(TAG, "isNeedRefreshMarkerState: "+currAppStatus+" need? "+isNeedRefresh);
        return isNeedRefresh;
    }

    @Override
    public boolean isSearchingCar() {
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus == AppStatus.SEARCH_CAR;
    }

    @Override
    public boolean isFinishing() {
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus == AppStatus.COMPLETED;
    }

    @Override
    public boolean isShowingPreOrderDetails() {
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus == AppStatus.SHOWING_PREORDER_DETAILS;
    }

    @Override
    public void cancelCurrentState() {
        Log.d(TAG, "cancelCurrentState: ");
        if(statusHistory.isEmpty()){
            changeStateAndClearHistory(AppStatus.READY_TO_ORDER);
            return;
        }
        AppStatus prevStatus = statusHistory.getLast();
        appStatus.postValue(prevStatus);
    }

    @Override
    public void paymentErrorOnCurrentOrder() {
        statusBeforePayInTripError=getCurrentStatus().getValue();
        changeStateAndClearHistory(AppStatus.PAYMENT_ERROR);
    }

    @Override
    public void selectCardForRePayCurrentOrder() {
        changeStateWithHistory(AppStatus.SELECTING_CARD_FOR_REPAY_CURRENT_ORDER);
    }

    @Override
    public void currOrderSuccessfullyPaid() {
        changeStateAndClearHistory(AppStatus.RIDING);
        statusBeforePayInTripError=null;
    }

    @Override
    public void outOfRegion() {
        if(!isRidingNow()) {
            changeStateAndClearHistory(AppStatus.OUT_OF_SERVICE);
        }
    }

    @Override
    public void switchToPreOrder() {
        AppStatus currStatus = appStatus.getValue();
        if(currStatus==AppStatus.MAKING_ORDER){
            changeStateAndClearHistory(AppStatus.MAKING_PRE_ORDER);
        }else if(currStatus==AppStatus.OUT_OF_SERVICE){
            readyToOrder();
        }
    }

    @Override
    public void switchToOrder() {
        AppStatus currStatus = appStatus.getValue();
        if(currStatus==AppStatus.MAKING_PRE_ORDER){
            changeStateAndClearHistory(AppStatus.MAKING_ORDER);
        }else if(currStatus==AppStatus.OUT_OF_SERVICE){
            readyToOrder();
        }

    }

    @Override
    public void swithToReadyIfNotOnTrip() {
        if(!isRidingNow()){
            readyToOrder();
        }
    }

    @Override
    public boolean isOutOfRegion() {
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus == AppStatus.OUT_OF_SERVICE;
    }

    @Override
    public void locationPermissionDenied() {
        changeStateAndClearHistory(AppStatus.LOCATION_PERMISSION_DENIED);
    }

    @Override
    public void needCardInRegion() {
        Log.d(TAG, "needCardInRegion: ");
        changeStateAndClearHistory(AppStatus.NEED_CARD);
    }


    @Override
    public void needCardNewUser() {
        Log.d(TAG, "needCardNewUser: ");
        changeStateAndClearHistory(AppStatus.NEED_CARD);
        isNewUserNeedCard=true;
    }

    private void changeStateWithHistory(AppStatus newStatus) {
        //if(appStatus.getValue()==AppStatus.HAVE_DEBT)return;
        if(newStatus==appStatus.getValue())return;
        if(newStatus==appStatus.getValue())return;
        if(isNewUserNeedCard)return;
        statusHistory.add(appStatus.getValue());
        appStatus.postValue(newStatus);
    }

    @Override
    public void skipNeedingCard() {
        isNewUserNeedCard=false;
        changeStateAndClearHistory(AppStatus.READY_TO_ORDER);
    }

    @Override
    public boolean isNeedCard() {
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus==AppStatus.NEED_CARD;
    }

    @Override
    public boolean isLocationPermissionDenied() {
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus==AppStatus.LOCATION_PERMISSION_DENIED;
    }

    @Override
    public boolean isShowingTariffDetails() {
        AppStatus currAppStatus = appStatus.getValue();
        return currAppStatus==AppStatus.SHOWING_TARIFF_DETAILS;
    }

    private void changeStateAndClearHistory(AppStatus newStatus){
        if(newStatus==appStatus.getValue())return;
        Log.d(TAG, "changeStateAndClearHistory: new status is "+newStatus+", curr status is "+appStatus.getValue());
        if(appStatus.getValue()==AppStatus.HAVE_DEBT)return;
        //prevent close after delete card
        if(appStatus.getValue()==AppStatus.SELECT_CARDS)return;
        //if(appStatus.getValue()==AppStatus.PAYMENT_ERROR)return;
        if(isNewUserNeedCard)return;
        appStatus.postValue(newStatus);
        Log.d(TAG, "changeStateAndClearHistory: status changed to "+newStatus);
        statusHistory.clear();
    }
}
