package manfred.ru.manfredpassenger.common.managers.appstatus;

import android.arch.lifecycle.LiveData;

public interface AppStatusManager {
    LiveData<AppStatus>getCurrentStatus();

    void readyToOrder();
    void makingOrder();
    void makingPreOrder();

    void showCardList();
    void selectActiveCard();
    void selectCardForRePayLastDebt();
    void addComment();
    //void cancelCurrentState();
    void searchingCar();
    void carRideToPassenger();
    void carOnPassenger();
    void riding();
    void completed();
    void searchCarTimeOut();
    void showTariffDetails();
    void selectTimeForPreOrder();
    void showPreOrderDetails();
    void haveDebt();
    void cancelCurrentState();

    void paymentErrorOnCurrentOrder();
    void selectCardForRePayCurrentOrder();
    void currOrderSuccessfullyPaid();
    void outOfRegion();

    boolean isRidingNow();
    boolean isOrderingNow();
    boolean isReadyToOrder();
    boolean isNeedRefreshMarkerState();
    boolean isSearchingCar();
    boolean isFinishing();
    boolean isShowingPreOrderDetails();

    //it is for switching when no free drivers
    void switchToPreOrder();
    void switchToOrder();
    void swithToReadyIfNotOnTrip();

    boolean isOutOfRegion();

    void locationPermissionDenied();

    void needCardInRegion();
    void needCardNewUser();
    void skipNeedingCard();

    boolean isNeedCard();

    boolean isLocationPermissionDenied();

    boolean isShowingTariffDetails();
}
