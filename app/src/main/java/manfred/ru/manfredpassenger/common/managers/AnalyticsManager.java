package manfred.ru.manfredpassenger.common.managers;

import android.support.annotation.Nullable;

import java.util.Date;

import manfred.ru.manfredpassenger.api.services.models.event.Event;

public interface AnalyticsManager {

    /**
     * Успешная
     * регистрация (первое
     * открытие главного
     * экрана)
     */
    void registered();

    /**
     * Регистрация или
     * Изменение номера
     * телефона в профиле
     * (после подтверждения
     * через смс)
     * @param phone Номер телефона
     */
    void phoneSetted(String phone);

    /**
     * Изменение имени в
     * профиле
     * @param name Имя пользователя
     */
    void nameSetted(String name);

    /**
     * При запуске
     приложения с
     отключенной геолокацией
     (проверять настройки
     при запуске
     приложения)
     * @param enabled
     */
    void geolocationEnabled(boolean enabled);

    /**
     * @param date дата и время
     * @param cardNumber последние четыре цифры карты
     */
    void cardAdded(Date date, String cardNumber);

    void cardDeleted(Date date, String cardNumber);

    /**
     * Поиск автомобиля
     * @param result success / fail / cancel / error
     */
    void searchCar(Event event, String result);

    /**
     * Успешное завершение поездки
     * @param date Дата последней успешной поездки
     */
    void rideSuccess(Date date, Event event);

    /**
     * Отправлено
     * приглашение - Не нужно!
     * @param date
     * @param phone
     */
    void inviteSended(Date date, String phone);

    /**
     * Ошибка
     * добавления карты
     * (expired или
     * declined)
     * @param errorCode код отказа c fondy
     * @param description описание из поля text c fondy
     */
    void addingCardError(String errorCode, String description);


    /**
     * Прочитал
     * лицензионное
     * соглашение перед
     * регистрацией
     * @param date
     */
    void readAgreement(Date date);

    /**
     * Поставлена оценка
     * поездке
     * @param rate рейтинг
     */
    void tripRated(Event event, int rate);

}
