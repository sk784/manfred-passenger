package manfred.ru.manfredpassenger.common.repositories;

import android.support.annotation.NonNull;
import android.util.Log;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.RegionAnswer;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.models.ListRegionAnswer;
import manfred.ru.manfredpassenger.common.models.RegionIdQuery;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HTTPRegionRepository implements RegionRepository {
    private final TokenManager tokenManager;
    private static final String TAG = "RegionRepository";

    public HTTPRegionRepository(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    @Override
    public void getRegions(final NetworkResponseCallback<ListRegionAnswer> responseCallback){
        APIClient.getRegionService().getRegions(tokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse<ListRegionAnswer>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<ListRegionAnswer>> call, @NonNull Response<ManfredResponse<ListRegionAnswer>> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<ListRegionAnswer>> call, @NonNull Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void setRegionById(int id, final NetworkResponseCallback<RegionAnswer> responseCallback){
        APIClient.getRegionService().setCurrentRegionById(tokenManager.getAuthToken(),new RegionIdQuery(id)).enqueue(new Callback<ManfredResponse<RegionAnswer>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<RegionAnswer>> call, @NonNull Response<ManfredResponse<RegionAnswer>> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<RegionAnswer>> call, @NonNull Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void setRegionByCords(ManfredLocation location, final NetworkResponseCallback<RegionAnswer> responseCallback){
        APIClient.getRegionService().setCurrentRegionByGeoLocation(tokenManager.getAuthToken(),location.toLatLng()).enqueue(new Callback<ManfredResponse<RegionAnswer>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<RegionAnswer>> call, @NonNull Response<ManfredResponse<RegionAnswer>> response) {
                ManfredResponse<RegionAnswer> manfredResponse = response.body();
                if(manfredResponse!=null) {
                    Log.d(TAG, "setRegionByCords onResponse: " + manfredResponse.toString());
                }
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<RegionAnswer>> call, @NonNull Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

}
