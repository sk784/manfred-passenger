package manfred.ru.manfredpassenger.common.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PaymentSystem{

	@SerializedName("name")
	private String name;

	@SerializedName("currenty")
	private String currency;

	@SerializedName("currency_symbol")
	private String currencySymbol;

	@SerializedName("id")
	private int id;

	public String getName(){
		return name;
	}

	public String getCurrency(){
		return currency;
	}

	public int getId(){
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	@Override
 	public String toString(){
		return 
			"PaymentSystem{" + 
			"name = '" + name + '\'' + 
			",currency = '" + currency + '\'' +
			",id = '" + id + '\'' + 
			"}";
		}
}