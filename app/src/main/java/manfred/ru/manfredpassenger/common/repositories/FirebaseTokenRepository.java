package manfred.ru.manfredpassenger.common.repositories;

import android.support.annotation.NonNull;
import android.util.Log;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.services.models.PushTokenRequest;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirebaseTokenRepository {
    private static final String TAG = "FirebaseTokenRepository";
    private TokenManager tokenManager;

    public FirebaseTokenRepository(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    public void setFirebaseToken(String firebaseToken, boolean isNotificationsEnabled){
        Log.d(TAG, "setFirebaseToken: "+firebaseToken);
        APIClient.getUserService().setPushToken(tokenManager.getAuthToken(),new PushTokenRequest(firebaseToken, isNotificationsEnabled))
                .enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                Log.d(TAG, "setFirebaseToken onResponse: "+response.message());
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                Log.e(TAG, "setFirebaseToken onFailure: ", t);
            }
        });
    }
}
