package manfred.ru.manfredpassenger.common.managers;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.appevents.AppEventsLogger;
import com.yandex.metrica.YandexMetrica;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;
import manfred.ru.manfredpassenger.api.services.models.event.Event;

public class ManfredAnalyticsManager implements AnalyticsManager {
    private final AppEventsLogger appEventsFacebookLogger;
    public ManfredAnalyticsManager(AppEventsLogger appEventsFacebookLogger) {
        this.appEventsFacebookLogger = appEventsFacebookLogger;
    }

    @Override
    public void registered() {
        YandexMetrica.reportEvent("Registration");
        appEventsFacebookLogger.logEvent("Registration");
    }

    @Override
    public void phoneSetted(String phone) {
        UserAttributes userAttributes = new UserAttributes.Builder()
                .withPhone(phone)
                .build();
        Intercom.client().updateUser(userAttributes);

    }

    @Override
    public void nameSetted(String name) {
        UserAttributes userAttributes = new UserAttributes.Builder()
                .withName(name)
                .build();
        Intercom.client().updateUser(userAttributes);
    }

    @Override
    public void geolocationEnabled(boolean enabled) {
        UserAttributes userAttributes = new UserAttributes.Builder()
                .withCustomAttribute("Enabled geolocation",enabled)
                .build();
        Intercom.client().updateUser(userAttributes);
    }

    @Override
    public void cardAdded(Date date, String cardNumber) {
        Map<String,String>data=new HashMap<>();
        data.put("Date_at", dateToTimestamp(date));
        data.put("Card", cardNumber);
        Intercom.client().logEvent("Card added",data);

        YandexMetrica.reportEvent("Card added");
        appEventsFacebookLogger.logEvent("Card added");
    }

    @Override
    public void cardDeleted(Date date, String cardNumber) {
        Map<String,String>data=new HashMap<>();
        data.put("Date_at", dateToTimestamp(date));
        data.put("Card", cardNumber);
        Intercom.client().logEvent("Card deleted",data);

        YandexMetrica.reportEvent("Card deleted");
        appEventsFacebookLogger.logEvent("Card deleted");
    }

    @Override
    public void searchCar(Event event, String result) {
        long durationInSec = (System.currentTimeMillis()-event.getCreated())/1000;
        Map<String, Object> eventAttributes = new HashMap<String, Object> ();
        eventAttributes.put("Duration", durationInSec);
        eventAttributes.put("Result", result);
        if(result.equals("success")){
            eventAttributes.put("Time to passenger", (event.getTimeToCostumer()/60));
        }
        Intercom.client().logEvent("Search",eventAttributes);

        YandexMetrica.reportEvent("Search", eventAttributes);

        appEventsFacebookLogger.logEvent("Search", mapToBundle(eventAttributes));
    }

    @Override
    public void rideSuccess(Date date, Event event) {
        long durationInMin=(Long.parseLong(event.getRideFinishTime())-Long.parseLong(event.getRideStartTime()))/60000;

        UserAttributes userAttributes = new UserAttributes.Builder()
                .withCustomAttribute("Last ride_at", dateToTimestamp(date))
                .build();
        Intercom.client().updateUser(userAttributes);

        Map<String, Object> eventAttributesForAppMetrica = new HashMap<String, Object> ();
        eventAttributesForAppMetrica.put("Cost", event.getTotalCost());
        eventAttributesForAppMetrica.put("Duration", durationInMin);
        eventAttributesForAppMetrica.put("Service", event.getTariff().getTitle());
        eventAttributesForAppMetrica.put("Assistant", boolToYesNo(event.isNeedAssistant()));
        YandexMetrica.reportEvent("Ride", eventAttributesForAppMetrica);

        appEventsFacebookLogger.logEvent("Ride", mapToBundle(eventAttributesForAppMetrica));
    }

    @Override
    public void inviteSended(Date date, String phone) {
        Map<String,String>data=new HashMap<>();
        data.put("Date_at", dateToTimestamp(date));
        data.put("Phone", phone);
        Intercom.client().logEvent("Invitation",data);

        YandexMetrica.reportEvent("Invitation");
        appEventsFacebookLogger.logEvent("Invitation");
    }

    @Override
    public void addingCardError(String errorCode, String description) {
        Map<String, Object> eventAttributes = new HashMap<String, Object> ();
        eventAttributes.put("Error code", errorCode);
        eventAttributes.put("Description", description);
        Intercom.client().logEvent("Card error",eventAttributes);

        YandexMetrica.reportEvent("Card error", eventAttributes);
        appEventsFacebookLogger.logEvent("Card error",mapToBundle(eventAttributes));
    }

    @Override
    public void readAgreement(Date date) {
        Map<String, Object> eventAttributes = new HashMap<String, Object> ();
        eventAttributes.put("Date_at", dateToTimestamp(date));
        Intercom.client().logEvent("Read agreament",eventAttributes);
    }

    @Override
    public void tripRated(Event event, int rate) {
        long durationInMin=(Long.parseLong(event.getRideFinishTime())-Long.parseLong(event.getRideStartTime()))/60000;
        String summary = intToRate(rate);
        Map<String, Object> eventAttributesForIntercom = new HashMap<String, Object> ();
        eventAttributesForIntercom.put("Cost", event.getTotalCost());
        eventAttributesForIntercom.put("Duration", durationInMin);
        eventAttributesForIntercom.put("Service", event.getTariff().getTitle());
        eventAttributesForIntercom.put("Rate", summary);
        eventAttributesForIntercom.put("Assistant", boolToYesNo(event.isNeedAssistant()));
        eventAttributesForIntercom.put("From", event.getLocation().getAddress());
        eventAttributesForIntercom.put("Where", event.getFinalLocation().getAddress());
        Intercom.client().logEvent("Ride",eventAttributesForIntercom);

        Map<String, Object>eventAttributesForOther = new HashMap<>();
        eventAttributesForOther.put("Summary", summary);

        YandexMetrica.reportEvent("Rating", eventAttributesForOther);
        appEventsFacebookLogger.logEvent("Rating", mapToBundle(eventAttributesForOther));
    }

    private String dateToTimestamp(Date date){
        return String.valueOf(date.getTime()/1000);
    }

    private String boolToYesNo(boolean bool){
        if(bool){
            return "yes";
        }else {
            return "no";
        }
    }

    private String intToRate(int rate){
        if(rate>4){
            return "good";
        }else {
            return "bad";
        }
    }

    private Bundle mapToBundle(Map<String, Object> data) {
        Bundle bundle = new Bundle();
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            if (entry.getValue() instanceof String)
                bundle.putString(entry.getKey(), (String) entry.getValue());
            else if (entry.getValue() instanceof Double) {
                bundle.putDouble(entry.getKey(), ((Double) entry.getValue()));
            } else if (entry.getValue() instanceof Integer) {
                bundle.putInt(entry.getKey(), (Integer) entry.getValue());
            } else if (entry.getValue() instanceof Float) {
                bundle.putFloat(entry.getKey(), ((Float) entry.getValue()));
            }
        }
        return bundle;
    }
}
