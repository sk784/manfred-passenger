package manfred.ru.manfredpassenger.common.managers.events;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

public class EventServiceManager {
    private MutableLiveData<Boolean> isForeground;

    public EventServiceManager() {
        isForeground=new MutableLiveData<>();
        isForeground.postValue(false);
    }

    public void goToForeground(){
        isForeground.postValue(true);
    }

    public void goToBackground(){
        isForeground.postValue(false);
    }

    public LiveData<Boolean> getIsForeground() {
        return isForeground;
    }
}
