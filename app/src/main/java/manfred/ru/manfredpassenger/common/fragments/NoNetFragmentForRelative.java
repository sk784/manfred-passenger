package manfred.ru.manfredpassenger.common.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import manfred.ru.manfredpassenger.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoNetFragmentForRelative extends Fragment {
    private ViewGroup.LayoutParams prevParentParams;

    public NoNetFragmentForRelative() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        View rootView = inflater.inflate(R.layout.fragment_no_net, container, false);
        rootView.setOnClickListener(null);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getView()!=null) {
            ViewGroup container = ((ViewGroup) getView().getParent());
            container.setLayoutParams(prevParentParams);
        }
    }

}
