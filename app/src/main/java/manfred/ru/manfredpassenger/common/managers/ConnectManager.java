package manfred.ru.manfredpassenger.common.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

public class ConnectManager {
    private final MutableLiveData<Boolean>isConnected;


    public ConnectManager() {
        isConnected = new MutableLiveData<>();
    }

    public void noInet(){
        isConnected.postValue(false);
    }

    public void haveInet(){
        isConnected.postValue(true);
    }

    public LiveData<Boolean> getIsConnected() {
        return isConnected;
    }

    public boolean isConnected(){
        Boolean isConn=isConnected.getValue();
        if(isConn!=null) {
            return isConnected.getValue();
        }else {
            return false;
        }
    }
}
