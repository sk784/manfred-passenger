package manfred.ru.manfredpassenger.common.repositories;

import android.support.annotation.Nullable;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.services.models.PushMessageAnswer;
import manfred.ru.manfredpassenger.api.services.models.RegionAnswer;
import manfred.ru.manfredpassenger.api.services.models.UnreadPushMessagesAnswer;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.models.ListRegionAnswer;
import manfred.ru.manfredpassenger.common.models.PushMessageIdQuery;
import manfred.ru.manfredpassenger.common.models.RegionIdQuery;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.utils.NetworkUtils;

public class WebsocketPushMessageRepository implements PushMessageRepository {
    private final NetworkManagerProvider networkManager;
    private final TokenManager tokenManager;

    public WebsocketPushMessageRepository(NetworkManagerProvider networkManager, TokenManager tokenManager) {
        this.networkManager = networkManager;
        this.tokenManager = tokenManager;
    }

    @Override
    public void getMessage(int id, NetworkResponseCallback<PushMessageAnswer> responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(), "mailing/get_message", new PushMessageIdQuery(id), new WebSocketAnswerCallback<WebSocketAnswer<PushMessageAnswer>>() {
            @Override
            public void messageReceived(WebSocketAnswer<PushMessageAnswer> message) {
                new NetworkUtils().validateSocketResponse(message,responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return PushMessageAnswer.class;
            }
        });
    }

    @Override
    public void setMessageRead(int id, NetworkResponseCallback responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(), "mailing/set_message_read", new PushMessageIdQuery(id), new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                new NetworkUtils().validateSocketResponse(message,responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Nullable
            @Override
            public Class getAnswerClass() {
                return null;
            }
        });
    }


    @Override
    public void getUnreadMessages(NetworkResponseCallback<UnreadPushMessagesAnswer> responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(), "mailing/get_unread_messages", new WebSocketAnswerCallback<WebSocketAnswer<UnreadPushMessagesAnswer>>() {
            @Override
            public void messageReceived(WebSocketAnswer<UnreadPushMessagesAnswer> message) {
                new NetworkUtils().validateSocketResponse(message,responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return UnreadPushMessagesAnswer.class;
            }
        });

    }
}
