package manfred.ru.manfredpassenger.common.repositories;

import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.PushMessageAnswer;
import manfred.ru.manfredpassenger.api.services.models.UnreadPushMessagesAnswer;

public interface PushMessageRepository {
	void getMessage(int id, NetworkResponseCallback<PushMessageAnswer> responseCallback);
	void setMessageRead(int id, NetworkResponseCallback responseCallback);
	void getUnreadMessages(NetworkResponseCallback<UnreadPushMessagesAnswer> responseCallback);
}
