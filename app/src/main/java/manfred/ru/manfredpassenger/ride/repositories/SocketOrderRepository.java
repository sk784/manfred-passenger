package manfred.ru.manfredpassenger.ride.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.model.NeedDebt;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.services.models.HistoryResponce;
import manfred.ru.manfredpassenger.api.services.models.NewOrderResponse;
import manfred.ru.manfredpassenger.api.services.models.IdQuery;
import manfred.ru.manfredpassenger.api.services.models.PreOrderListResponse;
import manfred.ru.manfredpassenger.api.services.models.TariffChangedResponce;
import manfred.ru.manfredpassenger.api.services.models.VoteOrderQuery;
import manfred.ru.manfredpassenger.api.services.models.event.EventsResponse;
import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.models.NewOrder;
import manfred.ru.manfredpassenger.ride.models.PreCalculateCostRequest;
import manfred.ru.manfredpassenger.ride.models.PreCalculatedCostDTO;
import manfred.ru.manfredpassenger.ride.models.Tariffs;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SocketOrderRepository implements OrderRepository {
    private static final String TAG = "SocketOrderRepository";
    private TokenManager tokenManager;
    private NetworkManagerProvider networkManager;
    private List<Tariff>cachedTariffs=new ArrayList<>();

    public SocketOrderRepository(TokenManager tokenManager, NetworkManagerProvider networkManager) {
        this.tokenManager = tokenManager;
        this.networkManager = networkManager;
    }

    @Override
    public void makeOrder(NewOrder newOrder, NetworkResponseCallback<NewOrderResponse> responseNetworkResponseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(), "order/request_journey", newOrder, new WebSocketAnswerCallback<WebSocketAnswer<NewOrderResponse>>() {
                    @Override
                    public void messageReceived(WebSocketAnswer<NewOrderResponse> message) {
                        new NetworkUtils().validateSocketResponse(message, responseNetworkResponseCallback);
                    }

                    @Override
                    public void error(ManfredNetworkError manfredError) {
                        responseNetworkResponseCallback.error(manfredError);
                    }

                    @Override
                    public Class getAnswerClass() {
                        return NewOrderResponse.class;
                    }
                });
    }

    @Override
    public void getTariffs(NetworkResponseCallback<Tariffs> tariffs) {
        if(cachedTariffs.isEmpty()) {
            getTariffsFromNet(tariffs);
        }else {
            tariffs.allOk(new Tariffs(cachedTariffs));
        }
    }

    @Override
    public void cancelOrder(NewOrderResponse currOrder, NetworkResponseCallback responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(), "order/cancel", currOrder, new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                if(message!=null){
                    switch (message.getResultCode()){
                        case "success":
                            responseCallback.allOk(null);
                            break;
                        case "invalid_order":
                            responseCallback.allOk(null);
                            break;
                        case "invalid_token":
                            responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR, message.getMessage()));
                            break;
                        case "no_free_drivers":
                            responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.NO_FREE_DRIVERS, message.getMessage()));
                            break;
                        case "passenger_out_region":
                            responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OUT_REGION,message.getMessage()));
                            break;
                        default:
                            responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, message.getMessage()));
                            break;
                    }
                }else {
                    responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, "сервер вернул пустой ответ"));
                }
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Nullable
            @Override
            public Class getAnswerClass() {
                return null;
            }
        });
    }

    @Deprecated
    @Override
    public void getEvents(NetworkResponseCallback<EventsResponse> responseCallback) {
        APIClient.getOrderService().getEvents(tokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse<EventsResponse>>() {
            @Override
            public void onResponse(Call<ManfredResponse<EventsResponse>> call, Response<ManfredResponse<EventsResponse>> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(Call<ManfredResponse<EventsResponse>> call, Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void voteRide(VoteOrderQuery query, NetworkResponseCallback<ManfredResponse> responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(), "order/vote", query, new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                new NetworkUtils().validateSocketResponse(message, responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Nullable
            @Override
            public Class getAnswerClass() {
                return null;
            }
        });
    }

    @Override
    public void getPreOrders(NetworkResponseCallback<PreOrderListResponse> responseNetworkResponseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(), "order/preorders", new WebSocketAnswerCallback<WebSocketAnswer<PreOrderListResponse>>() {
            @Override
            public void messageReceived(WebSocketAnswer<PreOrderListResponse> message) {
                new NetworkUtils().validateSocketResponse(message,responseNetworkResponseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseNetworkResponseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return PreOrderListResponse.class;
            }
        });
    }

    @Override
    public void tryRepay(int orderId, NetworkResponseCallback responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(), "order/force_payment", new IdQuery(orderId), new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                new NetworkUtils().validateSocketResponse(message, responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Nullable
            @Override
            public Class getAnswerClass() {
                return null;
            }
        });
    }

    @Override
    public void getLastOrderWithDebt(NetworkResponseCallback<OrderVM> responseCallback) {
        NeedDebt needDebt = new NeedDebt(true);
        networkManager.addQuery(tokenManager.getAuthToken(), "order/list", needDebt, new WebSocketAnswerCallback<WebSocketAnswer<HistoryResponce>>() {
            @Override
            public void messageReceived(WebSocketAnswer<HistoryResponce> message) {
                switch (message.getResultCode()) {
                    case "success":
                        if(message.getPayload().getOrders().isEmpty()){
                            responseCallback.allOk(null);
                        }else {
                            responseCallback.allOk((message.getPayload().getOrders().get((message.getPayload().getOrders().size() - 1))));
                        }
                        break;
                        default:
                            responseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER,message.getMessage()));
                            break;
                }
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return HistoryResponce.class;
            }
        });
    }

    @Override
    public void getPrecalculatedCost(PreCalculateCostRequest preCalculateRequest, NetworkResponseCallback<PreCalculatedCostDTO> responseCallback) {
        networkManager.addQuery(tokenManager.getAuthToken(),"order/precalculate_cost",preCalculateRequest,
                new WebSocketAnswerCallback<WebSocketAnswer<PreCalculatedCostDTO>>(){
            @Override
            public void messageReceived(WebSocketAnswer<PreCalculatedCostDTO> message) {
                new NetworkUtils().validateSocketResponse(message, responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return PreCalculatedCostDTO.class;
            }
        });
    }

    @Override
    public void checkForTariffChanged(NetworkResponseCallback<TariffChangedResponce>responseCallback, long timestamp) {
        networkManager.addQuery(tokenManager.getAuthToken(),"order/tariffs/modified",new WebSocketAnswerCallback<WebSocketAnswer<TariffChangedResponce>>(){
            @Override
            public void messageReceived(WebSocketAnswer<TariffChangedResponce> message) {
                new NetworkUtils().validateSocketResponse(message, responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return TariffChangedResponce.class;
            }
        });
    }

    @Override
    public void clearCache() {
        cachedTariffs.clear();
    }

    private void getTariffsFromNet(NetworkResponseCallback<Tariffs> tariffs) {
        networkManager.addQuery(tokenManager.getAuthToken(),"order/tariffs", new WebSocketAnswerCallback<WebSocketAnswer<Tariffs>>() {
            @Override
            public void messageReceived(WebSocketAnswer<Tariffs> message) {
                new NetworkUtils().validateSocketResponse(message, tariffs);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                tariffs.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return Tariffs.class;
            }
        });
    }


}
