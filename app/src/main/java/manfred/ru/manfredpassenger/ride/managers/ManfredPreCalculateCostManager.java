package manfred.ru.manfredpassenger.ride.managers;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.models.PreCalculateCostRequest;
import manfred.ru.manfredpassenger.ride.models.PreCalculatedCostDTO;
import manfred.ru.manfredpassenger.ride.repositories.OrderRepository;
import manfred.ru.manfredpassenger.ride.repositories.SocketOrderRepository;

import static manfred.ru.manfredpassenger.utils.CostUtils.removeAfterDotsIfNeed;

public class ManfredPreCalculateCostManager implements PreCalculateCostManager {
    private static final String TAG = "ManfredPreCalculateCost";
    private OrderRepository orderRepository;

    public ManfredPreCalculateCostManager(TokenManager tokenManager, NetworkManagerProvider networkManagerProvider) {
        this.orderRepository = new SocketOrderRepository(tokenManager, networkManagerProvider);
    }

    @Override
    public void calculateCost(Tariff tariff, ManfredLocation from, ManfredLocation to, boolean isNeedAssistant, Integer PromoCodeId, CostCalculateCallback costCalculateCallback) {
        //Log.d(TAG, "calculateCost: ");
        PreCalculateCostRequest preCalculateCostRequest =
                new PreCalculateCostRequest(String.valueOf(tariff.getId()),from.getLatitude(),from.getLongitude(),to.getLatitude(),to.getLongitude(),isNeedAssistant, PromoCodeId);
        orderRepository.getPrecalculatedCost(preCalculateCostRequest, new NetworkResponseCallback<PreCalculatedCostDTO>() {
            @Override
            public void allOk(PreCalculatedCostDTO response) {
                //Log.d(TAG, "allOk: calculated "+response.getCost());
                costCalculateCallback.calculated(removeAfterDotsIfNeed(response.getCost()));
            }

            @Override
            public void error(ManfredNetworkError error) {
                costCalculateCallback.calculated(null);
            }
        });
    }
}
