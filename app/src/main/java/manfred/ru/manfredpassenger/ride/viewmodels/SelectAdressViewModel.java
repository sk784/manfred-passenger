package manfred.ru.manfredpassenger.ride.viewmodels;

import android.arch.lifecycle.ViewModel;

public class SelectAdressViewModel extends ViewModel {
    private boolean isFinish;

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }
}
