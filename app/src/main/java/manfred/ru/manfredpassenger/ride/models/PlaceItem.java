package manfred.ru.manfredpassenger.ride.models;

/**
 * for types of prediction items
 */
public interface PlaceItem {
    enum PredictType {
        SAVED_LOCATION,
        PREDICTION_LOCATION,
        PLACE_PICKER,
        PICKED_LOCATION,
        GOOGLE_COPYRIGHT
    }

    PredictType getType();
}
