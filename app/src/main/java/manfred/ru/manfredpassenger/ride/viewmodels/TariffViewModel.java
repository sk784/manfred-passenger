package manfred.ru.manfredpassenger.ride.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.util.Objects;

import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.utils.CostUtils;

/**
 * for item in adapter
 */
public class TariffViewModel extends BaseObservable {
    private final Tariff tariff;
    private final String tariffName;
    private final int id;
    private final String pressedIcon;
    private final String icon;
    private boolean selected;
    private final String shortCaption;
    private final String shortCaptionAssistant;
    private final boolean isNeedAssistant;
    private int minPreorderTimeMin;
    private final String minimumCostOfRide;
    private final String carImageUrl;
    private boolean isCalculated;
    private String costOfRide;

    public int getMinPreOrderTimeMin() {
        return minPreorderTimeMin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final TariffViewModel that = (TariffViewModel) o;

        if (id != that.id) return false;
        if (selected != that.selected) return false;
        if (isNeedAssistant != that.isNeedAssistant) return false;
        if (tariffName != null ? !tariffName.equals(that.tariffName) : that.tariffName != null)
            return false;
        if (pressedIcon != null ? !pressedIcon.equals(that.pressedIcon) : that.pressedIcon != null)
            return false;
        return icon != null ? icon.equals(that.icon) : that.icon == null;
    }

    @Override
    public int hashCode() {
        int result = tariffName != null ? tariffName.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + (pressedIcon != null ? pressedIcon.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (selected ? 1 : 0);
        result = 31 * result + (isNeedAssistant ? 1 : 0);
        return result;
    }

    public TariffViewModel(Tariff tariff, boolean isNeedAssistant) {
        this.tariff = tariff;
        tariffName = tariff.getTitle();
        id = tariff.getId();
        selected = false;
        isCalculated = false;
        this.carImageUrl=tariff.getCarImage();


        pressedIcon = tariff.getIconPressed();
        icon = tariff.getIcon();
        shortCaption = createShortCaption(tariff, isNeedAssistant);
        shortCaptionAssistant = createShortCaptionAssistant(tariff);

        if(tariff.isAssistantAllowed()) {
            this.isNeedAssistant = isNeedAssistant;
            if (isNeedAssistant) {
                this.minPreorderTimeMin = tariff.getMinPreorderTimeAssistant();
                this.minimumCostOfRide=CostUtils.removeAfterDotsIfNeed(tariff.getPrepaymentPriceAssistant())+" "+tariff.getCurrencySymbol();
            } else {
                this.minPreorderTimeMin = tariff.getMinPreorderTime();
                this.minimumCostOfRide=CostUtils.removeAfterDotsIfNeed(tariff.getPrepaymentPrice())+" "+tariff.getCurrencySymbol();
            }
        }else {
            this.isNeedAssistant = false;
            this.minimumCostOfRide=CostUtils.removeAfterDotsIfNeed(tariff.getPrepaymentPrice())+" "+tariff.getCurrencySymbol();
        }
    }

    private String createShortCaption(Tariff tariff, boolean isNeedAssistant) {
        String tariffCaption = "";
        String currencySymbol = " "+tariff.getCurrencySymbol();
        if (isNeedAssistant) {
            tariffCaption = CostUtils.removeAfterDotsIfNeed(tariff.getRideToPassengerPriceAssistant()) + currencySymbol + " + "
                    + CostUtils.removeAfterDotsIfNeed(tariff.getRideKmPriceAssistant()) + currencySymbol+"/км + "
                    + CostUtils.removeAfterDotsIfNeed(tariff.getRideMinutePriceAssistant()) + currencySymbol+"/мин. Минимум "
                    + CostUtils.removeAfterDotsIfNeed(tariff.getPrepaymentPriceAssistant()) + currencySymbol;
        } else {
            tariffCaption = CostUtils.removeAfterDotsIfNeed(tariff.getRideToPassengerPrice()) + currencySymbol + " + "
                    + CostUtils.removeAfterDotsIfNeed(tariff.getRideKmPrice()) + currencySymbol+"/км + "
                    + CostUtils.removeAfterDotsIfNeed(tariff.getRideMinutePrice()) + currencySymbol+"/мин. Минимум "
                    + CostUtils.removeAfterDotsIfNeed(tariff.getPrepaymentPrice()) + currencySymbol;
        }
        return tariffCaption;
    }

    private String createShortCaptionAssistant(Tariff tariff) {
        String tariffCaption = "";
        String currencySymbol = " "+tariff.getCurrencySymbol();

            tariffCaption = CostUtils.removeAfterDotsIfNeed(String.valueOf((int)Double.parseDouble(tariff.getRideMinutePriceAssistant())*60)) + currencySymbol+"/час. Минимум "
                    + CostUtils.removeAfterDotsIfNeed(tariff.getPrepaymentPriceAssistant()) + currencySymbol;

        return tariffCaption;
    }

    public String getTariffName() {
        return tariffName;
    }

    public int getId() {
        return id;
    }

    public String getCarImageUrl() {
        return carImageUrl;
    }

    public String getMinimumCostOfRide() {
        return minimumCostOfRide;
    }

    @Bindable
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


    @Override
    public String toString() {
        return "TariffViewModel{" +
                "tariffName='" + tariffName + '\'' +
                ", id=" + id +
                ", selected=" + selected +
                '}';
    }

    public String getPressedIcon() {
        return pressedIcon;
    }

    public String getIcon() {
        return icon;
    }

    public String getCostOfRide() {
        return costOfRide;
    }

    public void setCostOfRide(String costOfRide){
        this.costOfRide = costOfRide;
    }

    public String getShortCaption() {
        return shortCaption;
    }

    public String getShortCaptionAssistant() {
        return shortCaptionAssistant;
    }


    public boolean isNeedAssistant() {
        return isNeedAssistant;
    }

    @Bindable
    public boolean isCalculated() {
        return isCalculated;
    }

    public void setCalculated(boolean isCalculated) {
        this.isCalculated = isCalculated;
    }



    public Tariff getTariff() {
        return tariff;
    }
}
