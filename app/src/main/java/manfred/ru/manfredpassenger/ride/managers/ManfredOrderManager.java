package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.location.Location;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Date;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.repositories.GeocodingRepository;
import manfred.ru.manfredpassenger.api.services.models.NewOrderResponse;
import manfred.ru.manfredpassenger.api.services.models.VoteOrderQuery;
import manfred.ru.manfredpassenger.api.services.models.event.Event;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.cards.models.CreditCardVM;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.history.managers.HistoryManager;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;
import manfred.ru.manfredpassenger.promocodes.models.PromoCode;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodeVM;
import manfred.ru.manfredpassenger.ride.models.DriverDelay;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.models.NewOrder;
import manfred.ru.manfredpassenger.ride.repositories.OrderRepository;
import manfred.ru.manfredpassenger.ride.repositories.SocketOrderRepository;
import manfred.ru.manfredpassenger.ride.repositories.PlacesDatabaseRepository;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.ride.viewmodels.PayErrorWM;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;
import manfred.ru.manfredpassenger.utils.SingleLiveEvent;

public class ManfredOrderManager implements OrderManager {
    private static final String TAG = "ManfredOrderManager";
    //mb not need?
    private final MutableLiveData<ManfredLocation> startLocation;
    private final MutableLiveData<ManfredLocation> finishLocation;
    private final MutableLiveData<Date> selectedDate;

    private final SingleLiveEvent<OrderError> errorStatus;
    private final MutableLiveData<ManfredLocation> driverLocation;
    private final MutableLiveData<OrderVM> currOrder;
    private final MutableLiveData<String> comment;
    private final SingleLiveEvent<DriverDelay>driverDelay;

    private final TimeToDriverManager timeToDriverManager;
    private final OrderRepository orderRepository;
    private final ManfredTokenManager manfredTokenManager;
    private final ManfredPreOrderManager preOrderManager;
    private final PromoCodeManager promoCodeManager;
    private final PlacesDatabaseRepository placesDatabaseRepository;
    private final AppStatusManager appStatusManager;
    private final TariffManager tariffManager;
    private final NetworkManagerProvider networkManager;
    private final AnalyticsManager analyticsManager;
    private final HistoryManager historyManager;

    private PayErrorWM payErrorWM;
    @Nullable
    private NewOrderResponse currNewOrder;
    @Nullable
    private NewOrder lastNewOrder;

    ManfredOrderManager(TimeToDriverManager timeToDriverManager, ManfredTokenManager manfredTokenManager,
                        ManfredPreOrderManager preOrderManager,
                        PromoCodeManager promoCodeManager,
                        PlacesDatabaseRepository placesDatabaseRepository,
                        AppStatusManager appStatusManager,
                        TariffManager tariffManager,
                        NetworkManagerProvider networkManager, AnalyticsManager analyticsManager, HistoryManager historyManager) {
        this.timeToDriverManager = timeToDriverManager;
        this.promoCodeManager = promoCodeManager;
        this.analyticsManager = analyticsManager;
        this.historyManager = historyManager;
        Log.d(TAG, "ManfredOrderManager: creating...");
        this.preOrderManager = preOrderManager;
        this.appStatusManager = appStatusManager;
        this.tariffManager = tariffManager;
        this.networkManager = networkManager;
        //observTariffChanging();

        this.driverDelay = new SingleLiveEvent<>();
        this.manfredTokenManager = manfredTokenManager;
        this.errorStatus = new SingleLiveEvent<>();
        this.orderRepository = new SocketOrderRepository(manfredTokenManager, networkManager);
        this.finishLocation = new MutableLiveData<>();
        this.startLocation = new MutableLiveData<>();
        this.currOrder = new MutableLiveData<>();
        this.driverLocation = new MutableLiveData<>();
        this.selectedDate = new MutableLiveData<>();
        this.placesDatabaseRepository = placesDatabaseRepository;
        this.comment = new MutableLiveData<>();

    }

    public LiveData<Date> getSelectedDate() {
        return selectedDate;
    }

    private Location prevCheckedLocation;
    private static final float MIN_DISTANCE_IN_METERS_FOR_NEW_LOCATION_QUERY = 30f;
    @Override
    public void changeMyLocation(Location location) {
        //Log.d(TAG, "changeMyLocation: "+location);
        if(prevCheckedLocation==null){
            prevCheckedLocation=location;
            setLocationNameByLocation(location);
        }else {
            if (!(location.distanceTo(prevCheckedLocation) < MIN_DISTANCE_IN_METERS_FOR_NEW_LOCATION_QUERY)) {
                prevCheckedLocation = location;
                setLocationNameByLocation(location);
            }
        }
        if (appStatusManager.isNeedRefreshMarkerState()) {
            TariffViewModel tariffViewModel = tariffManager.getSelectedTariff().getValue();
            //Log.d(TAG, "changeMyLocation: selected tariff is "+tariffViewModel);
            if(tariffViewModel!=null) {
                if(!tariffViewModel.isNeedAssistant()) {
                    Log.d(TAG, "changeMyLocation: "+location);
                    timeToDriverManager.calculateTimeToDriver(location, tariffViewModel.getId());
                }else {
                    timeToDriverManager.cancelCalculatingState();
                }
            }
        }

    }

    private void setLocationNameByLocation(Location location) {
        GeocodingRepository geocodingRepository = new GeocodingRepository(manfredTokenManager, networkManager);
        geocodingRepository.getLocationNameByLatLng(location, new NetworkResponseCallback<ManfredLocation>() {
            @Override
            public void allOk(ManfredLocation response) {
                if(response==null){
                    startLocation.postValue(new ManfredLocation(location.getLatitude(),
                            location.getLongitude(), "unknown","address"));
                }else {
                    startLocation.postValue(response);
                }

            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "changeMyLocation error: " + error.toString());
                //errorStatus.postValue(new OrderError(error,"Ошибка"));
            }
        });
    }

    @Override
    public void changeDestinationLocation(Location location) {
        GeocodingRepository geocodingRepository = new GeocodingRepository(manfredTokenManager, networkManager);
        geocodingRepository.getLocationNameByLatLng(location, new NetworkResponseCallback<ManfredLocation>() {
            @Override
            public void allOk(ManfredLocation response) {
                if(response==null){
                    finishLocation.postValue(new ManfredLocation(location.getLatitude(),
                            location.getLongitude(), "unknown","address"));
                }else {
                    finishLocation.postValue(response);
                }
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "changeDestinationLocation error: " + error.toString());
                errorStatus.postValue(new OrderError(error,"Ошибка"));
            }
        });
    }


    public void changeMyLocation(ManfredLocation location) {
        Log.d(TAG, "changeMyLocation: ");
        startLocation.postValue(location);
        TariffViewModel tariffViewModel = tariffManager.getSelectedTariff().getValue();
        if(tariffViewModel!=null) {
            timeToDriverManager.calculateTimeToDriver(location.toLocation(),tariffViewModel.getId());
        }
    }

    @Override
    public void makeOrder() {
        Log.d(TAG, "makeOrder: ");
        TariffViewModel curSelectedTariff = tariffManager.getSelectedTariff().getValue();
        appStatusManager.searchingCar();
        if (curSelectedTariff != null) {
            ManfredLocation startLoc=startLocation.getValue();
            if (startLoc == null) {
                errorStatus.postValue(new OrderError("Ошибка", "Неверная стартовая локация"));
                return;
            }
            String stringComment = comment.getValue();
            if(stringComment==null)stringComment="";
            placesDatabaseRepository.insert(startLoc, true);
            if(finishLocation.getValue()!=null){
                placesDatabaseRepository.insert(finishLocation.getValue(), false);
            }
            PromoCodeVM promoCode = promoCodeManager.getSelectedPromoCode().getValue();
            Integer promoCodeId;
            if(promoCode!=null){
                promoCodeId=promoCode.getId();
            }else {
                promoCodeId=null;
            }
            NewOrder newOrder = new NewOrder(curSelectedTariff.isNeedAssistant(), String.valueOf(curSelectedTariff.getId()), null,
                    startLoc, finishLocation.getValue(), stringComment, promoCodeId);
            doOrder(newOrder);
        }
    }

    private void doOrder(NewOrder newOrder) {
        OrderRepository manfredOrderRepository = new SocketOrderRepository(manfredTokenManager, networkManager);
        manfredOrderRepository.makeOrder(newOrder, new NetworkResponseCallback<NewOrderResponse>() {
            @Override
            public void allOk(NewOrderResponse response) {
                Log.d(TAG, "makeOrder allOk: ");
                currNewOrder = response;
                if (newOrder.getCarNeedTime() != null) {
                    appStatusManager.readyToOrder();
                    lastNewOrder = null;
                    clearFieldsForOrder();
                    preOrderManager.refreshPreOrders();
                }
                lastNewOrder = newOrder;
                comment.postValue(null);
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "makeOrder error: " + error.getText());
                errorStatus.postValue(new OrderError(error,"Ошибка"));
                appStatusManager.readyToOrder();
            }
        });
    }

    @Override
    public void makePreOrder(Date orderTime) {
        Log.d(TAG, "makePreOrder: ");
        TariffViewModel curSelectedTariff = tariffManager.getSelectedTariff().getValue();
        if (curSelectedTariff != null) {
            if (startLocation.getValue() == null) {
                errorStatus.postValue(new OrderError("Ошибка", "Неверная стартовая локация"));
                selectedDate.postValue(null);
                return;
            }
            String stringComment = comment.getValue();
            if(stringComment==null)stringComment="";
            PromoCodeVM promoCode = promoCodeManager.getSelectedPromoCode().getValue();
            Integer promoCodeId;
            if(promoCode!=null){
                promoCodeId=promoCode.getId();
            }else {
                promoCodeId=null;
            }
            NewOrder newOrder = new NewOrder(curSelectedTariff.isNeedAssistant(), String.valueOf(curSelectedTariff.getId()),
                    (float) orderTime.getTime() / 1000, startLocation.getValue(), finishLocation.getValue(), stringComment, promoCodeId);
            doOrder(newOrder);
        }
    }

    @Override
    public void showCommentAdding() {
        appStatusManager.addComment();
    }

    @Override
    public void addComment(String string) {
        comment.postValue(string);
        appStatusManager.cancelCurrentState();
    }

    public void removeComment() {
        comment.postValue(null);
        appStatusManager.cancelCurrentState();
    }

    public LiveData<String> getComment() {
        return comment;
    }

    /**
     * cancel current state(provide backstack)
     */
    @Override
    public void cancelCurrentState() {
        Log.d(TAG, "cancelCurrentState: ");
        if (appStatusManager.isRidingNow()) {
            cancelOrder();
            return;
        }

        if (appStatusManager.isOrderingNow()) {
            Log.d(TAG, "cancelCurrentState: it is order");
            clearFieldsForOrder();
            appStatusManager.readyToOrder();
        } else {
            Log.d(TAG, "cancelCurrentState: in appStatusManager");
            appStatusManager.cancelCurrentState();
        }

    }
    @Override
    public void carRideToPassenger(OrderVM orderVM) {
        Log.d(TAG, "carRideToPassenger: " + orderVM.getId());
        currOrder.postValue(orderVM);
        appStatusManager.carRideToPassenger();
        OrderVM currPreOrder = preOrderManager.getLastPreOrder().getValue();
        clearFieldsForOrder();
        if (currPreOrder != null) {
            if (currPreOrder.getId() == orderVM.getId()) {
                preOrderManager.refreshPreOrders();
            }
        }
    }

    public void clearDate() {
        Log.d(TAG, "clearDate: ");
        selectedDate.postValue(null);
    }

    private void clearFieldsForOrder() {
        Log.d(TAG, "clearFieldsForOrder: ");
        comment.postValue(null);
        //startLocation.postValue(null);
        finishLocation.postValue(null);
        selectedDate.postValue(null);
    }

    @Override
    public void searchingCar(OrderVM orderVM) {
        currOrder.postValue(orderVM);
        currNewOrder = new NewOrderResponse(orderVM.getId());
        appStatusManager.searchingCar();
    }

    @Override
    public void carOnPassenger(OrderVM orderVM) {
        currOrder.postValue(orderVM);
        appStatusManager.carOnPassenger();
    }

    @Override
    public void riding(OrderVM orderVM) {
        Log.d(TAG, "riding: ");
        currOrder.postValue(orderVM);
        appStatusManager.riding();
    }

    @Override
    public void completed(OrderVM orderVM) {
        currOrder.postValue(orderVM);
        appStatusManager.completed();
    }

    @Override
    public void reOrder() {
        if (lastNewOrder != null) {
            makeOrder();
        }
    }

    @Override
    public void driverDelay(OrderVM orderVM) {
        Log.d(TAG, "driverDelay: "+orderVM);
        if(currOrder.getValue()!=null) {
            //int delayedFor = orderVM.getTimeToCustomer() - currOrder.getValue().getTimeToCustomer();
            driverDelay.postValue(new DriverDelay(orderVM.getDriverDelayInMin(), orderVM));
        }
        currOrder.postValue(orderVM);
    }

    public SingleLiveEvent<DriverDelay> getDriverDelay() {
        return driverDelay;
    }

    @Override
    public void orderRefresh(OrderVM orderVM) {
        currOrder.postValue(orderVM);
    }

    /**
     * it call when making order and server return no_free_drivers
     */
    @Override
    public void noFreeDrivers(OrderVM orderVM) {
        preOrderManager.refreshPreOrders();
        PromoCode promoCode = orderVM.getPromoCode();
        Integer promoCodeId=null;
        if (promoCode != null) {
            promoCodeId = promoCode.getId();
        }
        lastNewOrder = new NewOrder(orderVM,promoCodeId);

        if(appStatusManager.isSearchingCar()) {
            appStatusManager.searchCarTimeOut();
        }
    }

    @Override
    public void voteRide(boolean isTripGood) {
        OrderRepository manfredOrderRepository = new SocketOrderRepository(manfredTokenManager, networkManager);
        OrderVM currOrderVM=currOrder.getValue();
        if (currOrderVM == null) {
            errorStatus.postValue(new OrderError("Ошибка", "Неверная стартовая локация"));
            return;
        }
        analyticsManager.tripRated(currOrderVM.getEvent(),isTripGood ? 5 : 1);
        VoteOrderQuery voteOrderQuery = new VoteOrderQuery(currOrder.getValue().getId(), isTripGood ? 5 : 1);
        manfredOrderRepository.voteRide(voteOrderQuery, new NetworkResponseCallback<ManfredResponse>() {
            @Override
            public void allOk(ManfredResponse response) {
                historyManager.refreshHistory();
                appStatusManager.readyToOrder();
                currOrder.postValue(null);
                currNewOrder = null;
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: voteRide" + error.toString());
                errorStatus.postValue(new OrderError(error,"Ошибка"));
            }
        });
    }

    @Override
    public void showTariffDetails() {
        //Log.d(TAG, "showTariffDetails: ");
        appStatusManager.showTariffDetails();
    }

    public void selectTime() {
        appStatusManager.selectTimeForPreOrder();
    }

    @Override
    public void timeSelected(Date date) {
        Log.d(TAG, "timeSelected: " + date);
        selectedDate.postValue(date);
        appStatusManager.makingPreOrder();
    }


    @Override
    public void orderCanceledPrePaymentError(int orderId, String errorText) {
        Log.d(TAG, "orderCanceledPrePaymentError: ");
        appStatusManager.readyToOrder();
        preOrderManager.refreshPreOrders();
        errorStatus.setValue(new OrderError("Поездка завершена","Мы не смогли списать средства с вашей карты"));
    }


    private void cancelOrder() {
        Log.d(TAG, "cancelOrder: cancelling...");
        int cancellingId=0;
        if (appStatusManager.isSearchingCar()) {
            if (currNewOrder != null) {
                cancellingId = currNewOrder.getId();
            }
        } else {
            if(currOrder.getValue()!=null) {
                cancellingId = currOrder.getValue().getId();
            }
        }
        OrderVM orderVM = currOrder.getValue();
        if(orderVM!=null) {
            analyticsManager.searchCar(orderVM.getEvent(), "cancel");
        }
        cancelOrder(cancellingId);
    }

    public void cancelOrder(int id) {
        NewOrderResponse currCancelingOrder = new NewOrderResponse(id);
        orderRepository.cancelOrder(currCancelingOrder, new NetworkResponseCallback() {
            @Override
            public void allOk(Object response) {
                appStatusManager.readyToOrder();
                preOrderManager.refreshPreOrders();
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: cancelOrder " + error.toString());
                errorStatus.setValue(new OrderError(error,"Ошибка"));
            }
        });

    }

    public LiveData<OrderError> getErrorStatus() {
        //Log.d(TAG, "getErrorStatus: ");
        return errorStatus;
    }

    public LiveData<ManfredLocation> getStartLocation() {
        return startLocation;
    }

    public LiveData<ManfredLocation> getFinishLocation() {
        return finishLocation;
    }


    @Override
    public void setFinishLocation(ManfredLocation manfredLocation) {
        finishLocation.setValue(manfredLocation);
        TariffViewModel tariffViewModel = tariffManager.getSelectedTariff().getValue();
        Log.d(TAG, "setFinishLocation: selected tariff is "+tariffViewModel);
        if(tariffViewModel!=null) {
            if(!tariffViewModel.isNeedAssistant()) {
                timeToDriverManager.calculateTimeToDriver(startLocation.getValue().toLocation(), tariffViewModel.getId());
            }else {
                timeToDriverManager.cancelCalculatingState();
            }
        }
        //appStatusManager.makingOrder();
    }

    @Override
    public void currTripPaymentError(Event event){
        Log.d(TAG, "currTripPaymentError: ");
        if(appStatusManager.isRidingNow()) {
            appStatusManager.paymentErrorOnCurrentOrder();
            CreditCardVM creditCardVM = new CreditCardVM(event.getCard(), null);
            this.payErrorWM = new PayErrorWM(event.getDebt(), event.getTariff().getCurrencySymbol(), creditCardVM.getLastFourDigitsWithFourStars(), new Date());
        }
    }

    public PayErrorWM getPayErrorWM() {
        return payErrorWM;
    }

    public void tryRePayCurrentOrder(){
        if(getCurrOrder().getValue()!=null) {
            int orderId = getCurrOrder().getValue().getId();
            orderRepository.tryRepay(orderId, new NetworkResponseCallback() {
                @Override
                public void allOk(Object response) {
                    cancelCurrentState();
                }

                @Override
                public void error(ManfredNetworkError error) {
                    errorStatus.postValue(new OrderError(error,"Ошибка"));
                }
            });
        }
    }

    public LiveData<OrderVM> getCurrOrder() {
        return currOrder;
    }

    public MutableLiveData<ManfredLocation> getDriverLocation() {
        return driverLocation;
    }

}
