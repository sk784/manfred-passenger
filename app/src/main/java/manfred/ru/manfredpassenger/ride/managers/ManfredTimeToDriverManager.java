package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Date;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.services.models.CoordForAverageTime;
import manfred.ru.manfredpassenger.api.services.models.LatLngLocation;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.models.AverageTimeToPassenger;
import manfred.ru.manfredpassenger.ride.repositories.DriverRepository;
import manfred.ru.manfredpassenger.ride.repositories.ManfredDriverRepository;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;

public class ManfredTimeToDriverManager implements TimeToDriverManager {
    private static final String TAG = "ManfredTimeToDriverMngr";
    private static final float MIN_DISTANCE_IN_METERS_FOR_NEW_LOCATION_QUERY = 50f;
    private static final long MIN_TIME_IN_MS_FOR_NEW_CHECKING = 10000;
    private MutableLiveData<AverageTimeToPassenger>averageTimeToPassenger=new MutableLiveData<>();
    @Nullable private Location prevCheckedLocation;
    @Nullable private Integer prevCalculatedTariffId;
    private AppStatusManager appStatusManager;
    private TokenManager tokenManager;
    private RegionManager regionManager;
    private TariffManager tariffManager;
    private NetworkManagerProvider networkManager;

    ManfredTimeToDriverManager(AppStatusManager appStatusManager,
                                      TokenManager tokenManager,
                                      RegionManager regionManager,
                                      TariffManager tariffManager,
                               NetworkManagerProvider networkManager) {
        this.appStatusManager = appStatusManager;
        this.tokenManager = tokenManager;
        this.regionManager = regionManager;
        this.tariffManager = tariffManager;
        this.networkManager = networkManager;

        regionManager.getCurrentRegion().observeForever(new Observer<Region>() {
            @Override
            public void onChanged(@Nullable Region region) {
                Log.d(TAG, "CurrentRegion onChanged: ");
                clearState();
            }
        });
        observeTariffChange();
    }

    @Override
    public void calculateTimeToDriver(Location location, int tariffId) {
        //Log.d(TAG, "calculateTimeToDriver: "+location);

        if(prevCalculatedTariffId==null){
            prevCalculatedTariffId=tariffId;
        }
        //String prevLocString = prevCheckedLocation==null ? "null" : prevCheckedLocation.toString();
        if(prevCheckedLocation!=null && prevCalculatedTariffId==tariffId){
            if ((location.distanceTo(prevCheckedLocation) > MIN_DISTANCE_IN_METERS_FOR_NEW_LOCATION_QUERY)) {
                Log.d(TAG, "calculateTimeToDriver: distance big, need recalculate");
                getAverageTime(location, tariffId);
                prevCheckedLocation=location;
                prevCalculatedTariffId = tariffId;
            }else {
                //if we have request pin is loading on UI, we need take it back to prev state
                Log.d(TAG, "calculateTimeToDriver: post prev state, prevCheckedLocation is "+prevCheckedLocation);
                AverageTimeToPassenger currAverageTime = averageTimeToPassenger.getValue();
                averageTimeToPassenger.postValue(currAverageTime);
            }
        }else {
            Log.d(TAG, "calculateTimeToDriver: new location or tariff, need recalculate");
            getAverageTime(location, tariffId);
            //prevCheckedLocation=location;
            //prevCalculatedTariffId = tariffId;
        }
    }

    private void getAverageTime(@NonNull Location location, int tariffId) {
        Log.d(TAG, "getAverageTime: for tariff "+tariffId + " and location "+location);
        CoordForAverageTime coordForAverageTime =
                new CoordForAverageTime(String.valueOf(tariffId), new LatLngLocation(location));
        averageTimeToPassenger.postValue(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.LOADING, null));
        ManfredDriverRepository manfredDriverRepository = new ManfredDriverRepository(networkManager);
        manfredDriverRepository.getAverageTimeToDriver(tokenManager.getAuthToken(), coordForAverageTime,
                new DriverRepository.AverageTimeCallback() {
                    @Override
                    public void timeToDriver(int averageWaitTime) {
                        averageTimeToPassenger.postValue(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.SHOW_TIME, averageWaitTime));
                        Log.d(TAG, "calculateTimeToDriver: allOk now");
                        appStatusManager.switchToOrder();
                        if(prevCheckedLocation!=null) {
                            Log.d(TAG, "timeToDriver: we equlising "+prevCheckedLocation+" and "+location);
                            if (prevCheckedLocation.getLatitude() != location.getLatitude() && prevCheckedLocation.getLongitude() !=location.getLongitude()){
                                Log.d(TAG, "timeToDriver: its new");
                                prevCheckedLocation = location;
                            }
                        }else {
                            prevCheckedLocation = location;
                        }
                        prevCalculatedTariffId = tariffId;
                    }

                    @Override
                    public void error(ManfredNetworkError error) {
                        Log.d(TAG, "calculateTimeToDriver: error: "+error.toString());
                        averageTimeToPassenger.postValue(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.NO_FREE_DRIVERS, null));
                        if (error.getType().equals(ManfredNetworkError.ErrorType.NO_FREE_DRIVERS)) {
                            Log.d(TAG, "error: NO_FREE_DRIVERS");
                            appStatusManager.switchToPreOrder();
                        }else {
                            Log.d(TAG, "error: calculateTimeToDriver " + error.toString());
                            //errorStatus.postValue(new OrderManager.OrderError(error,"Ошибка"));
                        }
                        prevCheckedLocation=location;
                        prevCalculatedTariffId = tariffId;
                    }

                    @Override
                    public void outOfService(@Nullable Integer regionId) {
                        Log.d(TAG, "calculateTimeToDriver: outOfService: ");
                        averageTimeToPassenger.postValue(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.NO_FREE_DRIVERS, null));
                        if(regionId!=null){
                            Log.d(TAG, "calculateTimeToDriver: outOfService: have region, setting");
                            regionManager.setRegionById(regionId);
                        }else {
                            appStatusManager.outOfRegion();
                        }
                        //tariffManager.clearTariffs();
                    }

                    @Override
                    public void tariffNotFound() {
                        Log.d(TAG, "calculateTimeToDriver: tariffNotFound: ");
                        prevCalculatedTariffId = tariffId;
                        tariffManager.refreshTariffs();
                        averageTimeToPassenger
                                .postValue(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.NO_FREE_DRIVERS, null));
                    }
                });
    }

    //need recalculate time to driver if tariff changed
    private void observeTariffChange() {
        tariffManager.getSelectedTariff().observeForever(new Observer<TariffViewModel>() {
            @Override
            public void onChanged(@Nullable TariffViewModel tariffViewModel) {
                if (tariffViewModel != null) {
                    Log.d(TAG, "SelectedTariff onChanged: "+tariffViewModel.getTariff().getId());
                    Log.d(TAG, "SelectedTariff onChanged: prevChecked location is "+prevCheckedLocation);
                    Log.d(TAG, "SelectedTariff onChanged: prevCalculatedTariffId "+prevCalculatedTariffId);
                    if (prevCalculatedTariffId != null && prevCheckedLocation!=null) {
                        if (prevCalculatedTariffId != tariffViewModel.getId()) {
                            getAverageTime(prevCheckedLocation, tariffViewModel.getId());
                        }else {
                            Log.d(TAG, "SelectedTariff onChanged: prev calculated tariff is same");
                        }
                    }
                }
            }
        });
    }

    @Override
    public void reCalculateLastState() {
        Log.d(TAG, "reCalculateLastState: ");
        if(prevCalculatedTariffId!=null) {
            getAverageTime(prevCheckedLocation, prevCalculatedTariffId);
        }
    }

    @Override
    public void cancelCalculatingState() {
        averageTimeToPassenger.postValue(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.NO_FREE_DRIVERS,null));
    }

    @Override
    public void clearState() {
        Log.d(TAG, "clearState: ");
        prevCheckedLocation=null;
        prevCalculatedTariffId=null;
    }

    @Override
    public LiveData<AverageTimeToPassenger> getAverageTimeToPassenger() {
        return averageTimeToPassenger;
    }
}
