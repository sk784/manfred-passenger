package manfred.ru.manfredpassenger.ride.managers;

import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.history.managers.HistoryManager;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;
import manfred.ru.manfredpassenger.ride.repositories.PlacesDatabaseRepository;

public class ManfredOrderManagerBuilder {
    private TimeToDriverManager timeToDriverManager;
    private ManfredTokenManager manfredTokenManager;
    private ManfredPreOrderManager preOrderManager;
    private PromoCodeManager promoCodeManager;
    private PlacesDatabaseRepository placesDatabaseRepository;
    private AppStatusManager appStatusManager;
    private TariffManager tariffManager;
    private NetworkManagerProvider networkManager;
    private AnalyticsManager analyticsManager;
    private HistoryManager historyManager;

    public ManfredOrderManagerBuilder setTimeToDriverManager(TimeToDriverManager timeToDriverManager) {
        this.timeToDriverManager = timeToDriverManager;
        return this;
    }

    public ManfredOrderManagerBuilder setManfredTokenManager(ManfredTokenManager manfredTokenManager) {
        this.manfredTokenManager = manfredTokenManager;
        return this;
    }

    public ManfredOrderManagerBuilder setPreOrderManager(ManfredPreOrderManager preOrderManager) {
        this.preOrderManager = preOrderManager;
        return this;
    }

    public ManfredOrderManagerBuilder setPromoCodeManager(PromoCodeManager promoCodeManager) {
        this.promoCodeManager = promoCodeManager;
        return this;
    }

    public ManfredOrderManagerBuilder setPlacesDatabaseRepository(PlacesDatabaseRepository placesDatabaseRepository) {
        this.placesDatabaseRepository = placesDatabaseRepository;
        return this;
    }

    public ManfredOrderManagerBuilder setAppStatusManager(AppStatusManager appStatusManager) {
        this.appStatusManager = appStatusManager;
        return this;
    }

    public ManfredOrderManagerBuilder setTariffManager(TariffManager tariffManager) {
        this.tariffManager = tariffManager;
        return this;
    }

    public ManfredOrderManagerBuilder setNetworkManager(NetworkManagerProvider networkManager) {
        this.networkManager = networkManager;
        return this;
    }

    public ManfredOrderManagerBuilder setAnalyticsManager(AnalyticsManager analyticsManager){
        this.analyticsManager = analyticsManager;
        return this;
    }

    public ManfredOrderManagerBuilder setHistoryManager(HistoryManager historyManager) {
        this.historyManager = historyManager;
        return this;
    }

    public ManfredOrderManager createManfredOrderManager() {
        return new ManfredOrderManager(timeToDriverManager, manfredTokenManager, preOrderManager, promoCodeManager,
                placesDatabaseRepository, appStatusManager, tariffManager,networkManager, analyticsManager, historyManager);
    }
}