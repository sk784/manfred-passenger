package manfred.ru.manfredpassenger.ride.models;

import android.support.annotation.Nullable;

public class AverageTimeToPassenger {
    @Nullable
    private final String markerCaption;
    private final MarkerState markerState;

    public enum MarkerState {
        LOADING,
        SHOW_TIME,
        NO_FREE_DRIVERS
    }

    public AverageTimeToPassenger(MarkerState markerState, @Nullable Integer averageTime) {
        if (averageTime != null) {
            this.markerCaption = String.valueOf(averageTime);
        } else {
            markerCaption = null;
        }
        this.markerState = markerState;
    }

    @Nullable
    public String getMarkerCaption() {
        return markerCaption;
    }

    public MarkerState getMarkerState() {
        return markerState;
    }

    @Override
    public String toString() {
        return "AverageTimeToPassenger{" +
                "markerCaption='" + markerCaption + '\'' +
                ", markerState=" + markerState +
                '}';
    }
}
