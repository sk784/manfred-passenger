package manfred.ru.manfredpassenger.ride.viewmodels;

import android.support.annotation.Nullable;

import manfred.ru.manfredpassenger.ride.models.AverageTimeToPassenger;

public class MarkerViewModel {
    private boolean isProgress;
    private boolean showClock;
    @Nullable
    private String text;

    public MarkerViewModel(AverageTimeToPassenger myMarker) {
        switch (myMarker.getMarkerState()) {
            case LOADING:
                isProgress = true;
                text = null;
                showClock = false;
                break;
            case SHOW_TIME:
                isProgress = false;
                text = myMarker.getMarkerCaption();
                showClock = false;
                break;
            case NO_FREE_DRIVERS:
                isProgress = false;
                text = null;
                showClock = true;
                break;
        }
    }

    public boolean isProgress() {
        return isProgress;
    }

    public boolean isShowClock() {
        return showClock;
    }

    @Nullable
    public String getText() {
        return text;
    }
}
