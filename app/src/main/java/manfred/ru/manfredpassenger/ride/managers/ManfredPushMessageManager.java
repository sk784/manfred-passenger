package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.PushMessageAnswer;
import manfred.ru.manfredpassenger.api.services.models.UnreadPushMessagesAnswer;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.models.PushMessage;
import manfred.ru.manfredpassenger.common.repositories.PushMessageRepository;
import manfred.ru.manfredpassenger.common.repositories.WebsocketPushMessageRepository;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;

public class ManfredPushMessageManager implements PushMessageManager {
	private static final String TAG = "PushMessageManager";
	private PushMessageRepository websocketPushMessageRepository;

	public ManfredPushMessageManager(TokenManager tokenManager, NetworkManagerProvider networkManagerProvider) {
		this.websocketPushMessageRepository = new WebsocketPushMessageRepository(networkManagerProvider, tokenManager);
	}


	@Override
	public void getPushMessage(int id, PushMessageCallback callback) {
		websocketPushMessageRepository.getMessage(id, new NetworkResponseCallback<PushMessageAnswer>() {
			@Override
			public void allOk(PushMessageAnswer response) {
				callback.pushMessageReceived(response.getMessage());
			}

			@Override
			public void error(ManfredNetworkError error) {
				Log.e(TAG, "Error get push message: " + error.getText());
			}
		});
	}

	@Override
	public void setMessageRead(int id) {
		websocketPushMessageRepository.setMessageRead(id, new NetworkResponseCallback() {
			@Override
			public void allOk(Object response) {
				Log.d(TAG, "push message read succes");
			}

			@Override
			public void error(ManfredNetworkError error) {
				Log.e(TAG, "Error set message read: " + error.getText());
			}
		});
	}

	@Override
	public void getUnreadMessages(UnreadPushMessagesCallback callback) {
		websocketPushMessageRepository.getUnreadMessages(new NetworkResponseCallback<UnreadPushMessagesAnswer>() {

			@Override
			public void allOk(UnreadPushMessagesAnswer response) {
				callback.pushMessagesReceived(response.getPushmessages());
			}

			@Override
			public void error(ManfredNetworkError error) {
				Log.e(TAG, "Error get unread push messages: " + error.getText());
			}
		});

	}
}
