package manfred.ru.manfredpassenger.ride.models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public class NewOrder {

    @SerializedName("need_assistant")
    private final boolean needAssistant;

    @SerializedName("tariff_id")
    private final String tariffId;

    //timestap
    @SerializedName("car_need_time")
    private final Float carNeedTime;

    @SerializedName("location")
    private final ManfredDTOLocation manfredDTOLocation;

    @Nullable
    @SerializedName("destination")
    private final ManfredDTOLocation finishLocation;

    @SerializedName("comment")
    private final String comment;

    @SerializedName("car_type")
    private final String carType;

    @Nullable
    @SerializedName("promocode_id")
    private Integer promocodeId;

    @Nullable
    public Integer getPromocodeId() {
        return promocodeId;
    }

    public NewOrder(boolean needAssistant, String tariffId, Float carNeedTime,
                    ManfredLocation startLocation, @Nullable ManfredLocation finishLocation, String comment, @Nullable Integer promocodeId) {
        this.needAssistant = needAssistant;
        this.tariffId = tariffId;
        this.carNeedTime = carNeedTime;
        this.manfredDTOLocation = startLocation.toDTOLocation();
        this.promocodeId = promocodeId;

        if (finishLocation != null) {
            this.finishLocation = finishLocation.toDTOLocation();
        }else {
            this.finishLocation=null;
        }
        this.comment = comment;
        //this field is deprecated, but is necessary on backend
        this.carType = "";
    }

    public NewOrder(OrderVM orderVM, @Nullable Integer promoCodeId) {
        this.needAssistant = orderVM.isNeedAssistant();
        this.tariffId = String.valueOf(orderVM.getTariff().getId());
        this.carNeedTime = null;
        this.manfredDTOLocation = orderVM.getStartLocation().toDTOLocation();
        this.promocodeId = promoCodeId;
        if (orderVM.getDestinationLocation() != null) {
            this.finishLocation = orderVM.getDestinationLocation().toDTOLocation();
        }else {
            this.finishLocation=null;
        }
        this.comment = orderVM.getComment();
        this.carType="";

    }

    public boolean isNeedAssistant() {
        return needAssistant;
    }

    public String getTariffId() {
        return tariffId;
    }

    public Float getCarNeedTime() {
        return carNeedTime;
    }

    public ManfredDTOLocation getManfredLocation() {
        return manfredDTOLocation;
    }

    public String getComment() {
        return comment;
    }

    public String getCarType() {
        return carType;
    }

    @Nullable
    public ManfredDTOLocation getFinishLocation() {
        return finishLocation;
    }
}