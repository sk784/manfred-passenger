package manfred.ru.manfredpassenger.ride.models;

import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public class DriverDelay {
    private final int delayValueInMin;
    private final OrderVM orderVM;

    public DriverDelay(int delayValueInMin, OrderVM orderVM) {
        this.delayValueInMin = delayValueInMin;
        this.orderVM = orderVM;
    }

    public int getDelayValueInMin() {
        return delayValueInMin;
    }

    public OrderVM getOrderVM() {
        return orderVM;
    }
}
