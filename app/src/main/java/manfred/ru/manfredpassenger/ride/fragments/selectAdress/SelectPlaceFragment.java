package manfred.ru.manfredpassenger.ride.fragments.selectAdress;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.BR;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.databinding.FragmentSelectPlaceBinding;
import manfred.ru.manfredpassenger.databinding.GooglePlaceItemBinding;
import manfred.ru.manfredpassenger.databinding.PlaceItemBinding;
import manfred.ru.manfredpassenger.databinding.SavedPlaceItemBinding;
import manfred.ru.manfredpassenger.ride.models.AdressPlaceItem;
import manfred.ru.manfredpassenger.ride.models.GoogleCopyrightPlaceItem;
import manfred.ru.manfredpassenger.ride.models.PlaceItem;
import manfred.ru.manfredpassenger.ride.models.PlacePickerItem;
import manfred.ru.manfredpassenger.ride.models.SavedAdressPlaceItem;
import manfred.ru.manfredpassenger.ride.viewmodels.AutocompleteViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.SelectAdressViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class SelectPlaceFragment extends Fragment {
    private static final String TAG = "SelectPlaceFragment";

    public SelectPlaceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        AutocompleteViewModel autocompleteViewModel = ViewModelProviders.of(getActivity()).get(AutocompleteViewModel.class);
        FragmentSelectPlaceBinding binding = FragmentSelectPlaceBinding.inflate(getLayoutInflater());
        init(binding, autocompleteViewModel);
        return binding.getRoot();
    }

    private void init(FragmentSelectPlaceBinding binding, AutocompleteViewModel autocompleteViewModel) {
        final List<PlaceItem> placeItems = new ArrayList<>();
        binding.clSelectOnMap.setOnClickListener(v->autocompleteViewModel.selectPlace(new PlacePickerItem()));
        ItemType<PlaceItemBinding> placeItem = new ItemType<PlaceItemBinding>(R.layout.place_item) {
            @Override
            public void onCreate(Holder<PlaceItemBinding> holder) {
                super.onCreate(holder);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        autocompleteViewModel.selectPlace(holder.getBinding().getAdress());
                    }
                });
            }
        };

        ItemType<SavedPlaceItemBinding>savedPlaceItem = new ItemType<SavedPlaceItemBinding>(R.layout.saved_place_item){
            @Override
            public void onCreate(Holder<SavedPlaceItemBinding> holder) {
                super.onCreate(holder);
                holder.itemView.setOnClickListener(v -> autocompleteViewModel
                        .selectPlace(new SavedAdressPlaceItem(holder.getBinding().getAdress().getManfredLocation())));
            }
        };

        ItemType<GooglePlaceItemBinding>googleCopyrightPlaceItem = new ItemType<GooglePlaceItemBinding>(R.layout.google_place_item){
            @Override
            public void onCreate(@NotNull Holder<GooglePlaceItemBinding> holder) {
                super.onCreate(holder);
            }
        };

        LastAdapter lastAdapter = new LastAdapter(placeItems, BR.adress)
                .map(AdressPlaceItem.class, placeItem)
                .map(SavedAdressPlaceItem.class, savedPlaceItem)
                .map(GoogleCopyrightPlaceItem.class, googleCopyrightPlaceItem)
                .into(binding.selectPlaceRv);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mLayoutManager.onSaveInstanceState();
        binding.selectPlaceRv.setLayoutManager(mLayoutManager);

        SelectAdressViewModel selectAdressViewModel = ViewModelProviders.of(getActivity()).get(SelectAdressViewModel.class);
        Log.d(TAG, "init: it isFinish? "+selectAdressViewModel.isFinish());
        if(selectAdressViewModel.isFinish()){
            autocompleteViewModel.getSearchedFinishPlaces().observe(this, new Observer<List<PlaceItem>>() {
                @Override
                public void onChanged(@Nullable List<PlaceItem> newPlaceItems) {
                    placeItems.clear();
                    if (newPlaceItems != null) {
                        Log.d(TAG, "onChanged: finish "+newPlaceItems.size()+", it is ");
                        placeItems.addAll(newPlaceItems);
                        placeItems.add(new GoogleCopyrightPlaceItem());
                    }else {
                        Log.d(TAG, "onChanged: placeItems is null");
                    }
                    //Log.d(TAG, "onChanged: we set to adapter "+placeItems.size());
                    lastAdapter.notifyDataSetChanged();
                }
            });
        }else {
            autocompleteViewModel.getSearchedStartPlaces().observe(this, new Observer<List<PlaceItem>>() {
                @Override
                public void onChanged(@Nullable List<PlaceItem> newPlaceItems) {
                    placeItems.clear();
                    if (newPlaceItems != null) {
                        Log.d(TAG, "onChanged: start "+newPlaceItems.size());
                        placeItems.addAll(newPlaceItems);
                        placeItems.add(new GoogleCopyrightPlaceItem());
                    }else {
                        Log.d(TAG, "onChanged: placeItems is null");
                    }
                    //Log.d(TAG, "onChanged: we set to adapter "+placeItems.size());
                    lastAdapter.notifyDataSetChanged();
                }
            });
        }



    }

}
