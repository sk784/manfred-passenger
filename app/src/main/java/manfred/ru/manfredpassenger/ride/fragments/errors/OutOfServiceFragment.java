package manfred.ru.manfredpassenger.ride.fragments.errors;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.ride.SelectCityActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class OutOfServiceFragment extends Fragment {


    public OutOfServiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_out_of, container, false);
        rootView.findViewById(R.id.btn_select_region).setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SelectCityActivity.class);
            startActivity(intent);
        });
        rootView.setOnClickListener(null);
        return rootView;
    }

}
