package manfred.ru.manfredpassenger.ride.models;

public class PickedPlaceItem implements PlaceItem {
    private final ManfredLocation manfredLocation;

    public PickedPlaceItem(ManfredLocation manfredLocation) {
        this.manfredLocation = manfredLocation;
    }

    public ManfredLocation getManfredLocation() {
        return manfredLocation;
    }

    @Override
    public PredictType getType() {
        return PredictType.PICKED_LOCATION;
    }


}
