package manfred.ru.manfredpassenger.ride.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarsAnswer {
    @SerializedName("cars")
    private final List<Car>cars;

    public CarsAnswer(List<Car> cars) {
        this.cars = cars;
    }

    public List<Car> getCars() {
        return cars;
    }
}
