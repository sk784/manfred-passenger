package manfred.ru.manfredpassenger.ride.fragments.ordering;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.util.Objects;

import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.databinding.FragmentCommentBinding;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class CommentFragment extends Fragment {
    private static final String TAG = "CommentFragment";
    private ViewGroup.LayoutParams prevParentParams;

    public CommentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentCommentBinding binding = FragmentCommentBinding.inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentCommentBinding binding) {
        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        binding.etComment.setText(model.getCurrentComment().getValue());
        binding.btnReady.setOnClickListener(v -> {
            model.addComment(binding.etComment.getText().toString());
        });
        binding.btnRemove.setOnClickListener(v -> model.removeComment());
        binding.btnCloseComment.setOnClickListener(v -> model.cancel());

        binding.etComment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d(TAG, "onFocusChange: "+hasFocus);
                if (hasFocus) {
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
                        }
                    });
                }
            }
        });
        binding.etComment.requestFocus();
    }

    @Override
    public void onDestroyView() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
        super.onDestroyView();
        Log.d(TAG, "onDestroyView: ");
        ViewGroup container = ((ViewGroup)getView().getParent());
        container.setLayoutParams(prevParentParams);
    }

}
