package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;

import java.util.List;

import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.ride.models.Car;
import manfred.ru.manfredpassenger.ride.managers.mapOverlay.models.CarOnMap;

/**
 * using for getting free cars(after showing on map)
 */
public interface FreeCarsManager {
    void refreshCars(List<Car>cars, Tariff tariff);
    LiveData<CarsOnMap>getFreeCars();
    void tariffChanged(Tariff tariff);
    void clearCars();

    class CarsOnMap{
        private final List<CarOnMap> carsOnMap;
        private final List<Integer> offlinedCarsIds;

        public CarsOnMap(List<CarOnMap> carsOnMap, List<Integer> offlinedCarsIds) {
            this.carsOnMap = carsOnMap;
            this.offlinedCarsIds = offlinedCarsIds;
        }

        public List<CarOnMap> getCarsOnMap() {
            return carsOnMap;
        }

        public List<Integer> getOfflinedCarsIds() {
            return offlinedCarsIds;
        }
    }
}
