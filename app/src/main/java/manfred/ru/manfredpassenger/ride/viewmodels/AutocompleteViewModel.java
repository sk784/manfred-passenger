package manfred.ru.manfredpassenger.ride.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.location.Location;
import android.util.Log;

import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.ride.managers.ManfredLocationAutocompliteManager;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.models.PlaceItem;

public class AutocompleteViewModel extends ViewModel {
    private final ManfredLocationAutocompliteManager manfredLocationAutocompliteManager;
    private static final String TAG = "AutocompleteViewModel";

    public AutocompleteViewModel(ManfredLocationAutocompliteManager manfredLocationAutocompliteManager) {
        this.manfredLocationAutocompliteManager = manfredLocationAutocompliteManager;
    }

    public void searchFromPlace(String string, Location centerOfSearchLocation) {
        manfredLocationAutocompliteManager.searchStringStart(string, centerOfSearchLocation);
    }

    public void searchFinishPlace(String string, Location centerOfSearchLocation){
        manfredLocationAutocompliteManager.searchStringFinish(string, centerOfSearchLocation);
    }

    public void selectPlace(PlaceItem placeItem) {
        Log.d(TAG, "selectPlace: " + placeItem.toString());
        manfredLocationAutocompliteManager.selectPlace(placeItem);
    }

    public LiveData<List<PlaceItem>> getSearchedStartPlaces() {
        return manfredLocationAutocompliteManager.getStartPlaces();
    }

    public LiveData<List<PlaceItem>> getSearchedFinishPlaces() {
        return manfredLocationAutocompliteManager.getFinishPlaces();
    }

    public LiveData<ManfredNetworkError> getErrors() {
        return manfredLocationAutocompliteManager.getErrors();
    }

    public LiveData<ManfredLocationAutocompliteManager.SearchState> getState() {
        return manfredLocationAutocompliteManager.getSearchState();
    }

    public LiveData<ManfredLocation> getCurrentSelectedPlace() {
        return manfredLocationAutocompliteManager.getSelectedPlace();
    }


}
