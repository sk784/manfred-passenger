package manfred.ru.manfredpassenger.ride.models;

import com.google.gson.annotations.SerializedName;

public class PreCalculatedCostDTO {
    @SerializedName("cost")
    private final String cost;

    public PreCalculatedCostDTO(String cost) {
        this.cost = cost;
    }

    public String getCost() {
        return cost;
    }
}
