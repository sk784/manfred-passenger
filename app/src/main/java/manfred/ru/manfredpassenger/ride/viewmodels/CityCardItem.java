package manfred.ru.manfredpassenger.ride.viewmodels;

public class CityCardItem {
    private final String city;
    private final String country;
    private final int id;

    public CityCardItem(String city, String country, int id) {
        this.city = city;
        this.country = country;
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public int getId() {
        return id;
    }
}
