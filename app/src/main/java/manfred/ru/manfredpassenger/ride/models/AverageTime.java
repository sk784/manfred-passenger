package manfred.ru.manfredpassenger.ride.models;

import com.google.gson.annotations.SerializedName;

public class AverageTime {
    @SerializedName("average_wait_time")
    private final int averageWaitTime;

    public AverageTime(int averageWaitTime) {
        this.averageWaitTime = averageWaitTime;
    }

    public int getAverageWaitTime() {
        return averageWaitTime;
    }
}
