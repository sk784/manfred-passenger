package manfred.ru.manfredpassenger.ride.repositories;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.HistoryResponce;
import manfred.ru.manfredpassenger.api.services.models.NewOrderResponse;
import manfred.ru.manfredpassenger.api.services.models.IdQuery;
import manfred.ru.manfredpassenger.api.services.models.PreOrderListResponse;
import manfred.ru.manfredpassenger.api.services.models.TariffChangedResponce;
import manfred.ru.manfredpassenger.api.services.models.VoteOrderQuery;
import manfred.ru.manfredpassenger.api.services.models.event.EventsResponse;
import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.ride.models.NewOrder;
import manfred.ru.manfredpassenger.ride.models.PreCalculateCostRequest;
import manfred.ru.manfredpassenger.ride.models.PreCalculatedCostDTO;
import manfred.ru.manfredpassenger.ride.models.Tariffs;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HTTPOrderRepository implements OrderRepository {
    private static final String TAG = "ManfredOrderRepository";
    private TokenManager tokenManager;
    private List<Tariff>cachedTariffs=new ArrayList<>();

    public HTTPOrderRepository(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    @Override
    public void makeOrder(NewOrder newOrder, NetworkResponseCallback<NewOrderResponse> responseNetworkResponseCallback) {
        APIClient.getOrderService().requestJourney(tokenManager.getAuthToken(), newOrder).enqueue(new Callback<ManfredResponse<NewOrderResponse>>() {
            @Override
            public void onResponse(Call<ManfredResponse<NewOrderResponse>> call, Response<ManfredResponse<NewOrderResponse>> response) {
                new NetworkUtils().validateResponse(response, responseNetworkResponseCallback);
            }

            @Override
            public void onFailure(Call<ManfredResponse<NewOrderResponse>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                new NetworkUtils().processFailureResponse(t, responseNetworkResponseCallback);
            }
        });
    }

    @Override
    public void getTariffs(NetworkResponseCallback<Tariffs> tariffs) {
        if(cachedTariffs.isEmpty()) {
            getTariffsFromNet(tariffs);
        }else {
            tariffs.allOk(new Tariffs(cachedTariffs));
        }
    }

    @Override
    public void cancelOrder(NewOrderResponse currOrder, NetworkResponseCallback responseCallback) {
        APIClient.getOrderService().cancelRide(tokenManager.getAuthToken(), currOrder).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(Call<ManfredResponse> call, Response<ManfredResponse> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(Call<ManfredResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage() + "");
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void getEvents(NetworkResponseCallback<EventsResponse> responseCallback) {
        APIClient.getOrderService().getEvents(tokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse<EventsResponse>>() {
            @Override
            public void onResponse(Call<ManfredResponse<EventsResponse>> call, Response<ManfredResponse<EventsResponse>> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(Call<ManfredResponse<EventsResponse>> call, Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void voteRide(VoteOrderQuery query, NetworkResponseCallback<ManfredResponse> responseCallback) {
        APIClient.getOrderService().voteRide(query, tokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(Call<ManfredResponse> call, Response<ManfredResponse> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(Call<ManfredResponse> call, Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void getPreOrders(NetworkResponseCallback<PreOrderListResponse> responseNetworkResponseCallback) {
        APIClient.getOrderService().getPreOrders(tokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse<PreOrderListResponse>>() {
            @Override
            public void onResponse(Call<ManfredResponse<PreOrderListResponse>> call, Response<ManfredResponse<PreOrderListResponse>> response) {
                new NetworkUtils().validateResponse(response, responseNetworkResponseCallback);
            }

            @Override
            public void onFailure(Call<ManfredResponse<PreOrderListResponse>> call, Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseNetworkResponseCallback);
            }
        });
    }

    @Override
    public void tryRepay(int orderId, NetworkResponseCallback responseCallback) {
        APIClient.getOrderService().tryPayment(tokenManager.getAuthToken(),new IdQuery(orderId)).enqueue(new Callback<ManfredResponse<PreOrderListResponse>>() {
            @Override
            public void onResponse(Call<ManfredResponse<PreOrderListResponse>> call, Response<ManfredResponse<PreOrderListResponse>> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(Call<ManfredResponse<PreOrderListResponse>> call, Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void getLastOrderWithDebt(NetworkResponseCallback<OrderVM> responseNetworkResponseCallback) {
        APIClient.getUserService().getHistoryWithDebt(tokenManager.getAuthToken(),true).enqueue(new Callback<ManfredResponse<HistoryResponce>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<HistoryResponce>> call, @NonNull Response<ManfredResponse<HistoryResponce>> response) {
                new NetworkUtils().validateResponse(response, new NetworkResponseCallback() {
                    @Override
                    public void allOk(Object response) {
                        List<OrderVM> orders = ((HistoryResponce)response).getOrders();
                        if(orders.isEmpty()){
                            responseNetworkResponseCallback.allOk(null);
                        }else {
                            responseNetworkResponseCallback.allOk(orders.get(orders.size() - 1));
                        }
                    }

                    @Override
                    public void error(ManfredNetworkError error) {
                        responseNetworkResponseCallback.error(error);
                    }
                });
            }

            @Override
            public void onFailure(Call<ManfredResponse<HistoryResponce>> call, Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseNetworkResponseCallback);
            }
        });
    }

    @Override
    public void getPrecalculatedCost(PreCalculateCostRequest preCalculateRequest, NetworkResponseCallback<PreCalculatedCostDTO> responseCallback) {

    }

    @Override
    public void checkForTariffChanged(NetworkResponseCallback<TariffChangedResponce>responseCallback, long timestamp) {
        APIClient.getOrderService().checkTariffForModification(tokenManager.getAuthToken(),timestamp).enqueue(new Callback<ManfredResponse<TariffChangedResponce>>() {
            @Override
            public void onResponse(Call<ManfredResponse<TariffChangedResponce>> call, Response<ManfredResponse<TariffChangedResponce>> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(Call<ManfredResponse<TariffChangedResponce>> call, Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void clearCache() {
        cachedTariffs.clear();
    }

    private void getTariffsFromNet(NetworkResponseCallback<Tariffs> tariffs) {
        APIClient.getOrderService().getTariffs(tokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse<Tariffs>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<Tariffs>> call, @NonNull Response<ManfredResponse<Tariffs>> response) {
                new NetworkUtils().validateResponse(response, tariffs);
                if(response.body()!=null) {
                    if(response.body().getData()!=null) {
                        cachedTariffs = response.body().getData().getTariffs();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<Tariffs>> call, @NonNull Throwable t) {
                new NetworkUtils().processFailureResponse(t, tariffs);
            }
        });
    }


}
