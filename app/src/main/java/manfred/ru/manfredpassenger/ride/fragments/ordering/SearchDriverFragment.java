package manfred.ru.manfredpassenger.ride.fragments.ordering;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import manfred.ru.manfredpassenger.databinding.FragmentSearchDriverBinding;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatus;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchDriverFragment extends Fragment {
    private static final String TAG = "SearchDriverFragment";


    public SearchDriverFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSearchDriverBinding binding = FragmentSearchDriverBinding
                .inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentSearchDriverBinding binding) {
        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        binding.searchDriverCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "searchDriverCancelButton onClick: ");
                model.cancel();

            }
        });

        binding.searchAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.reOrder();
            }
        });

        model.getStatus().observe(this, new Observer<AppStatus>() {
            @Override
            public void onChanged(@Nullable AppStatus appStatus) {
                Log.d(TAG, "status changed: "+ appStatus);
                if (appStatus == AppStatus.SEARCH_CAR_TIME_OUT) {
                    TransitionManager.beginDelayedTransition(binding.searchDriverLayout);
                    binding.searchAgain.setVisibility(View.VISIBLE);
                }
            }
        });

    }

}
