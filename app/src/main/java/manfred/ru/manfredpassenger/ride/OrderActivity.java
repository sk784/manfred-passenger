package manfred.ru.manfredpassenger.ride;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.transition.TransitionManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.view.ContextThemeWrapper;
import android.transition.Slide;
import android.util.Log;
import android.util.LongSparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import org.apache.commons.lang3.time.DateUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import io.intercom.android.sdk.Intercom;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.DriverLocationResponse;
import manfred.ru.manfredpassenger.authorisation.LoginActivity;
import manfred.ru.manfredpassenger.cards.PaymentsActivity;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatus;
import manfred.ru.manfredpassenger.common.managers.location.ManfredLocationManager;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.common.models.PushMessage;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.common.repositories.FirebaseTokenRepository;
import manfred.ru.manfredpassenger.common.services.EventService;
import manfred.ru.manfredpassenger.databinding.ActivityMainBinding;
import manfred.ru.manfredpassenger.databinding.DrawerHeaderBinding;
import manfred.ru.manfredpassenger.history.HistoryActivity;
import manfred.ru.manfredpassenger.profile.ProfileActivity;
import manfred.ru.manfredpassenger.promocodes.PromoCodeActivity;
import manfred.ru.manfredpassenger.ride.fragments.cards.CardListFragment;
import manfred.ru.manfredpassenger.ride.fragments.cards.SelectActiveCardFragment;
import manfred.ru.manfredpassenger.ride.fragments.cards.SelectCardForPay;
import manfred.ru.manfredpassenger.ride.fragments.cards.SelectCardForRePayCurrentOrder;
import manfred.ru.manfredpassenger.ride.fragments.errors.NeedCardFragment;
import manfred.ru.manfredpassenger.ride.fragments.errors.NeedGeoFragment;
import manfred.ru.manfredpassenger.ride.fragments.errors.OutOfServiceFragment;
import manfred.ru.manfredpassenger.ride.fragments.ordering.CommentFragment;
import manfred.ru.manfredpassenger.ride.fragments.ordering.OrderFragment;
import manfred.ru.manfredpassenger.ride.fragments.ordering.OrderMakingFragment;
import manfred.ru.manfredpassenger.ride.fragments.ordering.PreOrderDetailsFragment;
import manfred.ru.manfredpassenger.ride.fragments.ordering.SearchDriverFragment;
import manfred.ru.manfredpassenger.ride.fragments.ride.CompleteFragment;
import manfred.ru.manfredpassenger.ride.fragments.ride.HaveDebtFragment;
import manfred.ru.manfredpassenger.ride.fragments.ride.PayErrorFragment;
import manfred.ru.manfredpassenger.ride.fragments.ride.RideFragment;
import manfred.ru.manfredpassenger.ride.fragments.tariff_details.TariffDetailsFragment;
import manfred.ru.manfredpassenger.ride.managers.FreeCarsManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredDirectionsManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredPreCalculateCostManager;
import manfred.ru.manfredpassenger.ride.managers.PushMessageManager;
import manfred.ru.manfredpassenger.ride.managers.TariffManager;
import manfred.ru.manfredpassenger.ride.managers.mapOverlay.models.CarOnMap;
import manfred.ru.manfredpassenger.ride.models.AverageTimeToPassenger;
import manfred.ru.manfredpassenger.ride.models.CarsAnswer;
import manfred.ru.manfredpassenger.ride.models.DriverDelay;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.repositories.ManfredDriverRepository;
import manfred.ru.manfredpassenger.ride.viewmodels.CancelRideDialogVM;
import manfred.ru.manfredpassenger.ride.viewmodels.HamburgerState;
import manfred.ru.manfredpassenger.ride.viewmodels.MarkerViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModelFactory;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;
import manfred.ru.manfredpassenger.utils.Constants;
import manfred.ru.manfredpassenger.utils.DoubleArrayEvaluator;
import manfred.ru.manfredpassenger.utils.GraphicUtils;
import manfred.ru.manfredpassenger.utils.SharedData;
import manfred.ru.manfredpassenger.utils.SimpleGestureFilter;

import static manfred.ru.manfredpassenger.utils.Constants.IS_FROM_MENU_START;
import static manfred.ru.manfredpassenger.utils.Constants.IS_WANT_SHARE_PROMO;

@SuppressWarnings("Convert2Lambda")
public class OrderActivity extends AppCompatActivity implements OnMapReadyCallback,
        OrderMakingFragment.OnSearchClicker {
    private static final String TAG = "OrderActivity";
    @Nullable
    DatePickerDialog datePickerDialog;
    @Nullable
    TimePickerDialog timePickerDialog;
    ActionBarDrawerToggle toggle;
    Marker finishMarker;
    Marker smallStartPin;
    //private static final String DATE_PICKER_SHOWED = "showingDatePicker";
    private OrderViewModel orderViewModel;
    private ActivityMainBinding binding;
    private Location currentGeoLocation;
    //private ManfredLocation needCenterToThis;
    @Nullable
    private GoogleMap mMap;
    //private boolean mToolBarNavigationListenerIsRegistered;
    private ObservableBoolean isNeedCenterToMyLocation = new ObservableBoolean(true);
    //private Location currPinLocation;
    private ObservableBoolean isNeedToShowInfoButtonInActionBar = new ObservableBoolean(true);
    private ScheduledExecutorService checkForCarsTask;
    private ScheduledExecutorService checkForDriverLocationTask;
    private LongSparseArray<Marker> carMarkers = new LongSparseArray<>();
    private Marker timeMarker;
    private Marker myCarMarker;
    private Polyline currentRouteOnMap;
    private boolean driverLocationSchedulled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setSupportActionBar(binding.mainToolbar);
        initActionBar();

        getWindow().setEnterTransition(new Slide());
        // Build the map.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        OrderViewModelFactory orderViewModelFactory =
                new OrderViewModelFactory(((ManfredPassengerApplication) getApplication()).getManfredOrderManager(),
                        ((ManfredPassengerApplication) getApplication()).getPreOrderManager(),
                        ((ManfredPassengerApplication) getApplication()).getAppStatusManager(),
                        ((ManfredPassengerApplication) getApplication()).getTariffManager(),
                        ((ManfredPassengerApplication) getApplication()).getRegionManager(),
                        ((ManfredPassengerApplication) getApplication()).getTimeToDriverManager(),
                        new ManfredPreCalculateCostManager(((ManfredPassengerApplication) getApplication())
                                .getTokenManager(), ((ManfredPassengerApplication) getApplication()).getNetworkManager()),
                        ((ManfredPassengerApplication) getApplication()).getPromoCodeManager(),
                        ((ManfredPassengerApplication) getApplication()).getAnalyticsManager());
        orderViewModel = ViewModelProviders.of(this, orderViewModelFactory).get(OrderViewModel.class);

        orderViewModel.getPreOrderError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                AlertDialog.Builder builder = new AlertDialog.Builder(OrderActivity.this, R.style.AlertDialogCustom);
                builder.setMessage(s);
                builder.setPositiveButton("OK", null);
                builder.create().show();
            }
        });

        orderViewModel.getMarkerState().observe(this, new Observer<AverageTimeToPassenger>() {
            @Override
            public void onChanged(@Nullable AverageTimeToPassenger myMarker) {
                if (myMarker != null) {
                    Log.d(TAG, "MarkerState onChanged: MarkerState "+myMarker.getMarkerState());
                    @Nullable MarkerViewModel currMarkerState = binding.getMarkerState();
                    if (currMarkerState != null && currMarkerState.isProgress() && myMarker.getMarkerState() != AverageTimeToPassenger.MarkerState.LOADING) {
                        binding.getRoot().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "MarkerState run: setting with delay "+myMarker.getMarkerState());
                                binding.setMarkerState(new MarkerViewModel(myMarker));
                            }
                        }, 500);
                    } else {
                        Log.d(TAG, "MarkerState onChanged: setting now "+myMarker);
                        binding.setMarkerState(new MarkerViewModel(myMarker));
                    }

                }
            }
        });

        orderViewModel.getStatus().observe(this, new Observer<AppStatus>() {
            @Override
            public void onChanged(@Nullable AppStatus appStatus) {
                if (appStatus != null) {
                    //Log.d(TAG, "onChanged status: " + appStatus);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    //ManfredDTOLocation manfredLocation = orderViewModel.getStartLocation().getValue();
                    stopSearchAnimation();
                    changeRideStatus(appStatus, transaction);
                    transaction.commit();
                }
                //syncActionBarArrowState();
                //syncPreOrderHolderVisibility();
            }
        });

        orderViewModel.getMenuIconState().observe(this, new Observer<HamburgerState>() {
            @Override
            public void onChanged(@Nullable HamburgerState state) {
                if (state != null) {
                    //Log.d(TAG, "getMenuIconState onChanged: "+aBoolean);
                    setDrawerIndicatorState(state);
                }
            }
        });

        RegionManager regionManager = ((ManfredPassengerApplication) getApplication()).getRegionManager();
        regionManager.getRegionError().observe(this, new Observer<ManfredNetworkError>() {
            @Override
            public void onChanged(@Nullable ManfredNetworkError manfredNetworkError) {
                if (manfredNetworkError != null) {
                    if (manfredNetworkError.getType().equals(ManfredNetworkError.ErrorType.CANT_SET_REGION)) {
                        ((ManfredPassengerApplication) getApplication()).getAppStatusManager().outOfRegion();
                    }
                }
            }
        });

        FreeCarsManager freeCarsManager = ((ManfredPassengerApplication) getApplication()).getCarsManager();
        freeCarsManager.getFreeCars().observe(this, new Observer<FreeCarsManager.CarsOnMap>() {
            @Override
            public void onChanged(@Nullable FreeCarsManager.CarsOnMap carsOnMap) {
                if (carsOnMap != null) {
                    showCarsOnMap(carsOnMap.getCarsOnMap(), carsOnMap.getOfflinedCarsIds());
                }
            }
        });

        orderViewModel.getOrderError().observe(this, manfredNetworkError -> {
            if (manfredNetworkError != null) {
                if (manfredNetworkError.getType() == ManfredNetworkError.ErrorType.TOKEN_ERROR) {
                    Log.d(TAG, "getOrderError from orderViewModel: token error, current token is " + ((ManfredPassengerApplication) getApplication()).getTokenManager().getAuthToken());
                    final Intent intent = new Intent(this, LoginActivity.class);
                    SharedPreferences settings = getSharedPreferences(Constants.PREFS_FILE_NAME, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.clear().apply();
                    ((ManfredPassengerApplication) getApplication()).killAllManagers();
                    stopCheckEvenService();
                    startActivity(intent);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
                    builder.setMessage(manfredNetworkError.getText())
                            .setTitle(manfredNetworkError.getCaption());
                    builder.setPositiveButton("OK", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        orderViewModel.getCancelDialog().observe(this, new Observer<CancelRideDialogVM>() {
            @Override
            public void onChanged(@Nullable CancelRideDialogVM cancelRideDialogVM) {
                if (cancelRideDialogVM != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(OrderActivity.this, R.style.AlertDialogCustom));
                    builder.setTitle(cancelRideDialogVM.getTitle());
                    builder.setMessage(cancelRideDialogVM.getText());
                    builder.setPositiveButton(cancelRideDialogVM.getPositiveButtonText(),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    orderViewModel.cancelRideDismiss();
                                }
                            });
                    builder.setNegativeButton(cancelRideDialogVM.getNegativeButtonText(),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    orderViewModel.cancelRideConfirm();
                                }
                            });
                    builder.show();
                }
            }
        });

        binding.fragmentContainer.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                //Log.d(TAG, "onLayoutChange: ");
                if (top == oldTop) {
                    //ignoring because not changed size
                    //Log.d(TAG, "onLayoutChange: ignoring, size not changed");
                    return;
                }
                if (!orderViewModel.isShowingTariffDetails() ) {
                    //Log.d(TAG, "onLayoutChange: need recalc projections");
                    recalkMapProjection();
                }
                if (orderViewModel.isSearchingCar()) {
                    startSearchAnimation();
                }
            }

        });

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        isNeedCenterToMyLocation.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                //Log.d(TAG, "onPropertyChanged: isNeedCenterToMyLocation "+isNeedCenterToMyLocation.get());
                if (isNeedCenterToMyLocation.get()) {
                    binding.myLocationButton.hide();
                } else {
                    binding.myLocationButton.show();
                }
            }
        });

        initRouteDrawing();

        //initialisation
        if (isNeedCenterToMyLocation.get()) {
            binding.myLocationButton.hide();
        }

        binding.myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "myLocationButton onClick: ");
                isNeedCenterToMyLocation.set(true);
                centerMapToLocation(currentGeoLocation);
            }
        });

        isNeedToShowInfoButtonInActionBar.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                //Log.d(TAG, "isNeedToShowInfoButtonInActionBar onPropertyChanged");
                invalidateOptionsMenu();
            }
        });

        orderViewModel.getLastPreOrder().observe(this, new Observer<OrderVM>() {
            @Override
            public void onChanged(@Nullable OrderVM orderVM) {
                binding.setCurrentPreOrder(orderVM);
                //showPreOrderHolder(orderVM);
                syncPreOrderHolderVisibility();
            }
        });

        startCheckEventService();

        orderViewModel.getIsMapCanScroll().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean == null) return;
                if (aBoolean) {
                    if (mMap != null) {
                        mMap.getUiSettings().setScrollGesturesEnabled(true);
                        mMap.getUiSettings().setZoomGesturesEnabled(true);
                    }
                    if (isNeedCenterToMyLocation.get()) {
                        binding.myLocationButton.hide();
                    } else {
                        binding.myLocationButton.show();
                    }
                } else {
                    binding.myLocationButton.hide();
                    if (mMap != null) {
                        mMap.getUiSettings().setScrollGesturesEnabled(false);
                        mMap.getUiSettings().setZoomGesturesEnabled(false);
                    }
                }
            }
        });

        ((ManfredPassengerApplication) getApplication()).getManfredCreditCardManager().getCardErrors()
                .observe(this, new Observer<ManfredNetworkError>() {
                    @Override
                    public void onChanged(@Nullable ManfredNetworkError manfredNetworkError) {
                        if (manfredNetworkError != null) {
                            Toast.makeText(getApplicationContext(), "Ошибка: " + manfredNetworkError.getText(), Toast.LENGTH_LONG).show();
                        }
                    }
                });

        orderViewModel.getDriverDelay().observe(this, new Observer<DriverDelay>() {
            @Override
            public void onChanged(@Nullable DriverDelay driverDelay) {
                if (driverDelay != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderActivity.this, R.style.AlertDialogCustom);
                    builder.setMessage("Ваш автомобиль задерживается на " + driverDelay.getDelayValueInMin() + " минут");
                    builder.setPositiveButton("OK", null);
                    builder.create().show();
                    ridingToPassenger(driverDelay.getOrderVM());
                }
            }
        });

        initNoNetObserver();

        //observeTariffChanging(((ManfredPassengerApplication) getApplication()).getTariffManager());
        setMiuiStatusBarDarkMode(OrderActivity.this, true);
        Intercom.client().handlePushMessage();

        observeRegionChanged(((ManfredPassengerApplication) getApplication()).getRegionManager());

        orderViewModel.getStartLocation().observe(this, new Observer<ManfredLocation>() {
            @Override
            public void onChanged(@Nullable ManfredLocation manfredLocation) {
                if (manfredLocation != null) {
                    Log.d(TAG, "getStartLocation onChanged: ");
                    centerMapToRecognizedLocation(manfredLocation.toLocation());
                }
            }
        });

    }

    private void observeRegionChanged(RegionManager regionManager) {
        regionManager.getCurrentRegion().observe(this, new Observer<Region>() {
            @Override
            public void onChanged(@Nullable Region region) {
                if (region != null) {
                    if (((ManfredPassengerApplication) getApplication()).isNewUser()) {
                        if (!((ManfredPassengerApplication) getApplication()).getManfredCreditCardManager().isHaveCards()) {
                            Log.d(TAG, "onCreate: new user without card");
                            orderViewModel.newUserNeedCard();
                            Intent intent = new Intent(OrderActivity.this, PaymentsActivity.class);
                            startActivity(intent);
                        }
                    }
                }
            }
        });
    }

    private void initNoNetObserver() {
        ((ManfredPassengerApplication) getApplication()).getConnectManager().getIsConnected().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean != null) {
                    ConstraintLayout noInetLayout = findViewById(R.id.cl_no_inet);
                    if (aBoolean) {
                        noInetLayout.setVisibility(View.GONE);
                        hideSelectDateDialog();
                    } else {
                        noInetLayout.setVisibility(View.VISIBLE);
                        binding.myLocationButton.hide();
                    }

                }
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void showPreOrderHolder(@Nullable OrderVM orderVM) {
        Log.d(TAG, "showPreOrderHolder: ");
        SimpleGestureFilter.SimpleGestureListener simpleGestureListener = new SimpleGestureFilter.SimpleGestureListener() {
            @Override
            public void onSwipe(int direction) {
                switch (direction) {
                    case SimpleGestureFilter.SWIPE_UP:
                        TransitionManager.beginDelayedTransition(binding.currPreOrderHolder);
                        binding.preorderAdressHolder.setVisibility(View.VISIBLE);
                        break;
                    case SimpleGestureFilter.SWIPE_DOWN:
                        TransitionManager.beginDelayedTransition(binding.currPreOrderHolder);
                        binding.preorderAdressHolder.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onDoubleTap() {

            }

            @Override
            public void click() {
                orderViewModel.showPreOrderDetail(orderVM);
            }
        };

        SimpleGestureFilter detector = new SimpleGestureFilter(this, simpleGestureListener);

        if (orderVM != null) {
            Log.d(TAG, "showPreOrderHolder: showing");
            binding.currPreOrderHolder.setVisibility(View.VISIBLE);
            binding.preorderAdressHolder.setVisibility(View.VISIBLE);

            binding.currPreOrderHolder.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "showPreOrderHolder: hiding after timeout");
                    TransitionManager.beginDelayedTransition(binding.currPreOrderHolder);
                    binding.preorderAdressHolder.setVisibility(View.GONE);
                }
            }, 4000);

            binding.currPreOrderHolder.setClickable(true);
            binding.currPreOrderHolder.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.d(TAG, "onTouch: ");
                    detector.onTouchEvent(event);
                    return false;
                }
            });
        }
    }

    private void syncPreOrderHolderVisibility() {
        //Log.d(TAG, "syncPreOrderHolderVisibility: ");
        if (orderViewModel.isReadyForOrderState()) {
            OrderVM lastPreOrder = orderViewModel.getLastPreOrder().getValue();
            if (lastPreOrder != null) {
                showPreOrderHolder(lastPreOrder);
                //Log.d(TAG, "syncPreOrderHolderVisibility: need show");
            } else {
                //Log.d(TAG, "syncPreOrderHolderVisibility: last preOrder null, hiding");
                binding.currPreOrderHolder.setVisibility(View.GONE);
            }
        } else {
            //Log.d(TAG, "syncPreOrderHolderVisibility: need hide");
            binding.currPreOrderHolder.setVisibility(View.GONE);
        }
    }

    private void startCheckEventService() {
        Intent intent = new Intent(OrderActivity.this, EventService.class);
        startService(intent);
    }

    private void stopCheckEvenService() {
        Intent intent = new Intent(OrderActivity.this, EventService.class);
        stopService(intent);
    }

    private void changeRideStatus(@NonNull AppStatus appStatus, FragmentTransaction transaction) {
        Log.d(TAG, "changeRideStatus: " + appStatus);
        if (appStatus != AppStatus.SELECTING_TIME_FOR_PREORDER) {
            hideSelectDateDialog();
        }

        OrderVM currentRide = orderViewModel.getCurrentRide().getValue();
        syncPreOrderHolderVisibility();
        /*
        Метод setCustomAnimations() позволяет указать собственную анимацию. Методу передаются два параметра.
        Первый параметр описывает анимацию для фрагмента, который появляется, а второй — описывает анимацию для фрагмента, который убирается с экрана устройства.
        Метод следует вызывать до появления фрагментов, иначе анимация не будет применена.
         */
        switch (appStatus) {
            case NEED_CARD:
                isNeedToShowInfoButtonInActionBar.set(false);
                transaction.replace(binding.fragmentContainer.getId(), new NeedCardFragment());
                hideActionBar();
                hideCenterMarker();
                break;
            case OUT_OF_SERVICE:
                showActionBar();
                transaction.replace(binding.fragmentContainer.getId(), new OutOfServiceFragment());
                break;
            case LOCATION_PERMISSION_DENIED:
                hideActionBar();
                hideCenterMarker();
                transaction.replace(binding.fragmentContainer.getId(), new NeedGeoFragment());
                break;
            case SHOWING_TARIFF_DETAILS:
                hideCarsOnMap();
                isNeedToShowInfoButtonInActionBar.set(true);
                transaction.setCustomAnimations(R.animator.fade_in, 0, 0, 0);
                transaction.replace(binding.fragmentContainer.getId(), new TariffDetailsFragment());
                hideCenterMarker();
                hideActionBar();
                break;
            case READY_TO_ORDER:
                stopCheckDriverLocation();
                readyForOrder(transaction);
                removePathFromMap();
                break;
            case MAKING_ORDER:
                transaction.setCustomAnimations(R.animator.slide_up_animator, 0, 0, 0);
                showOrderMaking(transaction);
                break;
            case ADDING_COMMENT:
                hideActionBar();
                transaction.replace(binding.fragmentContainer.getId(), new CommentFragment());
                break;
            case SELECTING_TIME_FOR_PREORDER:
                showSelectDateDialog();
                break;
            case MAKING_PRE_ORDER:
                transaction.setCustomAnimations(R.animator.slide_up_animator, 0, 0, 0);
                showOrderMaking(transaction);
                break;
            case SELECTING_CARD_FOR_REPAY_LAST_DEBT:
                transaction.replace(binding.fragmentContainer.getId(), new SelectCardForPay());
                hideActionBar();
                break;
            case SELECTING_CARD_FOR_REPAY_CURRENT_ORDER:
                transaction.replace(binding.fragmentContainer.getId(), new SelectCardForRePayCurrentOrder());
                break;
            case PAYMENT_ERROR:
                transaction.replace(binding.fragmentContainer.getId(), new PayErrorFragment());
                hideCenterMarker();
                hideActionBar();
                break;
            case HAVE_DEBT:
                transaction.replace(binding.fragmentContainer.getId(), new HaveDebtFragment());
                hideCenterMarker();
                hideActionBar();
                break;
            case SELECT_CARDS:
                hideActionBar();
                transaction.setCustomAnimations(R.animator.enter_from_right, R.animator.fast_hide);
                transaction.replace(binding.fragmentContainer.getId(), new CardListFragment());
                break;
            case PICKING_CARDS:
                hideActionBar();
                hideCenterMarker();
                transaction.replace(binding.fragmentContainer.getId(), new SelectActiveCardFragment());
                break;
            case SEARCH_CAR:
                removePathFromMap();
                searchCar(transaction, currentRide);
                break;
            case SEARCH_CAR_TIME_OUT:
                startSearchAnimation();
                transaction.replace(binding.fragmentContainer.getId(), new SearchDriverFragment());
                binding.carNotFoundCaption.setVisibility(View.VISIBLE);
                if (mMap != null) {
                    hideMyLocationDot(mMap);
                }
                removeSmallPinMarker();
                break;
            case CAR_RIDE_TO_PASSENGER:
                hideCenterMarker();
                hideCarsOnMap();
                startGettingDriverLocation();
                transaction.replace(binding.fragmentContainer.getId(), new RideFragment());
                isNeedToShowInfoButtonInActionBar.set(false);
                ridingToPassenger(currentRide);
                break;
            case CAR_ON_PASSENGER:
                hideCenterMarker();
                hideCarsOnMap();
                startGettingDriverLocation();
                removePathFromMap();
                isNeedToShowInfoButtonInActionBar.set(false);
                carOnPassenger(transaction);
                break;
            case RIDING:
                hideCenterMarker();
                hideCarsOnMap();
                startGettingDriverLocation();
                isNeedToShowInfoButtonInActionBar.set(false);
                transaction.replace(binding.fragmentContainer.getId(), new RideFragment());
                riding(currentRide);
                break;
            case SHOWING_PREORDER_DETAILS:
                isNeedToShowInfoButtonInActionBar.set(false);
                transaction.replace(binding.fragmentContainer.getId(), new PreOrderDetailsFragment());
                showActionBar();
                OrderVM currPreOrder = orderViewModel.getLastPreOrder().getValue();
                if (currPreOrder != null) {
                    ManfredLocation endLocation = currPreOrder.getDestinationLocation();
                    hideCenterMarker();
                    if (endLocation == null) {
                        Log.d(TAG, "changeRideStatus: centering to startLocation");
                        showWithClockMarker(currPreOrder.getStartLocation());
                        centerMapToLocation(currPreOrder.getStartLocation().toLocation());
                    } else {
                        showPathOnMap(currPreOrder.getStartLocation(), currPreOrder.getDestinationLocation());
                    }
                }
                break;
            case COMPLETED:
                orderCompleted(transaction, currentRide);
                break;
        }


        //for test:
        //FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //transaction.replace(binding.fragmentContainer.getId(), new CompleteFragment());
        //transaction.commit();

    }

    private void orderCompleted(FragmentTransaction transaction, OrderVM currentRide) {
        hideCarsOnMap();
        isNeedToShowInfoButtonInActionBar.set(false);
        transaction.replace(binding.fragmentContainer.getId(), new CompleteFragment());
        if (currentRide != null) {
            Log.d(TAG, "changeRideStatus: ride is null");
            showPathOnMap(currentRide.getStartLocation(),
                    currentRide.getRealFinishLocation());
        }
        hideActionBar();
    }

    private void readyForOrder(FragmentTransaction transaction) {
        Log.d(TAG, "readyForOrder: ");
        isNeedToShowInfoButtonInActionBar.set(false);
        binding.carNotFoundCaption.setVisibility(View.GONE);
        transaction.setCustomAnimations(R.animator.fade_in, R.animator.slide_down_animator, 0, 0);
        removePathFromMap();
        transaction.replace(binding.fragmentContainer.getId(), new OrderFragment());
        if (isNeedCenterToMyLocation.get()) {
            Log.d(TAG, "changeRideStatus: centering to geo...");
            centerMapToLocation(currentGeoLocation);
        }
        showingCarsOnMap();
        showActionBar();
        if (mMap != null) {
            showMyLocationDot(mMap);
        }
    }

    private void removePathFromMap() {
        Log.d(TAG, "removePathFromMap: ");
        if (currentRouteOnMap != null) {
            currentRouteOnMap.remove();
            currentRouteOnMap = null;
        }
        if (finishMarker != null) {
            finishMarker.remove();
            finishMarker = null;
        }
        removeSmallPinMarker();
        if (timeMarker != null) {
            timeMarker.remove();
            timeMarker = null;
        }
    }


    private void searchCar(FragmentTransaction transaction, OrderVM currentRide) {
        hideCenterMarker();
        hideCarsOnMap();
        binding.carNotFoundCaption.setVisibility(View.GONE);
        if (currentRide != null) {
            //it is restoring
            Log.d(TAG, "searchCar: ");
            centerMapToLocation(currentRide.getStartLocation().toLocation());
            showSmallPinMarker(currentRide.getStartLocation());
        } else {
            //it is new order
            ManfredLocation startLoc = orderViewModel.getStartLocation().getValue();
            if (startLoc != null) {
                Log.d(TAG, "searchCar: ");
                centerMapToLocation(startLoc.toLocation());
            }
            showSmallPinMarker(startLoc);
        }

        hideActionBar();
        transaction.replace(binding.fragmentContainer.getId(), new SearchDriverFragment());

        if (currentRide != null) {
            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(this);
            Bundle params = new Bundle();
            params.putString("order", currentRide.toString());
            analytics.logEvent("making_order", params);
        }
        if (mMap != null) {
            hideMyLocationDot(mMap);
        }
    }

    private void showActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
        }
    }

    private void hideActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    private void riding(OrderVM currentRide) {
        Log.d(TAG, "riding: ");
        showActionBar();
        hideCenterMarker();
        showCarMarker(currentRide);
        removeSmallPinMarker();
        if (currentRide.getDestinationLocation() != null && currentRide.getDriverLocation() != null) {
            Log.d(TAG, "riding: have destination");
            showFinishMarker(currentRide.getDestinationLocation());
            fitToMarkers(mMap, currentRide.getDestinationLocation().toLatLng(), currentRide.getDriverLocation().toLatLng());
        } else {
            if (currentRide.getDriverLocation() != null) {
                centerMapToLocation(currentRide.getDriverLocation().toLocation());
            } else {
                centerMapToLocation(currentGeoLocation);
            }
        }
    }

    private void ridingToPassenger(OrderVM currentRide) {
        showActionBar();
        removeSmallPinMarker();
        if (currentRide != null) {
            //Log.d(TAG, "ridingToPassenger: " + currentRide.toString());
            showPinTimeMarker(String.valueOf(currentRide.getTimeToCustomer()),
                    currentRide.getStartLocation());
            showCarMarker(currentRide);
            centerMapToLocation(currentRide.getStartLocation().toLocation());
            //showPathOnMap(currentRide.getStartLocation(), currentRide.getDriverLocation());
            if (currentRide.getDriverLocation() != null) {
                fitToMarkers(mMap, currentRide.getDriverLocation().toLatLng(), currentRide.getStartLocation().toLatLng());
            }
        }

        if (mMap != null) {
            hideMyLocationDot(mMap);
        }

    }

    private void showOrderMaking(FragmentTransaction transaction) {
        //Log.d(TAG, "showOrderMaking: ");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (!(f instanceof OrderMakingFragment)) {
            //ignoring because showed
            transaction.replace(binding.fragmentContainer.getId(), new OrderMakingFragment(), "making_order");
            transaction.addToBackStack(null);
        }
        isNeedToShowInfoButtonInActionBar.set(true);
        if (orderViewModel.getFinishLocation().getValue() != null) {
            Log.d(TAG, "showOrderMaking: we have finish, showing route");
            showPathOnMap(orderViewModel.getStartLocation().getValue(), orderViewModel.getFinishLocation().getValue());
        } else {
            Log.d(TAG, "showOrderMaking: not have finish location, centering to start");
            ManfredLocation startLoc = orderViewModel.getStartLocation().getValue();

            if (startLoc != null) {
                centerMapToLocation(startLoc.toLocation());
            }
        }
        showActionBar();
        if (mMap != null) {
            showMyLocationDot(mMap);
        }
    }

    private void hideSelectDateDialog() {
        //Log.d(TAG, "hideSelectDateDialog: ");
        if (datePickerDialog != null) {
            if (datePickerDialog.isVisible()) {
                datePickerDialog.dismiss();
                datePickerDialog = null;
            }
        }
        if (timePickerDialog != null) {
            if (timePickerDialog.isVisible()) {
                timePickerDialog.dismiss();
                timePickerDialog = null;
            }
        }

    }

    private void showSelectDateDialog() {
        Log.d(TAG, "showSelectDateDialog: ");
        //Calendar currDateAndTime=Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.set(Calendar.YEAR, year);
                selectedDate.set(Calendar.MONTH, monthOfYear);
                selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Log.d(TAG, "onDateSet: " + selectedDate.toString());
                showSelectTimeDialog(selectedDate);
            }
        });
        if (orderViewModel.getSelectedTariff().getValue() != null) {
            Calendar calendar = Calendar.getInstance();

            Calendar maxDay = Calendar.getInstance();
            maxDay.add(Calendar.DATE, 30);

            datePickerDialog.setMinDate(calendar);
            datePickerDialog.setMaxDate(maxDay);
            Log.d(TAG, "showSelectDateDialog: minDate setted to " + calendar.getTime());
        }
        datePickerDialog.setThemeDark(true);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.card_background));
        datePickerDialog.setOkColor(getResources().getColor(R.color.route_line_color));
        datePickerDialog.setCancelColor(getResources().getColor(R.color.route_line_color));
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                orderViewModel.hideSelectingTimeForPreOrder();
            }
        });
        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
    }

    private void showSelectTimeDialog(Calendar selectedDate) {
        timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                selectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                selectedDate.set(Calendar.MINUTE, minute);
                Log.d(TAG, "onTimeSet: " + selectedDate.toString());
                orderViewModel.setOrderTime(selectedDate.getTime());
            }
        }, true);
        if (orderViewModel.getSelectedTariff().getValue() != null) {
            if (DateUtils.isSameDay(selectedDate, Calendar.getInstance())) {
                int preOrderTimeInMin = orderViewModel.getSelectedTariff().getValue().getMinPreOrderTimeMin() + 2;
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, preOrderTimeInMin);
                Timepoint timepoint = new Timepoint(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
                timePickerDialog.setMinTime(timepoint);
                //Log.d(TAG, "showSelectTimeDialog: minTime setted to " + timepoint.toString());
            }
        } else {
            Log.d(TAG, "showSelectTimeDialog: minTime is null");
        }
        timePickerDialog.enableMinutes(true);
        timePickerDialog.setThemeDark(true);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.card_background));
        timePickerDialog.setOkColor(getResources().getColor(R.color.route_line_color));
        timePickerDialog.setCancelColor(getResources().getColor(R.color.route_line_color));
        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                orderViewModel.hideSelectingTimeForPreOrder();
            }
        });
        timePickerDialog.show(getFragmentManager(), "TimePickerdialog");
    }

    private void carOnPassenger(FragmentTransaction transaction) {
        Log.d(TAG, "carOnPassenger: ");
        ManfredLocation currLocation;
        if (currentGeoLocation != null) {
            currLocation = new ManfredLocation(currentGeoLocation);
        } else {
            currLocation = orderViewModel.getCurrentRide().getValue().getStartLocation();
        }
        showSmallPinMarker(currLocation);
        showCarMarker(orderViewModel.getCurrentRide().getValue());
        centerMapToLocation(currLocation.toLocation());
        transaction.replace(binding.fragmentContainer.getId(), new RideFragment());
    }

    private void recalkMapProjection() {
        //Log.d(TAG, "recalkMapProjection: ");
        if (mMap == null) return;
        Location centerCordsOfMap = new Location("map");
        CameraPosition currCameraPosition = mMap.getCameraPosition();
        centerCordsOfMap.setLatitude(currCameraPosition.target.latitude);
        centerCordsOfMap.setLongitude(currCameraPosition.target.longitude);
        //Log.d(TAG, "recalkMapProjection:  center of map now " + centerCordsOfMap);
        mMap.setPadding(0, getStatusBarHeight(), 0, binding.fragmentContainer.getHeight());
        Projection projection = mMap.getProjection();
        Point screenPositionOfCenterMap = projection.toScreenLocation(mMap.getCameraPosition().target);
        if (orderViewModel.isReadyForOrderState()) {
            ManfredLocation startLocation = orderViewModel.getStartLocation().getValue();
            if (startLocation != null) {
                centerMapToLocation(startLocation.toLocation());
            }
            showLocationMarker(screenPositionOfCenterMap);
        }

        if (orderViewModel.isOutOfService()) {
            showLocationMarker(screenPositionOfCenterMap);
        }
        if (orderViewModel.isOrdering()) {
            if (orderViewModel.getFinishLocation().getValue() == null) {
                showLocationMarker(screenPositionOfCenterMap);
                centerMapToLocation(centerCordsOfMap);
            }
        }

        if (!orderViewModel.isRiding()) {
            Log.d(TAG, "recalkMapProjection: centerMapToLocation " + centerCordsOfMap);
            if (currentRouteOnMap == null) {
                centerMapToLocation(centerCordsOfMap);
            }
        }
        ManfredLocation currOrderLocation = null;
        if (orderViewModel.isSearchingCar()) {
            if (orderViewModel.getCurrentRide().getValue() != null) {
                currOrderLocation = orderViewModel.getCurrentRide().getValue().getStartLocation();
            }
            if (currOrderLocation != null) {
                centerMapToLocation(currOrderLocation.toLocation());
            } else {
                centerMapToLocation(centerCordsOfMap);
            }
        }

    }

    private void bindLocationListener() {
        Log.d(TAG, "bindLocationListener: ");
        ManfredLocationManager.bindLocationListenerIn(OrderActivity.this, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                AppStatus currStatus = orderViewModel.getStatus().getValue();
                if (currStatus == AppStatus.READY_TO_ORDER || currStatus == AppStatus.OUT_OF_SERVICE || currStatus == AppStatus.NEED_CARD) {
                    //Log.d(TAG, "onLocationChanged: isNeedCenterToMyLocation? "+isNeedCenterToMyLocation.get());
                    if (isNeedCenterToMyLocation.get()) {
                        //Log.d(TAG, "onLocationChanged: centering to currLocation");
                        centerMapToLocation(location);
                    }
                }
                currentGeoLocation = location;
                /*
                if(orderViewModel.isOutOfService()){
                    orderViewModel.tryFindRegion(new ManfredLocation(location));
                }*/
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        }, getApplicationContext());
    }

    private void showPathOnMap(@Nullable ManfredLocation startLocation, @Nullable ManfredLocation endLocation) {
        if (startLocation == null || endLocation == null) return;
        Log.d(TAG, "showPathOnMap: start: " + startLocation.toString() + ", end: " + endLocation.toString());
        //use key without restriction because it java library
        if (mMap == null) return;
        hideCenterMarker();
        orderViewModel.getManfredDirectionsManager().calculatePath(startLocation, endLocation);
    }

    private void initRouteDrawing() {
        Log.d(TAG, "initRouteDrawing: ");
        orderViewModel.getManfredDirectionsManager().getPath().observe(this, new Observer<ManfredDirectionsManager.DirectionResult>() {
            @Override
            public void onChanged(@Nullable ManfredDirectionsManager.DirectionResult result) {
                Log.d(TAG, "new route: for display: " + result);
                if (mMap == null) return;
                if (result != null) {
                    if (result.isCached()) {
                        Log.d(TAG, "new route: it is cached, not redraw ");
                    } else {
                        removePathFromMap();
                        if (orderViewModel.isOrdering()) {
                            showStartAndFinishMarkers(result.getStartLocation(), result.getFinishLocation());
                        }
                        if (orderViewModel.isRiding()) {
                            OrderVM currOrder = orderViewModel.getCurrentRide().getValue();
                            if (currOrder != null) {
                                showPinTimeMarker(String.valueOf(currOrder.getTimeToCustomer()), result.getStartLocation());
                            }
                        }
                        if (orderViewModel.isFinishing()) {
                            showSmallPinMarker(result.getStartLocation());
                            showFinishMarker(result.getFinishLocation());
                        }
                        if (orderViewModel.isShowingPreOrderDetails()) {
                            showWithClockMarker(result.getStartLocation());
                            showFinishMarker(result.getFinishLocation());
                        }
                        Log.d(TAG, "addyng polyline to map");
                        currentRouteOnMap = mMap.addPolyline(new PolylineOptions().addAll(result.getPath()).color(getResources().getColor(R.color.route_line_color)));
                        fitToPolyline(currentRouteOnMap);
                    }
                }
            }
        });
    }

    private void showFinishMarker(ManfredLocation finishLocation) {
        //Log.d(TAG, "showFinishMarker: "+finishLocation.toString());
        if (mMap == null || finishLocation == null) {
            Log.d(TAG, "showFinishMarker: not showing");
            return;
        }
        MarkerOptions finishMarkerOptions = new MarkerOptions()
                .position(finishLocation.toLatLng())
                .icon(bitmapDescriptorFromVector(this, R.drawable.ic_pin_to_small));
        finishMarker = mMap.addMarker(finishMarkerOptions);
    }

    private void showPinTimeMarker(String text, ManfredLocation markerLocation) {
        Log.d(TAG, "showPinTimeMarker: " + text);
        if (mMap == null) return;
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.pin_marker_with_time, null);
        TextView textView = view.findViewById(R.id.marker_text);
        textView.setText(text);
        if (timeMarker != null) {
            timeMarker.remove();
            timeMarker = null;
        }
        timeMarker = mMap.addMarker(new MarkerOptions()
                .position(markerLocation.toLatLng())
                .icon(BitmapDescriptorFactory.fromBitmap(GraphicUtils.getBitmapFromView(view))));
        //Log.d(TAG, "showPinTimeMarker: showing " + timeMarker.getPosition());
    }

    private void showWithClockMarker(ManfredLocation markerLocation) {
        if (mMap == null) return;
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.pin_marker_with_clock, null);
        if (timeMarker != null) {
            timeMarker.remove();
            timeMarker = null;
        }
        timeMarker = mMap.addMarker(new MarkerOptions()
                .position(markerLocation.toLatLng())
                .icon(BitmapDescriptorFactory.fromBitmap(GraphicUtils.getBitmapFromView(view))));
    }

    private void showSmallPinMarker(ManfredLocation pinLocation) {
        if (mMap == null) return;
        Log.d(TAG, "showSmallPinMarker: " + pinLocation.toString());
        MarkerOptions finishMarker = new MarkerOptions()
                .position(pinLocation.toLatLng())
                .icon(bitmapDescriptorFromVector(this, R.drawable.ic_pin_from_black));
        smallStartPin = mMap.addMarker(finishMarker);
    }

    private void removeSmallPinMarker() {
        if (smallStartPin != null) {
            smallStartPin.remove();
            smallStartPin = null;
        }
    }

    private void showCarMarker(OrderVM order) {
        //Log.d(TAG, "showCarMarker: "+order);
        if (order == null) return;
        if (mMap == null | order.getDriverLocation() == null) return;
        if (myCarMarker != null) {
            myCarMarker.remove();
            myCarMarker = null;
        }
        MarkerOptions carMarker = new MarkerOptions()
                .position(order.getDriverLocation().toLatLng())
                .icon(BitmapDescriptorFactory.fromResource(order.getCarIcon()));
        myCarMarker = mMap.addMarker(carMarker);
    }

    private void moveCarMarker(ManfredLocation newLocation, Marker marker, long duration) {
        //Log.d(TAG, "moveCarMarker: ");
        //MarkerAnimation.move(mMap,marker, newLocation.toLocation());
        LatLng destLatLng = newLocation.toLatLng();
        LatLng prevLatLng = marker.getPosition();
        if (prevLatLng.equals(destLatLng)) {
            return;
        }
        Location prevLocation = new Location("");
        prevLocation.setLongitude(prevLatLng.longitude);
        prevLocation.setLatitude(prevLatLng.latitude);
        float bearing = prevLocation.bearingTo(newLocation.toLocation());
        marker.setRotation(bearing);
        double[] startValues = new double[]{marker.getPosition().latitude, marker.getPosition().longitude};
        double[] endValues = new double[]{destLatLng.latitude, destLatLng.longitude};
        ValueAnimator latLngAnimator = ValueAnimator.ofObject(new DoubleArrayEvaluator(), startValues, endValues);
        latLngAnimator.setDuration(duration);
        latLngAnimator.setInterpolator(new DecelerateInterpolator());
        latLngAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                double[] animatedValue = (double[]) animation.getAnimatedValue();
                marker.setPosition(new LatLng(animatedValue[0], animatedValue[1]));
/*                if(!isNeedCenterToMyLocation.get()){
                    if(timeMarker!=null) {
                        //ride to passenger
                        fitToMarkers(mMap, marker.getPosition(), timeMarker.getPosition());
                    }else {
                        if (finishMarker != null) {
                            fitToMarkers(mMap, marker.getPosition(), finishMarker.getPosition());
                        }else {
                            Location location = new Location("");
                            location.setLongitude(marker.getPosition().longitude);
                            location.setLatitude(marker.getPosition().latitude);
                            centerMapToLocation(location);
                        }
                    }
                }*/
            }
        });
        latLngAnimator.start();
/*        latLngAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                OrderVM currentRide = orderViewModel.getCurrentRide().getValue();
                if(orderViewModel.isRiding()) {
                    if (currentRide != null) {
                        showPathOnMap(currentRide.getStartLocation(), newLocation);
                    }
                }
            }
        });*/
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void hideCenterMarker() {
        //Log.d(TAG, "hideCenterMarker: ");
        binding.marker.getRoot().setVisibility(View.GONE);
    }

    /**
     * showing central marker
     */
    private void showLocationMarker(Point screenPositionOfCenterMap) {
        //Log.d(TAG, "showLocationMarker: "+screenPositionOfCenterMap);
        //do this because we have two markers after making order(delay in clearing map)
        binding.marker.getRoot().setVisibility(View.VISIBLE);
        binding.marker.getRoot().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "showLocationMarker: animating");
                float newY = screenPositionOfCenterMap.y - (binding.marker.getRoot().getHeight() - 35);
                //Log.d(TAG, "showLocationMarker: animMarker newY: "+newY);
                ObjectAnimator animation = ObjectAnimator.ofFloat(binding.marker.getRoot(), "Y",
                        newY);
                animation.setInterpolator(new LinearInterpolator());
                animation.setDuration(200);
                animation.start();
            }
        }, 100);
    }

    private void initActionBar() {
        DrawerHeaderBinding headerBinding =
                DataBindingUtil.inflate(getLayoutInflater(), R.layout.drawer_header, binding.navigationDrawer, false);
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_FILE_NAME, 0);
        String username = settings.getString(Constants.PREFS_USERNAME_FIELD, "");
        String phoneCode = settings.getString(Constants.PREFS_PHONE_CODE_FIELD, "");
        String userPhone = settings.getString(Constants.PREFS_PHONE_NUMBER_FIELD, "");
        binding.navigationDrawer.addHeaderView(headerBinding.getRoot());
        binding.navigationDrawer.setItemIconTintList(null);
        headerBinding.username.setText(username);
        String fullNumber = phoneCode + userPhone;
        headerBinding.userNumber.setText(fullNumber);
        setTitle("");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            //getSupportActionBar().withActionBarDrawerToggleAnimated(true);
        }

        final int delayMillis = 250;
        binding.navigationDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.nav_pay:
                        binding.drawerLayout.closeDrawers();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                orderViewModel.showCardsList();
                            }
                        }, delayMillis);
                        break;
                    case R.id.nav_intercom:
                        binding.drawerLayout.closeDrawers();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intercom.client().displayMessenger();
                            }
                        }, delayMillis);
                        break;
                    case R.id.nav_history:
                        binding.drawerLayout.closeDrawers();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(OrderActivity.this, HistoryActivity.class);
                                startActivity(intent);
                            }
                        }, delayMillis);
                        break;
                    case R.id.nav_tariffs:
                        binding.drawerLayout.closeDrawers();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                orderViewModel.showTariffsDetails();
                            }
                        }, delayMillis);

                        break;
                    case R.id.nav_promocodes:
                        binding.drawerLayout.closeDrawers();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent promocodesIntent = new Intent(OrderActivity.this, PromoCodeActivity.class);
                                promocodesIntent.putExtra(IS_FROM_MENU_START, true);
                                startActivity(promocodesIntent);
                            }
                        }, delayMillis);
                        break;
                    case R.id.nav_invites:
                        binding.drawerLayout.closeDrawers();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent inviteIntent = new Intent(OrderActivity.this, PromoCodeActivity.class);
                                inviteIntent.putExtra(IS_WANT_SHARE_PROMO, true);
                                startActivity(inviteIntent);
                            }
                        }, delayMillis);
                        break;
                    case R.id.nav_select_region:
                        binding.drawerLayout.closeDrawers();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent inviteIntent = new Intent(OrderActivity.this, SelectCityActivity.class);
                                startActivity(inviteIntent);
                            }
                        }, delayMillis);
                        break;
                }
                return true;
            }
        });
        binding.navigationDrawerBottom.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_about:
                        binding.drawerLayout.closeDrawers();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent aboutIntent = new Intent(OrderActivity.this, AboutServiceActivity.class);
                                startActivity(aboutIntent);
                            }
                        }, delayMillis);
                        break;
                }
                return true;
            }
        });

        toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.mainToolbar,
                R.string.home_navigation_drawer_open, R.string.home_navigation_drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //invalidateOptionsMenu();
            }
        };

        binding.drawerLayout.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.setDrawerSlideAnimationEnabled(true);
        toggle.getDrawerArrowDrawable().setColor(Color.BLACK);
        toggle.syncState();
        binding.mainToolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        binding.navigationDrawer.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderActivity.this, ProfileActivity.class);
                startActivity(intent);
                binding.drawerLayout.closeDrawers();
            }
        });

    }

    final Handler movingMarkerDelay = new Handler();
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: ");
        mMap = googleMap;
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        showMyLocationDot(googleMap);
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //Log.d(TAG, "onCameraIdle: ");
                //get LatLng at the center by calling
                LatLng midLatLng = googleMap.getCameraPosition().target;
                //Log.d(TAG, "onCameraIdle: " + midLatLng.toString());
                Location needLocation = new Location("");
                needLocation.setLatitude(midLatLng.latitude);
                needLocation.setLongitude(midLatLng.longitude);
                //currPinLocation = needLocation;
                if (orderViewModel.isNeedRefreshMarker()) {
                    /*
                    Random r = new Random();
                    int id = r.nextInt((100 - 1) + 1) + 1;
                    Log.d(TAG, "onCameraIdle: schedulle "+id+" for moving, removing old");*/
                    movingMarkerDelay.removeCallbacksAndMessages(null);
                    movingMarkerDelay.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Log.d(TAG, "onCameraIdle: need refresh marker, move marker for orderManager "+id);
                            orderViewModel.moveMyMarker(needLocation);
                        }
                    },500);

                }
                /*
                if (orderViewModel.isNeedCard()) {
                    //orderViewModel.moveMyMarker(needLocation);
                    orderViewModel.tryFindRegion(new ManfredLocation(needLocation));
                }*/
                if (orderViewModel.isOutOfService()) {
                    Log.d(TAG, "onCameraIdle: out of service, need try find region...");
                    orderViewModel.tryFindRegion(new ManfredLocation(needLocation));
                }
            }
        });
        googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int reason) {
                if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                    isNeedCenterToMyLocation.set(false);
                    binding.setMarkerState(new MarkerViewModel(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.LOADING, 0)));
                }
            }
        });

        orderViewModel.needCenterToCoords().observe(this, new Observer<ManfredLocation>() {
            @Override
            public void onChanged(@Nullable ManfredLocation manfredLocation) {
                if (manfredLocation != null) {
                    Log.d(TAG, "onChanged: needCenterToCoords " + manfredLocation.toString());
                    isNeedCenterToMyLocation.set(false);
                    centerMapToLocation(manfredLocation.toLocation());
                } else {
                    Log.d(TAG, "onChanged: needCenterToCoords: center to startLocation because we have null location ");
                    ManfredLocation location = orderViewModel.getStartLocation().getValue();
                    centerMapToLocation(location != null ? location.toLocation() : null);
                }
            }
        });

        recalkMapProjection();

        googleMap.clear();
        AppStatus appStatus = orderViewModel.getStatus().getValue();
        if (appStatus != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            changeRideStatus(appStatus, transaction);
            //using commitAllowingStateLoss to prevent java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
            transaction.commitAllowingStateLoss();
        } else {
            Log.d(TAG, "onMapReady: ride status is null");
        }

        //syncMapScrollState(googleMap);
    }

    private void showMyLocationDot(@NonNull GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }
    }

    @SuppressLint("MissingPermission")
    private void hideMyLocationDot(@NonNull GoogleMap googleMap) {
        googleMap.setMyLocationEnabled(false);
    }

    private void observeTariffChanging(TariffManager tariffManager) {
        tariffManager.getSelectedTariff().observe(this, new Observer<TariffViewModel>() {
            @Override
            public void onChanged(@Nullable TariffViewModel tariffViewModel) {
                if (tariffViewModel != null) {
                    if (mMap != null) {
                        LatLng midLatLng = mMap.getCameraPosition().target;
                        Location needLocation = new Location("");
                        needLocation.setLatitude(midLatLng.latitude);
                        needLocation.setLongitude(midLatLng.longitude);
                        orderViewModel.moveMyMarker(needLocation);
                    }
                }
            }
        });
    }

    private void showStartAndFinishMarkers(ManfredLocation startLocation, ManfredLocation manfredLocation) {
        AverageTimeToPassenger marker = orderViewModel.getMarkerState().getValue();
        if (marker != null) {
            if (marker.getMarkerState() == AverageTimeToPassenger.MarkerState.SHOW_TIME) {
                showPinTimeMarker(marker.getMarkerCaption(), startLocation);
            } else {
                showWithClockMarker(startLocation);
            }
        } else {
            showWithClockMarker(startLocation);
        }
        showFinishMarker(manfredLocation);
    }

    private void fitToMarkers(GoogleMap map, LatLng... markersPosition) {
        if (map == null) {
            Log.d(TAG, "fitToMarkers: map is null, ignoring");
            return;
        } else {
            Log.d(TAG, "fitToMarkers: " + markersPosition.length);
        }
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : markersPosition) {
            builder.include(latLng);
        }

        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.20); // offset from edges of the map 10% of screen

        isNeedCenterToMyLocation.set(false);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        map.moveCamera(cu);

        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                map.moveCamera(cu);
            }
        });

    }

    private void fitToPolyline(Polyline polyline) {
        Log.d(TAG, "fitToPolyline: ");
        if (mMap == null) return;
        if (polyline.getPoints().isEmpty()) return;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (int i = 0; i < polyline.getPoints().size(); i++) {
            builder.include(polyline.getPoints().get(i));
        }

        LatLngBounds bounds = builder.build();
        int padding = binding.marker.getRoot().getHeight() + getStatusBarHeight(); // offset from edges of the map in pixels

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 36);
        mMap.animateCamera(cu);
    }

    private void centerMapToRecognizedLocation(@NonNull Location location){
        if(mMap!=null){
            mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
                    location.getLongitude())));
        }
    }

    private void centerMapToLocation(Location location) {
        //Log.d(TAG, "centerMapToLocation: "+location);
        if (mMap != null) {
            if (location != null) {
                //Log.d(TAG, "centerMapToLocation: " + location);
                //Log.d(TAG, "centerMapToLocation: ");
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                        location.getLongitude()), 18));
/*                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                        location.getLongitude()), 18));*/
            }
        }else {
            Log.d(TAG, "centerMapToLocation: map not ready");
        }
    }

    @Override
    public void searchFrom() {
        Intent intent = new Intent(this, SelectAdressActivity.class);
        intent.putExtra(Constants.FINISH_FIELD, false);
        startActivity(intent);
    }

    @Override
    public void searchDestination() {
        Intent intent = new Intent(this, SelectAdressActivity.class);
        intent.putExtra(Constants.FINISH_FIELD, true);
        startActivity(intent);
    }

    private void startSearchAnimation() {
        Log.d(TAG, "startSearchAnimation: ");
        //binding.radar.setVisibility(View.VISIBLE);
        binding.radar.start();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if (mMap != null) {
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 10000, null);
                }
            }
        }, 1000);

        /*
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.growing);
        binding.locator.setVisibility(View.VISIBLE);
        binding.locator.setPadding(0, getStatusBarHeight(), 0, 0);
        binding.locator.setAlpha(1f);
        binding.locator.startAnimation(animation);*/
    }

    private void stopSearchAnimation() {
        binding.radar.stop();
        //binding.radar.setVisibility(View.GONE);
        if(mMap!=null){
            mMap.stopAnimation();
        }
        //Log.d(TAG, "stopSearchAnimation: ");

        /*
        binding.locator.clearAnimation();
        binding.locator.setVisibility(View.GONE);*/
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    public void setDrawerIndicatorState(HamburgerState state) {
        //https://stackoverflow.com/questions/38335216/transition-animation-from-hamburger-to-arrow-icon-on-adding-fragment
        //Log.d(TAG, "setDrawerIndicatorState: isHamburger? "+isHamburger);
        Log.d(TAG, "setDrawerIndicatorState: " + state.getState());
        if (state.getState() == HamburgerState.HambState.CLOSE) {
            toggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
            toggle.setHomeAsUpIndicator(R.drawable.ic_close_black_24dp); //set your own
            toggle.syncState();
            Log.d(TAG, "setDrawerIndicatorState: to close");
            //getSupportActionBar().setHomeAsUpIndicator();*/

        } else {
            toggle.setDrawerIndicatorEnabled(true);
            boolean isHamburger = (state.getState() == HamburgerState.HambState.HAMBURGER);
            DrawerArrowDrawable drawerArrow = new DrawerArrowDrawable(this);
            //drawerArrow.setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_ATOP);
            drawerArrow.setColor(Color.BLACK);
            binding.mainToolbar.setNavigationIcon(drawerArrow);
            ValueAnimator anim = isHamburger ? ValueAnimator.ofFloat(1, 0) : ValueAnimator.ofFloat(0, 1);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    //Log.d(TAG, "setDrawerIndicatorState: onAnimationUpdate: ");
                    float slideOffset = (Float) valueAnimator.getAnimatedValue();
                    drawerArrow.setProgress(slideOffset);
                }
            });
            anim.setInterpolator(new DecelerateInterpolator());
            anim.setDuration(400);
            anim.start();
        }

        binding.mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    binding.drawerLayout.closeDrawer(GravityCompat.START);
                } else if (state.getState() != HamburgerState.HambState.HAMBURGER) {
                    onBackPressed();
                } else {
                    binding.drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Log.d(TAG, "onOptionsItemSelected: ");
        switch (item.getItemId()) {
            case R.id.action_show_tariff_details:
                // User chose the "Settings" item, show the app settings UI...
                showTariffsDetails();
                hideCenterMarker();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showTariffsDetails() {
        orderViewModel.showTariffsDetails();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        //Log.d(TAG, "onCreateOptionsMenu: need show? "+isNeedToShowInfoButtonInActionBar.get());
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_action_bar, menu);
        MenuItem item = menu.findItem(R.id.action_show_tariff_details);
        item.setVisible(isNeedToShowInfoButtonInActionBar.get());
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        //Log.d(TAG, "onBackPressed: ");
        if (!((ManfredPassengerApplication) getApplication()).getConnectManager().isConnected())
            return;
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Log.d(TAG, "onBackPressed, driver don't open ");
            //super.onBackPressed();
            orderViewModel.cancel();
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    protected void onPause() {
        hideSelectDateDialog();
        stopRefreshingCars();
        stopCheckDriverLocation();
        super.onPause();
    }

    private void stopCheckDriverLocation() {
        Log.d(TAG, "stopCheckDriverLocation: ");
        if (checkForDriverLocationTask != null) {
            checkForDriverLocationTask.shutdownNow();
            checkForDriverLocationTask = null;
            driverLocationSchedulled = false;
        }
        if (myCarMarker != null) {
            myCarMarker.remove();
        }
    }

    private void stopRefreshingCars() {
        if (checkForCarsTask != null) {
            checkForCarsTask.shutdownNow();
            checkForCarsTask = null;
        }
        for (int i = 0; i < carMarkers.size(); i++) {
            long key = carMarkers.keyAt(i);
            // get the object by the key.
            Marker marker = carMarkers.get(key);
            marker.remove();
        }
        carMarkers.clear();
        ((ManfredPassengerApplication) getApplication()).getCarsManager().clearCars();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");

        toggle.syncState();
        HamburgerState hamburgerState = orderViewModel.getMenuIconState().getValue();
        if (hamburgerState != null) {
            setDrawerIndicatorState(hamburgerState);
        }

        //do it for reInit finish location(mb need listener?)
        AppStatus appStatus = orderViewModel.getStatus().getValue();
        if (appStatus != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            changeRideStatus(appStatus, transaction);
            //using commitAllowingStateLoss to prevent java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
            transaction.commitAllowingStateLoss();
        } else {
            Log.d(TAG, "onResume: ride status is null");
        }
        startRefreshingCars();
        //Log.d(TAG, "onResume: "+datePickerDialog);
        if (orderViewModel.isReadyForOrderState()) {
            showingCarsOnMap();
        }

        if (orderViewModel.isRiding()) {
            startGettingDriverLocation();
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            orderViewModel.locationPermissionDenied();
        } else {
            bindLocationListener();
            orderViewModel.permissionGranted();
        }

        checkForEnabledNotification();

        Long messageId = SharedData.getMessageId();
        Log.d(TAG, "got messageId = " + messageId);
        if(messageId != null) {
            int id = messageId.intValue();
            showPushMessage(id);
        } else {
            checkUnreadPushMessages();
        }
    }


    private void checkUnreadPushMessages() {
        final OrderActivity orderActivity = this;
        ((ManfredPassengerApplication)getApplication()).getPushMessageManager().getUnreadMessages(new PushMessageManager.UnreadPushMessagesCallback() {
            @Override
            public void pushMessagesReceived(@Nullable List<PushMessage> pushMessages) {
                if(pushMessages != null && !pushMessages.isEmpty()) {
                    for(PushMessage pushMessage: pushMessages) {
                        SharedData.addPushMessage(pushMessage);
                    }
                    Intent intent = new Intent(orderActivity, PushMessageActivity.class);
                    startActivity(intent);
                }

            }

        });

    }


    private void showPushMessage(int messageId) {
        final OrderActivity orderActivity = this;
        ((ManfredPassengerApplication)getApplication()).getPushMessageManager().getPushMessage(messageId, new PushMessageManager.PushMessageCallback() {
            @Override
            public void pushMessageReceived(@Nullable PushMessage pushMessage) {
                if(!pushMessage.isRead()) {
                    SharedData.addPushMessage(pushMessage);
                    Intent intent = new Intent(orderActivity, PushMessageActivity.class);
                    startActivity(intent);
                    SharedData.setMessageId(null);
                }
            }
        });
    }


    private void checkForEnabledNotification() {
        final NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
        boolean isPushEnabled = notificationManagerCompat.areNotificationsEnabled();
        Log.d(TAG, "checkForEnabledNotification: isPushEnabled "+ isPushEnabled );

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                Log.d(TAG, "checkForEnabledNotification: resetting push state");
                FirebaseTokenRepository firebaseTokenRepository = new FirebaseTokenRepository(((ManfredPassengerApplication) getApplication()).getTokenManager());
                firebaseTokenRepository.setFirebaseToken(instanceIdResult.getToken(),isPushEnabled);
            }
        });
    }

    private void startRefreshingCars() {
        Log.d(TAG, "startRefreshingCars: ");
        if (checkForCarsTask != null) return;
        AtomicBoolean isCarsRefreshCompleted = new AtomicBoolean(true);
        checkForCarsTask = Executors.newSingleThreadScheduledExecutor();
        FreeCarsManager freeCarsManager = ((ManfredPassengerApplication) getApplication()).getCarsManager();
        final FreeCarsManager.CarsOnMap currFreeCars = freeCarsManager.getFreeCars().getValue();
        if (currFreeCars != null) {
            Log.d(TAG, "startRefreshingCars: we have cars for display");
            showCarsOnMap(currFreeCars.getCarsOnMap(), currFreeCars.getOfflinedCarsIds());
        }
        checkForCarsTask.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "run: checkForCarsTask");
                if (!isCarsRefreshCompleted.get()) {
                    //Log.d(TAG, "run: previous request not complete, skipping");
                    return;
                }
                if (orderViewModel.isOutOfService()) {
                    //Log.d(TAG, "checkForCarsTask: isOutOfService");
                    ((ManfredPassengerApplication) getApplication()).getCarsManager().clearCars();
                    isCarsRefreshCompleted.set(true);
                    return;
                }
                isCarsRefreshCompleted.set(false);
                ManfredDriverRepository manfredDriverRepository = new ManfredDriverRepository(((ManfredPassengerApplication) getApplication()).getNetworkManager());
                manfredDriverRepository.getFreeCars(((ManfredPassengerApplication) getApplication()).getTokenManager().getAuthToken(),
                        new NetworkResponseCallback<CarsAnswer>() {
                            @Override
                            public void allOk(CarsAnswer response) {
                                //Log.d(TAG, "checkForCarsTask allOk: ");
                                isCarsRefreshCompleted.set(true);
                                TariffViewModel currTariff = ((ManfredPassengerApplication) getApplication()).getTariffManager().getSelectedTariff().getValue();
                                if (currTariff != null) {
                                    ((ManfredPassengerApplication) getApplication()).getCarsManager().refreshCars(response.getCars(), currTariff.getTariff());
                                } else {
                                    ((ManfredPassengerApplication) getApplication()).getCarsManager().clearCars();
                                }
                            }

                            @Override
                            public void error(ManfredNetworkError error) {
                                Log.d(TAG, "checkForCarsTask error: " + error);
                                isCarsRefreshCompleted.set(true);
                                if (error.getType() == ManfredNetworkError.ErrorType.TOKEN_ERROR) {
                                    Log.d(TAG, "error: it is token error, stopping");
                                }
                            }
                        });
            }
        }, 0L, 10, TimeUnit.SECONDS);
    }

    private void startGettingDriverLocation() {
        Log.d(TAG, "startGettingDriverLocation: ");
        if (checkForDriverLocationTask != null) {
            Log.d(TAG, "startGettingDriverLocation: already creating, ignoring");
            return;
        }
        if (driverLocationSchedulled) {
            Log.d(TAG, "startGettingDriverLocation: already schedulled, return");
            return;
        }
        AtomicBoolean isDriverLocationCompleted = new AtomicBoolean(true);
        checkForDriverLocationTask = Executors.newSingleThreadScheduledExecutor();
        checkForDriverLocationTask.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                if (!isDriverLocationCompleted.get()) {
                    //Log.d(TAG, "startGettingDriverLocation run: previous request not complete, skipping");
                    return;
                }
                isDriverLocationCompleted.set(false);
                ManfredDriverRepository manfredDriverRepository = new ManfredDriverRepository(((ManfredPassengerApplication) getApplication()).getNetworkManager());
                manfredDriverRepository.getCurrentDriverLocation(((ManfredPassengerApplication) getApplication()).getTokenManager().getAuthToken(),
                        new NetworkResponseCallback<DriverLocationResponse>() {
                            @Override
                            public void allOk(DriverLocationResponse response) {
                                if (response.getDriver() != null) {
                                    if (myCarMarker == null) return;
                                    Log.d(TAG, "getCurrentDriverLocation: " + response.getDriver());
                                    ManfredLocation driverLocation = new ManfredLocation(Double.parseDouble(response.getDriver().getLatitude()),
                                            Double.parseDouble(response.getDriver().getLongitude()));
                                    moveCarMarker(driverLocation, myCarMarker, 15000);
                                    //orderViewModel.isRiding()
                                }
                                isDriverLocationCompleted.set(true);
                            }

                            @Override
                            public void error(ManfredNetworkError error) {
                                isDriverLocationCompleted.set(true);
                            }
                        });
            }
        }, 0L, 15, TimeUnit.SECONDS);
        driverLocationSchedulled = true;
        Log.d(TAG, "startGettingDriverLocation: created");
    }

    private boolean isCarHided=false;

    public void hideCarsOnMap(){
        isCarHided=true;
        for (int i = 0; i < carMarkers.size(); i++) {
            long key = carMarkers.keyAt(i);
            // get the object by the key.
            Marker marker = carMarkers.get(key);
            marker.remove();
        }
        carMarkers.clear();
    }

    public void showingCarsOnMap(){
        isCarHided=false;
        FreeCarsManager freeCarsManager = ((ManfredPassengerApplication) getApplication()).getCarsManager();
        final FreeCarsManager.CarsOnMap carsOnMapLiveData = freeCarsManager.getFreeCars().getValue();
        if(carsOnMapLiveData!=null) {
            showCarsOnMap(carsOnMapLiveData.getCarsOnMap(),carsOnMapLiveData.getOfflinedCarsIds());
        }
    }

    private void showCarsOnMap(List<CarOnMap> cars, List<Integer> needToRemove) {
        if(isCarHided)return;
        if (mMap == null) return;
        //Log.d(TAG, "showCarsOnMap: need show "+cars.size()+" cars on map");
        for (CarOnMap carOnMap : cars) {
            Marker marker = carMarkers.get(carOnMap.getId(), null);
            if (marker == null) {
                marker = createCarMarker(carOnMap);
                if (marker == null) {
                    continue;
                }
                carMarkers.put(carOnMap.getId(), marker);
            } else {
                //Log.d(TAG, "showCarsOnMap: icon of car "+carOnMap.getId()+" is "+carOnMap.getCarIcon());
                marker.setIcon(BitmapDescriptorFactory.fromResource(carOnMap.getCarIcon()));
            }
            //Log.d(TAG, "showCarsOnMap: showing "+carOnMap.toString());
            if (carOnMap.isBusy()) {
                //Log.d(TAG, "showCarsOnMap: it car busy, setting alpha");
                marker.setAlpha(0.5f);
            } else {
                marker.setAlpha(1f);
            }
            //MarkerAnimation.move(mMap,marker, carOnMap.getLocation());
            moveCarMarker(new ManfredLocation(carOnMap.getLocation()), marker, 10000);

            /*
            marker.setPosition(new LatLng(carOnMap.getLocation().getLatitude(), carOnMap.getLocation().getLongitude()));
            marker.setRotation(carOnMap.getBearing());*/

            //MarkerAnimation.animateMarkerToICS(ourGlobalMarker, newLatLng, new LatLngInterpolator.Spherical());
        }
        //removing cars what go offline
        for (Integer carId : needToRemove) {
            Marker currMark = carMarkers.get(carId);
            if (currMark != null) {
                currMark.remove();
            }
            carMarkers.remove(carId);
        }
    }

    private Marker createCarMarker(CarOnMap carOnMap) {
        Log.d(TAG, "createCarMarker: ");
        if (mMap == null) return null;
        LatLng latLng = new LatLng(carOnMap.getLocation().getLatitude(), carOnMap.getLocation().getLongitude());
        MarkerOptions carMarker = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(carOnMap.getCarIcon()));
        return mMap.addMarker(carMarker);
    }

    //https://stackoverflow.com/questions/43963206/marshmallow-windowlightstatusbar-true-not-working-in-mi-phone
    public boolean setMiuiStatusBarDarkMode(Activity activity, boolean darkmode) {
        Class<? extends Window> clazz = activity.getWindow().getClass();
        try {
            int darkModeFlag = 0;
            @SuppressLint("PrivateApi") Class<?> layoutParams = Class.forName("android.view.MiuiWindowManager$LayoutParams");
            Field field = layoutParams.getField("EXTRA_FLAG_STATUS_BAR_DARK_MODE");
            darkModeFlag = field.getInt(layoutParams);
            Method extraFlagField = clazz.getMethod("setExtraFlags", int.class, int.class);
            extraFlagField.invoke(activity.getWindow(), darkmode ? darkModeFlag : 0, darkModeFlag);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

 }
