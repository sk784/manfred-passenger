package manfred.ru.manfredpassenger.ride.models;

import android.location.Location;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

public class ManfredDTOLocation {

    @SerializedName("address")
    private String address;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("fixed_longitude")
    private String fixedLongitude;

    @SerializedName("accuracy")
    private String accuracy;

    @SerializedName("fixed_latitude")
    private String fixedLatitude;

    @SerializedName("id")
    private int id;

    @SerializedName("longitude")
    private String longitude;

    public String getAddress() {
        return address;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getFixedLongitude() {
        return fixedLongitude;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public String getFixedLatitude() {
        return fixedLatitude;
    }

    public int getId() {
        return id;
    }

    public String getLongitude() {
        return longitude;
    }

    public ManfredDTOLocation(String address, String latitude, String accuracy, String longitude) {
        this.address = address;
        this.latitude = latitude;
        this.accuracy = accuracy;
        this.longitude = longitude;
    }

    public ManfredDTOLocation(Location location) {
        this.latitude = String.valueOf(location.getLatitude());
        this.longitude = String.valueOf(location.getLongitude());
        this.accuracy = String.valueOf(location.getAccuracy());
    }

    public ManfredDTOLocation(ManfredLocation manfredLocation){
        this.latitude = String.valueOf(manfredLocation.getLatitude());
        this.longitude = String.valueOf(manfredLocation.getLongitude());
        this.address = manfredLocation.getAddress();
    }

    public LatLng toLatLng() {
        return new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
    }

    public Location toLocation() {
        Location location = new Location("");
        location.setLongitude(Double.parseDouble(longitude));
        location.setLatitude(Double.parseDouble(latitude));
        return location;
    }

    @Nullable public ManfredLocation toManfredLocation(){
        if(latitude.isEmpty() || longitude.isEmpty()){
            return null;
        }else {
            return new ManfredLocation(Double.parseDouble(latitude),Double.parseDouble(longitude),address,"");
        }
    }


    @Override
    public String toString() {
        return "ManfredDTOLocation{" +
                "latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}