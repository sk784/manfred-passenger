package manfred.ru.manfredpassenger.ride.viewmodels;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import manfred.ru.manfredpassenger.ride.managers.ManfredLocationAutocompliteManager;

public class AutocompleteViewModelFactory implements ViewModelProvider.Factory {
    private final ManfredLocationAutocompliteManager manfredLocationAutocompliteManager;

    public AutocompleteViewModelFactory(ManfredLocationAutocompliteManager manfredLocationAutocompliteManager) {
        this.manfredLocationAutocompliteManager = manfredLocationAutocompliteManager;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(AutocompleteViewModel.class)) {
            return (T) new AutocompleteViewModel(manfredLocationAutocompliteManager);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
