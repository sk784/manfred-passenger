package manfred.ru.manfredpassenger.ride.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

@Entity(tableName = "last_places")
public class ManfredLocationDB {
    private static final String TAG = "ManfredLocationDB";
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "street_id")
    private String id;
    @Embedded
    private ManfredLocation manfredLocation;

    @ColumnInfo(name = "is_from")
    private boolean isFrom;

    public ManfredLocationDB(ManfredLocation manfredLocation, boolean isFrom, @NonNull String id) {
        Log.d(TAG, "ManfredLocationDB: id is "+id);
        this.id=id;
        this.manfredLocation = manfredLocation;
        this.isFrom = isFrom;
    }

    public void setManfredLocation(ManfredLocation manfredLocation) {
        this.manfredLocation = manfredLocation;
    }

    public void setFrom(boolean from) {
        isFrom = from;
    }

    public ManfredLocation getManfredLocation() {
        return manfredLocation;
    }

    public boolean isFrom() {
        return isFrom;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "ManfredLocationDB{" +
                "id='" + id + '\'' +
                '}';
    }
}
