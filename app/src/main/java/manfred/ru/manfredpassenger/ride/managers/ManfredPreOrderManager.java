package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.services.models.PreOrderListResponse;
import manfred.ru.manfredpassenger.api.services.models.event.Event;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.repositories.OrderRepository;
import manfred.ru.manfredpassenger.ride.repositories.SocketOrderRepository;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public class ManfredPreOrderManager implements PreOrderManager {
    private final ManfredTokenManager tokenManager;
    private final NetworkManagerProvider networkManager;
    private MutableLiveData<List<OrderVM>> preOrders = new MutableLiveData<>();
    private MutableLiveData<OrderVM>lastOrder = new MutableLiveData<>();
    private MutableLiveData<OrderVM>selectedOrder = new MutableLiveData<>();
    private MutableLiveData<String> error = new MutableLiveData<>();
    private static final String TAG = "ManfredPreOrderManager";

    public ManfredPreOrderManager(ManfredTokenManager tokenManager, NetworkManagerProvider networkManager) {
        this.tokenManager = tokenManager;
        this.networkManager = networkManager;
        refreshPreOrders();
    }

    public MutableLiveData<String> getError() {
        return error;
    }

    private void setPreOrders(List<OrderVM> orders) {
        OrderVM selectedPreOrder = selectedOrder.getValue();
        for (OrderVM order : orders)
        if(selectedPreOrder!=null){
            if(order.getId()==selectedPreOrder.getId()){
                selectedOrder.postValue(order);
            }
        }
        Log.d(TAG, "addPreOrder: size of preOrders now is "+orders.size());
        preOrders.postValue(orders);
        Comparator<OrderVM> cmp = new Comparator<OrderVM>() {
            @Override
            public int compare(OrderVM order1, OrderVM order2) {
                return order1.getCarNeedTimeDate().compareTo(order2.getCarNeedTimeDate());
            }
        };
        if (!orders.isEmpty()) {
            lastOrder.postValue(Collections.min(orders, cmp));
        }
    }

    @Override
    public void preOrderTimeOut(OrderVM order) {
        refreshPreOrders();
        error.postValue("Нет свободных автомобилей "+order.getTariff().getTitle()+" для выполнения брони в "+order.getStartRideTimeHHMM());
    }

    @Override
    public LiveData<List<OrderVM>> getPreOrders() {
        return preOrders;
    }

    @Override
    public LiveData<OrderVM> getLastPreOrder() {
        return lastOrder;
    }

    @Override
    public void refreshPreOrders(){
        Log.d(TAG, "refreshPreOrders: ");
        OrderRepository orderRepository = new SocketOrderRepository(tokenManager, networkManager);
        orderRepository.getPreOrders(new NetworkResponseCallback<PreOrderListResponse>() {
            @Override
            public void allOk(PreOrderListResponse response) {
                Log.d(TAG, "allOk: we have "+response.getOrders().size()+" preOrders");
                preOrders.postValue(null);
                lastOrder.postValue(null);
                List<OrderVM>orderVMS = new ArrayList<>();
                for (Event event : response.getOrders()){
                    orderVMS.add(new OrderVM(event));
                }
                setPreOrders(orderVMS);
            }

            @Override
            public void error(ManfredNetworkError error) {

            }
        });
    }

    @Override
    public void selectPreOrderForDetails(int preOrderId) {
        List<OrderVM> orders = preOrders.getValue();
        if(orders!=null){
            for (OrderVM preOrder : orders){
                if(preOrder.getId()==preOrderId){
                    selectedOrder.postValue(preOrder);
                    break;
                }
            }
        }
    }

    @Override
    public LiveData<OrderVM> getDetailedPreOrder() {
        return selectedOrder;
    }

}
