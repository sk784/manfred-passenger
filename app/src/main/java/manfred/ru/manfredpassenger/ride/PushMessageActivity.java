package manfred.ru.manfredpassenger.ride;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.common.models.PushMessage;
import manfred.ru.manfredpassenger.ride.managers.PushMessageManager;
import manfred.ru.manfredpassenger.utils.SharedData;

public class PushMessageActivity  extends AppCompatActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_push_message);
		Toolbar myToolbar = findViewById(R.id.push_message_toolbar);
		setSupportActionBar(myToolbar);
		final ActionBar actionBar = getSupportActionBar();
		if(actionBar!=null){
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		PushMessage pushMessage = SharedData.getPushMessage();
		loadPushMessage(pushMessage);

		//setting black background for status bar
		Window window = getWindow();
		window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		window.setStatusBarColor(ContextCompat.getColor(this,R.color.black_10));

	}


	private void loadPushMessage(PushMessage pushMessage) {
		setTitle("Новости");
		WebView description = findViewById(R.id.wv_push_message_content);
		description.loadData("<h1>" + pushMessage.getCaption() + "</h1>" + pushMessage.getContent(), "text/html; charset=utf-8", "UTF-8");
		((ManfredPassengerApplication)getApplication()).getPushMessageManager().setMessageRead(pushMessage.getId());
	}


	@Override
	public boolean onSupportNavigateUp(){
		PushMessage pushMessage = SharedData.getPushMessage();
		if(pushMessage != null) {
			loadPushMessage(pushMessage);
		} else {
			finish();
		}
		return true;
	}


	@Override
	public void onBackPressed() {
		PushMessage pushMessage = SharedData.getPushMessage();
		if(pushMessage != null) {
			loadPushMessage(pushMessage);
		} else {
			super.onBackPressed();
		}
	}
}
