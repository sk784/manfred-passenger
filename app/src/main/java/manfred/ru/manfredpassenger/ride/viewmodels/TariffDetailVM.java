package manfred.ru.manfredpassenger.ride.viewmodels;

import android.util.Log;

import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.utils.CostUtils;

public class TariffDetailVM {
    private static final String TAG = "TariffDetailVM";
    private final int id;
    private final String tariffName;
    private final String carTypes;
    private final String servePrice;
    private final String oneMinutePrice;
    private final String oneKilometerPrice;
    private final String minimumCostOfRide;
    private final String maxPassengerCapacity;
    private final String maxBaggageCapacity;
    private final String carImageUrl;
    private final String fullHTMLDescr;
    private final boolean isAssistantAllowed;

    public boolean isAssistantAllowed() {
        return isAssistantAllowed;
    }

    public TariffDetailVM(Tariff tariff, boolean withAssistent) {
        this.id = tariff.getId();
        this.tariffName = tariff.getTitle();
        this.carTypes = tariff.getDesc();
        if(withAssistent) {
            this.servePrice = CostUtils.removeAfterDotsIfNeed(tariff.getRideToPassengerPriceAssistant())+" "+tariff.getCurrencySymbol();
        }else {
            this.servePrice = CostUtils.removeAfterDotsIfNeed(tariff.getRideToPassengerPrice())+" "+tariff.getCurrencySymbol();
        }
        if(withAssistent){
            this.oneMinutePrice=CostUtils.removeAfterDotsIfNeed(tariff.getRideMinutePriceAssistant())+" "+tariff.getCurrencySymbol();
        }else {
            this.oneMinutePrice=CostUtils.removeAfterDotsIfNeed(tariff.getRideMinutePrice())+" "+tariff.getCurrencySymbol();
        }
        if(withAssistent){
            this.oneKilometerPrice=CostUtils.removeAfterDotsIfNeed(tariff.getRideKmPriceAssistant())+" "+tariff.getCurrencySymbol();
        }else {
            this.oneKilometerPrice=CostUtils.removeAfterDotsIfNeed(tariff.getRideKmPrice())+" "+tariff.getCurrencySymbol();
        }
        if(withAssistent){
            this.minimumCostOfRide=CostUtils.removeAfterDotsIfNeed(tariff.getPrepaymentPriceAssistant())+" "+tariff.getCurrencySymbol();
        }else {
            this.minimumCostOfRide=CostUtils.removeAfterDotsIfNeed(tariff.getPrepaymentPrice())+" "+tariff.getCurrencySymbol();
        }
        this.isAssistantAllowed=tariff.isAssistantAllowed();

        this.maxPassengerCapacity=String.valueOf(tariff.getMaximumPassengers());
        this.maxBaggageCapacity=String.valueOf(tariff.getMaximumBaggage());
        this.carImageUrl=tariff.getCarImage();
        if(withAssistent) {
            this.fullHTMLDescr = tariff.getFormattedDescriptionAssistent();
        }else {
            this.fullHTMLDescr = tariff.getFormattedDescription();
        }
    }

    public String getTariffName() {
        return tariffName;
    }

    public String getCarTypes() {
        return carTypes;
    }

    public String getServePrice() {
        return servePrice;
    }

    public String getOneMinutePrice() {
        return oneMinutePrice;
    }

    public String getOneKilometerPrice() {
        return oneKilometerPrice;
    }

    public String getMinimumCostOfRide() {
        return minimumCostOfRide;
    }

    public String getMaxPassengerCapacity() {
        return maxPassengerCapacity;
    }

    public String getMaxBaggageCapacity() {
        return maxBaggageCapacity;
    }

    public String getCarImageUrl() {
        return carImageUrl;
    }

    public int getId() {
        return id;
    }

    public String getFullHTMLDescr() {
        return fullHTMLDescr;
    }
}
