package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.location.Location;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.ride.models.Car;
import manfred.ru.manfredpassenger.ride.managers.mapOverlay.models.CarOnMap;
import manfred.ru.manfredpassenger.ride.models.CarTariff;
import manfred.ru.manfredpassenger.ride.models.Driver;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;

public class ManfredFreeCarsManager implements FreeCarsManager {
    private static final String TAG = "ManfredFreeCarsManager";
    private final List<Car>allCarsCache = new ArrayList<>();
    private MutableLiveData<CarsOnMap>currCarsOnMap;
    private HashMap<Integer, CarOnMap> carsOnMap = new HashMap<Integer, CarOnMap>();

    public ManfredFreeCarsManager() {
        this.currCarsOnMap = new MutableLiveData<>();
    }

    @Override
    public void refreshCars(List<Car> cars,Tariff tariff) {
        //Log.d(TAG, "refreshCars: ");
        allCarsCache.clear();
        allCarsCache.addAll(cars);
        tariffChanged(tariff);
    }

    private void recalculateCars(List<Car> cars, Tariff currTariff) {
       // Log.d(TAG, "recalculateCars: count of cars is "+cars.size());
        //int i=0;

        for (Car car : cars) {
            //Log.d(TAG, "recalculateCars: step "+i);
            //Log.d(TAG, "recalculateCars: processing driver "+car.getName());
            //carsOnMap.get(car.getId(),new CarOnMap(car.getId(),createLocation(car),!car.is_free())).setLocation(createLocation(car));
            CarOnMap currCar = carsOnMap.get(car.getDriver().getId());
            if (currCar == null) {
                //Log.d(TAG, "recalculateCars: new driver, add");
                carsOnMap.put(car.getDriver().getId(), new CarOnMap(car.getDriver().getId(), createLocation(car.getDriver()),
                        !car.getDriver().isIsFree(),currTariff));
            } else {
                currCar.setTariff(currTariff);
                if (isBetterLocation(currCar.getLocation(), createLocation(car.getDriver()))) {
                    //Log.d(TAG, "recalculateCars: new location is better, refreshing");
                    currCar.setLocation(createLocation(car.getDriver()));
                    currCar.setBusy(!car.getDriver().isIsFree());
                    carsOnMap.put(car.getDriver().getId(), currCar);
                } /*else {
                    Log.d(TAG, "recalculateCars: new location not better, ignoring");
                }*/
            }
        }
    }

    private Location createLocation(Driver driver) {
        Location location = new Location("");
        //Log.d(TAG, "createLocation: "+driver.toString());
        if (driver.getManfredLocation() == null) return location;
        location = driver.getManfredLocation().toLocation();
        return location;
    }

    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        boolean isBetter = false;
        float newLocAccuracy = location.getAccuracy();
        float locDelta = currentBestLocation.distanceTo(location);
        if (locDelta > newLocAccuracy) isBetter = true;
        return isBetter;
    }

    private List<Integer> removeOfflineDriversFromMap(List<Car> cars) {
        List<Integer> offlineIds = new ArrayList<>();
        ArrayList<Integer> idCarsOnMap = new ArrayList<>(carsOnMap.keySet());
        ArrayList<Integer> idCarsFromServer = new ArrayList<>();
        for (Car car : cars) {
            idCarsFromServer.add(car.getDriver().getId());
        }
        for (Integer carIdOnMap : idCarsOnMap) {
            if (!idCarsFromServer.contains(carIdOnMap)) {
                //Log.d(TAG, "removeOfflineDriversFromMap: not have car with id " + carIdOnMap + ", removing");
                offlineIds.add(carIdOnMap);
                carsOnMap.remove(carIdOnMap);
            }
        }
        return offlineIds;
    }

    private List<Car> filterCarWithTaridd(int id, List<Car>cars){
        //Log.d(TAG, "filterCarWithTaridd: filter for tariff "+id);
        List<Car>actualCars=new ArrayList<>();
        for (Car car : cars) {
            for (CarTariff carTariff : car.getTariffs()){
                //Log.d(TAG, "filterCarWithTaridd: testing car "+car.getDriver().getName()+" tariff is "+carTariff.getTariff());
                if(carTariff.getTariff()==id){
                    actualCars.add(car);
                    //Log.d(TAG, "filterCarWithTaridd: find car "+car.getDriver().getName()+" in tariff");
                    break;
                }
            }
        }
        //Log.d(TAG, "filterCarWithTaridd: filtered cars "+actualCars.size());
        return actualCars;
    }

    @Override
    public LiveData<CarsOnMap>getFreeCars() {
        return currCarsOnMap;
    }

    @Override
    public void tariffChanged(Tariff tariff) {
        List<Car>actualCars = filterCarWithTaridd(tariff.getId(),allCarsCache);
        recalculateCars(actualCars,tariff);
        List<Integer>removingCars = removeOfflineDriversFromMap(actualCars);
        CarsOnMap refreshedCarsOnMap = new CarsOnMap(new ArrayList<>(carsOnMap.values()),removingCars);
        currCarsOnMap.postValue(refreshedCarsOnMap);
    }

    @Override
    public void clearCars() {
        allCarsCache.clear();
        List<Integer>removingCars = removeOfflineDriversFromMap(new ArrayList<>());
        currCarsOnMap.postValue(new CarsOnMap(new ArrayList<>(carsOnMap.values()),removingCars));
    }
}
