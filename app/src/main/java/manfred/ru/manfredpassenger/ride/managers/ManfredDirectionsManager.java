package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.services.models.googleMaps.directions.DirectionResults;
import manfred.ru.manfredpassenger.api.services.models.googleMaps.directions.Legs;
import manfred.ru.manfredpassenger.api.services.models.googleMaps.directions.Route;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.utils.MapUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManfredDirectionsManager {
    public class DirectionResult{
        private final List<LatLng>path;
        private final String timeInMin;
        private final ManfredLocation startLocation;
        private final ManfredLocation finishLocation;
        private final boolean cached;

        public ManfredLocation getStartLocation() {
            return startLocation;
        }

        public DirectionResult(List<LatLng> path, String timeInMin, ManfredLocation startLocation, ManfredLocation finishLocation, boolean cached) {
            this.path = path;
            this.timeInMin = timeInMin;
            this.startLocation = startLocation;

            this.finishLocation = finishLocation;
            this.cached = cached;
        }

        public boolean isCached() {
            return cached;
        }

        public List<LatLng> getPath() {
            return path;
        }

        public String getTimeInMin() {
            return timeInMin;
        }

        public ManfredLocation getFinishLocation() {
            return finishLocation;
        }

        @Override
        public String toString() {
            return "DirectionResult{" +
                    "startLocation=" + startLocation +
                    ", finishLocation=" + finishLocation +
                    '}';
        }
    }

    private static final String TAG = "ManfredDirectionsManage";
    private final MutableLiveData<DirectionResult> path;
    private final String apiKey;
    @Nullable private StartEndLoc currentDrawnLocations;
    @Nullable private DirectionResult currentDirectionResult;

    public ManfredDirectionsManager(String apiKey) {
        path = new MutableLiveData<>();
        this.apiKey = apiKey;
    }

    @NotNull
    @Override
    public String toString() {
        return "ManfredDirectionsManager{" +
                "currentDrawnLocations=" + currentDrawnLocations +
                ", currentDirectionResult=" + currentDirectionResult +
                '}';
    }

    public void calculatePath(ManfredLocation startLocation, ManfredLocation finishLocation) {
        Log.d(TAG, "calculatePath: "+startLocation+", finish is "+finishLocation);
        StartEndLoc wantToDraw = new StartEndLoc(startLocation, finishLocation);
        if(currentDrawnLocations!=null){
            if(wantToDraw.equals(currentDrawnLocations)){
                Log.d(TAG, "calculatePath: not need to draw, return");
                if(currentDirectionResult!=null) {
                    path.postValue(new DirectionResult(currentDirectionResult.path, currentDirectionResult.timeInMin,
                            startLocation, currentDirectionResult.finishLocation, true));
                }
                return;
            }
        }
        String origin = startLocation.getLatitude() + "," + startLocation.getLongitude();
        String destination = finishLocation.getLatitude() + "," + finishLocation.getLongitude();
        APIClient.getMapService().getDirection(origin, destination, apiKey).enqueue(new Callback<DirectionResults>() {
            @Override
            public void onResponse(@NonNull Call<DirectionResults> call, @NonNull Response<DirectionResults> response) {
                DirectionResults directionResults = response.body();
                if (directionResults != null) {
                    //Log.d(TAG, "calculatePath onResponse: ");
                    List<Route> routes = directionResults.getRoutes();
                    List<com.google.android.gms.maps.model.LatLng> coordsOfRoute = new ArrayList<com.google.android.gms.maps.model.LatLng>();
                    int time = 0;
                    for (Route route : routes) {
                        coordsOfRoute.addAll(MapUtils.decodePoly(route.getOverviewPolyLine().getPoints()));
                        for (Legs leg : route.getLegs()){
                            time = time + leg.getDuration().getValue();
                        }
                    }
                    currentDirectionResult = new DirectionResult(coordsOfRoute,String.valueOf(time), startLocation, finishLocation, false);
                    path.setValue(currentDirectionResult);
                    currentDrawnLocations = new StartEndLoc(startLocation, finishLocation);
                }

            }

            @Override
            public void onFailure(@NonNull Call<DirectionResults> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    public MutableLiveData<DirectionResult> getPath() {
        return path;
    }

    private class StartEndLoc{
        private final ManfredLocation startLocation;
        private final ManfredLocation finishLocation;

        StartEndLoc(ManfredLocation startLocation, ManfredLocation finishLocation) {
            this.startLocation = startLocation;
            this.finishLocation = finishLocation;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            final StartEndLoc that = (StartEndLoc) o;

            if (!startLocation.equals(that.startLocation)) return false;
            return finishLocation.equals(that.finishLocation);
        }

        @Override
        public String toString() {
            return "StartEndLoc{" +
                    "startLocation=" + startLocation +
                    ", finishLocation=" + finishLocation +
                    '}';
        }

        @Override
        public int hashCode() {
            int result = startLocation.hashCode();
            result = 31 * result + finishLocation.hashCode();
            return result;
        }
    }
}
