package manfred.ru.manfredpassenger.ride.fragments.errors;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.cards.PaymentsActivity;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.managers.CreditCardManager;
import manfred.ru.manfredpassenger.cards.managers.ManfredCreditCardManager;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.common.models.PaymentSystem;
import manfred.ru.manfredpassenger.databinding.FragmentNeedCardBinding;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class NeedCardFragment extends Fragment {


    public NeedCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final FragmentNeedCardBinding binding = FragmentNeedCardBinding.inflate(getLayoutInflater());
        binding.addCard.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), PaymentsActivity.class);
            startActivity(intent);
        });
        RegionManager regionManager = ((ManfredPassengerApplication)getActivity().getApplication()).getRegionManager();
        PaymentSystem paymentSystem = regionManager.getCurrentPaymentSystem();
        binding.skip.setOnClickListener(v -> {
            OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
            model.skipNeedCard();
        });

        if(isNoCardsInRegion(paymentSystem)){
            String regionName = regionManager.getCurrentRegion().getValue()!=null ? regionManager.getCurrentRegion().getValue().getName() : "";
            binding.tvNeedCardCaption.setText("Чтобы совершить поездки в "+regionName+", добавьте вашу карту заново ");
        }
        return binding.getRoot();
    }

    /**
     * @return true if have cards, but out of region
     */
    boolean isNoCardsInRegion(PaymentSystem paymentSystem){
        //isAvailable = currentPaymentSystem.getId() == creditCard.getPaymentSystem().getId();
        List<CreditCard>creditCards = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredCreditCardManager().cards().getValue();
        if(creditCards==null || creditCards.isEmpty())return false;

        if(paymentSystem==null)return false;
        for (CreditCard creditCard : creditCards){
            if(creditCard.getPaymentSystem().getId()==paymentSystem.getId()){
                return false;
            }
        }
        return true;
    }

}
