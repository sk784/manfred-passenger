package manfred.ru.manfredpassenger.ride.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.models.ManfredLocationDB;

@Database(entities = {ManfredLocationDB.class},version = 3, exportSchema = false)
public abstract class ManfredPassengerDatabase extends RoomDatabase{
    public abstract PlacesDao placesDao();
}
