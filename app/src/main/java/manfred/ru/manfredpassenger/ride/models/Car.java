package manfred.ru.manfredpassenger.ride.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Car {

    @SerializedName("color")
    private String color;

    @SerializedName("driver")
    private Driver driver;

    @SerializedName("car_number")
    private String carNumber;

    @SerializedName("assist")
    private Assist assist;

    @SerializedName("car_type")
    private CarType carType;

    @SerializedName("tariffs")
    private List<CarTariff> tariffs;

    public String getColor() {
        return color;
    }

    public Driver getDriver() {
        return driver;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public Assist getAssist() {
        return assist;
    }

    public CarType getCarType() {
        return carType;
    }

    public List<CarTariff> getTariffs() {
        return tariffs;
    }

    public void setTariffs(List<CarTariff> tariffs) {
        this.tariffs = tariffs;
    }


}