package manfred.ru.manfredpassenger.ride.models;

public class GoogleCopyrightPlaceItem implements PlaceItem {
    @Override
    public PredictType getType() {
        return PredictType.GOOGLE_COPYRIGHT;
    }
}
