package manfred.ru.manfredpassenger.ride.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatus;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;
import manfred.ru.manfredpassenger.promocodes.models.PromoCodeVM;
import manfred.ru.manfredpassenger.ride.managers.ManfredDirectionsManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredOrderManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredPreOrderManager;
import manfred.ru.manfredpassenger.ride.managers.OrderManager;
import manfred.ru.manfredpassenger.ride.managers.PreCalculateCostManager;
import manfred.ru.manfredpassenger.ride.managers.TariffManager;
import manfred.ru.manfredpassenger.ride.managers.TimeToDriverManager;
import manfred.ru.manfredpassenger.ride.models.AverageTimeToPassenger;
import manfred.ru.manfredpassenger.ride.models.Car;
import manfred.ru.manfredpassenger.ride.models.DriverDelay;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.utils.CostUtils;
import manfred.ru.manfredpassenger.utils.SingleLiveEvent;

public class OrderViewModel extends ViewModel {
    private static final String TAG = "OrderViewModel";

    private final MutableLiveData<List<Car>> carsOnMap = new MutableLiveData<>();
    private final ManfredOrderManager orderManager;
    private final RegionManager regionManager;
    private final ManfredPreOrderManager preOrderManager;
    private final AppStatusManager appStatusManager;
    private final ManfredDirectionsManager manfredDirectionsManager;
    private final TariffManager tariffManager;
    private final TimeToDriverManager timeToDriverManager;
    private final AnalyticsManager analyticsManager;
    //private final MediatorLiveData<ManfredNetworkError> manfredNetworkError = new MediatorLiveData<>();
    private final MutableLiveData<CancelRideDialogVM> cancelDialog = new MutableLiveData<>();
    //private final MutableLiveData<Date> orderTime = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isMapCanScroll;
    private final MutableLiveData<Boolean> isNeedAssistante;
    private final MutableLiveData<Boolean> isShowingDatePicker;
    private final MutableLiveData<Boolean> isCancelledDateDialog;
    private final MutableLiveData<HamburgerState> menuIconState;
    private final PreCalculateCostManager calculateCostManager;
    private final PromoCodeManager promoCodeManager;
    private HamburgerState prevMenuIconState;
    private MutableLiveData<List<String>> costOfRide = new MutableLiveData<>();
    private boolean isPermissionSkipped = false;

    public OrderViewModel(ManfredOrderManager orderManager,
                          RegionManager regionManager,
                          ManfredPreOrderManager manfredPreOrderManager,
                          AppStatusManager appStatusManager,
                          TariffManager tariffManager,
                          TimeToDriverManager timeToDriverManager,
                          PreCalculateCostManager calculateCostManager,
                          PromoCodeManager promoCodeManager, AnalyticsManager analyticsManager) {
        this.regionManager = regionManager;
        this.appStatusManager = appStatusManager;
        this.timeToDriverManager = timeToDriverManager;
        this.analyticsManager = analyticsManager;
        Log.d(TAG, "creating OrderViewModel: ");
        this.orderManager = orderManager;
        this.preOrderManager = manfredPreOrderManager;
        this.manfredDirectionsManager = new ManfredDirectionsManager("AIzaSyDJA0dTKUvPPKpoGTINQXEc_C5M3GHJx6Q");
        this.tariffManager = tariffManager;
        this.calculateCostManager = calculateCostManager;
        this.promoCodeManager = promoCodeManager;
        this.isMapCanScroll = new MutableLiveData<>();
        this.isNeedAssistante = new MutableLiveData<>();
        this.isNeedAssistante.setValue(false);
        this.isShowingDatePicker = new MutableLiveData<>();
        this.isShowingDatePicker.setValue(false);
        this.isCancelledDateDialog = new MutableLiveData<>();
        this.isCancelledDateDialog.setValue(false);
        this.menuIconState = new MutableLiveData<>();
        //initObservers();
        this.menuIconState.setValue(getMenuState());
        this.prevMenuIconState = getMenuState();
        initRideStatusObserver(appStatusManager.getCurrentStatus());
        initFinishLocation(getFinishLocation());
        initPromoCodeObserver();
    }

    public ManfredDirectionsManager getManfredDirectionsManager() {
        return manfredDirectionsManager;
    }

    private void initRideStatusObserver(LiveData<AppStatus> status) {
        status.observeForever(new Observer<AppStatus>() {
            @Override
            public void onChanged(@Nullable AppStatus appStatus) {
                syncScrollState(appStatus);
            }
        });
    }

    private void initPromoCodeObserver() {
        promoCodeManager.getSelectedPromoCode().observeForever(new Observer<PromoCodeVM>() {
            @Override
            public void onChanged(@Nullable PromoCodeVM promoCodeVM) {
                recalculateCost();
            }
        });
    }

    private void initFinishLocation(LiveData<ManfredLocation> finishLocation) {
        finishLocation.observeForever(new Observer<ManfredLocation>() {
            @Override
            public void onChanged(@Nullable ManfredLocation manfredLocation) {
                Log.d(TAG, "initFinishLocation onChanged: ");
                if (manfredLocation != null) {
                    recalculateCost();
                }
            }
        });
    }

    public boolean isReadyForOrderState() {
        return appStatusManager.isReadyToOrder();
    }

    public boolean isOrdering() {
        return appStatusManager.isOrderingNow();
    }

    public boolean isNeedRefreshMarker() {
        return appStatusManager.isNeedRefreshMarkerState();
    }

    /* when we do this, we take error after die services
    private void initObservers() {
        Log.d(TAG, "initObservers: ");
        manfredNetworkError.addSource(orderManager.getErrorStatus(), manfredNetworkError::setValue);
    }*/

    public boolean isOutOfService() {
        return appStatusManager.isOutOfRegion();
    }

    public void tryFindRegion(ManfredLocation location) {
        Log.d(TAG, "tryFindRegion: ");
        regionManager.setRegion(location);
    }

    public LiveData<AverageTimeToPassenger> getMarkerState() {
        return timeToDriverManager.getAverageTimeToPassenger();
    }

    public LiveData<List<Car>> getCarsOnMap() {
        return carsOnMap;
    }

    public LiveData<OrderManager.OrderError> getOrderError() {
        Log.d(TAG, "getOrderError: ");
        return orderManager.getErrorStatus();
    }

    public LiveData<List<TariffViewModel>> getTariffs() {
        //Log.d(TAG, "getTariffs: ");
        return tariffManager.getTariffs();
    }

    public List<TariffDetailVM> getDetailedTariff() {
        return tariffManager.getDetailedTariffs();
    }

    public LiveData<ManfredLocation> getStartLocation() {
        return orderManager.getStartLocation();
    }

    public LiveData<ManfredLocation> getFinishLocation() {
        return orderManager.getFinishLocation();
    }

    public void moveMyMarker(Location location) {
        orderManager.changeMyLocation(location);
    }

    public void moveDestinationMarker(Location location) {
        orderManager.changeDestinationLocation(location);
    }

    /**
     * selecting tariff, if this tariff was select before - show details
     *
     * @param id tariff id
     */
    public void selectTariffAndShowMore(int id) {
        tariffManager.selectTariff(id);
    }

    /**
     * only select tariff
     *
     * @param id tariff id
     */
    public void selectTariff(int id) {
        resetCost();
        final TariffDetailVM currSelectedTariff = tariffManager.getSelectedTariffDetails().getValue();
        //prevent to select two times(if selected two times - show details)
        if (currSelectedTariff != null) {
            if (currSelectedTariff.getId() != id) {
                tariffManager.selectTariff(id);
            }
        }
    }

    public void showMakingOrder() {
        Log.d(TAG, "showMakingOrder: ");
        Boolean needAssistante = getIsNeedAssistante().getValue();
        //it is preventing for show order when we need preOrder
        if (needAssistante != null) {
            if (needAssistante) {
                Log.d(TAG, "showMakingOrder: needAssistante");
                appStatusManager.makingPreOrder();
            } else {
                Log.d(TAG, "showMakingOrder: not need assistante");
                AverageTimeToPassenger averageTimeToPassenger = timeToDriverManager.getAverageTimeToPassenger().getValue();
                if (averageTimeToPassenger == null) {
                    appStatusManager.makingOrder();
                } else {
                    if (averageTimeToPassenger.getMarkerState() == AverageTimeToPassenger.MarkerState.NO_FREE_DRIVERS) {
                        appStatusManager.makingPreOrder();
                    } else {
                        appStatusManager.makingOrder();
                    }
                }
            }
        }

    }

    public void rePayCurrentOrder() {
        appStatusManager.selectCardForRePayCurrentOrder();
    }


    public void clearDate(){
        orderManager.clearDate();
    }


    public void makeOrder() {
        Date orderTime = getOrderTime().getValue();
        if (orderTime != null) {
            orderManager.makePreOrder(orderTime);
        } else {
            orderManager.makeOrder();
        }
    }

    public void tariffSelectedFromDetails(int id){
        tariffManager.selectTariff(id);
        cancel();
        recalculateCost();
    }

    public void cancel() {
        AppStatus appStatus = getStatus().getValue();
        Log.d(TAG, "cancel: " + appStatus);
        resetCost();
        CancelRideDialogVM cancelRideDialogVM = calculateCancel();
        if (cancelRideDialogVM == null) {
            if (appStatus != null) {
                if (appStatus == AppStatus.MAKING_PRE_ORDER) {
                    notNeedAssistant();
                }
            }
            orderManager.cancelCurrentState();
            cancelDialog.postValue(null);
            return;
        }
        if (appStatus != null) {
            switch (appStatus) {
                case SEARCH_CAR:
                    showCancelDialog(cancelRideDialogVM);
                    break;
                case CAR_RIDE_TO_PASSENGER:
                    showCancelDialog(cancelRideDialogVM);
                    break;
                case CAR_ON_PASSENGER:
                    showCancelDialog(cancelRideDialogVM);
                    break;
                case SEARCH_CAR_TIME_OUT:
                    orderManager.cancelCurrentState();
                    cancelDialog.postValue(null);
                    break;
                default:
                    orderManager.cancelCurrentState();
                    break;
            }
        }
    }

    private void showCancelDialog(CancelRideDialogVM cancelRideDialogVM) {
        Log.d(TAG, "showCancelDialog: ");
        cancelDialog.postValue(cancelRideDialogVM);
    }

    public void cancelRideConfirm() {
        orderManager.cancelCurrentState();
        cancelDialog.postValue(null);
    }

    public void cancelRideDismiss() {
        cancelDialog.postValue(null);
    }

    /**
     * @return model of cancel dialog
     */
    private CancelRideDialogVM calculateCancel() {
        if (getCurrentRide().getValue() != null) {
            if (getCurrentRide().getValue().isCancellChargeable()) {
                Log.d(TAG, "calculateCancel: creating dialog");
                return new CancelRideDialogVM("Отменить поездку?",
                        "Стоимость отмены поездки " + CostUtils.removeAfterDotsIfNeed(String.valueOf(getCurrentRide().getValue().getCancelPrice())) + " рублей.",
                        "Назад", "Отменить поездку");
            }
        }

        if (getStatus().getValue() == AppStatus.CAR_RIDE_TO_PASSENGER
                || getStatus().getValue() == AppStatus.SEARCH_CAR
                || getStatus().getValue() == AppStatus.CAR_ON_PASSENGER) {
            return new CancelRideDialogVM("Отменить поездку",
                    "Вы действительно хотите отменить поездку?", "Нет", "Отменить");
        }
        return null;
    }

    private void syncScrollState(AppStatus appStatus) {
        if (appStatus == null) return;
        Log.d(TAG, "syncScrollState: prevMenuIconState " + prevMenuIconState + " getMenuState " + getMenuState());
        if (prevMenuIconState.getState() != getMenuState().getState()) {
            //Log.d(TAG, "syncScrollState: need change state");
            menuIconState.postValue(getMenuState());
            prevMenuIconState = getMenuState();
        }
        switch (appStatus) {
            case NEED_CARD:
                isMapCanScroll.postValue(false);
                break;
            case OUT_OF_SERVICE:
                isMapCanScroll.postValue(true);
                break;
            case SHOWING_TARIFF_DETAILS:
                isMapCanScroll.postValue(false);
                break;
            case READY_TO_ORDER:
                isMapCanScroll.postValue(true);
                break;
            case MAKING_ORDER:
                isMapCanScroll.postValue(false);
                break;
            case ADDING_COMMENT:
                isMapCanScroll.postValue(false);
                break;
            case MAKING_PRE_ORDER:
                isMapCanScroll.postValue(false);
                break;
            case SELECTING_TIME_FOR_PREORDER:
                isMapCanScroll.postValue(false);
                break;
            case SELECTING_CARD_FOR_REPAY_LAST_DEBT:
                break;
            case PAYMENT_ERROR:
                isMapCanScroll.postValue(false);
                break;
            case HAVE_DEBT:
                isMapCanScroll.postValue(false);
                break;
            case SELECT_CARDS:
                isMapCanScroll.postValue(false);
                break;
            case PICKING_CARDS:
                isMapCanScroll.postValue(false);
                break;
            case SEARCH_CAR:
                isMapCanScroll.postValue(false);
                break;
            case SEARCH_CAR_TIME_OUT:
                isMapCanScroll.postValue(false);
                break;
            case CAR_ON_PASSENGER:
                isMapCanScroll.postValue(true);
                break;
            case CAR_RIDE_TO_PASSENGER:
                isMapCanScroll.postValue(true);
                break;
            case RIDING:
                isMapCanScroll.postValue(true);
                break;
            case SHOWING_PREORDER_DETAILS:
                isMapCanScroll.postValue(false);
                break;
            case COMPLETED:
                isMapCanScroll.postValue(false);
                break;
        }
    }

    private HamburgerState getMenuState() {
        AppStatus appStatus = appStatusManager.getCurrentStatus().getValue();
        HamburgerState hamburgerState = new HamburgerState(HamburgerState.HambState.HAMBURGER);
        if (appStatus != null) {
            switch (appStatus) {
                case NEED_CARD:
                    break;
                case OUT_OF_SERVICE:
                    hamburgerState = new HamburgerState(HamburgerState.HambState.HAMBURGER);
                    break;
                case SHOWING_TARIFF_DETAILS:
                    hamburgerState = new HamburgerState(HamburgerState.HambState.BACK);
                    break;
                case READY_TO_ORDER:
                    hamburgerState = new HamburgerState(HamburgerState.HambState.HAMBURGER);
                    break;
                case MAKING_ORDER:
                    hamburgerState = new HamburgerState(HamburgerState.HambState.BACK);
                    break;
                case ADDING_COMMENT:
                    break;
                case MAKING_PRE_ORDER:
                    hamburgerState = new HamburgerState(HamburgerState.HambState.BACK);
                    break;
                case SELECTING_TIME_FOR_PREORDER:
                    hamburgerState = new HamburgerState(HamburgerState.HambState.BACK);
                    break;
                case SELECTING_CARD_FOR_REPAY_LAST_DEBT:
                    break;
                case PAYMENT_ERROR:
                    break;
                case HAVE_DEBT:
                    break;
                case SELECT_CARDS:
                    break;
                case PICKING_CARDS:
                    break;
                case SEARCH_CAR:
                    break;
                case SEARCH_CAR_TIME_OUT:
                    break;
                case CAR_ON_PASSENGER:
                    break;
                case CAR_RIDE_TO_PASSENGER:
                    break;
                case RIDING:
                    break;
                case SHOWING_PREORDER_DETAILS:
                    hamburgerState = new HamburgerState(HamburgerState.HambState.CLOSE);
                    break;
                case COMPLETED:
                    break;
            }
        }
        return hamburgerState;
    }

    public void showCardsList() {
        appStatusManager.showCardList();
    }

    public void selectActiveCard() {
        appStatusManager.selectActiveCard();
    }

    public void successfullyPaid() {
        appStatusManager.currOrderSuccessfullyPaid();
    }

    public void selectTimeForPreOrder() {
        orderManager.selectTime();
    }

    public void hideSelectingTimeForPreOrder() {
        Log.d(TAG, "hideSelectingTimeForPreOrder: ");
        orderManager.cancelCurrentState();
        isCancelledDateDialog.setValue(true);
    }

    public void showPreOrderDetail(OrderVM orderVM) {
        preOrderManager.selectPreOrderForDetails(orderVM.getId());
        appStatusManager.showPreOrderDetails();
    }

    public void cancelPreOrder(int id) {
        orderManager.cancelOrder(id);
    }

    public LiveData<AppStatus> getStatus() {
        //Log.d(TAG, "getStatus: ");
        return appStatusManager.getCurrentStatus();
    }

    public LiveData<TariffViewModel> getSelectedTariff() {
        return tariffManager.getSelectedTariff();
    }

    public LiveData<List<String>> getPreCalculatedCost() {
        return costOfRide;
    }



    private void resetPreCalculatedCost() {
        costOfRide.postValue(null);
    }

    private void recalculateCost() {
      //  TariffViewModel tariff = getSelectedTariff().getValue();
        Boolean isNeedAssistant = getIsNeedAssistante().getValue();
        ManfredLocation startLocation = getStartLocation().getValue();
        ManfredLocation finishLocation = getFinishLocation().getValue();
        PromoCodeVM promoCodeVM = promoCodeManager.getSelectedPromoCode().getValue();
        Integer promoCodeId = null;
        if (promoCodeVM != null) {
            promoCodeId = promoCodeVM.getId();
        }

        ArrayList <String> listOfCots = new ArrayList<>();
        for (int i = 0; i < getTariffs().getValue().size(); i++){
            if (getTariffs().getValue().get(i) != null && isNeedAssistant != null && startLocation != null && finishLocation != null) {
                int finalI = i;
                calculateCostManager.calculateCost(getTariffs().getValue().get(i).getTariff(), startLocation, finishLocation, isNeedAssistant,
                        promoCodeId, cost -> {
                            if (cost == null) {
                                 listOfCots.add(finalI," ");
                            } else {
                                listOfCots.add(finalI,"     ~"  + cost + " " + getTariffs().getValue().get(finalI).getTariff().getCurrencySymbol());
                            }
                        });
            }
        }

        Handler handler = new Handler();
        handler.postDelayed(() -> costOfRide.postValue(listOfCots), 3000);
    }

    private void resetCost() {
        costOfRide.postValue(null);
    }

    public LiveData<OrderVM> getCurrentRide() {
        return orderManager.getCurrOrder();
    }

    public MutableLiveData<CancelRideDialogVM> getCancelDialog() {
        return cancelDialog;
    }

    public void reOrder() {
        orderManager.reOrder();
    }

    public LiveData<TariffDetailVM> getSelectedTariffDetails() {
        return tariffManager.getSelectedTariffDetails();
    }

    public void showTariffsDetails() {
        orderManager.showTariffDetails();
    }

    public LiveData<ManfredLocation> getCurrentDriverLocation() {
        return orderManager.getDriverLocation();
    }

    public LiveData<Date> getOrderTime() {
        return orderManager.getSelectedDate();
    }

    public void setOrderTime(Date date) {
        orderManager.timeSelected(date);
    }

    public LiveData<OrderVM> getLastPreOrder() {
        return preOrderManager.getLastPreOrder();
    }

    public LiveData<String> getCurrentComment() {
        return orderManager.getComment();
    }

    public void showAddComment() {
        orderManager.showCommentAdding();
    }

    public void addComment(String string) {
        orderManager.addComment(string);
    }

    public void needAssistant() {
        isNeedAssistante.setValue(true);
        tariffManager.needAssistant();
        appStatusManager.makingPreOrder();
        timeToDriverManager.cancelCalculatingState();
        recalculateCost();
    }

    public void notNeedAssistant() {
        isNeedAssistante.setValue(false);
        tariffManager.notNeedAssistant();
        appStatusManager.makingPreOrder();
        timeToDriverManager.reCalculateLastState();
        recalculateCost();
    }

    public SingleLiveEvent<ManfredLocation> needCenterToCoords() {
        return regionManager.getCenterOfCurrentRegion();
    }

    public void tryRePay() {
        orderManager.tryRePayCurrentOrder();
    }

    public SingleLiveEvent<DriverDelay> getDriverDelay() {
        return orderManager.getDriverDelay();
    }

    public PayErrorWM getError() {
        return orderManager.getPayErrorWM();
    }

    public LiveData<Boolean> getIsNeedAssistante() {
        return isNeedAssistante;
    }

    public LiveData<Boolean> getIsCancelledDateDialog() {
        return isCancelledDateDialog;
    }

    public LiveData<Boolean> getIsMapCanScroll() {
        return isMapCanScroll;
    }

    public LiveData<String> getPreOrderError() {
        return preOrderManager.getError();
    }

    @Deprecated
    public MutableLiveData<Boolean> getIsShowingDatePicker() {
        return isShowingDatePicker;
    }

    public void removeComment() {
        orderManager.removeComment();
    }

    public MutableLiveData<HamburgerState> getMenuIconState() {
        return menuIconState;
    }

    public boolean isRiding() {
        return appStatusManager.isRidingNow();
    }

    public boolean isFinishing() {
        return appStatusManager.isFinishing();
    }

    public boolean isShowingPreOrderDetails() {
        return appStatusManager.isShowingPreOrderDetails();
    }

    public boolean isSearchingCar() {
        return appStatusManager.isSearchingCar();
    }

    public void newUserNeedCard() {
        Log.d(TAG, "newUserNeedCard: ");
        appStatusManager.needCardNewUser();
    }

    public void locationPermissionDenied() {
        if (!isPermissionSkipped) {
            appStatusManager.locationPermissionDenied();
        }
    }

    public void permissionGranted() {
        isPermissionSkipped = true;
        if (appStatusManager.isLocationPermissionDenied()) {
            appStatusManager.readyToOrder();
        }
        analyticsManager.geolocationEnabled(true);
    }

    public void skipPermission() {
        isPermissionSkipped = true;
        appStatusManager.cancelCurrentState();
        analyticsManager.geolocationEnabled(false);
    }

    public void skipNeedCard() {
        appStatusManager.skipNeedingCard();
    }

    public LiveData<OrderVM> getSelectedForDetailsPreOrder() {
        return preOrderManager.getDetailedPreOrder();
    }

    public boolean isNeedCard() {
        return appStatusManager.isNeedCard();
    }

    public boolean isShowingTariffDetails() {
        return appStatusManager.isShowingTariffDetails();
    }
}
