package manfred.ru.manfredpassenger.ride.managers.mapOverlay;

import android.support.annotation.IdRes;

import manfred.ru.manfredpassenger.ride.models.ManfredLocation;

public class PinOnMap {
    @IdRes
    private final int pinIcon;
    private final ManfredLocation pinLocation;

    public PinOnMap(@IdRes int pinIcon, ManfredLocation pinLocation) {
        this.pinIcon = pinIcon;
        this.pinLocation = pinLocation;
    }

    @IdRes public int getPinIcon() {
        return pinIcon;
    }

    public ManfredLocation getPinLocation() {
        return pinLocation;
    }
}
