package manfred.ru.manfredpassenger.ride.viewmodels;

import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.models.CreditCardVM;
import manfred.ru.manfredpassenger.common.models.PaymentSystem;

public class CreditCardItem implements CardType {
    @Override
    public ItemType getType() {
        return ItemType.CREDIT_CARD;
    }

    private final CreditCardVM creditCard;

    public CreditCardItem(CreditCard creditCard, PaymentSystem currentPaymentSystem) {
        this.creditCard = new CreditCardVM(creditCard, currentPaymentSystem);
    }

    public CreditCardVM getCreditCard() {
        return creditCard;
    }
}
