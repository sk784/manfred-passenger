package manfred.ru.manfredpassenger.ride.managers.mapOverlay;

import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import manfred.ru.manfredpassenger.ride.managers.mapOverlay.models.MapObject;
import manfred.ru.manfredpassenger.ride.viewmodels.MarkerViewModel;

public class FullOverlay {
    private final List<MapObject>objects;
    @Nullable private final MarkerViewModel markerWithTime;

    public FullOverlay(List<MapObject> objects, @Nullable MarkerViewModel markerWithTime) {
        this.objects = objects;
        this.markerWithTime = markerWithTime;
    }

    public List<MapObject> getPins() {
        return objects;
    }

    @Nullable
    public MarkerViewModel getMarkerWithTime() {
        return markerWithTime;
    }
}
