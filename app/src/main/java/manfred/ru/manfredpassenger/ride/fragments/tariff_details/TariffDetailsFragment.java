package manfred.ru.manfredpassenger.ride.fragments.tariff_details;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.BR;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.databinding.FragmentTariffDetailsBinding;
import manfred.ru.manfredpassenger.databinding.TariffItemBinding;
import manfred.ru.manfredpassenger.databinding.TariffItemForDetailsBinding;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffDetailVM;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;
import manfred.ru.manfredpassenger.utils.TariffDiffUtilCallback;


/**
 * A simple {@link Fragment} subclass.
 */
public class TariffDetailsFragment extends Fragment {
    private static final String TAG = "TariffDetailsFragment";
    private ViewGroup.LayoutParams prevParentParams;
    int selectedInSessionTariff;
    MutableLiveData<List<TariffViewModel>>tariffList = new MutableLiveData<>();

    public TariffDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        FragmentTariffDetailsBinding binding = FragmentTariffDetailsBinding.inflate(inflater, container, false);
        //we do this because container not matching parent :(
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        init(binding);

        return binding.getRoot();
    }

    private void init(FragmentTariffDetailsBinding binding) {
        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        initRecycler(binding, model);

        binding.getRoot().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //for doesnt scroll map
                return true;
            }
        });

        PagerAdapter pagerAdapter = new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return TariffContentDetailFragment.newInstance(positionToTariffId(position,model));
            }

            @Override
            public int getCount() {
                return model.getDetailedTariff().size();
            }
        };

        binding.tariffDetailPager.setAdapter(pagerAdapter);
        binding.tariffDetailPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //Log.d(TAG, "onPageScrolled: "+position);
            }

            @Override
            public void onPageSelected(int position) {
                //Log.d(TAG, "onPageSelected: "+position);
                //selectedInSessionTariff=positionToTariffId(position,model);
                selectInRecycler(positionToTariffId(position,model), model, binding);

                //model.selectTariff(positionToTariffId(position,model));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        model.getSelectedTariffDetails().observe(this, new Observer<TariffDetailVM>() {
            @Override
            public void onChanged(@Nullable TariffDetailVM tariffDetailVM) {
                if(tariffDetailVM!=null) {
                    binding.tariffRecycler.scrollToPosition(tariffIdToViewPagerPosition(tariffDetailVM.getId(), model));
                    binding.tariffDetailPager.setCurrentItem(tariffIdToViewPagerPosition(tariffDetailVM.getId(), model));
                    //init
                    selectedInSessionTariff=tariffDetailVM.getId();
                }
            }
        });

        binding.selectTariff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //model.selectTariff();
                Log.d(TAG, "selectTariff onClick: "+selectedInSessionTariff);
                model.tariffSelectedFromDetails(selectedInSessionTariff);
                //model.cancel();
                //getFragmentManager().popBackStack();
            }
        });

        binding.closeTariffDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "closeTariffDetails onClick: ");
                model.cancel();
            }
        });

    }

    private void selectInRecycler(int selectedInSessionTariff, OrderViewModel model, FragmentTariffDetailsBinding binding) {
        binding.tariffRecycler.scrollToPosition(tariffIdToViewPagerPosition(selectedInSessionTariff, model));
        binding.tariffDetailPager.setCurrentItem(tariffIdToViewPagerPosition(selectedInSessionTariff, model));
        final List<TariffViewModel> tariffs = model.getTariffs().getValue();
        this.selectedInSessionTariff=selectedInSessionTariff;
        if(tariffs!=null){
            for (TariffViewModel tariffViewModel : tariffs){
                if(selectedInSessionTariff==tariffViewModel.getId()){
                    tariffViewModel.setSelected(true);
                }else {
                    tariffViewModel.setSelected(false);
                }
            }
            tariffList.postValue(tariffs);
        }
    }

    private int positionToTariffId(int position, OrderViewModel model) {
        return model.getDetailedTariff().get(position).getId();
    }

    private int tariffIdToViewPagerPosition(int tariffId, OrderViewModel model){
        for (TariffDetailVM checkingTariffDetailVM : model.getDetailedTariff()){
            if (checkingTariffDetailVM.getId()==tariffId) return model.getDetailedTariff().indexOf(checkingTariffDetailVM);
        }
        return -1;
    }

    private void initRecycler(FragmentTariffDetailsBinding binding, OrderViewModel model) {
        //Log.d(TAG, "initRecycler: ");
        List<TariffViewModel> tariffViewModels = new ArrayList<>();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager.onSaveInstanceState();
        binding.tariffRecycler.setLayoutManager(mLayoutManager);

        ItemType<TariffItemForDetailsBinding> tariffItemType = new ItemType<TariffItemForDetailsBinding>(R.layout.tariff_item_for_details) {
            @Override
            public void onCreate(Holder<TariffItemForDetailsBinding> holder) {
                super.onCreate(holder);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "onClick: tariffId "+holder.getBinding().getTariff().getId());
                        mLayoutManager.scrollToPosition(holder.getLayoutPosition());
                        selectInRecycler(holder.getBinding().getTariff().getId(), model, binding);
                    }
                });
            }
        };

        LastAdapter adapter = new LastAdapter(tariffViewModels, BR.tariff)
                .map(TariffViewModel.class, tariffItemType)
                .into(binding.tariffRecycler);

        tariffList.observe(this, new Observer<List<TariffViewModel>>() {
            @Override
            public void onChanged(@Nullable List<TariffViewModel> tariffs) {
                if (tariffs != null) {
                    Log.d(TAG, "tariffList onChanged: ");
                    TariffDiffUtilCallback tariffDiffUtilCallback = new TariffDiffUtilCallback(tariffViewModels, tariffs);
                    DiffUtil.DiffResult tariffResult = DiffUtil.calculateDiff(tariffDiffUtilCallback);
                    tariffResult.dispatchUpdatesTo(adapter);
                    adapter.notifyDataSetChanged();
                    tariffViewModels.clear();
                    tariffViewModels.addAll(tariffs);
                }else {
                    Log.d(TAG, "initRecycler: tariffs is null");
                }
            }
        });

        model.getTariffs().observe(this, tariffs -> {
            tariffList.postValue(tariffs);
        });
    }

    public class FadePageTransformer implements ViewPager.PageTransformer {
        public void transformPage(@NotNull View view, float position) {
            if (position <= -1.0F || position >= 1.0F) {
                view.setTranslationX(view.getWidth() * position);
                view.setAlpha(0.0F);
            } else if (position == 0.0F) {
                view.setTranslationX(view.getWidth() * position);
                view.setAlpha(1.0F);
            } else {
                // position is between -1.0F & 0.0F OR 0.0F & 1.0F
                view.setTranslationX(view.getWidth() * -position);
                view.setAlpha(1.0F - Math.abs(position));
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getView()!=null) {
            ViewGroup container = ((ViewGroup) getView().getParent());
            container.setLayoutParams(prevParentParams);
        }
    }
}
