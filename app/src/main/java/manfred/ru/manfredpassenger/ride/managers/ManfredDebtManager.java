package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.Nullable;
import android.util.Log;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.repositories.SocketOrderRepository;
import manfred.ru.manfredpassenger.ride.repositories.OrderRepository;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public class ManfredDebtManager implements DebtManager {
    private final MutableLiveData<OrderVM>lastOrderWithDebt;
    private final OrderRepository orderRepository;
    private final AppStatusManager appStatusManager;
    private static final String TAG = "ManfredDebtManager";

    public ManfredDebtManager(TokenManager tokenManager, AppStatusManager appStatusManager, NetworkManagerProvider networkManager) {
        Log.d(TAG, "ManfredDebtManager: creating");
        this.appStatusManager = appStatusManager;
        this.lastOrderWithDebt = new MutableLiveData<>();
        this.orderRepository = new SocketOrderRepository(tokenManager, networkManager);
        refreshDebts();
    }

    @Override
    public void refreshDebts() {
        orderRepository.getLastOrderWithDebt(new NetworkResponseCallback<OrderVM>() {
            @Override
            public void allOk(@Nullable OrderVM response) {
                lastOrderWithDebt.postValue(response);
                if (response != null) {
                    Log.d(TAG, "refreshDebts: order with debt "+response.getId());
                    appStatusManager.haveDebt();
                }
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: "+error.toString());
            }
        });
    }

    @Override
    public void refreshDebtsAndGetReadyIfNoDebts() {
        orderRepository.getLastOrderWithDebt(new NetworkResponseCallback<OrderVM>() {
            @Override
            public void allOk(@Nullable OrderVM response) {
                lastOrderWithDebt.postValue(response);
                if (response != null) {
                    Log.d(TAG, "refreshDebts: order with debt "+response.getId());
                    appStatusManager.haveDebt();
                }else {
                    appStatusManager.readyToOrder();
                }
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: "+error.toString());
            }
        });
    }

    @Override
    public LiveData<OrderVM> getLastDebtOrder() {
        return lastOrderWithDebt;
    }
}
