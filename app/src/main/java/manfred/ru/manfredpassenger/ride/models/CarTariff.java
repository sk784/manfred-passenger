package manfred.ru.manfredpassenger.ride.models;

import com.google.gson.annotations.SerializedName;

public class CarTariff {
    @SerializedName("id")
    private final int tariff;
    @SerializedName("title")
    private final String title;

    public CarTariff(int tariff, String title) {
        this.tariff = tariff;
        this.title = title;
    }

    public int getTariff() {
        return tariff;
    }

    public String getTitle() {
        return title;
    }
}
