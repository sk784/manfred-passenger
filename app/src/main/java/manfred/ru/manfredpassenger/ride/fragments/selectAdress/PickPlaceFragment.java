package manfred.ru.manfredpassenger.ride.fragments.selectAdress;


import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.Objects;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.repositories.GeocodingRepository;
import manfred.ru.manfredpassenger.api.services.models.CoordForAverageTime;
import manfred.ru.manfredpassenger.api.services.models.LatLngLocation;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.databinding.FragmentPickPlaceBinding;
import manfred.ru.manfredpassenger.ride.managers.TariffManager;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.models.AverageTimeToPassenger;
import manfred.ru.manfredpassenger.ride.models.PickedPlaceItem;
import manfred.ru.manfredpassenger.ride.repositories.DriverRepository;
import manfred.ru.manfredpassenger.ride.repositories.ManfredDriverRepository;
import manfred.ru.manfredpassenger.ride.viewmodels.AutocompleteViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.MarkerViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.SelectAdressViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class PickPlaceFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "PickPlaceFragment";
    private OrderViewModel orderViewModel;
    SelectAdressViewModel selectAdressViewModel;
    Location pickedLocation;
    FragmentPickPlaceBinding binding;


    public PickPlaceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentPickPlaceBinding.inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        init();
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    private void init() {
        SupportMapFragment mapFragment = (SupportMapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        orderViewModel = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        selectAdressViewModel = ViewModelProviders.of(getActivity()).get(SelectAdressViewModel.class);

        if (selectAdressViewModel.isFinish()) {
            binding.selectAdressMarker.setVisibility(View.VISIBLE);
            binding.marker.getRoot().setVisibility(View.GONE);
        } else {
            binding.selectAdressMarker.setVisibility(View.GONE);
            binding.marker.getRoot().setVisibility(View.VISIBLE);
        }

        final TokenManager tokenManager = ((ManfredPassengerApplication)getActivity().getApplication()).getTokenManager();
        final NetworkManagerProvider networkManager = ((ManfredPassengerApplication) getActivity().getApplication()).getNetworkManager();

        AutocompleteViewModel autocompleteViewModel = ViewModelProviders.of(getActivity()).get(AutocompleteViewModel.class);
        binding.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.done.setEnabled(false);
                GeocodingRepository geocodingRepository = new GeocodingRepository(tokenManager, networkManager);
                geocodingRepository.getLocationNameByLatLng(pickedLocation, new NetworkResponseCallback<ManfredLocation>() {
                    @Override
                    public void allOk(ManfredLocation response) {
                        autocompleteViewModel.selectPlace(new PickedPlaceItem(response));
                    }

                    @Override
                    public void error(ManfredNetworkError error) {
                        autocompleteViewModel.selectPlace(new PickedPlaceItem(new ManfredLocation(pickedLocation)));
                    }
                });
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fragmentManager = getFragmentManager();
        if(fragmentManager!=null) {
            Fragment fragment = (getFragmentManager().findFragmentById(R.id.map));
            if(fragment!=null) {
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.remove(fragment);
                ft.commitAllowingStateLoss();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Rect rectf = new Rect();
        //For coordinates location relative to the parent
        binding.done.getLocalVisibleRect(rectf);
        //For coordinates location relative to the screen/display
        binding.done.getGlobalVisibleRect(rectf);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                googleMap.setPadding(0, getStatusBarHeight(), 0, (rectf.height()+72));
                Projection projection = googleMap.getProjection();
                showLocationMarker(projection.toScreenLocation(googleMap.getCameraPosition().target));
            }
        });
        initLocation(googleMap);
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //get latlng at the center by calling
                LatLng midLatLng = googleMap.getCameraPosition().target;
                //Log.d(TAG, "onCameraIdle: "+midLatLng.toString());
                pickedLocation = new Location("");
                pickedLocation.setLatitude(midLatLng.latitude);
                pickedLocation.setLongitude(midLatLng.longitude);
                Log.d(TAG, "onCameraIdle: VM is " + orderViewModel);
                if(!selectAdressViewModel.isFinish()){
                    ManfredPassengerApplication application = ((ManfredPassengerApplication)getActivity().getApplication());
                    ManfredDriverRepository manfredDriverRepository = new ManfredDriverRepository(application.getNetworkManager());
                    TokenManager tokenManager = (application.getTokenManager());
                    TariffManager tariffManager = (application.getTariffManager());
                    CoordForAverageTime coordForAverageTime =
                            new CoordForAverageTime(String.valueOf(tariffManager.getSelectedTariff().getValue().getId()), new LatLngLocation(pickedLocation));

                    manfredDriverRepository.getAverageTimeToDriver(tokenManager.getAuthToken(), coordForAverageTime,
                            new DriverRepository.AverageTimeCallback() {
                                @Override
                                public void timeToDriver(int averageWaitTime) {
                                    binding.setMarkerState(new MarkerViewModel(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.SHOW_TIME, averageWaitTime)));
                                }

                                @Override
                                public void error(ManfredNetworkError error) {
                                    Log.d(TAG, "error: "+error.toString());
                                    binding.setMarkerState(new MarkerViewModel(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.NO_FREE_DRIVERS, null)));
                                }

                                @Override
                                public void outOfService(@Nullable Integer regionId) {
                                    binding.setMarkerState(new MarkerViewModel(new AverageTimeToPassenger(AverageTimeToPassenger.MarkerState.NO_FREE_DRIVERS, null)));
                                }

                                @Override
                                public void tariffNotFound() {
                                }
                            });
                }

            }
        });
    }

    private int pxFromDp(final Context context, final float dp) {
        return (int)(dp * context.getResources().getDisplayMetrics().density);
    }


    private int getStatusBarHeight() {
        int result = 0;
        if(isAdded()) {
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = getResources().getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }

    private void initLocation(GoogleMap googleMap) {
        //Log.d(TAG, "initLocation: ");
        ManfredLocation location;
        if(selectAdressViewModel.isFinish()){
            //Log.d(TAG, "initLocation: ");
            location = orderViewModel.getFinishLocation().getValue();
            if (location==null){
                location = orderViewModel.getStartLocation().getValue();
            }
        }else {
            location = orderViewModel.getStartLocation().getValue();
        }
        if(location!=null){
            centerToLocation(location,googleMap);
        }else {
            centerToCurrentLocation(googleMap);
        }
    }

    /**
     * showing central marker
     */
    private void showLocationMarker(Point screenPositionOfCenterMap) {
        //Log.d(TAG, "showLocationMarker: "+screenPositionOfCenterMap);
        //do this because we have two markers after making order(delay in clearing map)
        //binding.marker.getRoot().setVisibility(View.VISIBLE);
        binding.marker.getRoot().post(new Runnable() {
            @Override
            public void run() {
                //Log.d(TAG, "showLocationMarker: animating");
                float newY=screenPositionOfCenterMap.y - (binding.marker.getRoot().getHeight()-35);
                binding.marker.getRoot().setY(newY);
            }
        });
        binding.selectAdressMarker.post(new Runnable() {
            @Override
            public void run() {
                float newY=screenPositionOfCenterMap.y - (binding.selectAdressMarker.getHeight()-35);
                binding.selectAdressMarker.setY(newY);
            }
        });
    }

    private void centerToLocation(ManfredLocation location, GoogleMap googleMap) {
        if(googleMap==null)return;
        LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,18));
    }

    private void centerToCurrentLocation(GoogleMap googleMap) {
        Activity activity = getActivity();
        if(activity!=null) {
            FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(location -> {
                if (location != null) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
                }
            });
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
            criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);

            // Now create a location manager
            final LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            // This is the Best And IMPORTANT part
            final Looper looper = null;

            final LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            if (locationManager != null) {
                locationManager.requestSingleUpdate(criteria, locationListener, looper);
            }
        }
    }
}
