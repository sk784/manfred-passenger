package manfred.ru.manfredpassenger.ride.fragments.ride;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.intercom.android.sdk.Intercom;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.databinding.FragmentHaveDebtBinding;
import manfred.ru.manfredpassenger.history.HistoryActivity;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;


/**
 * A simple {@link Fragment} subclass.
 */
public class HaveDebtFragment extends Fragment {
    private static final String TAG = "HaveDebtFragment";

    private ViewGroup.LayoutParams prevParentParams;

    public HaveDebtFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        FragmentHaveDebtBinding binding = FragmentHaveDebtBinding.inflate(getLayoutInflater());

        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentHaveDebtBinding binding) {
        ((ManfredPassengerApplication)getActivity().getApplication()).getDebtManager().getLastDebtOrder().observe(this, new Observer<OrderVM>() {
            @Override
            public void onChanged(@Nullable OrderVM orderVM) {
                if (orderVM != null) {
                    Log.d(TAG, "onChanged: debit "+orderVM.getId());
                }
                binding.setOrder(orderVM);
                binding.btnOrderDetails.setOnClickListener(v -> {
                    ((ManfredPassengerApplication)getActivity().getApplication()).getHistoryManager().showHistoryDetail(orderVM);
                    Intent intent = new Intent(getActivity(), HistoryActivity.class);
                    startActivity(intent);
                });
                binding.btnSelectCard.setOnClickListener(v -> ((ManfredPassengerApplication)getActivity().getApplication()).getAppStatusManager().selectCardForRePayLastDebt());
            }
        });
        binding.getRoot().setOnClickListener(null);
        binding.btnIntercom.setOnClickListener(v -> Intercom.client().displayMessenger());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup container = ((ViewGroup)getView().getParent());
        container.setLayoutParams(prevParentParams);
    }

}
