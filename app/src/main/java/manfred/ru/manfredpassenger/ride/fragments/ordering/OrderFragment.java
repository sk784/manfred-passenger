package manfred.ru.manfredpassenger.ride.fragments.ordering;


import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import manfred.ru.manfredpassenger.BR;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.databinding.FragmentOrderBinding;
import manfred.ru.manfredpassenger.databinding.TariffItemBinding;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;
import manfred.ru.manfredpassenger.utils.TariffDiffUtilCallback;


/**
 * A simple {@link Fragment} subclass.
 */
public class OrderFragment extends Fragment implements LifecycleObserver {
    private static final String TAG = "OrderFragment";
    LinearLayoutManager mLayoutManager;
    OrderViewModel model;

    public OrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentOrderBinding binding = FragmentOrderBinding.inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        init(binding);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        return binding.getRoot();
    }

    void init(FragmentOrderBinding binding) {
        model = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(OrderViewModel.class);

        model.getStartLocation().observe(this, location -> {
            if (location != null) {
                binding.myLocationNameTv.setText(location.getAddress());
            }
        });

        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager.onSaveInstanceState();
        binding.tariffRecycle.setLayoutManager(mLayoutManager);

        List<TariffViewModel> tariffViewModels = new ArrayList<>();
        ItemType<TariffItemBinding> tariffItemType = new ItemType<TariffItemBinding>(R.layout.tariff_item) {
            @Override
            public void onCreate(@NonNull Holder<TariffItemBinding> holder) {
                super.onCreate(holder);
                holder.itemView.setOnClickListener(v -> model.selectTariffAndShowMore(holder.getBinding().getTariff().getId()));
            }
        };

        LastAdapter adapter = new LastAdapter(tariffViewModels, BR.tariff)
                .map(TariffViewModel.class, tariffItemType)
                .into(binding.tariffRecycle);

        model.getTariffs().observe(this, tariffs -> {
            if (tariffs != null) {
                Log.d(TAG, "Tariffs changed: new size is "+tariffs.size());
                for (TariffViewModel tariffViewModel:tariffs){
                    Log.d(TAG, tariffViewModel.toString());
                }

                /*
                TariffDiffUtilCallback tariffDiffUtilCallback = new TariffDiffUtilCallback(tariffViewModels, tariffs);
                DiffUtil.DiffResult tariffResult = DiffUtil.calculateDiff(tariffDiffUtilCallback);
                tariffResult.dispatchUpdatesTo(adapter);*/

                tariffViewModels.clear();
                tariffViewModels.addAll(tariffs);
                adapter.notifyDataSetChanged();
            }
        });

        model.getSelectedTariff().observe(this, tariffViewModel -> {
            Log.d(TAG, "getSelectedTariff onChanged: ");
            int selectedPosition = tariffViewModels.indexOf(tariffViewModel);
            if(selectedPosition>-1) {
                //Log.d(TAG, "getSelectedTariff onChanged: "+selectedPosition);
                mLayoutManager.scrollToPosition(selectedPosition);
            }
        });

        binding.whereHolder.setOnClickListener(v -> model.showMakingOrder());

        binding.forwardButton.setOnClickListener(v -> model.showMakingOrder());


    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    void resume() {
        List<TariffViewModel> allTariffs = model.getTariffs().getValue();
        TariffViewModel currTariff = model.getSelectedTariff().getValue();
        if(allTariffs!=null) {
            int selectedPosition = allTariffs.indexOf(currTariff);
            if (selectedPosition > -1) {
                mLayoutManager.scrollToPosition(selectedPosition);
            }
        }

    }

}
