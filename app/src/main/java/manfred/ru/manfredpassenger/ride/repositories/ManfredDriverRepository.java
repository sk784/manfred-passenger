package manfred.ru.manfredpassenger.ride.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.internal.LinkedTreeMap;

import java.net.SocketTimeoutException;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.services.models.AverageWaitTime;
import manfred.ru.manfredpassenger.api.services.models.CoordForAverageTime;
import manfred.ru.manfredpassenger.api.services.models.DriverLocationResponse;
import manfred.ru.manfredpassenger.api.services.models.RegionAnswer;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.models.AverageTime;
import manfred.ru.manfredpassenger.ride.models.CarsAnswer;
import manfred.ru.manfredpassenger.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManfredDriverRepository implements DriverRepository {
    private static final String TAG = "ManfredDriverRepository";
    private NetworkManagerProvider networkManager;

    public ManfredDriverRepository(NetworkManagerProvider networkManager) {
        this.networkManager = networkManager;
    }

    @Override
    public void getAverageTimeToDriver(String token, CoordForAverageTime coordForAverageTime,
                                       final AverageTimeCallback averageTimeCallback) {
        Log.d(TAG, "getAverageTimeToDriver: "+coordForAverageTime.toString());
        networkManager.addQuery(token, "driver/average_wait_time", coordForAverageTime, new WebSocketAnswerCallback<WebSocketAnswer<Object>>() {
                    @Override
                    public void messageReceived(WebSocketAnswer<Object> message) {
                        switch (message.getResultCode()) {
                            case "success":
                                Object averageTime = message.getPayload();
                                LinkedTreeMap<Object,Object> averageTimeFromServer = (LinkedTreeMap) averageTime;
                                if(averageTimeFromServer!=null) {
                                    Double timeToDriver = Double.parseDouble(averageTimeFromServer.get("average_wait_time").toString());
                                    averageTimeCallback.timeToDriver(timeToDriver.intValue());
                                }else {
                                    averageTimeCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.NO_FREE_DRIVERS, message.getMessage()));
                                }
                                break;
                            case "invalid_token":
                                averageTimeCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR, message.getMessage()));
                                break;
                            case "no_free_drivers":
                                averageTimeCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.NO_FREE_DRIVERS, message.getMessage()));
                                break;
                            case "tariff_not_found":
                                averageTimeCallback.tariffNotFound();
                                break;
                            case "invalid_passenger_region":
                                averageTimeCallback.outOfService(null);
                                break;
                            case "passenger_out_region":
                                Object region = message.getPayload();
                                LinkedTreeMap<Object,Object> t = (LinkedTreeMap) region;
                                if(t!=null && t.get("region")!=null && t.get("id")!=null) {
                                    Log.d(TAG, "onResponse: region is "+t);
                                    int regionId = (Integer) t.get("id");
                                    averageTimeCallback.outOfService(regionId);
                                }else {
                                    averageTimeCallback.outOfService(null);
                                }
                                break;
                            default:
                                averageTimeCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, message.getMessage()));
                                break;
                        }
                    }

                    @Override
                    public void error(ManfredNetworkError manfredError) {
                        averageTimeCallback.error(manfredError);
                    }

                    @Override
                    public Class getAnswerClass() {
                        return Object.class;
                    }
                });
    }

    @Override
    public void getCurrentDriverLocation(String token, NetworkResponseCallback<DriverLocationResponse> driverLocationCallback) {
        networkManager.addQuery(token,"driver_location", new WebSocketAnswerCallback<WebSocketAnswer<DriverLocationResponse>>() {
            @Override
            public void messageReceived(WebSocketAnswer<DriverLocationResponse> message) {
                new NetworkUtils().validateSocketResponse(message,driverLocationCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                driverLocationCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return DriverLocationResponse.class;
            }
        });
    }

    @Override
    public void getFreeCars(String token, NetworkResponseCallback<CarsAnswer> networkResponseCallback) {
        networkManager.addQuery(token, "car/list/free", new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                new NetworkUtils().validateSocketResponse(message,networkResponseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                networkResponseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return CarsAnswer.class;
            }
        });
    }
}
