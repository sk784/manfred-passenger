package manfred.ru.manfredpassenger.ride.fragments.ride;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;
import java.util.Objects;

import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.databinding.FragmentRideBinding;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatus;
import manfred.ru.manfredpassenger.history.viewmodel.HistoryVM;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;
import manfred.ru.manfredpassenger.utils.SimpleGestureFilter;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class RideFragment extends Fragment {
    private static final String TAG = "RideFragment";
    String orderTime;
    String orderDirection;


    public RideFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentRideBinding binding = FragmentRideBinding.inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentRideBinding binding) {
        ObservableBoolean isShowingMore = new ObservableBoolean(true);
        //binding.setIsShowMore(isShowingMore);

        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        model.getCurrentRide().observe(this, new Observer<OrderVM>() {
            @Override
            public void onChanged(@Nullable OrderVM orderVM) {
                binding.setOrder(orderVM);
                if (orderVM != null) {
                    Glide
                            .with(RideFragment.this)
                            .load(orderVM.getDriverAvatarUrl())
                            .apply(RequestOptions.centerCropTransform())
                            .into(binding.driverAvatar);
                    orderTime = orderVM.getCarNeedTime();
                    if (orderVM.getDestinationLocation() != null) {
                        orderDirection = orderVM.getDestinationLocation().getAddress();
                    }

                }
            }
        });


        model.getStatus().observe(this, new Observer<AppStatus>() {
            @Override
            public void onChanged(@Nullable AppStatus appStatus) {
                if (appStatus != null) {
                    switch (appStatus){
                        case CAR_RIDE_TO_PASSENGER:
                            rideToPassengerShow(isShowingMore.get(),binding);
                            break;
                        case CAR_ON_PASSENGER:
                            carOnPassengerShow(isShowingMore.get(),binding);
                            break;
                        case RIDING:
                            tripInCarShow(isShowingMore.get(),binding);
                            break;
                    }
                }
            }
        });

        binding.cancelRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.cancel();
            }
        });



        binding.moreLessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowingMore.set(!isShowingMore.get());
            }
        });

        SimpleGestureFilter.SimpleGestureListener simpleGestureListener = new SimpleGestureFilter.SimpleGestureListener() {
            @Override
            public void onSwipe(int direction) {
                Log.d(TAG, "onSwipe: "+direction);
                switch (direction){
                    case SimpleGestureFilter.SWIPE_UP:
                        isShowingMore.set(true);
                        break;
                    case SimpleGestureFilter.SWIPE_DOWN:
                        isShowingMore.set(false);
                        break;
                }
            }

            @Override
            public void onDoubleTap() {
                Log.d(TAG, "onDoubleTap: ");
            }

            @Override
            public void click() {
                Log.d(TAG, "click: ");
            }
        };

        SimpleGestureFilter detector = new SimpleGestureFilter(getActivity(),simpleGestureListener);
        binding.rideLayout.setClickable(true);
        binding.rideLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Log.d(TAG, "onTouch: ");
                detector.onTouchEvent(event);
                return false;
            }
        });

        isShowingMore.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if(model.getStatus().getValue()!=null) {
                    switch (model.getStatus().getValue()) {
                        case CAR_RIDE_TO_PASSENGER:
                            rideToPassengerShow(isShowingMore.get(), binding);
                            break;
                        case RIDING:
                            tripInCarShow(isShowingMore.get(), binding);
                            break;
                        case CAR_ON_PASSENGER:
                            carOnPassengerShow(isShowingMore.get(), binding);
                            break;
                    }
                }
            }
        });

        binding.ivCall.setOnClickListener(v -> {
            String phone ="+"+ model.getCurrentRide().getValue().getDriverPhoneNumber();
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            startActivity(intent);
        });

        binding.ivChat.setOnClickListener(v -> {

            LayoutInflater inflater = (LayoutInflater) Objects.requireNonNull(getContext()).getSystemService(LAYOUT_INFLATER_SERVICE);
            View popupView = inflater.inflate(R.layout.dialog_choose_chat, null);

            int width = 600;
            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
            boolean focusable = true; // lets taps outside the popup also dismiss it
            final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

            popupWindow.showAtLocation(binding.rideLayout, Gravity.CENTER, 0, 0);

            RelativeLayout driverChatRl = popupView.findViewById(R.id.rl_driver_chat);
            RelativeLayout supportChatRl = popupView.findViewById(R.id.rl_support_chat);

            TextView tvOrderTime = popupView.findViewById(R.id.tv_order_time);
            TextView tvOrderTo = popupView.findViewById(R.id.tv_order_to);

            tvOrderTime.setText(orderTime);
            tvOrderTo.setText(orderDirection);

            driverChatRl.setOnClickListener(view -> {
                Log.d("sk7847","driver_chat");
                popupWindow.dismiss();
            });

            supportChatRl.setOnClickListener(view -> {
                Log.d("sk7847","support_chat");
                popupWindow.dismiss();
            });
        });
    }

    private void carOnPassengerShow(boolean isShowingMore, FragmentRideBinding binding) {
        Log.d(TAG, "carOnPassengerShow: ");
        binding.tvCarInTripCaption.setText(R.string.car_on_passenger);
        showingMoreStatus(isShowingMore, binding);
    }

    //android:src="@{isShowMore ? @drawable/ic_expand_more : @drawable/ic_expand_less}"
    private void rideToPassengerShow(boolean isShowingMore, FragmentRideBinding binding) {
        binding.tvCarInTripCaption.setText(R.string.car_on_route);
        showingMoreStatus(isShowingMore, binding);
    }

    private void showingMoreStatus(boolean isShowingMore, FragmentRideBinding binding) {
        if(isShowingMore){
            binding.cancelRide.setVisibility(View.VISIBLE);
            binding.moreLessButton.setBackgroundResource(R.drawable.ic_expand_more);
        }else {
            binding.cancelRide.setVisibility(View.GONE);
            binding.moreLessButton.setBackgroundResource(R.drawable.ic_expand_less);
        }
    }

    private void tripInCarShow(boolean isShowingMore, FragmentRideBinding binding) {
        binding.tvCarInTripCaption.setText(R.string.riding);
        binding.cancelRide.setVisibility(View.GONE);
        if(isShowingMore){
            binding.moreLessButton.setBackgroundResource(R.drawable.ic_expand_more);
            binding.llCallZone.setVisibility(View.VISIBLE);
        }else {
            binding.moreLessButton.setBackgroundResource(R.drawable.ic_expand_less);
            binding.llCallZone.setVisibility(View.GONE);
        }
    }



}

