package manfred.ru.manfredpassenger.ride;

import android.arch.lifecycle.Observer;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.ride.fragments.tariffFullDescription.FullDescriptionFragment;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffDetailVM;

public class TariffDescriptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tariff_description);
        Toolbar myToolbar = findViewById(R.id.tariff_desrc_toolbar);
        setSupportActionBar(myToolbar);
        ((ManfredPassengerApplication)getApplication()).getTariffManager().
                getSelectedTariffDetails().observe(this, new Observer<TariffDetailVM>() {
            @Override
            public void onChanged(@Nullable TariffDetailVM tariffDetailVM) {
                if(tariffDetailVM!=null){
                    setTitle(tariffDetailVM.getTariffName());
                    if(tariffDetailVM.isAssistantAllowed()){
                        initPageAdapter(2);
                    }else {
                        initPageAdapter(1);
                    }
                }
            }
        });

        final ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //setting black background for status bar
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.black_10));


    }

    private void initPageAdapter(int count) {
        PagerAdapter adapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return new FullDescriptionFragment();
            }

            @Override
            public int getCount() {
                return count;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                String title;
                if(position==0){
                    title="Без ассистента";
                }else {
                    title="C ассистентом";
                }
                return title;
            }
        };

        ViewPager pager = findViewById(R.id.tariff_descr_pager);
        pager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tabs);
        if(count!=1) {
            tabLayout.setupWithViewPager(pager);
        }else {
            tabLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
