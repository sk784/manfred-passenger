package manfred.ru.manfredpassenger.ride.managers.mapOverlay;

import android.arch.lifecycle.LiveData;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import manfred.ru.manfredpassenger.ride.managers.FreeCarsManager;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.viewmodels.MarkerViewModel;

public interface MapDrawingManager {

    void setStartPin(ManfredLocation startLocation);
    void showFinishPin(ManfredLocation finishLocation);
    void placeCarMarker(ManfredLocation cartLocation);
    void showCarsOnMap(FreeCarsManager.CarsOnMap carsOnMap);
    void calculateRouteOnMap(ManfredLocation startLocation, ManfredLocation endLocation);

    LiveData<List<PinOnMap>>getPins();
    LiveData<MarkerViewModel>getPinWithTime();
    LiveData<List<LatLng>>getRoute();
}
