package manfred.ru.manfredpassenger.ride.fragments.tariffFullDescription;


import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffDetailVM;


/**
 * A simple {@link Fragment} subclass.
 */
public class FullDescriptionFragment extends Fragment {


    public FullDescriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_full_description, container, false);
        //TextView description = view.findViewById(R.id.full_description_text);
        WebView description = view.findViewById(R.id.wv_fullDescr);
        ((ManfredPassengerApplication)getActivity().getApplication()).getTariffManager().getSelectedTariffDetails().observe(this, new Observer<TariffDetailVM>() {
            @Override
            public void onChanged(@Nullable TariffDetailVM tariffDetailVM) {
                if (tariffDetailVM != null) {
                    //description.setText(Html.fromHtml(tariffDetailVM.getFullHTMLDescr()));
                    description.loadData(tariffDetailVM.getFullHTMLDescr(), "text/html; charset=utf-8", "UTF-8");
                }
            }
        });
        /*
        String descr = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredOrderManager().
                getSelectedTariffDetails().getValue().getFullHTMLDescr();
        description.setText(Html.fromHtml(descr));*/
        return view;
    }




}
