package manfred.ru.manfredpassenger.ride.fragments.cards;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.BR;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.cards.PaymentsActivity;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.managers.ManfredCreditCardManager;
import manfred.ru.manfredpassenger.cards.models.CreditCardVM;
import manfred.ru.manfredpassenger.databinding.AddCardHolderBinding;
import manfred.ru.manfredpassenger.databinding.FragmentSelectActiveCardBinding;
import manfred.ru.manfredpassenger.databinding.SelecatbleCreditCardItemBinding;
import manfred.ru.manfredpassenger.ride.viewmodels.ButtonAddCardItem;
import manfred.ru.manfredpassenger.ride.viewmodels.CardType;
import manfred.ru.manfredpassenger.ride.viewmodels.CreditCardItem;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.utils.CardsDiffUtilCallback;


/**
 * A simple {@link Fragment} subclass.
 */
public class SelectActiveCardFragment extends Fragment {
    private static final String TAG = "SelectActiveCardFragmen";
    private ViewGroup.LayoutParams prevParentParams;

    public SelectActiveCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //we do this because container not matching parent :(
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        FragmentSelectActiveCardBinding binding = FragmentSelectActiveCardBinding.inflate(getLayoutInflater());
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentSelectActiveCardBinding binding) {
        binding.getRoot().setOnClickListener(v -> {});
        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        binding.transparentView.setOnClickListener(v -> model.cancel());
        List<CardType> creditCards = new ArrayList<>();
        ManfredCreditCardManager creditCardManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredCreditCardManager();
        ItemType<AddCardHolderBinding> addCard = new ItemType<AddCardHolderBinding>(R.layout.add_card_holder){
            @Override
            public void onCreate(@NonNull Holder<AddCardHolderBinding> holder) {
                super.onCreate(holder);
                holder.getBinding().btnAddCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), PaymentsActivity.class);
                        startActivity(intent);
                        Log.d(TAG, "onClick: ");
                    }
                });
            }
        };

        ItemType<SelecatbleCreditCardItemBinding>cardItem = new ItemType<SelecatbleCreditCardItemBinding>(R.layout.selecatble_credit_card_item){
            @Override
            public void onCreate(@NonNull Holder<SelecatbleCreditCardItemBinding> holder) {
                super.onCreate(holder);
                CreditCardVM creditCardVM = holder.getBinding().getCreditCard().getCreditCard();
                if(creditCardVM.isAvailable()){
                    holder.getBinding().getRoot().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            creditCardManager.selectCard(holder.getBinding().getCreditCard().getCreditCard().getId());
                            model.cancel();
                        }
                    });
                    if(creditCardVM.isSelected()){
                        holder.getBinding().ivIsSelected.setVisibility(View.VISIBLE);
                    }
                }else {
                    holder.getBinding().tvUnavailble.setVisibility(View.VISIBLE);
                }

            }
        };

        LastAdapter adapter = new LastAdapter(creditCards, BR.creditCard)
                .map(CreditCardItem.class,cardItem)
                .map(ButtonAddCardItem.class,addCard)
                .into(binding.rvSelectCard);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.onSaveInstanceState();
        binding.rvSelectCard.setLayoutManager(mLayoutManager);

        creditCardManager.cards().observe(this, new Observer<List<CreditCard>>() {
            @Override
            public void onChanged(@Nullable List<CreditCard> cards) {
                creditCards.clear();
                if(cards!=null) {
                    List<CardType> newCreditCards = new ArrayList<>();
                    for (CreditCard creditCard : cards) {
                        Log.d(TAG, "onChanged: card is "+creditCard.getCardNumber());
                        CreditCardItem creditCardItem = new CreditCardItem(creditCard,
                                ((ManfredPassengerApplication)getActivity().getApplication()).getRegionManager().getCurrentPaymentSystem());
                        newCreditCards.add(creditCardItem);
                    }
                    newCreditCards.add(new ButtonAddCardItem());
                    CardsDiffUtilCallback cardsDiffUtilCallback = new CardsDiffUtilCallback(creditCards, newCreditCards);
                    DiffUtil.DiffResult cardsResult = DiffUtil.calculateDiff(cardsDiffUtilCallback);
                    cardsResult.dispatchUpdatesTo(adapter);
                    adapter.notifyDataSetChanged();
                    creditCards.clear();
                    creditCards.addAll(newCreditCards);
                    Log.d(TAG, "onChanged: new credits cards is "+newCreditCards.size());
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup container = ((ViewGroup)getView().getParent());
        container.setLayoutParams(prevParentParams);
    }

}
