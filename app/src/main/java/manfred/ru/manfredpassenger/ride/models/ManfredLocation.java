package manfred.ru.manfredpassenger.ride.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.location.Location;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
public class ManfredLocation {
    @ColumnInfo(name = "latitude")
    private final double latitude;
    @ColumnInfo(name = "longitude")
    private final double longitude;
    @ColumnInfo(name = "place_adress")
    @Nullable private String street;
    @ColumnInfo(name = "place_city")
    @Nullable private String city;
    @Ignore Location location;

    public void setStreet(@Nullable String street) {
        this.street = street;
    }

    public void setCity(@Nullable String city) {
        this.city = city;
    }

    @Ignore private float bearing;

    public float getBearing() {
        return bearing;
    }

    @Ignore
    public ManfredLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        //id = hashCode();
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }

    @Nullable
    public String getStreet() {
        return street;
    }

    public ManfredLocation(double latitude, double longitude, @Nullable String street, @Nullable String city) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.street = street;
        this.city = city;
    }

    @Ignore
    public ManfredLocation(Location location) {
        this.location=location;
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public LatLng toLatLng() {
        return new LatLng(latitude, longitude);
    }

    public Location toLocation() {
        if(location==null) {
            Location location = new Location("created from ManfredLocation");
            location.setLongitude(longitude);
            location.setLatitude(latitude);
            return location;
        }else {
            return location;
        }
    }

    public ManfredDTOLocation toDTOLocation(){
        return new ManfredDTOLocation(getAddress(),String.valueOf(latitude),"",String.valueOf(longitude));
    }

    @Nullable
    public String getAddress() {
        return street + ", "+city;
    }

    @Override
    public String toString() {
        return "ManfredLocation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", address='" + getAddress() + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final ManfredLocation that = (ManfredLocation) o;

        if (Double.compare(that.latitude, latitude) != 0) return false;
        if (Double.compare(that.longitude, longitude) != 0) return false;
        if (Float.compare(that.bearing, bearing) != 0) return false;
        if (street != null ? !street.equals(that.street) : that.street != null) return false;
        return city != null ? city.equals(that.city) : that.city == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(latitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Nullable
    public String getCity() {
        return city;
    }
}
