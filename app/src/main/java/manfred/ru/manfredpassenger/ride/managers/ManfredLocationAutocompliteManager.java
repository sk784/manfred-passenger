package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.ride.models.AdressPlaceItem;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.models.PickedPlaceItem;
import manfred.ru.manfredpassenger.ride.models.PlaceItem;
import manfred.ru.manfredpassenger.ride.models.SavedAdressPlaceItem;
import manfred.ru.manfredpassenger.ride.repositories.PlacesDatabaseRepository;

public class ManfredLocationAutocompliteManager implements LocationAutocompliteManager {
    private static final String TAG = "ManfredLocationAutocomp";
    private final PlacesClient placesClient;
    private final AutocompleteSessionToken autocompleteSessionToken = AutocompleteSessionToken.newInstance();
    //private final GoogleApiClient googleApiClient;
    private MutableLiveData<List<PlaceItem>> startPlaces;
    private MutableLiveData<List<PlaceItem>> finishPlaces;
    private MutableLiveData<ManfredNetworkError> errors;
    private MutableLiveData<SearchState> searchState;
    private MutableLiveData<ManfredLocation> selectedPlace;
    private PlacesDatabaseRepository placesDatabaseRepository;
    public ManfredLocationAutocompliteManager(PlacesDatabaseRepository placesDatabaseRepository, PlacesClient placesClient) {
        this.placesClient = placesClient;
        //this.googleApiClient = googleApiClient;
        this.startPlaces = new MutableLiveData<>();
        this.finishPlaces = new MutableLiveData<>();
        this.errors = new MutableLiveData<>();
        this.searchState = new MutableLiveData<>();
        this.searchState.setValue(SearchState.SEARCH_ADRESS);
        this.selectedPlace = new MutableLiveData<>();
        this.placesDatabaseRepository = placesDatabaseRepository;
        this.startPlaces.postValue(initStartPlaces());
        this.finishPlaces.postValue(initFinishPlaces());
    }

    private List<PlaceItem> initStartPlaces() {
        List<PlaceItem> storedPlaces = savedLocationToPlaceItem(placesDatabaseRepository.getStartSavedLocations());
        Log.d(TAG, "initStartPlaces: " + storedPlaces);
        return storedPlaces;
    }

    private List<PlaceItem> initFinishPlaces() {
        Log.d(TAG, "initFinishPlaces: ");
        List<PlaceItem> storedPlaces = savedLocationToPlaceItem(placesDatabaseRepository.getFinishSavedLocations());
        return storedPlaces;
    }

    @Override
    public void searchStringStart(String string, Location centerOfSearchLocation) {
        askForPlaces(string, true,centerOfSearchLocation);
    }

    @Override
    public void searchStringFinish(String string, Location centerOfSearch) {
        askForPlaces(string, false,centerOfSearch);
    }

    public LiveData<List<PlaceItem>> getStartPlaces() {
        return startPlaces;
    }

    public LiveData<List<PlaceItem>> getFinishPlaces() {
        return finishPlaces;
    }

    public LiveData<ManfredNetworkError> getErrors() {
        return errors;
    }

    public LiveData<SearchState> getSearchState() {
        return searchState;
    }

    public LiveData<ManfredLocation> getSelectedPlace() {
        return selectedPlace;
    }

    private void askForPlaces(String string, boolean isFrom, @Nullable Location centerOfSearchLocation) {
        Log.i(TAG, "Starting autocomplete query for: " + string + " ,isFrom " + isFrom);
        final RectangularBounds bounds;
        if(centerOfSearchLocation!=null) {
            bounds = createBounds(centerOfSearchLocation, 200000);
        }else {
            bounds=null;
        }
        placesDatabaseRepository.filterPlace(string, new PlacesDatabaseRepository.FilteredText() {
            @Override
            public void filtered(List<ManfredLocation> places) {
                List<PlaceItem> placeItemList = new ArrayList<>();
                if (places != null) {
                    Log.d(TAG, "filtered: places from BD " + places.size() + ", it is:");
                    for (ManfredLocation manfredLocation : places) {
                        Log.d(TAG, manfredLocation.toString());
                    }
                    List<PlaceItem> saved = savedLocationToPlaceItem(places);
                    //places.postValue(saved);
                    placeItemList.addAll(saved);
                }
                if (string.isEmpty()) {
                    if (isFrom) {
                        startPlaces.postValue(placeItemList);
                    } else {
                        finishPlaces.postValue(placeItemList);
                    }
                } else {
                    getNetPredictions(string, bounds, placeItemList, isFrom);
                }
            }
        }, isFrom);
    }

    private RectangularBounds createBounds(Location centerOfSearchLocation, int DistanceInMeters) {
        double latRadian = Math.toRadians(centerOfSearchLocation.getLatitude());

        double degLatKm = 110.574235;
        double degLongKm = 110.572833 * Math.cos(latRadian);
        double deltaLat = DistanceInMeters / 1000.0 / degLatKm;
        double deltaLong = DistanceInMeters / 1000.0 / degLongKm;

        double minLat = centerOfSearchLocation.getLatitude() - deltaLat;
        double minLong = centerOfSearchLocation.getLongitude() - deltaLong;
        double maxLat = centerOfSearchLocation.getLatitude() + deltaLat;
        double maxLong = centerOfSearchLocation.getLongitude() + deltaLong;

        Log.d(TAG, "Min: " + Double.toString(minLat) + "," + Double.toString(minLong));
        Log.d(TAG, "Max: " + Double.toString(maxLat) + "," + Double.toString(maxLong));
        return RectangularBounds.newInstance(new LatLng(minLat, minLong),new LatLng(maxLat, maxLong));
    }

    private List<PlaceItem> savedLocationToPlaceItem(List<ManfredLocation> locations) {
        Log.d(TAG, "savedLocationToPlaceItem: ");
        List<PlaceItem> placeItemList = new ArrayList<>();
        for (ManfredLocation location : locations) {
            Log.d(TAG, "savedLocationToPlaceItem: " + location.toString());
            placeItemList.add(new SavedAdressPlaceItem(location));
        }
        return placeItemList;
    }

    private void getNetPredictions(String string, @Nullable RectangularBounds rectangularBounds, @Nullable final List<PlaceItem> placeItems, boolean isFrom) {
        Log.d(TAG, "getNetPredictions: ");
        final List<PlaceItem> newPlacesItem;
        if (placeItems == null) {
            newPlacesItem = new ArrayList<>();
        } else {
            newPlacesItem = placeItems;
        }
        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                .setLocationRestriction(rectangularBounds)
                .setTypeFilter(TypeFilter.ADDRESS)
                .setSessionToken(autocompleteSessionToken)
                .setQuery(string)
                .build();
        placesClient.findAutocompletePredictions(request)
                .addOnSuccessListener(new OnSuccessListener<FindAutocompletePredictionsResponse>() {
                    @Override
                    public void onSuccess(FindAutocompletePredictionsResponse response) {
                        Log.d(TAG, "getNetPredictions onSuccess: "+response.getAutocompletePredictions());
                        for (AutocompletePrediction prediction : response.getAutocompletePredictions()) {
                            //Add as a new item to avoid IllegalArgumentsException when buffer is released
                            newPlacesItem.add(new AdressPlaceItem(
                                    prediction.getPrimaryText(null).toString(),
                                    prediction.getSecondaryText(null).toString(),
                                    prediction.getPlaceId()));
                            if (isFrom) {
                                startPlaces.postValue(newPlacesItem);
                            } else {
                                finishPlaces.postValue(newPlacesItem);
                            }
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            Log.e(TAG, "Place not found: " + apiException.getStatusCode()+", "+apiException.getMessage());
                        }else {
                            Log.e(TAG, "getNetPredictions onFailure: ", e);
                        }
                        if (isFrom) {
                            startPlaces.postValue(newPlacesItem);
                        } else {
                            finishPlaces.postValue(newPlacesItem);
                        }
                    }
                });

    }

    public void selectPlace(PlaceItem placeItem) {
        //Log.d(TAG, "selectPlace: "+placeItem.getType());
        switch (placeItem.getType()) {
            case PREDICTION_LOCATION:
                selectPlaceFromPrediction((AdressPlaceItem) placeItem);
                break;
            case PLACE_PICKER:
                searchState.postValue(SearchState.PICK_ADRESS);
                break;
            case SAVED_LOCATION:
                selectedPlace.postValue(((SavedAdressPlaceItem) placeItem).getManfredLocation());
                break;
            case PICKED_LOCATION:
                selectedPlace.postValue(((PickedPlaceItem) placeItem).getManfredLocation());
                break;
        }
    }

    private void selectPlaceFromPrediction(AdressPlaceItem placeItem) {
        String placeId = placeItem.getPlaceId();
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
        FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields)
                .build();
        placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
            Place place = response.getPlace();
            if(place.getLatLng()!=null) {
                ManfredLocation location = new ManfredLocation(place.getLatLng().latitude,
                        place.getLatLng().longitude, placeItem.getStreet(), placeItem.getCity());
                selectedPlace.setValue(location);
            }
            Log.i(TAG, "Place found: " + place.getName());
        }).addOnFailureListener((exception) -> {
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
                int statusCode = apiException.getStatusCode();
                // Handle error with given status code.
                Log.e(TAG, "Place not found: " + exception.getMessage());
            }
        });
    }

    public enum SearchState {
        SEARCH_ADRESS,
        PICK_ADRESS
    }
}
