package manfred.ru.manfredpassenger.ride.models;

import com.google.gson.annotations.SerializedName;

public class Driver {

    @SerializedName("birthday")
    private String birthday;

    @SerializedName("image")
    private String image;

    @SerializedName("patronymic")
    private Object patronymic;

    @SerializedName("phoneNumber")
    private String phoneNumber;

    @SerializedName("previous_location")
    private Object previousLocation;

    @SerializedName("surname")
    private String surname;

    @SerializedName("is_free")
    private boolean isFree;

    @SerializedName("name")
    private String name;

    @SerializedName("driverLicense")
    private String driverLicense;

    @SerializedName("location")
    private ManfredDTOLocation manfredDTOLocation;

    @SerializedName("id")
    private int id;

    @SerializedName("order_id")
    private int orderId;

    @SerializedName("car_id")
    private int carId;

    @SerializedName("registerDate")
    private String registerDate;

    public String getBirthday() {
        return birthday;
    }

    public String getImage() {
        return image;
    }

    public Object getPatronymic() {
        return patronymic;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Object getPreviousLocation() {
        return previousLocation;
    }

    public String getSurname() {
        return surname;
    }

    public boolean isIsFree() {
        return isFree;
    }

    public String getName() {
        return name;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public ManfredDTOLocation getManfredLocation() {
        return manfredDTOLocation;
    }

    public int getId() {
        return id;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getCarId() {
        return carId;
    }

    public String getRegisterDate() {
        return registerDate;
    }
}