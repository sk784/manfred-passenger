package manfred.ru.manfredpassenger.ride.fragments.ride;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import manfred.ru.manfredpassenger.databinding.FragmentPayErrorBinding;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;



/**
 * A simple {@link Fragment} subclass.
 */
public class PayErrorFragment extends Fragment {
    private ViewGroup.LayoutParams prevParentParams;
    private static final long COUNTDOWN_TIMER_VALUE = 10 * 60 * 1000;
    private static final String COUNTDOWN_START_AT = "countdown_state";
    private long timerStart = 0;


    public PayErrorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentPayErrorBinding binding = FragmentPayErrorBinding.inflate(getLayoutInflater());
        //we do this because container not matching parent :(
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));

        init(binding);

        SharedPreferences preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("pref", Context.MODE_PRIVATE);


        if (new Date().getTime() - preferences.getLong(COUNTDOWN_START_AT, 0) > COUNTDOWN_TIMER_VALUE){
            preferences.edit().putLong(COUNTDOWN_START_AT, 0).apply();
        }

        if (preferences.getLong(COUNTDOWN_START_AT, 0)==0) {
            initTimer(binding, COUNTDOWN_TIMER_VALUE);
            preferences.edit().putLong(COUNTDOWN_START_AT, timerStart).apply();
        } else {
            timerStart = preferences.getLong(COUNTDOWN_START_AT,0);
            long dateDelta = new Date().getTime() - timerStart;
            initTimer(binding, COUNTDOWN_TIMER_VALUE - dateDelta);
        }
        return binding.getRoot();
    }

    private void init(FragmentPayErrorBinding binding) {
        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        binding.getRoot().setOnClickListener(v -> {});
        binding.setError(model.getError());
        binding.btnSelectCard.setOnClickListener(v -> model.rePayCurrentOrder());
        binding.btnCheckBalance.setOnClickListener(v->model.tryRePay());
    }


    private void initTimer(FragmentPayErrorBinding binding, long countdownMillis) {
        if (timerStart == 0) {
            timerStart = new Date().getTime();
        }
        if (countdownMillis < 0) countdownMillis = 0;
        new CountDownTimer(countdownMillis, TimeUnit.SECONDS.toMillis(1)) {
            public void onTick(long millisUntilFinished) {
                binding.tvCountdown.setText(String.valueOf(millisUntilFinished/60000));
            }

            public void onFinish() {

            }
        }.start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getView()!=null) {
            ViewGroup container = ((ViewGroup) getView().getParent());
            container.setLayoutParams(prevParentParams);
        }
    }
}
