package manfred.ru.manfredpassenger.ride;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.common.managers.regions.ManfredRegionManager;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.databinding.ActivitySelectAdressBinding;
import manfred.ru.manfredpassenger.ride.fragments.selectAdress.PickPlaceFragment;
import manfred.ru.manfredpassenger.ride.fragments.selectAdress.SelectPlaceFragment;
import manfred.ru.manfredpassenger.ride.managers.ManfredLocationAutocompliteManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredPreCalculateCostManager;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.viewmodels.AutocompleteViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.AutocompleteViewModelFactory;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModelFactory;
import manfred.ru.manfredpassenger.ride.viewmodels.SelectAdressViewModel;
import manfred.ru.manfredpassenger.utils.Constants;

public class SelectAdressActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "SelectAdressActivity";
    //public boolean isFinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySelectAdressBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_select_adress);

        setSupportActionBar(binding.toolbar2);
        setTitle("");

        //setting black background for status bar
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.black_10));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        binding.toolbar2.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        Log.d(TAG, "onCreate: creating VM");
        OrderViewModelFactory orderViewModelFactory = new OrderViewModelFactory(((ManfredPassengerApplication)getApplication()).getManfredOrderManager(),
                ((ManfredPassengerApplication)getApplication()).getPreOrderManager(),
                ((ManfredPassengerApplication)getApplication()).getAppStatusManager(),
                ((ManfredPassengerApplication)getApplication()).getTariffManager(),
                ((ManfredPassengerApplication)getApplication()).getRegionManager(),
                ((ManfredPassengerApplication) getApplication()).getTimeToDriverManager(),
                new ManfredPreCalculateCostManager(((ManfredPassengerApplication) getApplication()).getTokenManager(),
                        ((ManfredPassengerApplication) getApplication()).getNetworkManager()),
                ((ManfredPassengerApplication) getApplication()).getPromoCodeManager(),
                ((ManfredPassengerApplication) getApplication()).getAnalyticsManager());
        //init for fragment
        OrderViewModel orderViewModel = ViewModelProviders.of(this, orderViewModelFactory).get(OrderViewModel.class);

        // Initialize Places.
        ApplicationInfo ai = null;
        String myApiKey=null;
        try {
            ai = getPackageManager().getApplicationInfo(getApplication().getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            myApiKey = bundle.getString("com.google.android.geo.API_KEY");
            Log.d(TAG, "onCreate: myApiKey ");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(myApiKey==null)myApiKey="";
        Places.initialize(getApplicationContext(), myApiKey);
        PlacesClient placesClient = Places.createClient(this);

        AutocompleteViewModelFactory factory = new AutocompleteViewModelFactory(new ManfredLocationAutocompliteManager(
                ((ManfredPassengerApplication)getApplication()).getPlacesDatabaseRepository(), placesClient));
        AutocompleteViewModel autocompleteViewModel = ViewModelProviders.of(this, factory).get(AutocompleteViewModel.class);

        SelectAdressViewModel selectAdressViewModel = ViewModelProviders.of(this).get(SelectAdressViewModel.class);
        boolean isFinish = getIntent().getBooleanExtra(Constants.FINISH_FIELD, false);
        selectAdressViewModel.setFinish(isFinish);

        final RegionManager manfredRegionManager = ((ManfredPassengerApplication) getApplication()).getRegionManager();
        Region currRegion = manfredRegionManager.getCurrentRegion().getValue();
        final Location centerRegionLocation;
        if(currRegion!=null){
            centerRegionLocation = new Location("region manager");
            centerRegionLocation.setLatitude(Double.parseDouble(currRegion.getCenterLatitude()));
            centerRegionLocation.setLongitude(Double.parseDouble(currRegion.getCenterLongitude()));
        }else {
            centerRegionLocation=null;
        }

        TextWatcher searchWatcher = new TextWatcher() {
            long lastPress = 0l;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(binding.selCityBarEdit.getText().toString().length()==0){
                    if(isFinish) {
                        autocompleteViewModel.searchFinishPlace("",centerRegionLocation);
                    }else {
                        autocompleteViewModel.searchFromPlace("",centerRegionLocation);
                    }
                }else {
                    if (System.currentTimeMillis() - lastPress > 500) {
                        lastPress = System.currentTimeMillis();
                        if(isFinish) {
                            autocompleteViewModel.searchFinishPlace(binding.selCityBarEdit.getText().toString(),centerRegionLocation);
                        }else {
                            autocompleteViewModel.searchFromPlace(binding.selCityBarEdit.getText().toString(),centerRegionLocation);
                        }
                    } else {
                        Log.d(TAG, "onTextChanged: so quick, ignoring");
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        binding.selCityBarEdit.addTextChangedListener(searchWatcher);

        binding.clearSearchedPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.selCityBarEdit.setText("");
            }
        });


        autocompleteViewModel.getState().observe(this, new Observer<ManfredLocationAutocompliteManager.SearchState>() {
            @Override
            public void onChanged(@Nullable ManfredLocationAutocompliteManager.SearchState searchState) {
                if (searchState != null) {
                    Log.d(TAG, "onChanged state: " + searchState);
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    switch (searchState) {
                        case SEARCH_ADRESS:
                            transaction.replace(binding.fragmentContainer.getId(), new SelectPlaceFragment());
                            binding.selCityBarEdit.setVisibility(View.VISIBLE);
                            binding.clearSearchedPlace.setVisibility(View.VISIBLE);
                            binding.selectAdressCaption.setVisibility(View.GONE);
                            break;
                        case PICK_ADRESS:
                            transaction.replace(binding.fragmentContainer.getId(), new PickPlaceFragment());
                            binding.selCityBarEdit.setVisibility(View.GONE);
                            binding.clearSearchedPlace.setVisibility(View.GONE);
                            binding.selectAdressCaption.setVisibility(View.VISIBLE);
                            if (selectAdressViewModel.isFinish()) {
                                binding.selectAdressCaption.setText("Куда");
                            } else {
                                binding.selectAdressCaption.setText("Откуда");
                            }
                            break;
                    }
                    transaction.commit();
                }
            }
        });

        autocompleteViewModel.getCurrentSelectedPlace().observe(this, new Observer<ManfredLocation>() {
            @Override
            public void onChanged(@Nullable ManfredLocation location) {
                if (location != null) {
                    if (selectAdressViewModel.isFinish()) {
                        ((ManfredPassengerApplication)getApplication()).getManfredOrderManager().setFinishLocation(location);
                    } else {
                        ((ManfredPassengerApplication)getApplication()).getManfredOrderManager().changeMyLocation(location);
                    }
                }
                finish();
            }
        });

        if (selectAdressViewModel.isFinish()) {
            binding.selCityBarEdit.setHint("Куда");
        } else {
            binding.selCityBarEdit.setHint("Откуда");
        }

        initNoInetObserver();
    }

    void initNoInetObserver(){
        ((ManfredPassengerApplication)getApplication()).getConnectManager().getIsConnected().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    ConstraintLayout noInetLayout = findViewById(R.id.cl_no_inet);
                    if(aBoolean){
                        noInetLayout.setVisibility(View.GONE);
                    }else {
                        noInetLayout.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onBackPressed() {
        if(!((ManfredPassengerApplication)getApplication()).getConnectManager().isConnected())return;
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        if(((ManfredPassengerApplication)getApplication()).getConnectManager().isConnected()) {
            super.onBackPressed();
        }
        return false;
    }
}
