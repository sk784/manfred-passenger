package manfred.ru.manfredpassenger.ride.fragments.cards;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.BR;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.cards.PaymentsActivity;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.managers.ManfredCreditCardManager;
import manfred.ru.manfredpassenger.databinding.AddCardHolderBinding;
import manfred.ru.manfredpassenger.databinding.CreditCardItemForDebtBinding;
import manfred.ru.manfredpassenger.databinding.FragmentSelectActiveCardBinding;
import manfred.ru.manfredpassenger.ride.managers.DebtManager;
import manfred.ru.manfredpassenger.ride.viewmodels.ButtonAddCardItem;
import manfred.ru.manfredpassenger.ride.viewmodels.CardType;
import manfred.ru.manfredpassenger.ride.viewmodels.CreditCardItem;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.utils.CardsDiffUtilCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectCardForRePayCurrentOrder extends Fragment {
    private static final String TAG = "SelectCardForPay";
    private ViewGroup.LayoutParams prevParentParams;


    public SelectCardForRePayCurrentOrder() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        FragmentSelectActiveCardBinding binding = FragmentSelectActiveCardBinding.inflate(getLayoutInflater());
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentSelectActiveCardBinding binding) {
        binding.getRoot().setOnClickListener(v -> {});
        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        binding.transparentView.setOnClickListener(v -> model.cancel());
        List<CardType> creditCards = new ArrayList<>();
        ManfredCreditCardManager creditCardManager =
                ((ManfredPassengerApplication)getActivity().getApplication()).getManfredCreditCardManager();
        DebtManager debtManager = ((ManfredPassengerApplication)getActivity().getApplication()).getDebtManager();
        ItemType<AddCardHolderBinding> addCard = new ItemType<AddCardHolderBinding>(R.layout.add_card_holder){
            @Override
            public void onCreate(@NonNull Holder<AddCardHolderBinding> holder) {
                super.onCreate(holder);
                holder.getBinding().btnAddCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), PaymentsActivity.class);
                        startActivity(intent);
                    }
                });
            }
        };

        ItemType<CreditCardItemForDebtBinding>cardItem = new ItemType<CreditCardItemForDebtBinding>(R.layout.credit_card_item_for_debt){
            @Override
            public void onCreate(@NonNull Holder<CreditCardItemForDebtBinding> holder) {
                super.onCreate(holder);
                holder.getBinding().getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "select card for pay: ");
                        binding.rvSelectCard.setVisibility(View.GONE);
                        binding.tvSelCardsCaption.setText("Попытка оплаты");
                        binding.pbSelectingCard.setVisibility(View.VISIBLE);
                        if( model.getCurrentRide().getValue()!=null) {
                            creditCardManager.selectCardAndRepayOrder(holder.getBinding().getCreditCard()
                                            .getCreditCard().getId(), model.getCurrentRide().getValue().getId(),
                                    new NetworkResponseCallback() {
                                        @Override
                                        public void allOk(Object response) {
                                            model.successfullyPaid();
                                        }

                                        @Override
                                        public void error(ManfredNetworkError error) {
                                            AlertDialog.Builder builder =
                                                    new AlertDialog.Builder(getActivity(), R.style.AlertDialogCustom);
                                            builder.setTitle("Ошибка");
                                            builder.setMessage(error.getText());
                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    binding.rvSelectCard.setVisibility(View.VISIBLE);
                                                    binding.tvSelCardsCaption.setText("Карты оплаты");
                                                    binding.pbSelectingCard.setVisibility(View.GONE);
                                                }
                                            });
                                            builder.create().show();
                                        }
                                    });
                        }
                    }
                });
            }
        };

        LastAdapter adapter = new LastAdapter(creditCards, BR.creditCard)
                .map(CreditCardItem.class,cardItem)
                .map(ButtonAddCardItem.class,addCard)
                .into(binding.rvSelectCard);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.onSaveInstanceState();
        binding.rvSelectCard.setLayoutManager(mLayoutManager);

        creditCardManager.cards().observe(this, new Observer<List<CreditCard>>() {
            @Override
            public void onChanged(@Nullable List<CreditCard> cards) {
                creditCards.clear();
                if(cards!=null) {
                    List<CardType> newCreditCards = new ArrayList<>();
                    for (CreditCard creditCard : cards) {
                        Log.d(TAG, "onChanged: card is "+creditCard.getCardNumber());
                        CreditCardItem creditCardItem = new CreditCardItem(creditCard,
                                ((ManfredPassengerApplication)getActivity().getApplication())
                                        .getRegionManager().getCurrentPaymentSystem());
                        creditCardItem.getCreditCard().setSelected(false);
                        newCreditCards.add(creditCardItem);
                    }
                    newCreditCards.add(new ButtonAddCardItem());
                    CardsDiffUtilCallback cardsDiffUtilCallback = new CardsDiffUtilCallback(creditCards, newCreditCards);
                    DiffUtil.DiffResult cardsResult = DiffUtil.calculateDiff(cardsDiffUtilCallback);
                    cardsResult.dispatchUpdatesTo(adapter);
                    adapter.notifyDataSetChanged();
                    creditCards.clear();
                    creditCards.addAll(newCreditCards);
                    //Log.d(TAG, "onChanged: new credits cards is "+newCreditCards.size());
                }
            }
        });
    }

    private void showModalDialog(String title, String message){
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.AlertDialogCustom);
        builder.setMessage(message)
                .setTitle(title);
        builder.setPositiveButton("OK",null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup container = ((ViewGroup)getView().getParent());
        container.setLayoutParams(prevParentParams);
    }

}
