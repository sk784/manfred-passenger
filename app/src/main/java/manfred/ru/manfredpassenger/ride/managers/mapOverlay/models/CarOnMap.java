package manfred.ru.manfredpassenger.ride.managers.mapOverlay.models;

import android.location.Location;
import android.support.annotation.IdRes;

import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.services.models.event.Tariff;

public class CarOnMap implements MapObject {
    private float bearing;
    private long id;
    private Location location;
    private boolean busy;
    @IdRes
    private int carIcon;
    //private static final String TAG = "CarOnMap";

    public CarOnMap(long id, Location location, boolean busy, Tariff tariff) {
        //Log.d(TAG, "CarOnMap: creating with "+id+", "+location.toString());
        this.id = id;
        this.location = location;
        this.bearing = 0;
        this.busy = busy;
        setTariff(tariff);

    }

    public void setTariff(Tariff tariff) {
        if(tariff.getIconMapName()!=null) {
            switch (tariff.getIconMapName()){
                case "maybach":
                    this.carIcon = R.drawable.maybach;
                    break;
                case "rolls":
                    this.carIcon = R.drawable.rolls;
                    break;
                case "eclass":
                    this.carIcon = R.drawable.eclass;
                    break;
                case "sclass":
                    this.carIcon = R.drawable.sclass;
                    break;
                default:
                    this.carIcon = R.drawable.maybach;
                    break;
            }
        }else {
            this.carIcon = R.drawable.maybach;
        }
    }

    public int getCarIcon() {
        return carIcon;
    }

    public void setLocation(Location location) {
        float newBearing = this.location.bearingTo(location);
        if (newBearing != 0) {
            setBearing(newBearing);
        }
        this.location = location;
    }

    private void setBearing(float bearing) {
        //Log.d(TAG, "setBearing: to "+bearing);
        this.bearing = bearing;
    }

    public float getBearing() {
        return bearing;
    }

    public long getId() {
        return id;
    }

    public Location getLocation() {
        return location;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

    @Override
    public String toString() {
        return "CarOnMap{" +
                "bearing=" + bearing +
                ", id=" + id +
                ", location=" + location +
                '}';
    }
}
