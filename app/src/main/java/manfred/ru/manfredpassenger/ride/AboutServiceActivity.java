package manfred.ru.manfredpassenger.ride;

import android.arch.lifecycle.Observer;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;

public class AboutServiceActivity extends AppCompatActivity {
    private static final String TAG = "AboutServiceActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.animator.enter_from_right,R.animator.exit_to_left);
        setContentView(R.layout.activity_about_service);
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(android.R.color.black));
        setupToolBar();
        setupWebView();
        initNoInetObserver();
    }

    private void setupWebView() {
        WebView webView = findViewById(R.id.wv_about);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("https://manfred.ru/terms/passenger/license-agreement.html");
    }

    private void setupToolBar() {
        Toolbar toolbar = findViewById(R.id.about_service_toolbar);
        //setTitle(R.string.about_title);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    void initNoInetObserver(){
        ((ManfredPassengerApplication)getApplication()).getConnectManager().getIsConnected().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean!=null){
                    ConstraintLayout noInetLayout = findViewById(R.id.cl_no_inet);
                    if(aBoolean){
                        noInetLayout.setVisibility(View.GONE);
                    }else {
                        noInetLayout.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Log.d(TAG, "onOptionsItemSelected: ");
        if(((ManfredPassengerApplication)getApplication()).getConnectManager().isConnected()) {
            switch (item.getItemId()) {
                // Respond to the action bar's Up/Home/back button
                case android.R.id.home:
                    finish();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }else {
            return false;
        }

    }
}
