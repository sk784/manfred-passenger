package manfred.ru.manfredpassenger.ride.models;

import android.location.Location;

import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

/**
 * For map visualisation: car location, bearing, icon etc
 */
public class CarVM {
    private final Location carLocation;
    private final float bearing;
    private final String carImageUrl;

    public CarVM(Location currLocation, Location prevCarLocation, String carImageUrl) {
        bearing = prevCarLocation.bearingTo(currLocation);
        carLocation=currLocation;
        this.carImageUrl = carImageUrl;
    }

    public Location getCarLocation() {
        return carLocation;
    }

    public String getCarImageUrl() {
        return carImageUrl;
    }

    public float getBearing() {
        return bearing;
    }
}
