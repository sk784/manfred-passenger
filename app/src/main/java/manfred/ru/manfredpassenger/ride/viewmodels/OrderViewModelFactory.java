package manfred.ru.manfredpassenger.ride.viewmodels;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.promocodes.manager.PromoCodeManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredOrderManager;
import manfred.ru.manfredpassenger.ride.managers.ManfredPreOrderManager;
import manfred.ru.manfredpassenger.ride.managers.PreCalculateCostManager;
import manfred.ru.manfredpassenger.ride.managers.TariffManager;
import manfred.ru.manfredpassenger.ride.managers.TimeToDriverManager;

public class OrderViewModelFactory implements ViewModelProvider.Factory {
    private final ManfredOrderManager orderManager;
    private final ManfredPreOrderManager preOrderManager;
    private final AppStatusManager statusManager;
    private final TariffManager tariffManager;
    private final RegionManager regionManager;
    private final TimeToDriverManager timeToDriverManager;
    private final PreCalculateCostManager preCalculateCostManager;
    private final PromoCodeManager promoCodeManager;
    private final AnalyticsManager analyticsManager;

    public OrderViewModelFactory(ManfredOrderManager orderManager, ManfredPreOrderManager preOrderManager,
                                 AppStatusManager statusManager, TariffManager tariffManager, RegionManager regionManager,
                                 TimeToDriverManager timeToDriverManager, PreCalculateCostManager preCalculateCostManager,
                                 PromoCodeManager promoCodeManager, AnalyticsManager analyticsManager) {
        this.orderManager = orderManager;
        this.preOrderManager = preOrderManager;
        this.statusManager = statusManager;
        this.tariffManager = tariffManager;
        this.regionManager = regionManager;
        this.timeToDriverManager = timeToDriverManager;
        this.preCalculateCostManager = preCalculateCostManager;
        this.promoCodeManager = promoCodeManager;
        this.analyticsManager = analyticsManager;
    }

    @NonNull
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(OrderViewModel.class)) {
            return (T) new OrderViewModel(orderManager, regionManager, preOrderManager, statusManager,tariffManager,
                    timeToDriverManager,preCalculateCostManager,promoCodeManager, analyticsManager);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
