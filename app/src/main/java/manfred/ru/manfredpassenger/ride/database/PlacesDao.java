package manfred.ru.manfredpassenger.ride.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.support.annotation.Keep;

import java.util.List;

import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.models.ManfredLocationDB;

@Keep
@Dao
public interface PlacesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(ManfredLocationDB manfredLocation);

    @Query("SELECT * FROM last_places")
    LiveData<List<ManfredLocationDB>>getAllPlaces();

    @Query("SELECT * FROM last_places WHERE is_from = 1")
    LiveData<List<ManfredLocationDB>>getStartPlaces();

    @Query("SELECT * FROM last_places WHERE is_from = 0")
    LiveData<List<ManfredLocationDB>>getFinishPlaces();

    @Query("SELECT * FROM last_places WHERE is_from = 1")
    LiveData<List<ManfredLocationDB>>getStartPlacesSync();

    @Query("SELECT * FROM last_places WHERE is_from = 0")
    LiveData<List<ManfredLocationDB>>getFinishPlacesSync();

    /*
    @Query("SELECT * FROM last_places WHERE address LIKE :placeName")
    List<ManfredLocation>getPlaceLike(String placeName);*/
}
