package manfred.ru.manfredpassenger.ride.viewmodels;

public class ButtonAddCardItem implements CardType{
    @Override
    public ItemType getType() {
        return ItemType.ADD_CARD;
    }
}
