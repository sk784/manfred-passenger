package manfred.ru.manfredpassenger.ride.models;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.Nullable;

public class PreCalculateCostRequest {
    @SerializedName("tariff_id")
    private final String tariffId;

    @SerializedName("latitude_from")
    private final double latitudeFrom;

    @SerializedName("longitude_from")
    private final double longitudeFrom;

    @SerializedName("latitude_to")
    private final double latitudeTo;

    @SerializedName("longitude_to")
    private final double longitudeTo;

    @SerializedName("need_assistant")
    private final boolean isNeedAssistant;

    @SerializedName("promocode_id")
    @Nullable private final Integer promocodeId;

    @Nullable public Integer getPromocodeId() {
        return promocodeId;
    }

    public PreCalculateCostRequest(String tariffId, double latitudeFrom, double longitudeFrom, double latitudeTo,
                                   double longitudeTo, boolean isNeedAssistant, @Nullable Integer promocodeId) {
        this.tariffId = tariffId;
        this.latitudeFrom = latitudeFrom;
        this.longitudeFrom = longitudeFrom;
        this.latitudeTo = latitudeTo;
        this.longitudeTo = longitudeTo;
        this.isNeedAssistant = isNeedAssistant;
        this.promocodeId = promocodeId;
    }

    public String getTariffId() {
        return tariffId;
    }

    public double getLatitudeFrom() {
        return latitudeFrom;
    }

    public double getLongitudeFrom() {
        return longitudeFrom;
    }

    public double getLatitudeTo() {
        return latitudeTo;
    }

    public double getLongitudeTo() {
        return longitudeTo;
    }

    public boolean isNeedAssistant() {
        return isNeedAssistant;
    }
}
