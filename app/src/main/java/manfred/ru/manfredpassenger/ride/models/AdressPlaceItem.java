package manfred.ru.manfredpassenger.ride.models;

import manfred.ru.manfredpassenger.api.services.models.googleMaps.geocode.Location;

public class AdressPlaceItem implements PlaceItem {
    private final String street;
    private final String city;
    private final String placeId;

    public AdressPlaceItem(String street, String city, String placeId) {
        this.street = street;
        this.city = city;
        this.placeId = placeId;
    }

    @Override
    public PredictType getType() {
        return PredictType.PREDICTION_LOCATION;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getPlaceId() {
        return placeId;
    }
}
