package manfred.ru.manfredpassenger.ride.models;

import com.google.gson.annotations.SerializedName;

public class CarType {

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    @SerializedName("brand")
    private String brand;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }
}