package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.support.annotation.Nullable;

import java.util.List;

import manfred.ru.manfredpassenger.common.models.PushMessage;

public interface PushMessageManager {
	interface PushMessageCallback{
		void pushMessageReceived(@Nullable PushMessage pushMessage);
	}
	interface UnreadPushMessagesCallback{
		void pushMessagesReceived(@Nullable List<PushMessage> pushMessages);
	}

	void getPushMessage(int id, PushMessageCallback callback);
	void getUnreadMessages(UnreadPushMessagesCallback callback);

	void setMessageRead(int id);
}
