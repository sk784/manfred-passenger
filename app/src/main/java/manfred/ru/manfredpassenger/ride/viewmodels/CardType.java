package manfred.ru.manfredpassenger.ride.viewmodels;

public interface CardType {
    public enum ItemType{
        CREDIT_CARD,
        ADD_CARD
    }

    ItemType getType();
}
