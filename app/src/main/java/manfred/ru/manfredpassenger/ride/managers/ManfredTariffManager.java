package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.models.Tariffs;
import manfred.ru.manfredpassenger.ride.repositories.OrderRepository;
import manfred.ru.manfredpassenger.ride.repositories.SocketOrderRepository;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffDetailVM;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;
import manfred.ru.manfredpassenger.utils.SingleLiveEvent;

public class ManfredTariffManager implements TariffManager {
    private static final String TAG = "ManfredTariffManager";
    private final MutableLiveData<List<TariffViewModel>> tariffs;
    private final MutableLiveData<TariffViewModel> selectedTariff;
    private final MutableLiveData<TariffDetailVM> selectedTariffDetails;
    private final SingleLiveEvent<ManfredNetworkError> errorStatus;
    private final OrderRepository orderRepository;
    private List<TariffDetailVM> detailedTariffs = new ArrayList<>();
    private final AppStatusManager appStatusManager;
    private final FreeCarsManager freeCarsManager;
    private long lastUpdatedStamp;

    public ManfredTariffManager(ManfredTokenManager manfredTokenManager, AppStatusManager appStatusManager,
                                FreeCarsManager freeCarsManager, NetworkManagerProvider networkManager) {
        this.freeCarsManager = freeCarsManager;
        this.tariffs = new MutableLiveData<>();
        this.selectedTariff = new MutableLiveData<>();
        this.selectedTariffDetails = new MutableLiveData<>();
        this.orderRepository = new SocketOrderRepository(manfredTokenManager, networkManager);
        this.errorStatus = new SingleLiveEvent<>();
        this.appStatusManager = appStatusManager;
        initTariffs();
    }

    private void initTariffs() {
        Log.d(TAG, "initTariffs: ");
        setTariffs(orderRepository, false);
    }

    @Override
    public void selectTariff(int id) {
        List<TariffViewModel> tariffViewModels = tariffs.getValue();
        setTariffSelected(tariffViewModels, id);
    }

    @Override
    public void needAssistant() {
        Log.d(TAG, "needAssistant: ");
        setTariffs(orderRepository, true);
        appStatusManager.makingPreOrder();
    }

    @Override
    public void refreshTariffs() {
        Log.d(TAG, "refreshTariffs: ");
        orderRepository.clearCache();
        selectedTariff.postValue(null);
        setTariffs(orderRepository, false);
    }

    @Override
    public void clearTariffs() {
        Log.d(TAG, "clearTariffs: ");
        orderRepository.clearCache();
        selectedTariff.postValue(null);
        tariffs.postValue(new ArrayList<>());
        detailedTariffs.clear();
    }

    private void setTariffs(OrderRepository orderRepository, boolean isNeedAssistante) {
        Log.d(TAG, "setTariffs: ");
        orderRepository.getTariffs(new NetworkResponseCallback<Tariffs>() {
            @Override
            public void allOk(Tariffs response) {
                //Log.d(TAG, "getTariffs allOk: tariffs getted "+response.getTariffs().size());
                List<TariffViewModel>tariffsVM = tariffsToWM(response.getTariffs(), isNeedAssistante);
                //Log.d(TAG, "getTariffs allOk: tariff setted to  "+tariffsVM.size());
                detailedTariffs.clear();
                detailedTariffs.addAll(tariffsToDetailedVM(response.getTariffs(), isNeedAssistante));
                lastUpdatedStamp=(new Date().getTime())/1000;
                //Log.d(TAG, "getTariffs allOk: detailed tariffs is " + detailedTariffs.toString());
                if(!tariffsVM.isEmpty()) {
                    Log.d(TAG, "getTariffs allOk: init selection");
                    if(selectedTariff.getValue() != null){
                        setTariffSelected(tariffsVM, selectedTariff.getValue().getId());
                    }else {
                        setTariffSelected(tariffsVM, tariffsVM.get(0).getId());
                    }
                }else {
                    Log.d(TAG, "allOk: set tariffs");
                    tariffs.postValue(tariffsVM);
                }
                /*
                if (selectedTariff.getValue() != null) {
                    setTariffsSelectedDetail(selectedTariff.getValue().getId());
                }*/
                //tariffs.postValue(tariffsVM);
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "setTariffs error: " + error.toString());
                errorStatus.postValue(error);
            }
        });
    }

    private void setTariffSelected(List<TariffViewModel> tariffViewModels, int id) {
        //Log.d(TAG, "setTariffSelected: "+id);
        setTariffsSelectedDetail(id);
        if (tariffViewModels != null) {
            for (TariffViewModel tariffViewModel : tariffViewModels) {
                //Log.d(TAG, "setTariffSelected: test "+tariffViewModel);
                if (tariffViewModel.getId() == id) {
                    selectedTariff.setValue(tariffViewModel);
                    if (tariffViewModel.isSelected()) {
                        //Log.d(TAG, "setTariffSelected: selected now");
                        needShowDetails(id);
                    } else {
                        //Log.d(TAG, "setTariffSelected: selecting tariff " + tariffViewModel.getId());
                        tariffViewModel.setSelected(true);
                    }
                } else {
                    tariffViewModel.setSelected(false);
                }
            }
            //Log.d(TAG, "setTariffSelected: post new tariffs");
            tariffs.postValue(tariffViewModels);
        }
        if (selectedTariff.getValue()!=null) {
            freeCarsManager.tariffChanged(selectedTariff.getValue().getTariff());
        }
    }

    private void needShowDetails(int id) {
        Log.d(TAG, "needShowDetails: ");
        appStatusManager.showTariffDetails();
    }

    private void setTariffsSelectedDetail(int id) {
        //Log.d(TAG, "setTariffsSelectedDetail: searching for "+id+" in "+detailedTariffs.toString());
        for (TariffDetailVM detailVM : detailedTariffs) {
            //Log.d(TAG, "setTariffsSelectedDetail: checking detailVM id "+detailVM.getId());
            if (detailVM.getId() == id) {
                //Log.d(TAG, "setTariffsSelectedDetail: "+id);
                selectedTariffDetails.setValue(detailVM);
            }
        }
    }

    private List<TariffDetailVM> tariffsToDetailedVM(List<Tariff> tariffs, boolean isNeedAssistante) {
        List<TariffDetailVM> tariffDetailVMs = new ArrayList<>();
        for (Tariff tariff : tariffs) {
            tariffDetailVMs.add(new TariffDetailVM(tariff, isNeedAssistante));
        }
        //Log.d(TAG, "tariffsToDetailedVM: " + tariffDetailVMs.toString());
        return tariffDetailVMs;
    }

    private List<TariffViewModel> tariffsToWM(List<Tariff> tariffs, boolean isNeedAssistante) {
        List<TariffViewModel> tariffViewModels = new ArrayList<>();
        for (Tariff tariff : tariffs) {
            tariffViewModels.add(new TariffViewModel(tariff, isNeedAssistante));
        }
        //Log.d(TAG, "tariffsToWM: " + tariffViewModels.size());
        return tariffViewModels;
    }


    @Override
    public LiveData<List<TariffViewModel>> getTariffs() {
        return tariffs;
    }

    @Override
    public LiveData<TariffViewModel> getSelectedTariff() {
        return selectedTariff;
    }

    @Override
    public List<TariffDetailVM> getDetailedTariffs() {
        return detailedTariffs;
    }

    @Override
    public LiveData<TariffDetailVM> getSelectedTariffDetails() {
        return selectedTariffDetails;
    }

    @Override
    public void notNeedAssistant() {
        Log.d(TAG, "notNeedAssistant: ");
        setTariffs(orderRepository, false);
    }

    public MutableLiveData<ManfredNetworkError> getErrorStatus() {
        return errorStatus;
    }

    public long getLastUpdatedStamp() {
        return lastUpdatedStamp;
    }
}
