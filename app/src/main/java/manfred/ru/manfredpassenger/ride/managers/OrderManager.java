package manfred.ru.manfredpassenger.ride.managers;

import android.location.Location;

import java.util.Date;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.services.models.event.Event;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.ride.models.ManfredDTOLocation;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public interface OrderManager {
    public class OrderError{
        private final String caption;
        private final String text;
        private final ManfredNetworkError.ErrorType errorType;

        public OrderError(String caption, String text) {
            this.caption = caption;
            this.text = text;
            errorType = ManfredNetworkError.ErrorType.OTHER;
        }

        public OrderError(ManfredNetworkError networkError, String caption) {
            this.caption = caption;
            this.errorType = networkError.getType();
            this.text = networkError.getText();
        }

        public String getCaption() {
            return caption;
        }

        public String getText() {
            return text;
        }

        public ManfredNetworkError.ErrorType getType() {
            return errorType;
        }
    }
    void changeMyLocation(Location location);
    void changeDestinationLocation(Location location);
    void makeOrder();
    void makePreOrder(Date orderTime);
    void showCommentAdding();
    void addComment(String string);
    void setFinishLocation(ManfredLocation manfredDTOLocation);
    void cancelCurrentState();
    void driverDelay(OrderVM orderVM);
    void orderRefresh(OrderVM orderVM);
    void noFreeDrivers(OrderVM orderVM);
    void voteRide(boolean isTripGood);
    void showTariffDetails();
    void currTripPaymentError(Event event);
    void timeSelected(Date date);
    //void showPickingCards();
    void orderCanceledPrePaymentError(int orderId, String errorText);

    //it is for restore status
    void carRideToPassenger(OrderVM orderVM);
    void searchingCar(OrderVM orderVM);
    void carOnPassenger(OrderVM orderVM);
    void riding(OrderVM orderVM);
    void completed(OrderVM orderVM);

    void reOrder();


}
