package manfred.ru.manfredpassenger.ride.repositories;

import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.NewOrderResponse;
import manfred.ru.manfredpassenger.api.services.models.PreOrderListResponse;
import manfred.ru.manfredpassenger.api.services.models.TariffChangedResponce;
import manfred.ru.manfredpassenger.api.services.models.VoteOrderQuery;
import manfred.ru.manfredpassenger.api.services.models.event.EventsResponse;
import manfred.ru.manfredpassenger.ride.models.NewOrder;
import manfred.ru.manfredpassenger.ride.models.PreCalculateCostRequest;
import manfred.ru.manfredpassenger.ride.models.PreCalculatedCostDTO;
import manfred.ru.manfredpassenger.ride.models.Tariffs;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public interface OrderRepository {
    void makeOrder(NewOrder newOrder, NetworkResponseCallback<NewOrderResponse> responseNetworkResponseCallback);

    void getTariffs(NetworkResponseCallback<Tariffs> tariffs);

    void cancelOrder(NewOrderResponse currOrder, NetworkResponseCallback responseCallback);

    void getEvents(NetworkResponseCallback<EventsResponse> responseCallback);

    void voteRide(VoteOrderQuery query, NetworkResponseCallback<ManfredResponse> responseCallback);

    void getPreOrders(NetworkResponseCallback<PreOrderListResponse> responseNetworkResponseCallback);

    void tryRepay(int orderId, NetworkResponseCallback responseCallback);

    void getLastOrderWithDebt(NetworkResponseCallback<OrderVM> responseNetworkResponseCallback);

    void getPrecalculatedCost(PreCalculateCostRequest preCalculateRequest, NetworkResponseCallback<PreCalculatedCostDTO>responseCallback);

    void checkForTariffChanged(NetworkResponseCallback<TariffChangedResponce>responseCallback, long timestamp);

    void clearCache();
}
