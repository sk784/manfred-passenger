package manfred.ru.manfredpassenger.ride.viewmodels;

import android.databinding.ObservableField;

import java.util.Date;


import manfred.ru.manfredpassenger.utils.CostUtils;

public class PayErrorWM {
    private ObservableField<String> minCountdown;
    private final String titleText;
    private final String errorText;

    public PayErrorWM(float debtSum, String currencySymbol, String cardNumberLastFourDigits, Date startTime) {
        this.minCountdown = new ObservableField<String>();
        this.titleText = "Проблема  \nс оплатой — "+ CostUtils.removeAfterDotsIfNeed(String.valueOf(debtSum))+" "+currencySymbol;
        this.errorText = "Не удалось списать средства с карты \n" +
                cardNumberLastFourDigits+". Выберите карту для оплаты.";


    }

    public ObservableField<String> getMinCountdown() {
        return minCountdown;
    }

    public String getTitleText() {
        return titleText;
    }

    public String getErrorText() {
        return errorText;
    }

}
