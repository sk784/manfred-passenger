package manfred.ru.manfredpassenger.ride.fragments.errors;


import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.ride.SelectCityActivity;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class NeedGeoFragment extends Fragment {
    private static final String TAG = "NeedGeoFragment";
    private static final int MY_PERMISSIONS_REQUEST_GEO = 400;
    private ViewGroup.LayoutParams prevParentParams;
    OrderViewModel model;

    public NeedGeoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        View rootView = inflater.inflate(R.layout.fragment_need_geo, container, false);
        model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);

        rootView.setOnClickListener(null);
        Button skip = rootView.findViewById(R.id.btn_geo_skip);
        Button toPreferences = rootView.findViewById(R.id.btn_geo_preferences);

        skip.setOnClickListener(v -> {
            model.skipPermission();
            Intent intent = new Intent(getActivity(), SelectCityActivity.class);
            startActivity(intent);
        });
        toPreferences.setOnClickListener(v -> openLocationSettings());
        return rootView;
    }

    private void openLocationSettings() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Log.d(TAG, "openLocationSettings: shouldShowRequestPermissionRationale");
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_GEO);
            } else {
                Log.d(TAG, "openLocationSettings: No explanation needed; request the permission");
                // No explanation needed; request the permission
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_GEO);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            Log.d(TAG, "openLocationSettings: ");
            // Permission has already been granted
        }

        /*Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getActivity().getPackageName()));
        startActivity(appSettingsIntent);*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult: ");
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_GEO: {
                if (grantResults.length > 0) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        // Denied
                        Log.d(TAG, "onRequestPermissionsResult: Denied");
                        model.skipPermission();
                    } else {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            Log.d(TAG, "onRequestPermissionsResult: ");
                            model.skipPermission();
                        } else {
                            // Bob never checked click
                            Log.d(TAG, "onRequestPermissionsResult: never checked click");
                            Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.parse("package:" + getActivity().getPackageName()));
                            startActivity(appSettingsIntent);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getView()!=null) {
            ViewGroup container = ((ViewGroup) getView().getParent());
            container.setLayoutParams(prevParentParams);
        }
    }

}
