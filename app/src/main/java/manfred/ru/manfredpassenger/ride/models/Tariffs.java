package manfred.ru.manfredpassenger.ride.models;

import java.util.List;

import manfred.ru.manfredpassenger.api.services.models.event.Tariff;

public class Tariffs {
    private final List<Tariff> tariffs;

    public Tariffs(List<Tariff> tariffs) {
        this.tariffs = tariffs;
    }

    public List<Tariff> getTariffs() {
        return tariffs;
    }
}
