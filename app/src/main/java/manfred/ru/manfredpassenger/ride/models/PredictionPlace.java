package manfred.ru.manfredpassenger.ride.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


public class PredictionPlace {
    private final String street;
    private final String city;
    private final String placeId;

    public PredictionPlace(String street, String city, String placeId) {
        this.street = street;
        this.city = city;
        this.placeId = placeId;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getPlaceId() {
        return placeId;
    }
}
