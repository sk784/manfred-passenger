package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffDetailVM;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;

public interface TariffManager {
    LiveData<List<TariffViewModel>> getTariffs();
    LiveData<TariffViewModel> getSelectedTariff();
    List<TariffDetailVM> getDetailedTariffs();
    LiveData<TariffDetailVM> getSelectedTariffDetails();
    void notNeedAssistant();
    void selectTariff(int id);
    void needAssistant();
    void refreshTariffs();
    MutableLiveData<ManfredNetworkError> getErrorStatus();

    void clearTariffs();
}
