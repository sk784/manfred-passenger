package manfred.ru.manfredpassenger.ride.viewmodels;

import java.util.Objects;

public class HamburgerState {
    public enum HambState{
        HAMBURGER,
        BACK,
        CLOSE
    }
    private final HambState state;

    public HamburgerState(HambState state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HamburgerState that = (HamburgerState) o;
        return state == that.state;
    }

    @Override
    public int hashCode() {

        return Objects.hash(state);
    }

    public HambState getState() {
        return state;
    }
}
