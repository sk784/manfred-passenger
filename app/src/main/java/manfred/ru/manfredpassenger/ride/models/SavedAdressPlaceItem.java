package manfred.ru.manfredpassenger.ride.models;

public class SavedAdressPlaceItem implements PlaceItem {
    private final ManfredLocation manfredLocation;

    public SavedAdressPlaceItem(ManfredLocation manfredLocation) {
        this.manfredLocation = manfredLocation;
    }

    @Override
    public PredictType getType() {
        return PredictType.SAVED_LOCATION;
    }

    public ManfredLocation getManfredLocation() {
        return manfredLocation;
    }
}
