package manfred.ru.manfredpassenger.ride;

import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import manfred.ru.manfredpassenger.BR;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.databinding.ActivitySelectCityBinding;
import manfred.ru.manfredpassenger.databinding.CityItemHolderBinding;
import manfred.ru.manfredpassenger.ride.viewmodels.CityCardItem;

import static manfred.ru.manfredpassenger.utils.Constants.CITY_SELECTED;

public class SelectCityActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.animator.enter_from_right,R.animator.exit_to_left);
        ActivitySelectCityBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_select_city);

        setSupportActionBar(binding.selectCityToolbar);
        final ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //setting black background for status bar
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.black_10));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.onSaveInstanceState();
        binding.rvSelectCity.setLayoutManager(mLayoutManager);

        ObservableList<CityCardItem>regions = new ObservableArrayList<>();

        RegionManager regionManager = ((ManfredPassengerApplication)getApplication()).getRegionManager();

        ItemType<CityItemHolderBinding> cityItemBindingItemType = new ItemType<CityItemHolderBinding>(R.layout.city_item_holder){
            @Override
            public void onCreate(@NotNull Holder<CityItemHolderBinding> holder) {
                super.onCreate(holder);
                holder.getBinding().getRoot().setOnClickListener(v -> {
                    binding.rvSelectCity.setVisibility(View.GONE);
                    binding.pbSelectCity.setVisibility(View.VISIBLE);
                    regionManager.setRegionAndZoomToCenter(holder.getBinding().getCity().getId(), new RegionManager.WorkDoneCallback() {
                        @Override
                        public void done() {
                            setResult(CITY_SELECTED);
                            finish();
                        }

                        @Override
                        public void error(String text) {
                            Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
                            binding.rvSelectCity.setVisibility(View.VISIBLE);
                            binding.pbSelectCity.setVisibility(View.GONE);
                        }
                    });
                });
            }
        };

        LastAdapter lastAdapter = new LastAdapter(regions,BR.city)
                .map(CityCardItem.class, cityItemBindingItemType)
                .into(binding.rvSelectCity);

        regionManager.getRegions().observe(this, new Observer<List<Region>>() {
            @Override
            public void onChanged(@Nullable List<Region> mangedRegions) {
                if(mangedRegions==null)return;
                for (Region region : mangedRegions){
                    regions.add(new CityCardItem(region.getName(),region.getCountry(),region.getId()));
                }
                lastAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
