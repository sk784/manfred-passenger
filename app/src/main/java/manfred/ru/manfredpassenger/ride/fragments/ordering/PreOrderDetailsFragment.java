package manfred.ru.manfredpassenger.ride.fragments.ordering;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import io.intercom.android.sdk.Intercom;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.databinding.FragmentPreOrderDetailsBinding;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreOrderDetailsFragment extends Fragment {
    private static final String TAG = "PreOrderDetailsFragment";
    private String supportPhone;

    public PreOrderDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentPreOrderDetailsBinding binding = FragmentPreOrderDetailsBinding.inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        init(binding);
        RegionManager regionManager = ((ManfredPassengerApplication)getActivity().getApplication()).getRegionManager();
        supportPhone = Objects.requireNonNull(regionManager.getCurrentRegion().getValue()).getSupportPhone().toString();
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    private void init(FragmentPreOrderDetailsBinding binding){
        binding.getRoot().setOnClickListener(v -> {});
        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        model.getSelectedForDetailsPreOrder().observe(this, new Observer<OrderVM>() {
            @Override
            public void onChanged(@Nullable OrderVM preOrder) {
                if(preOrder!=null) {
                    binding.setPreOrder(preOrder);
                    TariffViewModel tariffViewModel = new TariffViewModel(preOrder.getTariff(), preOrder.isNeedAssistant());
                    binding.setTariff(tariffViewModel);
                    binding.cancelPreorder.setOnClickListener(v -> buildCancelPreOrderDialog(model,preOrder).show());
                }
            }
        });

        binding.getRoot().setOnClickListener(view -> hideContactMenu(binding));

        binding.preorderCall.setOnClickListener(v->showContactMenu(binding));

        View.OnClickListener selectCard = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.selectActiveCard();
            }
        };

        binding.dialDriverMenu.setOnClickListener(null);
        binding.ivOrderCardIcon.setOnClickListener(selectCard);
        binding.tvOrderCardNumber.setOnClickListener(selectCard);
    }

    private AlertDialog buildCancelPreOrderDialog(OrderViewModel model, OrderVM preOrder){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogCustom);
        builder.setTitle(R.string.cancel_pre_order_title);
        builder.setPositiveButton(R.string.yes_string, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                model.cancelPreOrder(preOrder.getId());
            }
        });
        builder.setNegativeButton(R.string.no_string, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        return builder.create();
    }
    private void showContactMenu(FragmentPreOrderDetailsBinding binding){
        binding.dialDriverMenu.setVisibility(View.VISIBLE);
        binding.tvCallDiver.setOnClickListener(view -> makeCallDriver());
        binding.tvWriteSupport.setOnClickListener(view -> Intercom.client().displayMessenger());
    }

    private void hideContactMenu(FragmentPreOrderDetailsBinding binding){
        binding.dialDriverMenu.setVisibility(View.GONE);
    }

    private void makeCallDriver() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", supportPhone, null));
        startActivity(intent);
    }

}
