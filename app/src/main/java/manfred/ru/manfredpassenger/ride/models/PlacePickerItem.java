package manfred.ru.manfredpassenger.ride.models;

public class PlacePickerItem implements PlaceItem {
    @Override
    public PredictType getType() {
        return PredictType.PLACE_PICKER;
    }
}
