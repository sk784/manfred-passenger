package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public interface PreOrderManager {
    void preOrderTimeOut(OrderVM order);
    LiveData<List<OrderVM>> getPreOrders();
    LiveData<OrderVM> getLastPreOrder();
    MutableLiveData<String> getError();
    void refreshPreOrders();
    void selectPreOrderForDetails(int preOrderId);
    LiveData<OrderVM> getDetailedPreOrder();
}
