package manfred.ru.manfredpassenger.ride.fragments.tariff_details;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Objects;

import manfred.ru.manfredpassenger.databinding.FragmentTariffContentDetailBinding;
import manfred.ru.manfredpassenger.ride.TariffDescriptionActivity;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffDetailVM;

/**
 * A simple {@link Fragment} subclass.
 */
public class TariffContentDetailFragment extends Fragment {
    static final String TARIFF_ID = "tariff_id";

    public TariffContentDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentTariffContentDetailBinding binding =
                FragmentTariffContentDetailBinding.inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        if (getArguments() != null) {
            init(binding,getArguments().getInt(TARIFF_ID));
        }
        return binding.getRoot();
    }

    static TariffContentDetailFragment newInstance(int tariffId) {
        TariffContentDetailFragment pageFragment = new TariffContentDetailFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(TARIFF_ID, tariffId);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    private void init(FragmentTariffContentDetailBinding binding,int tariffId) {
        binding.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TariffDescriptionActivity.class);
                startActivity(intent);
            }
        });

        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        List<TariffDetailVM>tariffsDetails = model.getDetailedTariff();
        for (TariffDetailVM tariffDetailVM : tariffsDetails){
            if(tariffDetailVM.getId()==tariffId) {
                binding.setTariffDetails(tariffDetailVM);
            }
        }
    }

}
