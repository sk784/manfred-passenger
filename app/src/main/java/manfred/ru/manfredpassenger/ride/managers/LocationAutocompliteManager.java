package manfred.ru.manfredpassenger.ride.managers;


import android.location.Location;

public interface LocationAutocompliteManager {
    void searchStringStart(String string, Location centerOfSearchLocation);
    void searchStringFinish(String string, Location centerOfSearchLocation);
}
