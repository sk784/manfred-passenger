package manfred.ru.manfredpassenger.ride.managers;

import manfred.ru.manfredpassenger.api.managers.NetworkManager;
import manfred.ru.manfredpassenger.api.token.TokenManager;
import manfred.ru.manfredpassenger.common.managers.appstatus.AppStatusManager;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;

public class ManfredTimeToDriverManagerBuilder {
    private AppStatusManager appStatusManager;
    private TokenManager tokenManager;
    private RegionManager regionManager;
    private TariffManager tariffManager;
    private NetworkManagerProvider networkManager;

    public ManfredTimeToDriverManagerBuilder setAppStatusManager(AppStatusManager appStatusManager) {
        this.appStatusManager = appStatusManager;
        return this;
    }

    public ManfredTimeToDriverManagerBuilder setTokenManager(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
        return this;
    }

    public ManfredTimeToDriverManagerBuilder setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
        return this;
    }

    public ManfredTimeToDriverManagerBuilder setTariffManager(TariffManager tariffManager) {
        this.tariffManager = tariffManager;
        return this;
    }

    public ManfredTimeToDriverManagerBuilder setNetworkManager(NetworkManagerProvider networkManager) {
        this.networkManager = networkManager;
        return this;
    }

    public ManfredTimeToDriverManager createManfredTimeToDriverManager() {
        return new ManfredTimeToDriverManager(appStatusManager, tokenManager, regionManager, tariffManager, networkManager);
    }
}