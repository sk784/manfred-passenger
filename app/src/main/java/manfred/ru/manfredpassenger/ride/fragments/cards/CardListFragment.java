package manfred.ru.manfredpassenger.ride.fragments.cards;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import manfred.ru.manfredpassenger.BR;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.cards.PaymentsActivity;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.managers.CreditCardManager;
import manfred.ru.manfredpassenger.cards.models.CreditCardVM;
import manfred.ru.manfredpassenger.common.models.PaymentSystem;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.databinding.AddCardHolderBinding;
import manfred.ru.manfredpassenger.databinding.FragmentCardListBinding;
import manfred.ru.manfredpassenger.databinding.SelecatbleCreditCardItemBinding;
import manfred.ru.manfredpassenger.ride.viewmodels.ButtonAddCardItem;
import manfred.ru.manfredpassenger.ride.viewmodels.CardType;
import manfred.ru.manfredpassenger.ride.viewmodels.CreditCardItem;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.utils.CardsDiffUtilCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class CardListFragment extends Fragment {
    private static final String TAG = "CardListFragment";
    private ViewGroup.LayoutParams prevParentParams;

    public CardListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        prevParentParams = container.getLayoutParams();
        container.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT));
        FragmentCardListBinding binding = FragmentCardListBinding.inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentCardListBinding binding) {
        List<CardType> creditCards = new ArrayList<>();
        //binding.getRoot().setOnClickListener(v -> hideCardMenu(binding));
        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        ItemType<SelecatbleCreditCardItemBinding> cardHolderBindingItemType = new ItemType<SelecatbleCreditCardItemBinding>(R.layout.selecatble_credit_card_item) {
            @Override
            public void onCreate(@NotNull Holder<SelecatbleCreditCardItemBinding> holder) {
                super.onCreate(holder);
                holder.getBinding().getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showCardMenu(holder.getBinding().getCreditCard().getCreditCard(), binding);
                    }
                });
            }
        };

        ItemType<AddCardHolderBinding> addCard = new ItemType<AddCardHolderBinding>(R.layout.add_card_holder){
            @Override
            public void onCreate(Holder<AddCardHolderBinding> holder) {
                super.onCreate(holder);
                holder.getBinding().btnAddCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), PaymentsActivity.class);
                        startActivity(intent);
                        Log.d(TAG, "onClick: ");
                    }
                });
            }
        };


        LastAdapter adapter = new LastAdapter(creditCards, BR.creditCard)
                .map(CreditCardItem.class,cardHolderBindingItemType)
                .map(ButtonAddCardItem.class,addCard)
                .into(binding.cardListRecycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.onSaveInstanceState();
        binding.cardListRecycler.setLayoutManager(mLayoutManager);

        ((ManfredPassengerApplication)getActivity().getApplication()).getManfredCreditCardManager().cards().observe(this, new Observer<List<CreditCard>>() {
            @Override
            public void onChanged(@Nullable List<CreditCard> cards) {
                if(cards!=null){
                    Log.d(TAG, "onChanged: card size is "+cards.size());
                }else {
                    Log.d(TAG, "onChanged: cards is null");
                }
                if(cards!=null) {
                    List<CardType> newCreditCards = new ArrayList<>();
                    for (CreditCard creditCard : cards) {
                        Log.d(TAG, "onChanged: card is "+creditCard.getCardNumber());
                        newCreditCards.add(new CreditCardItem(creditCard,
                                ((ManfredPassengerApplication)getActivity().getApplication()).getRegionManager().getCurrentPaymentSystem()));
                    }
                    newCreditCards.add(new ButtonAddCardItem());
                    CardsDiffUtilCallback cardsDiffUtilCallback = new CardsDiffUtilCallback(creditCards, newCreditCards);
                    DiffUtil.DiffResult cardsResult = DiffUtil.calculateDiff(cardsDiffUtilCallback);
                    cardsResult.dispatchUpdatesTo(adapter);
                    adapter.notifyDataSetChanged();
                    creditCards.clear();
                    creditCards.addAll(newCreditCards);
                    Log.d(TAG, "onChanged: new credits cards is "+newCreditCards.size());
                }
            }
        });

        ((ManfredPassengerApplication)getActivity().getApplication()).getRegionManager()
                .getCurrentRegion().observe(this, new Observer<Region>() {
            @Override
            public void onChanged(@Nullable Region region) {
                if(region!=null) {
                    boolean noCardInRegion = isNoCardsInRegion(region.getPaymentSystem(),
                            ((ManfredPassengerApplication) getActivity().getApplication()).getManfredCreditCardManager());
                    if(noCardInRegion){
                        binding.tvCardUnavailableCaption.setVisibility(View.VISIBLE);
                        binding.tvCardUnavailebleText.setVisibility(View.VISIBLE);
                        binding.tvCardUnavailebleText.setText("Чтобы совершать поездки в "+region.getName()+" добавьте вашу карту заново.");
                    }else {
                        binding.tvCardUnavailableCaption.setVisibility(View.GONE);
                        binding.tvCardUnavailebleText.setVisibility(View.GONE);
                    }
                }
            }
        });

        binding.btnCloseComment.setOnClickListener(v -> model.cancel());

        binding.cardListRecycler.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN
                        && binding.cardListRecycler.findChildViewUnder(event.getX(), event.getY()) == null)
                {
                    // Touch outside items here, you do whatever you want
                    hideCardMenu(binding);
                }
                return false;
            }
        });
    }

    private void showCardMenu(CreditCardVM creditCardVM, FragmentCardListBinding binding){
        binding.selectCardMenu.setVisibility(View.VISIBLE);
        binding.tvCardCaption.setText(creditCardVM.getCardNumberForMenu());
        if(creditCardVM.isSelected()) {
            binding.tvSelectedCardCaption.setTextColor(getResources().getColor(R.color.gray_text_for_menu));
            Drawable img = getContext().getResources().getDrawable( R.drawable.ic_check_gray_24dp );
            binding.tvSelectedCardCaption.setCompoundDrawablesWithIntrinsicBounds(img,null,null,null);
            binding.tvSelectedCardCaption.setText("Выбрана для оплаты");
            binding.tvSelectedCardCaption.setOnClickListener(null);
        }else {
            Drawable img = getContext().getResources().getDrawable( R.drawable.ic_check_black_24dp );
            binding.tvSelectedCardCaption.setCompoundDrawablesWithIntrinsicBounds(img,null,null,null);
            binding.tvSelectedCardCaption.setTextColor(getResources().getColor(R.color.black));
            if(creditCardVM.isAvailable()) {
                binding.tvSelectedCardCaption.setText("Выбрать для оплаты");
                binding.tvSelectedCardCaption.setOnClickListener(v -> selectCard(creditCardVM.getId(), binding));
            }else {
                binding.tvSelectedCardCaption.setText("Недоступна для оплаты");
                binding.tvSelectedCardCaption.setOnClickListener(null);
            }
        }
        binding.tvDeleteCard.setOnClickListener(v -> deleteCard(creditCardVM,binding));

    }

    private void hideCardMenu(FragmentCardListBinding binding){
        binding.selectCardMenu.setVisibility(View.GONE);
    }

    private void deleteCard(CreditCardVM creditCardVM, FragmentCardListBinding binding){
        //Log.d(TAG, "delete card onClick: "+cardId);
        AlertDialog.Builder deleteCardDialog = new AlertDialog.Builder(getActivity(),R.style.AlertDialogCustom);
        deleteCardDialog.setTitle("Удалить карту?");
        deleteCardDialog.setMessage("Карту придется добавлять заново");
        deleteCardDialog.setNegativeButton("Не удалять", (dialog, which) -> dialog.dismiss());
        deleteCardDialog.setPositiveButton("удалить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((ManfredPassengerApplication)getActivity().getApplication())
                        .getManfredCreditCardManager().deleteCard(creditCardVM);
                hideCardMenu(binding);
            }
        });
        deleteCardDialog.show();
    }

    private void selectCard(int cardId, FragmentCardListBinding binding){
        ((ManfredPassengerApplication)getActivity().getApplication())
                .getManfredCreditCardManager().selectCard(cardId);
        hideCardMenu(binding);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup container = ((ViewGroup)getView().getParent());
        container.setLayoutParams(prevParentParams);
    }

    private boolean isNoCardsInRegion(PaymentSystem paymentSystem, CreditCardManager creditCardManager){
        //isAvailable = currentPaymentSystem.getId() == creditCard.getPaymentSystem().getId();
        List<CreditCard>creditCards = creditCardManager.cards().getValue();
        if(creditCards==null || creditCards.isEmpty())return false;
        if(paymentSystem==null)return false;
        for (CreditCard creditCard : creditCards){
            if(creditCard.getPaymentSystem().getId()==paymentSystem.getId()){
                return false;
            }
        }
        return true;
    }

}
