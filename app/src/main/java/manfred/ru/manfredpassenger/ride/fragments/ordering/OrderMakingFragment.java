package manfred.ru.manfredpassenger.ride.fragments.ordering;


import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;


import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import manfred.ru.manfredpassenger.BR;
import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.cards.PaymentsActivity;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.managers.ManfredCreditCardManager;
import manfred.ru.manfredpassenger.cards.models.CreditCardVM;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.common.models.PaymentSystem;
import manfred.ru.manfredpassenger.databinding.FragmentOrderMakingBinding;
import manfred.ru.manfredpassenger.databinding.TariffItemBinding;
import manfred.ru.manfredpassenger.databinding.TariffItemOrderMakingBinding;
import manfred.ru.manfredpassenger.promocodes.PromoCodeActivity;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;
import manfred.ru.manfredpassenger.ride.viewmodels.TariffViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderMakingFragment extends Fragment implements LifecycleObserver, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "OrderMakingFragment";
    private boolean isHaveActiveCard;
    LinearLayoutManager mLayoutManager;
    OrderViewModel model;
    FragmentOrderMakingBinding binding;
    boolean isAssistantChecked;


    public interface OnSearchClicker {
        void searchFrom();
        void searchDestination();
    }

    public OrderMakingFragment() {
        // Required empty public constructor
    }

    private OnSearchClicker onSearchClicker;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentOrderMakingBinding.inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        init(binding);
        isAssistantChecked = false;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        return binding.getRoot();
    }

    private void init(FragmentOrderMakingBinding binding) {
        model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);

        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager.onSaveInstanceState();
        binding.tariffRecycle.setLayoutManager(mLayoutManager);

        List<TariffViewModel> tariffViewModels = new ArrayList<>();
        ItemType<TariffItemOrderMakingBinding> tariffItemType = new ItemType<TariffItemOrderMakingBinding>(R.layout.tariff_item_order_making) {
            @Override
            public void onCreate(@NonNull Holder<TariffItemOrderMakingBinding> holder) {
                super.onCreate(holder);
                holder.itemView.setOnClickListener(v -> model.selectTariffAndShowMore(holder.getBinding().getTariff().getId()));
            }
        };

        LastAdapter adapter = new LastAdapter(tariffViewModels, BR.tariff)
                .map(TariffViewModel.class, tariffItemType)
                .into(binding.tariffRecycle);


        model.getTariffs().observe(this, tariffs -> {
            if (tariffs != null) {
                Log.d(TAG, "getTariff onChanged: ");
                tariffViewModels.clear();
                tariffViewModels.addAll(tariffs);
                adapter.notifyDataSetChanged();

            }
        });

        model.getSelectedTariff().observe(this, tariffViewModel -> {
            Log.d(TAG, "getSelectedTariff onChanged: ");
            int selectedPosition = tariffViewModels.indexOf(tariffViewModel);
            if(selectedPosition>-1) {
                mLayoutManager.scrollToPosition(selectedPosition);
            }

            if (tariffViewModel != null) {
                binding.setTariff(tariffViewModel);
                initNeedAssistantVisibility(tariffViewModel.getTariff().isAssistantAllowed(),binding,model);
                if(model.getOrderTime().getValue()!=null){
                    binding.tvTime.setText(DateFormat.format("dd MMMM, HH:mm", model.getOrderTime().getValue().getTime()));
                } else {
                    binding.tvTime.setText(DateFormat.format("dd MMMM, HH:mm", Calendar.getInstance().getTime().getTime() + model.getSelectedTariff().getValue().getMinPreOrderTimeMin()*60000));
                }

//                if(model.getIsNeedAssistante().getValue()){
//                    model.needAssistant();
//                    Log.d("sk7847",String.valueOf(model.getIsNeedAssistante().getValue()));
//                } else if (!model.getIsNeedAssistante().getValue()) {
//                    Log.d("sk7847",String.valueOf(model.getIsNeedAssistante().getValue()));

     //           }

            }else {
                Log.d(TAG, "getSelectedTariff onChanged: is null");
            }

//            if(binding.btnRemoveAssistante.getVisibility()==View.VISIBLE){
//                binding.makeOrder.setVisibility(View.GONE);
//                binding.timeHolder.setVisibility(View.GONE);
//            }
        });


        model.getIsCancelledDateDialog().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean == null) return;
                if (aBoolean) {
                    binding.timeSwitch.setChecked(false);
                }
            }
        });

        model.getPreCalculatedCost().observe(this, s -> {
            Log.d(TAG, "getPreCalculatedCost onChanged: "+s);
            if(s!=null && !s.isEmpty()){
                if(s.size() == tariffViewModels.size()){
                    for (int i=0; i< tariffViewModels.size();i++){
                        tariffViewModels.get(i).setCostOfRide(s.get(i));
                        tariffViewModels.get(i).setCalculated(true);
                    }
                }
            }else {
                for (int i=0; i< tariffViewModels.size();i++){
                    tariffViewModels.get(i).setCalculated(false);
                }
            }
            adapter.notifyDataSetChanged();
        });

        model.getStartLocation().observe(this, location -> {
            if (location != null) {
                binding.startPlace.setText(location.getAddress());
            }
        });

        model.getFinishLocation().observe(this, location -> {
            if (location != null) {
                binding.finishPlace.setText(location.getAddress());
                binding.finishPlace.setTextColor(getResources().getColor(R.color.white));
            }
        });

        model.getIsNeedAssistante().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean == null) return;
                if (aBoolean) {
                    binding.btnAddAssistante.setVisibility(View.GONE);
                    binding.btnRemoveAssistante.setVisibility(View.VISIBLE);
                    isAssistantChecked = true;
                } else {
                    binding.btnAddAssistante.setVisibility(View.VISIBLE);
                    binding.btnRemoveAssistante.setVisibility(View.GONE);
                    isAssistantChecked = false;
                }
            }
        });

        model.getStatus().observe(this, rideStatus -> {
            if (rideStatus != null) {
                Log.d(TAG, "init: status is "+rideStatus);
                switch (rideStatus){
                    case MAKING_ORDER:
                        if(isAssistantChecked){
                            binding.btnSelectTime.setVisibility(View.VISIBLE);
                            binding.btnMakePreorder.setVisibility(View.GONE);
                            binding.makeOrder.setVisibility(View.GONE);
                            binding.btnAddCard.setVisibility(View.GONE);
                            binding.timeHolder.setVisibility(View.GONE);
                        } else {
                            binding.btnSelectTime.setVisibility(View.GONE);
                            binding.btnMakePreorder.setVisibility(View.GONE);
                            binding.makeOrder.setVisibility(View.VISIBLE);
                            binding.btnAddCard.setVisibility(View.GONE);
                            binding.timeHolder.setVisibility(View.VISIBLE);
                        }
                        break;
                    case SELECTING_TIME_FOR_PREORDER:
                        //binding.btnSelectTime.setEnabled(false);
                        break;
                    case MAKING_PRE_ORDER:
                        binding.btnSelectTime.setVisibility(View.VISIBLE);
                        binding.btnMakePreorder.setVisibility(View.GONE);
                        binding.makeOrder.setVisibility(View.GONE);
                     //   binding.makePreorder.setVisibility(View.GONE);
                        binding.btnAddCard.setVisibility(View.GONE);
                        binding.timeHolder.setVisibility(View.GONE);
                    //    initTime(model,binding);
                        break;
                }
                if(model.getOrderTime().getValue()!=null){
                    Log.d(TAG, "initTime: "+model.getOrderTime().getValue());
                    binding.btnSelectTime.setVisibility(View.GONE);
                    binding.btnMakePreorder.setVisibility(View.VISIBLE);
                    //binding.llOrderButtons.setVisibility(View.GONE);
                    binding.makeOrder.setVisibility(View.GONE);
                 //   binding.makePreorder.setVisibility(View.GONE);
                 //   binding.timeSwitch.setVisibility(View.GONE);
                    binding.timeHolder.setVisibility(View.VISIBLE);
                    binding.timeSwitch.setVisibility(View.GONE);

               //         binding.tvTime.setText(DateFormat.format("dd MMMM, HH:mm", model.getOrderTime().getValue().getTime()));

                } else {
              //     binding.tvTime.setText(DateFormat.format("dd MMMM, HH:mm", Calendar.getInstance().getTime().getTime() + model.getSelectedTariff().getValue().getMinPreOrderTimeMin()*60000));
                }
                ManfredPassengerApplication application = (ManfredPassengerApplication)getActivity().getApplication();
                if(!isCardAvailable(application.getRegionManager(),application.getManfredCreditCardManager())){
                    needCardForOrder(binding);
                }
            }
        });

     //   binding.timeHolder.setVisibility(View.GONE);
        model.getOrderTime().observe(this, date -> {
            if(date!=null){
                binding.btnSelectTime.setVisibility(View.GONE);
                binding.btnMakePreorder.setVisibility(View.VISIBLE);
                //binding.llOrderButtons.setVisibility(View.GONE);
                binding.timeHolder.setVisibility(View.VISIBLE);
                binding.makeOrder.setVisibility(View.GONE);
           //     binding.makePreorder.setVisibility(View.GONE);
                binding.tvTime.setText(DateFormat.format("dd MMMM, HH:mm", date.getTime()));
                binding.tvTime.setTextColor(getResources().getColor(R.color.white));
                binding.ivTimeWhite.setVisibility(View.VISIBLE);
                binding.ivTimeTransparent.setVisibility(View.GONE);
                binding.timeHolder.setOnClickListener(v -> model.selectTimeForPreOrder());
            }else {
                Log.d(TAG, "init: not have time");
                binding.btnMakePreorder.setVisibility(View.GONE);
                if(binding.btnSelectTime.getVisibility()==View.GONE){
                    binding.makeOrder.setVisibility(View.VISIBLE);
                }
                binding.ivTimeWhite.setVisibility(View.GONE);
                binding.ivTimeTransparent.setVisibility(View.VISIBLE);
         //       binding.timeHolder.setVisibility(View.GONE);
                binding.tvTime.setText(DateFormat.format("dd MMMM, HH:mm", Calendar.getInstance().getTime().getTime() + model.getSelectedTariff().getValue().getMinPreOrderTimeMin()*60000));
            }
        });

        binding.btnMakePreorder.setOnClickListener(v -> {
            if(isHaveActiveCard) {
                binding.btnMakePreorder.setEnabled(false);
                model.makeOrder();
            }else {
                model.selectActiveCard();
            }
        });

        binding.startPlaceHolder.setOnClickListener(v -> onSearchClicker.searchFrom());

        binding.finishPlaceHolder.setOnClickListener(v -> onSearchClicker.searchDestination());

        binding.btnSelectTime.setOnClickListener(v -> {
            model.selectTimeForPreOrder();
            //binding.btnSelectTime.setEnabled(false);
        });

//        binding.makePreorder.setOnClickListener(v -> {
//            model.selectTimeForPreOrder();
//            binding.makePreorder.setEnabled(false);
//            binding.makePreorder.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    binding.makePreorder.setEnabled(true);
//                }
//            },100);
//        });

            binding.timeSwitch.setOnCheckedChangeListener(this);



        binding.btnUsePromocode.setOnClickListener(v->{
            Intent promoIntent = new Intent(getActivity(), PromoCodeActivity.class);
            startActivity(promoIntent);
        });

        ((ManfredPassengerApplication)getActivity().getApplication())
                .getPromoCodeManager().getSelectedPromoCode().observe(this, promoCodeVM -> {
                    if(promoCodeVM!=null){
                        binding.btnUsePromocode.setText(promoCodeVM.getPromoCodeCaption());
                    }else {
                        binding.btnUsePromocode.setText("Промокод");
                    }
                });

        binding.makeOrder.setOnClickListener(v -> {
            Log.d(TAG, "onClick: makeOrder");
            if(isHaveActiveCard) {
                model.makeOrder();
            }else {
                model.selectActiveCard();
            }
        });

        binding.commentField.setOnClickListener(v -> {
            Log.d(TAG, "commentField click ");
            model.showAddComment();
        });

        model.getCurrentComment().observe(this, string -> {
            if(string!=null && !string.isEmpty()){
                binding.commentField.setText(string);
            }else {
                binding.commentField.setText("Комментарий");
                binding.commentField.setGravity(Gravity.CENTER);
            }
        });

        binding.btnAddAssistante.setOnClickListener(v ->
                model.needAssistant()
        );

        binding.btnRemoveAssistante.setOnClickListener(v ->
                model.notNeedAssistant()
        );

    //    binding.ivCancelAssist.setOnClickListener(v -> model.notNeedAssistant());

        ManfredCreditCardManager creditCardManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredCreditCardManager();
        creditCardManager.getSelectedCreditCard().observe(this, creditCard -> {
            if (creditCard != null) {
                binding.setCreditCard(new CreditCardVM(creditCard,null));
                isHaveActiveCard=true;
            }else {
                isHaveActiveCard=false;
            }
        });


        View.OnClickListener cardClick = v -> model.selectActiveCard();

        binding.llCard.setOnClickListener(cardClick);

        binding.btnAddCard.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), PaymentsActivity.class);
            startActivity(intent);
        });

        binding.tvOrderingInfo.setOnClickListener(v -> {
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.hint_assistante, null);
            ((ViewGroup)getActivity().getWindow().getDecorView()).addView(view);

            ImageView closeImage = view.findViewById(R.id.iv_cancel_assist);
            Button acceptBtn = view.findViewById(R.id.btn_select_assistance);
            Button cancelBtn = view.findViewById(R.id.btn_cancel);

            acceptBtn.setOnClickListener(v1 -> {
                model.needAssistant();
                binding.btnAddAssistante.setVisibility(View.GONE);
                binding.btnRemoveAssistante.setVisibility(View.VISIBLE);
                ((ViewGroup)getActivity().getWindow().getDecorView()).removeView(view);
            });

            closeImage.setOnClickListener(v12 -> ((ViewGroup)getActivity().getWindow().getDecorView()).removeView(view));

            cancelBtn.setOnClickListener(v13 -> ((ViewGroup)getActivity().getWindow().getDecorView()).removeView(view));
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            model.selectTimeForPreOrder();

        } else {
            model.clearDate();
            binding.tvTime.setTextColor(getResources().getColor(R.color.half_transparent));
        }
    }

    private void initNeedAssistantVisibility(boolean assistantAllowed, FragmentOrderMakingBinding binding, OrderViewModel model) {
        if(assistantAllowed){
//            model.getIsNeedAssistante().observe(OrderMakingFragment.this, isNeedAssistante -> {
//                if(isNeedAssistante==null)return;
//                if(isNeedAssistante) {
//                    binding.vCaptionUndeline.setVisibility(View.VISIBLE);
//                    binding.tvWithAssistCaption.setVisibility(View.VISIBLE);
//                    binding.ivAssistante.setVisibility(View.GONE);
//                    binding.ivCancelAssist.setVisibility(View.VISIBLE);
//                    binding.tvOrderingInfo.setVisibility(View.VISIBLE);
//                    final View parent = (View) binding.tvOrderingInfo.getParent();  // button: the view you want to enlarge hit area
//                    parent.post( new Runnable() {
//                        public void run() {
//                            final Rect rect = new Rect();
//                            binding.tvOrderingInfo.getHitRect(rect);
//                            rect.top -= 50;    // increase top hit area
//                            rect.left -= 50;   // increase left hit area
//                            rect.bottom += 50; // increase bottom hit area
//                            rect.right += 50;  // increase right hit area
//                            parent.setTouchDelegate( new TouchDelegate( rect , binding.tvOrderingInfo));
//                        }
//                    });
//                }else {
//                    binding.tvOrderingInfo.setVisibility(View.GONE);
//                    binding.ivAssistante.setVisibility(View.VISIBLE);
//                    binding.vCaptionUndeline.setVisibility(View.GONE);
//                    binding.tvWithAssistCaption.setVisibility(View.GONE);
//                    binding.ivCancelAssist.setVisibility(View.GONE);
//                }

            binding.llCaptionAndAssistant.setVisibility(View.VISIBLE);
        }else {
              binding.llCaptionAndAssistant.setVisibility(View.GONE);
//            binding.ivAssistante.setVisibility(View.GONE);
//            binding.tvOrderingInfo.setVisibility(View.GONE);
//            binding.vCaptionUndeline.setVisibility(View.GONE);
//            binding.tvWithAssistCaption.setVisibility(View.GONE);
//            binding.ivCancelAssist.setVisibility(View.GONE);
        }
    }

    private void initTime(OrderViewModel model, FragmentOrderMakingBinding binding){
        Log.d(TAG, "initTime: ");
        if(model.getOrderTime().getValue()!=null){
            Log.d(TAG, "initTime: "+model.getOrderTime().getValue());
            binding.timeHolder.setVisibility(View.VISIBLE);
            String dateOfPreOrder = DateFormat.format("dd MMMM, HH:mm", model.getOrderTime().getValue()).toString();
            Log.d(TAG, "initTime: formatted date is "+dateOfPreOrder);
            binding.tvTime.setText(dateOfPreOrder);
        }else {
            binding.timeHolder.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSearchClicker) {
            onSearchClicker = (OnSearchClicker) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSearchClicker");
        }
    }

    private boolean isCardAvailable(RegionManager regionManager, ManfredCreditCardManager creditCardManager){
        if(creditCardManager.isHaveCards()){
            PaymentSystem paymentSystem = regionManager.getCurrentPaymentSystem();
            List<CreditCard> cards = creditCardManager.cards().getValue();
            if(cards!=null && paymentSystem!=null) {
                for (CreditCard creditCard :cards){
                    if(creditCard.getPaymentSystem().getId()==paymentSystem.getId())return true;
                }
            }
        }
        return false;
    }

    private void needCardForOrder(FragmentOrderMakingBinding binding){
        binding.btnSelectTime.setVisibility(View.GONE);
        //binding.btnSelectTime.setEnabled(true);
        binding.btnMakePreorder.setVisibility(View.GONE);
        binding.makeOrder.setVisibility(View.GONE);
   //     binding.makePreorder.setVisibility(View.GONE);
        binding.btnAddCard.setVisibility(View.VISIBLE);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    void resume() {
        List<TariffViewModel> allTariffs = model.getTariffs().getValue();
        TariffViewModel currTariff = model.getSelectedTariff().getValue();
        if(allTariffs!=null) {
            int selectedPosition = allTariffs.indexOf(currTariff);
            if (selectedPosition > -1) {
                mLayoutManager.scrollToPosition(selectedPosition);
            }
        }

    }

}
