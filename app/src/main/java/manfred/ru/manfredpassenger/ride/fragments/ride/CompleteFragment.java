package manfred.ru.manfredpassenger.ride.fragments.ride;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Objects;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.databinding.FragmentCompleteBinding;
import manfred.ru.manfredpassenger.ride.managers.OrderManager;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;
import manfred.ru.manfredpassenger.ride.viewmodels.OrderViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class CompleteFragment extends Fragment {


    public CompleteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentCompleteBinding binding = FragmentCompleteBinding.inflate(Objects.requireNonNull(getActivity()).getLayoutInflater());
        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentCompleteBinding binding) {
        OrderViewModel model = ViewModelProviders.of(getActivity()).get(OrderViewModel.class);
        OrderVM orderVM = model.getCurrentRide().getValue();
        if(orderVM!=null) {
            binding.price.setText(orderVM.getTotalCost());
            binding.tvDiscountDescr.setText(orderVM.getDiscountCaption());
            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(getContext());
            Bundle params = new Bundle();
            params.putString("order_price", (orderVM.getTotalCost()));
            analytics.logEvent("order_completed", params);
        }

        binding.badTrip.setText(getEmojiByUnicode(0x1F641));
        binding.goodTrip.setText(getEmojiByUnicode(0x1F44D));
        OrderManager orderManager = ((ManfredPassengerApplication)getActivity().getApplication()).getManfredOrderManager();
        binding.badTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderManager.voteRide(false);
            }
        });
        binding.goodTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderManager.voteRide(true);
            }
        });

        model.getCurrentRide().observe(this, new Observer<OrderVM>() {
            @Override
            public void onChanged(@Nullable OrderVM orderVM) {
                if(orderVM!=null){
                    binding.setCreditCard(orderVM.getCreditCardVM());
                }
            }
        });
    }

    public String getEmojiByUnicode(int unicode){
        return new String(Character.toChars(unicode));
    }


}
