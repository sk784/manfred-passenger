package manfred.ru.manfredpassenger.ride.viewmodels;

import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.util.Log;

import java.util.Date;

import manfred.ru.manfredpassenger.BuildConfig;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.services.models.event.CarType;
import manfred.ru.manfredpassenger.api.services.models.event.Driver;
import manfred.ru.manfredpassenger.api.services.models.event.Event;
import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.models.CreditCardVM;
import manfred.ru.manfredpassenger.promocodes.models.PromoCode;
import manfred.ru.manfredpassenger.ride.models.CarVM;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.utils.CostUtils;

public class OrderVM {
    private static final String TAG = "OrderVM";
    private final int id;
    private final String carType;
    private final String carModel;
    private final String carNumber;
    private final String carNumberRegion;
    private final String driverName;
    private final String driverAvatarUrl;
    @IdRes private final int carIcon;
    private final String driverPhoneNumber;
    private final String carColor;
    @Nullable
    private final Long orderStartTime;
    private final Integer freeCancelTimeInMin;
    private final Float cancelPrice;
    @Nullable
    private final CarVM driverCar;
    @Nullable
    private final ManfredLocation driverLocation;
    private final ManfredLocation startLocation;
    @Nullable
    private final ManfredLocation destinationLocation;
    @Nullable
    private final ManfredLocation realFinishLocation;
    private final String totalCost;
    private final String currencySymbol;
    private final String carNeedTime;
    @Nullable private final Date carNeedTimeDate;
    private final String comment;
    private final String rideStartTime;
    private final boolean needAssistant;
    private final Tariff tariff;
    private final String startRideTimeHHMM;
    private final int driverDelay;

    @Nullable
    public Date getCarNeedTimeDate() {
        return carNeedTimeDate;
    }

    private final String finishRideTimeHHMM;
    private final String ratingSymbol;
    private final String maskerCardNumber;
    @Nullable private final String discountCaption;
    @Nullable private final PromoCode promoCode;

    private final boolean cashPayment;

    @IdRes private int cardPic;

    private CreditCard card;

    @Override
    public String toString() {
        return "OrderVM{" +
                "id=" + id +
                ", carType='" + carType + '\'' +
                ", carModel='" + carModel + '\'' +
                ", carNumber='" + carNumber + '\'' +
                ", carNumberRegion='" + carNumberRegion + '\'' +
                ", driverName='" + driverName + '\'' +
                ", driverAvatarUrl='" + driverAvatarUrl + '\'' +
                ", carIcon=" + carIcon +
                ", driverPhoneNumber='" + driverPhoneNumber + '\'' +
                ", carColor='" + carColor + '\'' +
                ", orderStartTime=" + orderStartTime +
                ", freeCancelTimeInMin=" + freeCancelTimeInMin +
                ", cancelPrice=" + cancelPrice +
                ", driverCar=" + driverCar +
                ", driverLocation=" + driverLocation +
                ", startLocation=" + startLocation +
                ", destinationLocation=" + destinationLocation +
                ", realFinishLocation=" + realFinishLocation +
                ", totalCost='" + totalCost + '\'' +
                ", carNeedTime='" + carNeedTime + '\'' +
                ", comment='" + comment + '\'' +
                ", rideStartTime='" + rideStartTime + '\'' +
                ", needAssistant=" + needAssistant +
                ", tariff=" + tariff +
                ", startRideTimeHHMM='" + startRideTimeHHMM + '\'' +
                ", finishRideTimeHHMM='" + finishRideTimeHHMM + '\'' +
                ", ratingSymbol='" + ratingSymbol + '\'' +
                ", maskerCardNumber='" + maskerCardNumber + '\'' +
                ", creditCardVM=" + creditCardVM +
                ", debitSize='" + debitSize + '\'' +
                ", timeToCustomerInSec=" + timeToCustomerInSec +
                '}';
    }

    private final CreditCardVM creditCardVM;
    private final String debitSize;

    public String getRideStartTime() {
        return rideStartTime;
    }

    /**

     * it is initial value, current must be calculated
     */
    private final int timeToCustomerInSec;
    @Nullable
    public ManfredLocation getRealFinishLocation() {
        return realFinishLocation;
    }

    public boolean isNeedAssistant() {
        return needAssistant;
    }

    @Nullable
    public Long getOrderStartTime() {
        return orderStartTime;
    }

    public String getStartRideTimeHHMM() {
        return startRideTimeHHMM;
    }

    public String getFinishRideTimeHHMM() {
        return finishRideTimeHHMM;
    }

    public String getRatingSymbol() {
        return ratingSymbol;
    }

    public String getMaskerCardNumber() {
        return maskerCardNumber;
    }

    public String getShortMaskedNumber(){
        if(isCashPayment()) {
            return "НАЛИЧНЫЕ";
        }
        if(maskerCardNumber.isEmpty())return "unknown card";
        String last4Chars = this.maskerCardNumber.substring(this.maskerCardNumber.length()-4,this.maskerCardNumber.length());
        return "**** "+last4Chars;
    }

    public String getDebitSize() {
        return debitSize;
    }

    @Nullable
    public String getDiscountCaption() {
        return discountCaption;
    }

    private final Event event;

    public Event getEvent() {
        return event;
    }

    public OrderVM(Event event) {
        //Log.d(TAG, "OrderVM: creating OrderVM "+event.getId());
        Driver driver = event.getDriver();
        this.event=event;
        if(driver!=null) {
            CarType carType = driver.getCarTypeObj();
            if (carType != null) {
                this.carType = driver.getCarTypeObj().getName();
                this.carModel = driver.getCarTypeObj().getBrand();
            } else {
                this.carType = "unknown";
                this.carModel = "unknown";
            }
            String fullNumber = driver.getCarNumber();
            if(fullNumber!=null) {
                this.carNumber = getNumber(fullNumber, getRegionOfCarNumber(fullNumber));
                this.carNumberRegion = getRegionOfCarNumber(fullNumber);
            }else {
                this.carNumber="";
                this.carNumberRegion="";
            }
            this.driverName = driver.getDriverName();
            this.driverAvatarUrl = removeLastChar(BuildConfig.BASE_URL, 4) + driver.getImage();
            this.driverPhoneNumber = driver.getDriverPhone();
            this.carColor = driver.getCarColor();
        }else {
            this.carType = "unknown";
            this.carModel = "unknown";
            this.driverPhoneNumber = "unknown";
            this.carColor = "unknown";
            this.carNumber="";
            this.carNumberRegion="";
            this.driverName="";
            this.driverAvatarUrl = "";
        }

        int carImage=R.drawable.maybach;
        if(event.getTariff()!=null && event.getTariff().getIconMapName()!=null) {
            switch (event.getTariff().getIconMapName()) {
                case "maybach":
                    carImage = R.drawable.maybach;
                    break;
                case "eclass":
                    carImage = R.drawable.eclass;
                    break;
                case "rolls":
                    carImage = R.drawable.rolls;
                    break;
            }
        }
        this.carIcon =carImage;

        this.freeCancelTimeInMin = event.getFreeCancelTime();
        this.cancelPrice = Float.parseFloat(event.getCancelRidePrice());
        this.timeToCustomerInSec = event.getTimeToCostumer();
        //ManfredDTOLocation currCarLocation = new ManfredDTOLocation("",driver.getLatitude(),"",driver.getLongitude());
        //this.driverCar = new CarVM(currCarLocation.toLocation(),driver.getpr);
        if(driver.getLatitude()!=null) {
            this.driverLocation = new ManfredLocation(Double.valueOf(driver.getLatitude()), Double.valueOf(driver.getLongitude()), "", "");
        }else {
            this.driverLocation = null;
        }
        this.driverCar=null;

        //Log.d(TAG, "OrderVM: location is "+event.getLocation().toString());
        this.startLocation=event.getLocation().toManfredLocation();
        this.destinationLocation=event.getDestination().toManfredLocation();
        this.realFinishLocation=event.getFinalLocation().toManfredLocation();


        this.id = event.getId();

        if(event.getCarNeedTime()!=null & !event.getCarNeedTime().isEmpty() ) {
/*            Log.d(TAG, "OrderVM: id is "+getId());
            Log.d(TAG, "OrderVM: need time is "+event.getCarNeedTime());*/
            Date preOrderNeedTime = new Date(Long.parseLong(event.getCarNeedTime()+"000"));
            if(event.getPreorderStartTime()!=null && !event.getPreorderStartTime().isEmpty()) {
                this.orderStartTime = Long.parseLong(event.getPreorderStartTime());
            }else {
                this.orderStartTime=0L;
            }
            //Log.d(TAG, "OrderVM: date parsed "+preOrderNeedTime.toString());
            this.carNeedTime = DateFormat.format("dd MMMM, HH:mm", preOrderNeedTime.getTime()).toString();
            this.carNeedTimeDate=preOrderNeedTime;
            //Log.d(TAG, "OrderVM: carNeedTime formatted to "+this.carNeedTime);
        }else {
            this.carNeedTimeDate=null;
            if(!event.getAcceptOrderTime().equals("")) {
                this.orderStartTime = Long.parseLong(event.getAcceptOrderTime());
            }else {
                this.orderStartTime=0L;
            }
            this.carNeedTime = "";
        }

        this.tariff=event.getTariff();
        this.currencySymbol = tariff != null ? tariff.getCurrencySymbol() : "\u20BD";

        if(event.getStatus().equals("CANCELLED")){
            this.totalCost="отмена - "+CostUtils.removeAfterDotsIfNeed(event.getTotalCost()) + "  " + currencySymbol;
            this.rideStartTime = DateFormat.format("dd MMMM, HH:mm", new Date(event.getCreated()*1000)).toString();
            this.startRideTimeHHMM = DateFormat.format("HH:mm", new Date(event.getCreated()*1000)).toString();
        }else {
            this.totalCost=CostUtils.removeAfterDotsIfNeed(event.getTotalCost()) + "  " + currencySymbol;
            if (event.getRideStartTime() != null & !event.getRideStartTime().isEmpty()) {

                //Log.d(TAG, "OrderVM: start time is "+event.getCarNeedTime());
                Date startTime = new Date(Long.parseLong(event.getRideStartTime() + "000"));
                this.rideStartTime = DateFormat.format("dd MMMM, HH:mm", startTime.getTime()).toString();
                this.startRideTimeHHMM = DateFormat.format("HH:mm", startTime.getTime()).toString();
                //Log.d(TAG, "OrderVM: RideStartTime parsed to "+this.carNeedTime);
            } else {
                this.rideStartTime = "";
                this.startRideTimeHHMM = "";
            }
        }

        if(event.getRideFinishTime()!=null & !event.getRideFinishTime().isEmpty()){
            Date endTime = new Date(Long.parseLong(event.getRideFinishTime()+"000"));
            this.finishRideTimeHHMM = DateFormat.format("HH:mm", endTime.getTime()).toString();
        }else {
            this.finishRideTimeHHMM = "";

        }

        if(event.getComments()==null || event.getComments().equals("")){
            this.comment=null;
        }else {
            this.comment = event.getComments();
        }

        this.needAssistant=event.isNeedAssistant();

        Log.d(TAG, "OrderVM: "+id+" vote is "+event.getPassengerVote());
        if(event.getPassengerVote() == 5){
            this.ratingSymbol="\uD83D\uDC4D️";
        } else if(event.getPassengerVote() == 1) {
            this.ratingSymbol="☹";
        } else {
            this.ratingSymbol="";
        }

        card = event.getCard();
        if(card != null) {
            this.maskerCardNumber = event.getCard().getCardNumber();
            this.creditCardVM=new CreditCardVM(event.getCard(),null);
            switch (card.getCardType()) {
                case CreditCard.CARD_TYPE_VISA:
                    cardPic = R.drawable.ic_visa;
                    break;
                case CreditCard.CARD_TYPE_MASTERCARD:
                    cardPic = R.drawable.ic_card_master_32_white;
                    break;
                case CreditCard.CARD_TYPE_CASH:
                    cardPic = R.drawable.ic_payment_cash;
                    break;
                default:
                    cardPic = R.drawable.ic_card_unknown_32_white;
            }
        }else {
            this.maskerCardNumber = "";
            this.creditCardVM = null;
			cardPic = R.drawable.ic_card_unknown_32_white;
        }

        String currency;
        if(event.getTariff()!=null) {
            currency =event.getTariff().getCurrencySymbol();
        }else {
            currency = "n/a";
        }
        debitSize = CostUtils.removeAfterDotsIfNeed(String.valueOf(event.getDebt())) +" "+currency;
        this.promoCode = event.getPromoCode();

        if(promoCode!=null && event.getTotalCostWithoutPromocode()!=null && !event.getTotalCostWithoutPromocode().isEmpty()) {
            String discount;
            float totalCost = Float.parseFloat(event.getTotalCostWithoutPromocode());
            if(promoCode.getType().equals("SUM")){
                discount = "Скидка "+promoCode.getDiscountSum()+promoCode.getRegion().getPaymentSystem().getCurrencySymbol()+". "
                        +totalCost+"-"+promoCode.getDiscountSum()+"="+event.getTotalCost();
            }else {
                int inProcent = (int) (promoCode.getDiscountPercent() * event.getDebt() / 100.0);
                //int withDiscount = (int)(event.getDebt() - (promoCode.getDiscountPercent() * event.getDebt()) / 100.0);
                discount = "Скидка"+promoCode.getDiscountPercent()+"%. "+totalCost+"-"+inProcent+"="+event.getTotalCost();
            }
            this.discountCaption=discount;
        }else {
            this.discountCaption = null;
        }

        this.driverDelay=event.getDelay()/60;

        this.cashPayment = event.isCashPayment();

        cardPic = event.isCashPayment() ? R.drawable.ic_payment_cash : R.drawable.ic_visa;
    }

    public int getDriverDelayInMin() {
        return driverDelay;
    }

    @Nullable
    public PromoCode getPromoCode() {
        return promoCode;
    }

    public CreditCardVM getCreditCardVM() {
        return creditCardVM;
    }

    private String getRegionOfCarNumber(String fullNumber) {
        if(fullNumber==null)return "";
        String rawRegion = fullNumber.substring(fullNumber.length() - 3);
        return extractDigits(rawRegion);
    }

    public Tariff getTariff() {
        return tariff;
    }

    private String getNumber(String fullNumber, String region) {
        String numberWithoutRegion = fullNumber.substring(0, fullNumber.length()-region.length());
        return numberWithoutRegion;

    }

    private String removeLastChar(String str, int size) {
        return str.substring(0, str.length() - size);
    }

    private String extractDigits(String src) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < src.length(); i++) {
            char c = src.charAt(i);
            if (Character.isDigit(c)) {
                builder.append(c);
            }
        }
        String digits=builder.toString();
        return builder.toString();
    }

    public String getCarType() {
        return carType;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getCarNumberRegion() {
        return carNumberRegion;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getDriverAvatarUrl() {
        return driverAvatarUrl;
    }

    public String getDriverPhoneNumber() {
        return driverPhoneNumber;
    }

    public String getCarColor() {
        return carColor;
    }

    public boolean isCancellChargeable() {
        if (orderStartTime == null) {
            return false;
        }

        long freeCancelTime = orderStartTime + (freeCancelTimeInMin * 60);
        long currTime = new Date().getTime() / 1000;
        return (currTime > freeCancelTime);
    }

    public float getCancelPrice() {
        return cancelPrice;
    }

    public int getId() {
        return id;
    }

    /**
     * @return planned time to customer in min for current moment
     */
    public int getTimeToCustomer(){
        if(orderStartTime==null)return 0;
        long currTime = new Date().getTime() / 1000;
        long tripTime = currTime - orderStartTime;
        long planedTimeToCustomer = timeToCustomerInSec-tripTime;
        int calculatedTimeToCustomerInMin = (int) (planedTimeToCustomer/60);
        Log.d(TAG, "getTimeToCustomer: calculatedTimeToCustomerInMin "+calculatedTimeToCustomerInMin);
        if(calculatedTimeToCustomerInMin<2){
            Log.d(TAG, "getTimeToCustomer: time so small, set to two minutes");
            calculatedTimeToCustomerInMin=2;
        }
        return calculatedTimeToCustomerInMin;
    }

    public int getCarIcon() {
        return carIcon;
    }

    public String getTotalCost() {
        return CostUtils.removeAfterDotsIfNeed(totalCost);
    }

    @Nullable
    public ManfredLocation getDriverLocation() {
        return driverLocation;
    }

    public ManfredLocation getStartLocation() {
        //Log.d(TAG, "getStartLocation: "+startLocation.toString());
        return startLocation;
    }

    @Nullable
    public ManfredLocation getDestinationLocation() {
        return destinationLocation;
    }

    public String getCarNeedTime() {
        return carNeedTime;
    }

    public String getComment() {
        return comment;
    }

    public boolean isCashPayment() {
        return cashPayment;
    }

    public int getCardPic() {
        return cardPic;
    }
}
