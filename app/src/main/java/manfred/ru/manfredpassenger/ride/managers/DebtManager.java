package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;

import manfred.ru.manfredpassenger.ride.viewmodels.OrderVM;

public interface DebtManager {
    LiveData<OrderVM>getLastDebtOrder();
    void refreshDebts();
    void refreshDebtsAndGetReadyIfNoDebts();
}
