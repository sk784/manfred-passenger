package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.support.annotation.Nullable;

import manfred.ru.manfredpassenger.api.services.models.event.Tariff;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;

public interface PreCalculateCostManager {

    interface CostCalculateCallback{
        void calculated(@Nullable String cost);
    }

    void calculateCost(Tariff tariff, ManfredLocation from, ManfredLocation to, boolean isNeedAssistant, Integer PromoCodeId, CostCalculateCallback costCalculateCallback);
}
