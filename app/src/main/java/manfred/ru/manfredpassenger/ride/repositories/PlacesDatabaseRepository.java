package manfred.ru.manfredpassenger.ride.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import manfred.ru.manfredpassenger.ride.database.ManfredPassengerDatabase;
import manfred.ru.manfredpassenger.ride.database.PlacesDao;
import manfred.ru.manfredpassenger.ride.models.ManfredLocation;
import manfred.ru.manfredpassenger.ride.models.ManfredLocationDB;

public class PlacesDatabaseRepository {
    private static final String TAG = "PlacesDatabaseRepo";
    private ManfredPassengerDatabase manfredPassengerDatabase;
    //private MutableLiveData<List<ManfredLocation>> filteredPlaces;
    private List<ManfredLocation>startSavedLocations;
    private List<ManfredLocation>finishSavedLocations;

    public PlacesDatabaseRepository(ManfredPassengerDatabase manfredPassengerDatabase) {
        this.manfredPassengerDatabase = manfredPassengerDatabase;
        //filteredPlaces = new MutableLiveData<>();
        startSavedLocations = new ArrayList<>();
        finishSavedLocations = new ArrayList<>();
        //testDB(manfredPassengerDatabase);
        fillPlaces(manfredPassengerDatabase);
    }

    private void fillPlaces(ManfredPassengerDatabase manfredPassengerDatabase) {
        Log.d(TAG, "fillPlaces: ");
        manfredPassengerDatabase.placesDao().getStartPlaces().observeForever(new Observer<List<ManfredLocationDB>>() {
            @Override
            public void onChanged(@Nullable List<ManfredLocationDB> manfredLocationDBS) {
                if(manfredLocationDBS!=null){
                    Log.d(TAG, "StartPlaces onChanged: "+manfredLocationDBS);
                    startSavedLocations = dbToLocation(manfredLocationDBS);
                }else {
                    startSavedLocations = new ArrayList<>();
                }
            }
        });

        manfredPassengerDatabase.placesDao().getFinishPlaces().observeForever(new Observer<List<ManfredLocationDB>>() {
            @Override
            public void onChanged(@Nullable List<ManfredLocationDB> manfredLocationDBS) {
                if(manfredLocationDBS!=null){
                    finishSavedLocations = dbToLocation(manfredLocationDBS);
                }else {
                    finishSavedLocations = new ArrayList<>();
                }
            }
        });
    }

    private List<ManfredLocation> dbToLocation(List<ManfredLocationDB>locationDBs){
        List<ManfredLocation>locations = new ArrayList<>();
        for (ManfredLocationDB manfredLocationDB : locationDBs){
            locations.add(manfredLocationDB.getManfredLocation());
        }
        return locations;
    }

    private void testDB(ManfredPassengerDatabase manfredPassengerDatabase) {
        Log.d(TAG, "testDB: "+manfredPassengerDatabase.placesDao().getAllPlaces());
    }

    public void insert(ManfredLocation location, boolean isFrom){
        String adress = location.getStreet();
        if(adress==null)return;
        new InsertAsyncTask(manfredPassengerDatabase).execute(new ManfredLocationDB(location, isFrom, adress));
    }

    public List<ManfredLocation> getStartSavedLocations(){
        //Log.d(TAG, "getAllPlaces: "+allSavedLocations);
        return startSavedLocations;
    }

    public List<ManfredLocation>getFinishSavedLocations(){
        return finishSavedLocations;
    }

    public interface FilteredText{
        void filtered(List<ManfredLocation>places);
    }
    public void filterPlace(String address, FilteredText filteredText, boolean isStartLocation){
        List<ManfredLocation>filteredLocations = new ArrayList<>();
        List<ManfredLocation>savedLocations= new ArrayList<>();
        if(isStartLocation){
            savedLocations=startSavedLocations;
        }else {
            Log.d(TAG, "filterPlace: finish locations");
            savedLocations=finishSavedLocations;
        }
        for (ManfredLocation manfredLocation : savedLocations){
            if(manfredLocation.getAddress()!=null) {
                if (manfredLocation.getAddress().toLowerCase().contains(address.toLowerCase())){
                    filteredLocations.add(manfredLocation);
                }
            }
        }
        Log.d(TAG, "filterPlace: "+filteredLocations);
        filteredText.filtered(filteredLocations);
        //filteredPlaces.postValue(filteredLocations);
    }

    private static class InsertAsyncTask extends AsyncTask<ManfredLocationDB, Void, Void> {
        private PlacesDao mAsyncTaskDao;
        InsertAsyncTask(ManfredPassengerDatabase manfredPassengerDatabase) {
            mAsyncTaskDao = manfredPassengerDatabase.placesDao();
        }

        @Override
        protected Void doInBackground(final ManfredLocationDB... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
