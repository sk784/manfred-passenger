package manfred.ru.manfredpassenger.ride.viewmodels;

public class CancelRideDialogVM {
    private final String title;
    private final String text;
    private final String positiveButtonText;
    private final String negativeButtonText;

    public CancelRideDialogVM(String title, String text, String positiveButtonText, String negativeButtonText) {
        this.title = title;
        this.text = text;
        this.positiveButtonText = positiveButtonText;
        this.negativeButtonText = negativeButtonText;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getPositiveButtonText() {
        return positiveButtonText;
    }

    public String getNegativeButtonText() {
        return negativeButtonText;
    }
}
