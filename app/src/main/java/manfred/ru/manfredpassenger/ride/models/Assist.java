package manfred.ru.manfredpassenger.ride.models;

import com.google.gson.annotations.SerializedName;

public class Assist {

    @SerializedName("image")
    private String image;

    @SerializedName("phone")
    private String phone;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    public String getImage() {
        return image;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}