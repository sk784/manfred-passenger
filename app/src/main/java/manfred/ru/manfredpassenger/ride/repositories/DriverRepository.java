package manfred.ru.manfredpassenger.ride.repositories;

import android.support.annotation.Nullable;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.AverageWaitTime;
import manfred.ru.manfredpassenger.api.services.models.CoordForAverageTime;
import manfred.ru.manfredpassenger.api.services.models.DriverLocationResponse;
import manfred.ru.manfredpassenger.common.models.Region;
import manfred.ru.manfredpassenger.ride.models.CarsAnswer;

public interface DriverRepository {
    interface AverageTimeCallback{
        void timeToDriver(int averageWaitTime);
        void error(ManfredNetworkError error);
        void outOfService(@Nullable Integer regionId);
        void tariffNotFound();
    }
    void getAverageTimeToDriver(String token, CoordForAverageTime CoordForAverageTime, AverageTimeCallback averageTimeCallback);
    void getCurrentDriverLocation(String token, NetworkResponseCallback<DriverLocationResponse>driverLocationCallback);
    void getFreeCars(String token, NetworkResponseCallback<CarsAnswer>networkResponseCallback);
}
