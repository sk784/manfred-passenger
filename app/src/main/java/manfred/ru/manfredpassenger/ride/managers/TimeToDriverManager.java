package manfred.ru.manfredpassenger.ride.managers;

import android.arch.lifecycle.LiveData;
import android.location.Location;

import manfred.ru.manfredpassenger.ride.models.AverageTimeToPassenger;

public interface TimeToDriverManager {
    void calculateTimeToDriver(Location location, int tariffId);
    void reCalculateLastState();
    void cancelCalculatingState();
    void clearState();
    LiveData<AverageTimeToPassenger> getAverageTimeToPassenger();
}
