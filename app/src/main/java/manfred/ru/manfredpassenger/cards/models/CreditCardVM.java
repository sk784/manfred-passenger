package manfred.ru.manfredpassenger.cards.models;

import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.util.Log;

import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.common.models.PaymentSystem;

public class CreditCardVM {
    private static final String TAG = "CreditCardVM";
    private static final String CASH = "Наличные";

    @IdRes private final int cardPicBig;
    @IdRes private final int cadrPicSmall;
    @IdRes private final int cadrPicSmallWhite;
    private int cardType;
    private final String cardNumberForMenu;
    private String maskedCardNumber;
    private String lastFourDigits;
    private String lastFourDigitsWithFourStars;
    private final int id;
    private boolean isSelected;
    private boolean isAvailable;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getCardNumberForMenu() {
        return cardNumberForMenu;
    }

    public CreditCardVM(CreditCard creditCard, @Nullable PaymentSystem currentPaymentSystem) {
        //Log.d(TAG, "CreditCardVM: creating "+creditCard.getId());
        if(currentPaymentSystem==null){
            //Log.d(TAG, "CreditCardVM: currentPaymentSystem is null");
            isAvailable=false;
        }else {
            //Log.d(TAG, "CreditCardVM: curr paymentSystem is "+currentPaymentSystem+", card system is "+creditCard.getPaymentSystem());
            isAvailable = currentPaymentSystem.getId() == creditCard.getPaymentSystem().getId();
        }
        //Log.d(TAG, "CreditCardVM: availeble? "+isAvailable);
        this.maskedCardNumber=maskCardNumber(creditCard.getCardNumber());
        this.id=creditCard.getId();
        this.lastFourDigits = "* " + getLastFourDigits(creditCard.getCardNumber());
        this.lastFourDigitsWithFourStars = "**** " + getLastFourDigits(creditCard.getCardNumber());
        cardType = creditCard.getCardType();
        if (creditCard.getCardType() == CreditCard.CARD_TYPE_UNKNOWN) {
            this.cardPicBig = R.drawable.ic_card_unknown_48;
            this.cadrPicSmall = R.drawable.ic_card_unknown_32;
            this.cadrPicSmallWhite = R.drawable.ic_card_unknown_32_white;
            this.cardNumberForMenu = "Неизвестна — "+this.lastFourDigitsWithFourStars;
        } else if (creditCard.getCardType() == CreditCard.CARD_TYPE_VISA) {
            this.cardPicBig = R.drawable.ic_card_visa_48;
            this.cadrPicSmall = R.drawable.ic_card_visa_32;
            this.cadrPicSmallWhite = R.drawable.ic_card_visa_32_white;
            this.cardNumberForMenu = "VISA — "+this.lastFourDigitsWithFourStars;
        } else if (creditCard.getCardType() == CreditCard.CARD_TYPE_MASTERCARD) {
            this.cardPicBig = R.drawable.ic_card_master_48;
            this.cadrPicSmall = R.drawable.ic_card_master_32;
            this.cadrPicSmallWhite = R.drawable.ic_card_master_32_white;
            this.cardNumberForMenu = "Master Card — "+this.lastFourDigitsWithFourStars;
        } else if (creditCard.getCardType() == CreditCard.CARD_TYPE_MAESTRO) {
            this.cardPicBig = R.drawable.ic_card_unknown_48;
            this.cadrPicSmall = R.drawable.ic_card_unknown_32;
            this.cadrPicSmallWhite = R.drawable.ic_card_master_32_white;
            this.cardNumberForMenu = "Maestro — "+this.lastFourDigitsWithFourStars;
        } else if (creditCard.getCardType() == CreditCard.CARD_TYPE_CASH) {
            this.cardPicBig = R.drawable.ic_payment_cash;
            this.cadrPicSmall = R.drawable.ic_payment_cash_black;
            this.cadrPicSmallWhite = R.drawable.ic_payment_cash;
            //FIXME брать строку с ресурсов
            this.cardNumberForMenu = CASH;
            this.maskedCardNumber = CASH;
            this.lastFourDigits = CASH;
            this.lastFourDigitsWithFourStars = CASH;
        }else {
            this.cardPicBig =R.drawable.ic_card_unknown_48;
            this.cadrPicSmall = R.drawable.ic_card_unknown_32;
            this.cadrPicSmallWhite = R.drawable.ic_card_unknown_32_white;
            this.cardNumberForMenu = "Неизвестна — "+this.lastFourDigitsWithFourStars;
        }
        if(isAvailable) {
            this.isSelected = creditCard.isActive();
        }else {
            this.isSelected=false;
        }
        //Log.d(TAG, "CreditCardVM: we create card with icon "+this.cardPicBig);
    }

    private String getLastFourDigits(String cardNumber){
        return cardNumber.substring(cardNumber.length()-4,cardNumber.length());
    }

    private String maskCardNumber(String cardNumber){
        String firstFourNumbers = cardNumber.substring(0,4);
        String lastFourNumber = getLastFourDigits(cardNumber);
        return firstFourNumbers + " **** **** " + lastFourNumber;
    }

    public int getCardPicBig() {
        return cardPicBig;
    }

    public int getCadrPicSmall() {
        return cadrPicSmall;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public String getLastFourDigitsWithStar() {
        if (cardType == CreditCard.CARD_TYPE_CASH) {
            return CASH;
        }
        return lastFourDigits;
    }

    @IdRes public int getCadrPicSmallWhite() {
        return cadrPicSmallWhite;
    }

    public String getMaskedCardNumber() {
        if (cardType == CreditCard.CARD_TYPE_CASH) {
            return CASH;
        }
        return maskedCardNumber;
    }

    public int getId() {
        return id;
    }

    public String getLastFourDigitsWithFourStars() {
        if (cardType == CreditCard.CARD_TYPE_CASH) {
            return CASH;
        }
        return lastFourDigitsWithFourStars;
    }
}
