package manfred.ru.manfredpassenger.cards.repositories;

import java.util.List;

import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.CreditCardForServer;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.models.CardForServer;
import manfred.ru.manfredpassenger.cards.models.ConfirmedCard;
import manfred.ru.manfredpassenger.cards.models.CardAddingAnswer;
import manfred.ru.manfredpassenger.cards.models.PublicIdResponse;

public interface CardRepository {
    void getCards(NetworkResponseCallback<List<CreditCard>> cards);
    void getPublicApiKey(NetworkResponseCallback<PublicIdResponse> callback);
    void sendCryptogram(CardForServer cardForServer, NetworkResponseCallback<CardAddingAnswer>callback);
    void addConfirmedCard(ConfirmedCard confirmedCard, NetworkResponseCallback<CreditCard>callback);
    @Deprecated
    void sendNewCard(CreditCardForServer creditCard, NetworkResponseCallback<CreditCard> creditCardNetworkResponseCallback);
    void removeCard(int cardId, NetworkResponseCallback responseCallback);
    void selectCard(int cardId, NetworkResponseCallback<CreditCard> responseCallback);
}
