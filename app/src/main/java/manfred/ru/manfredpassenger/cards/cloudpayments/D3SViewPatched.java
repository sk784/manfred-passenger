package manfred.ru.manfredpassenger.cards.cloudpayments;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class D3SViewPatched extends WebView {
    private static final String TAG = "D3SViewPatched";
    /**
     * Namespace for JS bridge
     */
    private static String JavaScriptNS = "D3SJS";

    /**
     * Pattern to find the MD value in the ACS server post response
     */
    private static Pattern mdFinder = Pattern.compile(".*?(<input[^<>]* name=\\\"MD\\\"[^<>]*>).*?", 32);

    /**
     * Pattern to find the PaRes value in the ACS server post response
     */
    private static Pattern paresFinder = Pattern.compile(".*?(<input[^<>]* name=\\\"PaRes\\\"[^<>]*>).*?", 32);

    private static Pattern valuePattern = Pattern.compile(".*? value=\\\"(.*?)\\\"", 32);

    /**
     * Internal flag for tracking web page url changes in WebView
     */
    private boolean urlReturned = false;

    /**
     * When set to <b>true</b>, SSL certificate errors and self-signed certificates errors will be ignored.
     */
    private boolean debugMode = false;

    /**
     * Url that will be used by ACS server for posting result data on authorization completion. We will be monitoring
     * this URL in WebView handler to intercept its loading and grabbing the resulting data from POST message instead.
     */
    private String postbackUrl = "manfred.ru";

    /**
     * In simple mode we do not try to inject JS code and parse ACS server POST data, we just intercept
     * postback url and call the simplified result listener instead. This is used
     * for some stacked providers where we need to pass extra provider's postback url to an acs server and then wait for it to complete.
     */
    private String stackedModePostbackUrl;

    private AtomicBoolean postbackHandled = new AtomicBoolean(false);

    /**
     * Callback to send authorization events to
     */
    private D3SSViewAuthorizationListenerPatched authorizationListener = null;


    public D3SViewPatched(final Context context) {
        super(context);
        initUI();
    }

    public D3SViewPatched(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public D3SViewPatched(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        initUI();
    }

    public D3SViewPatched(final Context context, final AttributeSet attrs, final int defStyle, final boolean privateBrowsing) {
        super(context, attrs, defStyle);
        initUI();
    }

    private void initUI() {
        getSettings().setJavaScriptEnabled(true);
        getSettings().setBuiltInZoomControls(true);
        addJavascriptInterface(new D3SJSInterface(), JavaScriptNS);

        setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
                final boolean stackedMode = !TextUtils.isEmpty(stackedModePostbackUrl);

                if (!postbackHandled.get() && (!stackedMode && url.toLowerCase().contains(postbackUrl.toLowerCase())
                        || (stackedMode
                        && url.toLowerCase().contains(stackedModePostbackUrl.toLowerCase())))) {
                    if (!TextUtils.isEmpty(stackedModePostbackUrl)) {
                        if (postbackHandled.compareAndSet(false, true)) {
                            authorizationListener.onAuthorizationCompletedInStackedMode(url);
                        }
                    } else {
                        view.loadUrl(String.format("javascript:window.%s.processHTML(document.getElementsByTagName('body')[0].innerHTML);",
                                JavaScriptNS));
                    }
                    return true;
                } else {
                    return super.shouldOverrideUrlLoading(view, url);
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap icon) {
                Log.d(TAG, "onPageStarted: " + url);

                final boolean stackedMode = !TextUtils.isEmpty(stackedModePostbackUrl);

                if (!urlReturned && !postbackHandled.get()) {
                    if ((!stackedMode && url.toLowerCase().contains(postbackUrl.toLowerCase()))
                            || (stackedMode && url.toLowerCase().contains(stackedModePostbackUrl.toLowerCase()))) {
                        if (!TextUtils.isEmpty(stackedModePostbackUrl)) {
                            if (postbackHandled.compareAndSet(false, true)) {
                                authorizationListener.onAuthorizationCompletedInStackedMode(url);
                            }
                        } else {
                            view.loadUrl(String.format("javascript:window.%s.processHTML(document.getElementsByTagName('html')[0].innerHTML);"
                                    , JavaScriptNS));
                        }
                        urlReturned = true;
                    } else {
                        super.onPageStarted(view, url, icon);
                    }
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

//                if (url.toLowerCase().contains(postbackUrl.toLowerCase())) {
//                    return;
//                }

                view.loadUrl(String.format("javascript:window.%s.processHTML(document.getElementsByTagName('body')[0].innerHTML);",
                        JavaScriptNS));
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.d(TAG, "onReceivedError: "+description);
                if (!failingUrl.startsWith(postbackUrl)) {
                    authorizationListener.onAuthorizationWebPageLoadingError(errorCode, description, failingUrl);
                }
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                Log.d(TAG, "onReceivedError: (M) "+rerr.getDescription());
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }

            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if (debugMode) {
                    handler.proceed();
                }
            }


        });

        setWebChromeClient(new WebChromeClient() {

            public void onProgressChanged(WebView view, int newProgress) {
                if (authorizationListener != null) {
                    authorizationListener.onAuthorizationWebPageLoadingProgressChanged(newProgress);
                }
            }
        });
    }

    public void setStackedMode(String stackedModePostbackUrl) {
        this.stackedModePostbackUrl = stackedModePostbackUrl;
    }

    private void completeAuthorizationIfPossible(String html) {
        // If the postback has already been handled, stop now
        if (postbackHandled.get()) {
            return;
        }

        // Try and find the MD and PaRes form elements in the supplied html
        String md = "";
        String pares = "";

        Matcher mdMatcher = mdFinder.matcher(html);
        if (mdMatcher.find()) {
            md = mdMatcher.group(1);
        } else {
            return; // Not Found
        }

        Matcher paresMatcher = paresFinder.matcher(html);
        if (paresMatcher.find()) {
            pares = paresMatcher.group(1);
        } else {
            return; // Not Found
        }

        // Now extract the values from the previously captured form elements
        Matcher mdValueMatcher = valuePattern.matcher(md);
        if (mdValueMatcher.find()) {
            md = mdValueMatcher.group(1);
        } else {
            return; // Not Found
        }

        Matcher paresValueMatcher = valuePattern.matcher(pares);
        if (paresValueMatcher.find()) {
            pares = paresValueMatcher.group(1);
        } else {
            return; // Not Found
        }

        // If we get to this point, we've definitely got values for both the MD and PaRes

        // The postbackHandled check is just to ensure we've not already called back.
        // We don't want onAuthorizationCompleted to be called twice.
        if (postbackHandled.compareAndSet(false, true) && authorizationListener != null) {
            authorizationListener.onAuthorizationCompleted(md, pares);
        }
    }

    /**
     * Checks if debug mode is on. Note, that you must not turn debug mode for production app !
     *
     * @return
     */
    public boolean isDebugMode() {
        return debugMode;
    }

    /**
     * Sets the debug mode state. When set to <b>true</b>, ssl errors will be ignored. Do not turn debug mode ON
     * for production environment !
     *
     * @param debugMode
     */
    public void setDebugMode(final boolean debugMode) {
        this.debugMode = debugMode;
    }

    /**
     * Sets the callback to receive authorization events
     *
     * @param authorizationListener
     */
    public void setAuthorizationListener(final D3SSViewAuthorizationListenerPatched authorizationListener) {
        this.authorizationListener = authorizationListener;
    }

    /**
     * Starts 3DS authorization
     *
     * @param acsUrl ACS server url, returned by the credit card processing gateway
     * @param md     MD parameter, returned by the credit card processing gateway
     * @param paReq  PaReq parameter, returned by the credit card processing gateway
     */
    public void authorize(final String acsUrl, final String md, final String paReq) {
        authorize(acsUrl, md, paReq, null);
    }

    /**
     * Starts 3DS authorization
     *
     * @param acsUrl      ACS server url, returned by the credit card processing gateway
     * @param md          MD parameter, returned by the credit card processing gateway
     * @param paReq       PaReq parameter, returned by the credit card processing gateway
     * @param postbackUrl custom postback url for intercepting ACS server result posting. You may use any url you like
     *                    here, if you need, even non existing ones.
     */
    public void authorize(final String acsUrl, final String md, final String paReq, final String postbackUrl) {
        urlReturned = false;
        postbackHandled.set(false);

        if (authorizationListener != null) {
            authorizationListener.onAuthorizationStarted(this);
        }

        if (!TextUtils.isEmpty(postbackUrl)) {
            this.postbackUrl = postbackUrl;
        }

        String postParams;
        try {
            postParams = String.format(Locale.US, "MD=%1$s&TermUrl=%2$s&PaReq=%3$s",
                    URLEncoder.encode(md, "UTF-8"), URLEncoder.encode(this.postbackUrl, "UTF-8"),
                    URLEncoder.encode(paReq, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        postUrl(acsUrl, postParams.getBytes());
    }

    class D3SJSInterface {

        D3SJSInterface() {
        }

        @android.webkit.JavascriptInterface
        public void processHTML(final String html) {
            Log.d(TAG, "processHTML: " + html);
            completeAuthorizationIfPossible(html);
        }
    }
}
