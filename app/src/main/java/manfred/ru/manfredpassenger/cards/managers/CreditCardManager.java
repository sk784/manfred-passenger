package manfred.ru.manfredpassenger.cards.managers;

import android.arch.lifecycle.LiveData;

import java.util.List;

import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.cards.models.CreditCardVM;

public interface CreditCardManager {
    void addCard(CreditCard creditCard);

    void selectCard(int cardId);

    void selectCardAndRepayOrder(int cardId, int orderId, NetworkResponseCallback callback);

    void addCards(List<CreditCard> cards);

    void deleteCard(CreditCardVM creditCardVM);
    void refreshCards();
    boolean isHaveCards();


    LiveData<List<CreditCard>> cards();
}
