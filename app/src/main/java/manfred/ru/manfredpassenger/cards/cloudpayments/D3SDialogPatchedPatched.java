package manfred.ru.manfredpassenger.cards.cloudpayments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import manfred.ru.manfredpassenger.R;

public class D3SDialogPatchedPatched extends Dialog implements D3SSViewAuthorizationListenerPatched {
    private D3SViewPatched authenticator;
    private ProgressBar progressBar;
    private TextView textView;
    private String acs, md, pareq, postback, stackedModePostbackUrl;

    private D3SDialogListener authorizationListener;

    private Handler handler;

    public D3SDialogPatchedPatched(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static D3SDialogPatchedPatched newInstance(Context context, final String acsUrl, final String md, final String paReq, D3SDialogListener listener) {
        return newInstance(context, acsUrl, md, paReq, null, listener);
    }

    public static D3SDialogPatchedPatched newInstance(Context context, final String acsUrl,
                                                      final String md, final String paReq, final String postbackUrl, D3SDialogListener listener) {
        D3SDialogPatchedPatched dialog = new D3SDialogPatchedPatched(context, R.style.Theme_CustomDialog);
        dialog.setContentView(R.layout.dialog_3ds_patched);

        dialog.acs = acsUrl;
        dialog.md = md;
        dialog.pareq = paReq;
        dialog.postback = postbackUrl;
        dialog.authorizationListener = listener;

        return dialog;
    }

    public static D3SDialogPatchedPatched newInstance(Context context, final String acsUrl, final String md, final String paReq, final String acsPostbackUrl, final String stackedModePostbackUrl, D3SDialogListener listener) {
        D3SDialogPatchedPatched dialog = new D3SDialogPatchedPatched(context, R.style.Theme_CustomDialog);
        dialog.setContentView(R.layout.dialog_3ds_patched);

        dialog.acs = acsUrl;
        dialog.md = md;
        dialog.pareq = paReq;
        dialog.postback = acsPostbackUrl;
        dialog.stackedModePostbackUrl = stackedModePostbackUrl;
        dialog.authorizationListener = listener;

        return dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        authenticator = new D3SViewPatched(getContext());
        authenticator.setAuthorizationListener(this);
        ((FrameLayout) findViewById(R.id.main_3ds)).addView(authenticator, 0);
        progressBar = findViewById(R.id.pr);
        textView = findViewById(R.id.wait_text);

        progressBar.setVisibility(View.VISIBLE);
        handler = new Handler();

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!TextUtils.isEmpty(stackedModePostbackUrl)) {
                    authenticator.setStackedMode(stackedModePostbackUrl);
                }

                if (TextUtils.isEmpty(postback)) {
                    authenticator.authorize(acs, md, pareq);
                } else {
                    authenticator.authorize(acs, md, pareq, postback);
                }

            }
        });
    }

    /*    public void showDialogAndAuthenticate(FragmentActivity activity)
    {
        if (activity.getCurrentFocus() != null)
        {
            activity.getCurrentFocus().clearFocus();
        }

        try
        {
            activity.getSupportFragmentManager().executePendingTransactions();
        }
        catch (Throwable err)
        {
            err.printStackTrace();
        }

        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.add(this, "d3sdialog");
        ft.commitAllowingStateLoss();
    }*/

   /* public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState)
    {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(null);

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        View v = inflater.inflate(R.layout.dialog_3ds_patched, container, false);

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        authenticator = (D3SViewPatched) v.findViewById(R.id.authenticator);
        authenticator.setAuthorizationListener(this);

        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(true);

        handler = new Handler();

        return v;
    }

    public void onViewCreated(final View view, final Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (!TextUtils.isEmpty(stackedModePostbackUrl))
        {
            authenticator.setStackedMode(stackedModePostbackUrl);
        }

        if (TextUtils.isEmpty(postback))
        {
            authenticator.authorize(acs, md, pareq);
        }
        else
        {
            authenticator.authorize(acs, md, pareq, postback);
        }
    }*/

    public void onAuthorizationCompleted(final String md, final String paRes) {
        handler.post(new Runnable() {
            public void run() {
                dismiss();
                if (authorizationListener != null) {
                    authorizationListener.onAuthorizationCompleted(md, paRes);
                }
            }
        });
    }

    public void onAuthorizationStarted(final D3SViewPatched view) {
        handler.post(new Runnable() {
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

    }

    public void onAuthorizationWebPageLoadingProgressChanged(final int progress) {
        handler.post(new Runnable() {
            public void run() {
                progressBar.setVisibility(progress > 0 && progress < 100 ? View.VISIBLE : View.GONE);
                textView.setVisibility(progress > 0 && progress < 100 ? View.VISIBLE : View.GONE);

            }
        });

    }

    public void onAuthorizationWebPageLoadingError(final int errorCode, final String description, final String failingUrl) {
        handler.post(new Runnable() {
            public void run() {
                dismiss();
                if (authorizationListener != null) {
                    authorizationListener.onAuthorizationFailed(errorCode, description, failingUrl);
                }
            }
        });
    }

    @Override
    public void onAuthorizationCompletedInStackedMode(final String finalizationUrl) {
        handler.post(new Runnable() {
            public void run() {
                dismiss();
                if (authorizationListener != null) {
                    authorizationListener.onAuthorizationCompletedInStackedMode(finalizationUrl);
                }
            }
        });
    }

    public interface D3SDialogListener {
        void onAuthorizationCompleted(final String md, final String paRes);

        void onAuthorizationFailed(final int code, final String message, final String failedUrl);

        void onAuthorizationCompletedInStackedMode(String finalizationUrl);
    }
}
