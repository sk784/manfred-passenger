package manfred.ru.manfredpassenger.cards.repositories;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import manfred.ru.manfredpassenger.api.APIClient;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.ManfredResponse;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.CardIdRequest;
import manfred.ru.manfredpassenger.api.services.models.Cards;
import manfred.ru.manfredpassenger.api.services.models.CreditCardForServer;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.models.CardAddingAnswer;
import manfred.ru.manfredpassenger.cards.models.CardForServer;
import manfred.ru.manfredpassenger.cards.models.ConfirmedCard;
import manfred.ru.manfredpassenger.cards.models.PublicIdResponse;
import manfred.ru.manfredpassenger.utils.NetworkUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HTTPManfredCardRepository implements CardRepository {
    private static final String TAG = "HTTPManfredCardRepo";
    private ManfredTokenManager manfredTokenManager;

    public HTTPManfredCardRepository(ManfredTokenManager manfredTokenManager) {
        this.manfredTokenManager = manfredTokenManager;
    }

    @Override
    public void getCards(final NetworkResponseCallback<List<CreditCard>> cards) {
        Log.d(TAG, "getCards: with token " + manfredTokenManager.getAuthToken());
        APIClient.getUserService().getCards(manfredTokenManager.getAuthToken()).enqueue(new Callback<ManfredResponse<Cards>>() {
            @Override
            public void onResponse(Call<ManfredResponse<Cards>> call, Response<ManfredResponse<Cards>> response) {
                ManfredResponse<Cards> manfredResponse = response.body();
                if (manfredResponse != null) {
                    if (manfredResponse.getResult_code().equals("success")) {
                        cards.allOk(manfredResponse.getData().getCards());
                    } else {
                        if (manfredResponse.getResult_code().equals("invalid_token")) {
                            cards.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR,
                                    manfredResponse.getMessage()));
                        } else {
                            cards.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER,
                                    "wrong status of server answer: " + manfredResponse.getResult_code()));
                        }
                    }
                } else {
                    cards.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER,
                            "пустой ответ"));
                }
            }

            @Override
            public void onFailure(Call<ManfredResponse<Cards>> call, Throwable t) {
                new NetworkUtils().processFailureResponse(t, cards);
            }
        });
    }

    @Override
    public void getPublicApiKey(NetworkResponseCallback<PublicIdResponse> callback) {

    }

    @Override
    public void sendCryptogram(CardForServer cardForServer, NetworkResponseCallback<CardAddingAnswer> callback) {

    }

    @Override
    public void addConfirmedCard(ConfirmedCard confirmedCard, NetworkResponseCallback<CreditCard> callback) {

    }

    @Override
    public void sendNewCard(CreditCardForServer creditCard, final NetworkResponseCallback<CreditCard> creditCardNetworkResponseCallback) {
        APIClient.getUserService().addCard(manfredTokenManager.getAuthToken(), creditCard)
                .enqueue(new Callback<ManfredResponse<CreditCard>>() {
                    @Override
                    public void onResponse(@NonNull Call<ManfredResponse<CreditCard>> call, @NonNull Response<ManfredResponse<CreditCard>> response) {
                        ManfredResponse<CreditCard> manfredResponse = response.body();
                        if (manfredResponse != null) {
                            if (manfredResponse.getResult_code().equals("success") || manfredResponse.getResult_code().equals("card_already_exists")) {
                                CreditCard newCard = manfredResponse.getData();
                                //CreditCard newCreditCard = new CreditCard(newCard.getCardNumber(), newCard.isActive(), newCard.getId(), newCard.getId());
                                creditCardNetworkResponseCallback.allOk(newCard);
                            } else {
                                if (manfredResponse.getResult_code().equals("invalid_token")) {
                                    creditCardNetworkResponseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR,
                                            manfredResponse.getMessage()));
                                } else {
                                    creditCardNetworkResponseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER,
                                            "wrong status of server answer: " + manfredResponse.getResult_code()));
                                }
                            }
                        } else {
                            creditCardNetworkResponseCallback.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER,
                                    "пустой ответ"));
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ManfredResponse<CreditCard>> call, @NonNull Throwable t) {
                        new NetworkUtils().processFailureResponse(t, creditCardNetworkResponseCallback);
                    }
                });
    }

    @Override
    public void removeCard(int cardId, NetworkResponseCallback responseCallback) {
        APIClient.getUserService().removeCreditCard(manfredTokenManager.getAuthToken(),new CardIdRequest(cardId)).enqueue(new Callback<ManfredResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse> call, @NonNull Response<ManfredResponse> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse> call, @NonNull Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

    @Override
    public void selectCard(int cardId, NetworkResponseCallback<CreditCard> responseCallback) {
        APIClient.getUserService().selectCard(manfredTokenManager.getAuthToken(), new CardIdRequest(cardId)).enqueue(new Callback<ManfredResponse<CreditCard>>() {
            @Override
            public void onResponse(@NonNull Call<ManfredResponse<CreditCard>> call, @NonNull Response<ManfredResponse<CreditCard>> response) {
                new NetworkUtils().validateResponse(response, responseCallback);
            }

            @Override
            public void onFailure(@NonNull Call<ManfredResponse<CreditCard>> call, @NonNull Throwable t) {
                new NetworkUtils().processFailureResponse(t, responseCallback);
            }
        });
    }

}
