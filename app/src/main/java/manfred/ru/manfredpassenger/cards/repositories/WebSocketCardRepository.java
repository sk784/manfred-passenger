package manfred.ru.manfredpassenger.cards.repositories;

import android.support.annotation.Nullable;

import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswer;
import manfred.ru.manfredpassenger.api.model.WebSocketAnswerCallback;
import manfred.ru.manfredpassenger.api.services.models.CardIdRequest;
import manfred.ru.manfredpassenger.api.services.models.Cards;
import manfred.ru.manfredpassenger.api.services.models.CreditCardForServer;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.models.CardAddingAnswer;
import manfred.ru.manfredpassenger.cards.models.CardForServer;
import manfred.ru.manfredpassenger.cards.models.ConfirmedCard;
import manfred.ru.manfredpassenger.cards.models.PublicIdResponse;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.utils.NetworkUtils;

public class WebSocketCardRepository implements CardRepository {
    private final NetworkManagerProvider networkManager;
    private final ManfredTokenManager manfredTokenManager;

    public WebSocketCardRepository(NetworkManagerProvider networkManager, ManfredTokenManager manfredTokenManager) {
        this.networkManager = networkManager;
        this.manfredTokenManager = manfredTokenManager;
    }

    @Override
    public void getCards(NetworkResponseCallback<List<CreditCard>> cards) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(),"user/cards", new WebSocketAnswerCallback<WebSocketAnswer<Cards>>() {
            @Override
            public void messageReceived(WebSocketAnswer<Cards> message) {
                switch (message.getResultCode()){
                    case "success":
                        cards.allOk(message.getPayload().getCards());
                        break;
                    case "invalid_token":
                        cards.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.TOKEN_ERROR, message.getMessage()));
                        break;
                    case "no_free_drivers":
                        cards.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.NO_FREE_DRIVERS, message.getMessage()));
                        break;
                    case "passenger_out_region":
                        cards.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OUT_REGION,message.getMessage()));
                        break;
                    default:
                        cards.error(new ManfredNetworkError(ManfredNetworkError.ErrorType.OTHER, message.getMessage()));
                        break;
                }
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                cards.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return Cards.class;
            }
        });
    }

    @Override
    public void getPublicApiKey(NetworkResponseCallback<PublicIdResponse> callback) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(), "cloudpayments/get_public_id", new WebSocketAnswerCallback<WebSocketAnswer<PublicIdResponse>>() {
            @Override
            public void messageReceived(WebSocketAnswer<PublicIdResponse> message) {
                new NetworkUtils().validateSocketResponse(message, callback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                callback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return PublicIdResponse.class;
            }
        });
    }

    @Override
    public void sendCryptogram(CardForServer cardForServer, NetworkResponseCallback<CardAddingAnswer> callback) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(), "cloudpayments/request_add_card",
                cardForServer, new WebSocketAnswerCallback<WebSocketAnswer<CardAddingAnswer>>() {
            @Override
            public void messageReceived(WebSocketAnswer<CardAddingAnswer> message) {
                new NetworkUtils().validateSocketResponse(message, callback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                callback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return CardAddingAnswer.class;
            }
        });
    }

    @Override
    public void addConfirmedCard(ConfirmedCard confirmedCard, NetworkResponseCallback<CreditCard> callback) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(), "cloudpayments/confirm_add_card", confirmedCard,
                new WebSocketAnswerCallback<WebSocketAnswer<CreditCard>>() {
                    @Override
                    public void messageReceived(WebSocketAnswer<CreditCard> message) {
                        new NetworkUtils().validateSocketResponse(message, callback);
                    }

                    @Override
                    public void error(ManfredNetworkError manfredError) {
                        callback.error(manfredError);
                    }

                    @Override
                    public Class getAnswerClass() {
                        return CreditCard.class;
                    }
                });
    }

    @Override
    public void sendNewCard(CreditCardForServer creditCard, NetworkResponseCallback<CreditCard> creditCardNetworkResponseCallback) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(), "user/add_card", creditCard, new WebSocketAnswerCallback<WebSocketAnswer<CreditCard>>() {
            @Override
            public void messageReceived(WebSocketAnswer<CreditCard> message) {
                new NetworkUtils().validateSocketResponse(message, creditCardNetworkResponseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                creditCardNetworkResponseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return CreditCard.class;
            }
        });
    }

    @Override
    public void removeCard(int cardId, NetworkResponseCallback responseCallback) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(), "user/delete_card", new CardIdRequest(cardId), new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                new NetworkUtils().validateSocketResponse(message, responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Nullable
            @Override
            public Class getAnswerClass() {
                return null;
            }
        });
    }

    @Override
    public void selectCard(int cardId, NetworkResponseCallback<CreditCard> responseCallback) {
        networkManager.addQuery(manfredTokenManager.getAuthToken(), "user/set_active_card", new CardIdRequest(cardId), new WebSocketAnswerCallback() {
            @Override
            public void messageReceived(WebSocketAnswer message) {
                new NetworkUtils().validateSocketResponse(message, responseCallback);
            }

            @Override
            public void error(ManfredNetworkError manfredError) {
                responseCallback.error(manfredError);
            }

            @Override
            public Class getAnswerClass() {
                return CreditCard.class;
            }
        });
    }
}
