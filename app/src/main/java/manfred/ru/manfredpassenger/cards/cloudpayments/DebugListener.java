package manfred.ru.manfredpassenger.cards.cloudpayments;

public interface DebugListener {
    void debug(String html);
}
