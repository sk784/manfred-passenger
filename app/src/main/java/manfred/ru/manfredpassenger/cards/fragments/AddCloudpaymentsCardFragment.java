package manfred.ru.manfredpassenger.cards.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.cards.cloudpayments.D3SDialogPatchedPatched;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.models.CardAddingAnswer;
import manfred.ru.manfredpassenger.cards.models.CardForServer;
import manfred.ru.manfredpassenger.cards.models.ConfirmedCard;
import manfred.ru.manfredpassenger.cards.models.PublicIdResponse;
import manfred.ru.manfredpassenger.cards.repositories.CardRepository;
import manfred.ru.manfredpassenger.cards.repositories.WebSocketCardRepository;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.databinding.FragmentAddCloudpaymentsCardBinding;
import manfred.ru.manfredpassenger.utils.InternetHelper;
import ru.cloudpayments.cpcard.CPCard;
import ru.cloudpayments.cpcard.CPCardFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddCloudpaymentsCardFragment extends Fragment {
    private static final String TAG = "AddCloudpaymentsCardFra";
    /*
    Public ID
    pk_f05cd7ed8e95464466cfb8cbffc8c
    Пароль для API
    1f3e6ea27f5007dec34e5ab134c06d9
     */
    public AddCloudpaymentsCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        FragmentAddCloudpaymentsCardBinding binding = FragmentAddCloudpaymentsCardBinding.inflate(getLayoutInflater());

        init(binding);
        return binding.getRoot();
    }

    private void init(FragmentAddCloudpaymentsCardBinding binding)  {
        Log.d(TAG, "init: mask installed");
        binding.editCardNumber.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int pos = 0;
                while (true) {
                    if (pos >= s.length()) break;
                    if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                        s.delete(pos, pos + 1);
                    } else {
                        pos++;
                    }
                }

                // Insert char where needed.
                pos = 4;
                while (true) {
                    if (pos >= s.length()) break;
                    final char c = s.charAt(pos);
                    // Only if its a digit where there should be a space we insert a space
                    if ("0123456789".indexOf(c) >= 0) {
                        s.insert(pos, "" + space);
                    }
                    pos += 5;
                }
                if(extractCardNumberFromFields(binding).length()==16){
                    binding.editCardholderName.requestFocus();
                }
                if(checkFieldsForCorrectInput(binding)){
                    binding.addCard.setEnabled(true);
                }else {
                    binding.addCard.setEnabled(false);
                }
            }
        });
        binding.editMm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(checkFieldsForCorrectInput(binding)){
                    binding.addCard.setEnabled(true);
                }else {
                    binding.addCard.setEnabled(false);
                }
                if(editable.toString().length()==2)binding.editYy.requestFocus();
            }
        });
        binding.editYy.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(checkFieldsForCorrectInput(binding)){
                    binding.addCard.setEnabled(true);
                }else {
                    binding.addCard.setEnabled(false);
                }
                if(editable.toString().length()==2)binding.editCvv.requestFocus();
            }
        });
        binding.editCvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(checkFieldsForCorrectInput(binding)){
                    binding.addCard.setEnabled(true);
                }else {
                    binding.addCard.setEnabled(false);
                }
            }
        });
        binding.editCardholderName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(checkFieldsForCorrectInput(binding)){
                    binding.addCard.setEnabled(true);
                }else {
                    binding.addCard.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.editCardNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if(!getCpCard(binding).isValidNumber()){
                        binding.editCardNumber.setError("Проверьте номер карты");
                    }
                }
            }
        });

        binding.editCardholderName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if(!validateName(binding)){
                        binding.editCardholderName.setError("Имя слишком короткое");
                    }
                }
            }
        });

        binding.editCardNumber.requestFocus();

        final NetworkManagerProvider networkManager = ((ManfredPassengerApplication) getActivity().getApplication()).getNetworkManager();
        final ManfredTokenManager tokenManager = ((ManfredPassengerApplication) getActivity().getApplication()).getTokenManager();
        CardRepository manfredCardRepository = new WebSocketCardRepository(networkManager, tokenManager);

        binding.addCard.setOnClickListener(v->{
            binding.addCard.setEnabled(false);
            showProgress(binding);
            Log.d(TAG, "init: addCard click");
            manfredCardRepository.getPublicApiKey(new NetworkResponseCallback<PublicIdResponse>() {
                @Override
                public void allOk(PublicIdResponse response) {
                    Log.d(TAG, "allOk: "+response.toString());
                    String cryptogram = null;
                    try {
                        cryptogram = createCryptogram(response.getPublicId(),getCpCard(binding));
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                        showModalDialog("Ошибка",e.getLocalizedMessage());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        showModalDialog("Ошибка",e.getLocalizedMessage());
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                        showModalDialog("Ошибка",e.getLocalizedMessage());
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                        showModalDialog("Ошибка",e.getLocalizedMessage());
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                        showModalDialog("Ошибка",e.getLocalizedMessage());
                    } catch (NoSuchProviderException e) {
                        e.printStackTrace();
                        showModalDialog("Ошибка",e.getLocalizedMessage());
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                        showModalDialog("Ошибка",e.getLocalizedMessage());
                    }
                    if(cryptogram!=null){
                        Log.d(TAG, "cryptogram is: "+cryptogram);
                        requestForAddCard(binding, manfredCardRepository, maskNumber(extractCardNumberFromFields(binding)),
                                cryptogram, cardTypefromString(getCpCard(binding).getType()));
                    }
                }

                @Override
                public void error(ManfredNetworkError error) {
                    binding.addCard.setEnabled(true);
                    hideProgress(binding);
                    showModalDialog("Ошибка", error.getText());
                }
            });
        });
    }

    private void requestForAddCard(FragmentAddCloudpaymentsCardBinding binding, CardRepository cardRepository,
                                   String maskNumber, String cryptogram, int cardType) {
        Log.d(TAG, "requestForAddCard: ");
        cardRepository.sendCryptogram(new CardForServer(maskNumber, cryptogram,
                        binding.editCardholderName.getEditableText().toString(), InternetHelper.getIPAddress(), cardType),
                new NetworkResponseCallback<CardAddingAnswer>() {
            @Override
            public void allOk(CardAddingAnswer response) {
                Log.d(TAG, "sendCryptogram allOk: "+response.toString());
                if(response.isAuthRequired()){
                    Log.d(TAG, "sendCryptogram allOk: need 3DS");
                    show3DS(binding, cardRepository, response.getAuthUrl(), String.valueOf(response.getTransactionId()), response.getPaReq());
                }else {
                    Log.d(TAG, "sendCryptogram allOk: not need 3DS");
                    post3ds(binding,cardRepository, String.valueOf(response.getTransactionId()), response.getPaReq());
                }
            }

            @Override
            public void error(ManfredNetworkError error) {
                binding.addCard.setEnabled(true);
                hideProgress(binding);
                showModalDialog("Ошибка",error.getText());
            }
        });
    }

    private String createCryptogram(String publicId, CPCard cpCard) throws NoSuchPaddingException,
            UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException,
            NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        Log.d(TAG, "createCryptogram: publicId"+publicId+", cpCard="+cpCard);
        return cpCard.cardCryptogram(publicId);
    }

    // TODO: 11.02.2019 we prevent crash here(checking for activity), but we must show error when user back
    private void showModalDialog(String title, String text){
        Activity activity = getActivity();
        if (activity != null) {
            AlertDialog alertDialog = new AlertDialog.Builder(activity, R.style.AlertDialogCustom)
                    .setTitle(title)
                    .setMessage(text)
                    .setPositiveButton("OK",null)
                    .create();
            alertDialog.show();
        }

    }

    private void show3DS(FragmentAddCloudpaymentsCardBinding binding, CardRepository cardRepository, String authUrl,
                         String transactionId, String paReq){
        Log.d(TAG, "show3DS: "+authUrl+", "+transactionId+", "+paReq);
        //Log.d(TAG, "show3DS: ");
        // Открываем форму 3ds
        D3SDialogPatchedPatched.newInstance(getActivity(),authUrl, transactionId, paReq, "https://manfred.ru/processing.php",
                new D3SDialogPatchedPatched.D3SDialogListener() {
            @Override
            public void onAuthorizationCompleted(String md, String paRes) {
                Log.d(TAG, "show3DS onAuthorizationCompleted: ");
                post3ds(binding, cardRepository, transactionId, paRes);
            }

            @Override
            public void onAuthorizationFailed(int code, String message, String failedUrl) {
                binding.addCard.setEnabled(true);
                hideProgress(binding);
                Log.d(TAG, "show3DS onAuthorizationFailed: "+message+", "+failedUrl+"code "+code);
                Toast.makeText(getActivity(),"AuthorizationFailed: " + message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthorizationCompletedInStackedMode(String finalizationUrl) {
                Log.d(TAG, "show3DS onAuthorizationCompletedInStackedMode: "+finalizationUrl);
            }
        }).show();
    }

    private void showProgress(FragmentAddCloudpaymentsCardBinding binding){
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.addCard.setText("");
    }
    private void hideProgress(FragmentAddCloudpaymentsCardBinding binding){
        binding.progressBar.setVisibility(View.GONE);
        binding.addCard.setText("Добавить");
    }
    //md is transaction id
    private void post3ds(FragmentAddCloudpaymentsCardBinding binding, CardRepository cardRepository,
                         String md, String paRes) {
        Log.d(TAG, "post3ds: "+md+", "+paRes);
        cardRepository.addConfirmedCard(new ConfirmedCard(md, paRes), new NetworkResponseCallback<CreditCard>() {
            @Override
            public void allOk(CreditCard response) {
                Log.d(TAG, "post3ds allOk: ");
                ((ManfredPassengerApplication)getActivity().getApplication()).getManfredCreditCardManager().refreshCards();
                getActivity().onBackPressed();
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "post3ds error: "+error);
                showModalDialog("Ошибка",error.getText());
                binding.addCard.setEnabled(true);
                hideProgress(binding);
            }
        });
    }

    private boolean checkFieldsForCorrectInput(FragmentAddCloudpaymentsCardBinding binding){
        int month;
        if(binding.editMm.getText().toString().isEmpty()){
            month=0;
        }else {
            month=Integer.parseInt(binding.editMm.getText().toString());
        }

        CPCard cpCard = getCpCard(binding);
        boolean cardIsValid = cpCard.isValidNumber();

        return cardIsValid &&
                validateName(binding) &&
                month < 13 &&
                binding.editYy.getText().toString().length() == 2 &&
                binding.editCvv.getText().toString().length() == 3;
    }

    private boolean validateName(FragmentAddCloudpaymentsCardBinding binding) {
        final String name = binding.editCardholderName.getText().toString();
        String[] separated = name.split(" ");
        int iWordCount = separated.length;
        if(iWordCount<2){
            return false;
        }

        for (String s : separated) {
            if (s.length() < 2) {
                return false;
            }
        }
        return true;
    }

    @NotNull
    private CPCard getCpCard(FragmentAddCloudpaymentsCardBinding binding) {
        return CPCardFactory.create(extractCardNumberFromFields(binding), extractExpireFromFields(binding), extractCVVFromFields(binding));
    }

    private String extractCVVFromFields(FragmentAddCloudpaymentsCardBinding binding) {
        return binding.editCvv.getText().toString();
    }

    private String extractExpireFromFields(FragmentAddCloudpaymentsCardBinding binding) {
        String expire = binding.editMm.getText().toString()+binding.editYy.getText().toString();
        return expire;
    }

    @NotNull
    private String extractCardNumberFromFields(FragmentAddCloudpaymentsCardBinding binding) {
        final String cardNumber = binding.editCardNumber.getText().toString().replaceAll(" ", "");
        //Log.d(TAG, "extractCardNumberFromFields: "+cardNumber);
        return cardNumber;
    }

    private String maskNumber(final String creditCardNumber) {
        final String s = creditCardNumber.replaceAll("\\D", "");

        final int start = 6;
        final int end = s.length() - 4;
        final String overlay = StringUtils.repeat("*", end - start);

        return StringUtils.overlay(s, overlay, start, end);
    }

    private int cardTypefromString(String value) {
        /*
        CardTypeUnknown =0
        CardTypeVisa = 1
        CardTypeMastercard = 2
        CardTypeMaestro= 3
         */
        if ("visa".equals(value.toLowerCase())) {
            return 1;
        } else if ("mastercard".equals(value.toLowerCase())) {
            return 2;
        } else if ("maestro".equals(value.toLowerCase())) {
            return 3;
        } else if ("amex".equals(value.toLowerCase())) {
            return 0;
        } else if ("americanexpress".equals(value.toLowerCase())) {
            return 0;
        } else if ("dinersclub".equals(value.toLowerCase())) {
            return 5;
        } else if ("carteblanche".equals(value.toLowerCase())) {
            return 0;
        } else {
            return 0;
        }
    }

}
