package manfred.ru.manfredpassenger.cards.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PublicIdResponse{

	@SerializedName("public_id")
	private final String publicId;

	public PublicIdResponse(String publicId) {
		this.publicId = publicId;
	}

	public String getPublicId(){
		return publicId;
	}

	@Override
 	public String toString(){
		return 
			"PublicIdResponse{" + 
			"public_id = '" + publicId + '\'' + 
			"}";
		}
}