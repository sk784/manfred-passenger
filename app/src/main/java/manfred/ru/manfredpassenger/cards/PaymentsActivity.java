package manfred.ru.manfredpassenger.cards;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.R;
import manfred.ru.manfredpassenger.cards.fragments.AddFondyCardFragment;
import manfred.ru.manfredpassenger.cards.fragments.AddCloudpaymentsCardFragment;
import manfred.ru.manfredpassenger.common.managers.regions.RegionManager;
import manfred.ru.manfredpassenger.databinding.ActivityPaymentsBinding;

public class PaymentsActivity extends AppCompatActivity {
    private static final String TAG = "PaymentsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityPaymentsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_payments);

        //setting black background for status bar
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.black_10));
        setSupportActionBar(binding.toolbarPayments);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        setTitle("Добавить карту");

        RegionManager regionManager = ((ManfredPassengerApplication)getApplication()).getRegionManager();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(regionManager.getCurrentPaymentSystemType()== RegionManager.PaymentSystemType.FONDY_KZ
                || regionManager.getCurrentPaymentSystemType()== RegionManager.PaymentSystemType.FONDY_MSK) {
            fragmentTransaction.add(binding.fragmentContainer.getId(), new AddFondyCardFragment());
            Log.d(TAG, "need use fondy: ");
        }else {
            fragmentTransaction.add(binding.fragmentContainer.getId(), new AddCloudpaymentsCardFragment());
            Log.d(TAG, "onCreate: need use cloudpayments");
        }
        fragmentTransaction.commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up
        return false;
    }

}
