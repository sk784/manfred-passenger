package manfred.ru.manfredpassenger.cards.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class CardForServer{

	@SerializedName("card_number")
	private final String cardNumber;

	@SerializedName("card_data")
	private final String cardData;

	@SerializedName("name")
	private final String name;

	@SerializedName("ip_address")
	private final String ipAdress;

	@SerializedName("card_type")
	private final int cardType;

	public CardForServer(String cardNumber, String cardData, String name, String ipAdress, int cardType) {
		this.cardNumber = cardNumber;
		this.cardData = cardData;
		this.name = name;
		this.ipAdress = ipAdress;
		this.cardType = cardType;
	}

	public String getName() {
		return name;
	}

	public String getIpAdress() {
		return ipAdress;
	}

	public String getCardNumber(){
		return cardNumber;
	}

	public String getCardData(){
		return cardData;
	}

	@Override
 	public String toString(){
		return 
			"CardForServer{" + 
			"card_number = '" + cardNumber + '\'' + 
			",card_data = '" + cardData + '\'' + 
			"}";
		}
}