package manfred.ru.manfredpassenger.cards.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ConfirmedCard{

	@SerializedName("transaction_id")
	private String transactionId;

	@SerializedName("pa_res")
	private String paRes;

	public ConfirmedCard(String transactionId, String paRes) {
		this.transactionId = transactionId;
		this.paRes = paRes;
	}

	public String getTransactionId(){
		return transactionId;
	}

	public String getPaRes(){
		return paRes;
	}

	@Override
 	public String toString(){
		return 
			"ConfirmedCard{" + 
			"transaction_id = '" + transactionId + '\'' + 
			",pa_res = '" + paRes + '\'' + 
			"}";
		}
}