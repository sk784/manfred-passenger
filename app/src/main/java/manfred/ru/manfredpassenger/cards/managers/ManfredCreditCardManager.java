package manfred.ru.manfredpassenger.cards.managers;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.token.ManfredTokenManager;
import manfred.ru.manfredpassenger.cards.models.CreditCardVM;
import manfred.ru.manfredpassenger.cards.repositories.CardRepository;
import manfred.ru.manfredpassenger.cards.repositories.WebSocketCardRepository;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.common.services.NetworkManagerProvider;
import manfred.ru.manfredpassenger.ride.managers.PreOrderManager;
import manfred.ru.manfredpassenger.ride.repositories.OrderRepository;
import manfred.ru.manfredpassenger.ride.repositories.SocketOrderRepository;

public class ManfredCreditCardManager implements CreditCardManager {
    private static final String TAG = "ManfredCreditCardManage";
    private final MutableLiveData<List<CreditCard>> creditCards;
    private final MutableLiveData<CreditCard> selectedCreditCard;
    private final CardRepository manfredCardRepository;
    private final OrderRepository orderRepository;
    private final MutableLiveData<ManfredNetworkError>errors;
    private final PreOrderManager preOrderManager;
    private final AnalyticsManager analyticsManager;

    public ManfredCreditCardManager(ManfredTokenManager manfredTokenManager, PreOrderManager preOrderManager,
                                    NetworkManagerProvider networkManager, AnalyticsManager analyticsManager) {
        this.manfredCardRepository = new WebSocketCardRepository(networkManager, manfredTokenManager);
        this.analyticsManager = analyticsManager;
        this.orderRepository = new SocketOrderRepository(manfredTokenManager, networkManager);
        this.preOrderManager = preOrderManager;
        this.creditCards = new MutableLiveData<>();
        this.selectedCreditCard = new MutableLiveData<>();
        this.errors = new MutableLiveData<>();
        refreshCards();
    }

    @Override
    public void addCard(CreditCard creditCard) {
        List<CreditCard>cards = creditCards.getValue();
        if(cards==null){
            cards = new ArrayList<>();
        }
        cards.add(creditCard);
        creditCards.postValue(cards);
    }

    @Override
    public void selectCard(int cardId) {
        List<CreditCard> cards = new ArrayList<>();
        if(creditCards.getValue()!=null) {
            for (CreditCard credCard : creditCards.getValue()) {
                if(credCard.getId()==cardId){
                    CreditCard selectedCard = new CreditCard(credCard,true);
                    cards.add(selectedCard);
                    selectedCreditCard.postValue(selectedCard);
                }else {
                    cards.add(new CreditCard(credCard,false));
                }
            }
            creditCards.postValue(cards);
        }

        manfredCardRepository.selectCard(cardId, new NetworkResponseCallback<CreditCard>() {
            @Override
            public void allOk(CreditCard response) {
                //Log.d(TAG, "selectCard allOk: "+response.toString());
                preOrderManager.refreshPreOrders();
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: "+error.toString());
                refreshCards();
            }
        });
    }

    @Override
    public void selectCardAndRepayOrder(int cardId, int orderId, NetworkResponseCallback callback) {
        manfredCardRepository.selectCard(cardId, new NetworkResponseCallback<CreditCard>() {
            @Override
            public void allOk(CreditCard response) {
                //Log.d(TAG, "selectCard allOk: "+response.toString());
                //Log.d(TAG, "selectCardAndRepayOrder allOk: card id "+cardId+" setted for active, truing pay order "+orderId);
                orderRepository.tryRepay(orderId, new NetworkResponseCallback() {
                    @Override
                    public void allOk(Object response) {
                        callback.allOk(null);
                    }

                    @Override
                    public void error(ManfredNetworkError error) {
                        //Log.d(TAG, "selectCardAndRepayOrder error: "+error);
                        callback.error(error);
                    }
                });
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: "+error.toString());
                refreshCards();
                callback.error(error);
            }
        });
    }


    @Override
    public void addCards(List<CreditCard> cards) {
        List<CreditCard>currCards = creditCards.getValue();
        if(currCards==null){
            currCards = new ArrayList<>();
        }
        currCards.addAll(cards);
        creditCards.postValue(currCards);
    }

    @Override
    public void deleteCard(CreditCardVM creditCardVM) {
        manfredCardRepository.removeCard(creditCardVM.getId(), new NetworkResponseCallback() {
            @Override
            public void allOk(Object response) {
                refreshCards();
                analyticsManager.cardDeleted(new Date(), creditCardVM.getLastFourDigitsWithStar().substring(creditCardVM.getLastFourDigitsWithStar().length()-4));
            }

            @Override
            public void error(ManfredNetworkError error) {
                Log.d(TAG, "error: "+error.toString());
            }
        });
    }

    @Override
    public void refreshCards(){
        manfredCardRepository.getCards(new NetworkResponseCallback<List<CreditCard>>() {
            @Override
            public void allOk(List<CreditCard> response) {
                //Log.d(TAG, "allOk: card refreshed "+response.toString());
                creditCards.postValue(response);
                for (CreditCard creditCard: response) {
                    if(creditCard.isActive()){
                        selectedCreditCard.postValue(creditCard);
                    }
                }
            }

            @Override
            public void error(ManfredNetworkError error) {

            }
        });
    }

    @Override
    public boolean isHaveCards() {
        boolean haveCards = false;
        List<CreditCard> cards = creditCards.getValue();
        if(cards!=null){
            if(!cards.isEmpty()){
                haveCards = true;
            }
        }
        return haveCards;
    }

    @Override
    public LiveData<List<CreditCard>> cards() {
        //Log.d(TAG, "cards: size is " + creditCards.size());
        return creditCards;
    }

    public MutableLiveData<CreditCard> getSelectedCreditCard() {
        return selectedCreditCard;
    }

    public MutableLiveData<ManfredNetworkError> getCardErrors() {
        return errors;
    }
}
