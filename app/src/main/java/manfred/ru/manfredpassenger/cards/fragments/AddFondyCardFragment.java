package manfred.ru.manfredpassenger.cards.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.cloudipsp.android.Card;
import com.cloudipsp.android.CardInputLayout;
import com.cloudipsp.android.Cloudipsp;
import com.cloudipsp.android.Currency;
import com.cloudipsp.android.Order;
import com.cloudipsp.android.Receipt;

import java.util.Date;
import java.util.Objects;

import manfred.ru.manfredpassenger.ManfredPassengerApplication;
import manfred.ru.manfredpassenger.api.ManfredNetworkError;
import manfred.ru.manfredpassenger.api.NetworkResponseCallback;
import manfred.ru.manfredpassenger.api.services.models.CreditCardForServer;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;
import manfred.ru.manfredpassenger.cards.repositories.WebSocketCardRepository;
import manfred.ru.manfredpassenger.common.managers.AnalyticsManager;
import manfred.ru.manfredpassenger.databinding.FragmentAddCardBinding;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddFondyCardFragment extends Fragment {
    private static final String TAG = "AddFondyCardFragment";
    private FragmentAddCardBinding binding;

    public AddFondyCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final int MERCHANT_ID = 1402367;
        // Inflate the layout for this fragment
        binding = FragmentAddCardBinding.inflate(getLayoutInflater());
        AnalyticsManager analyticsManager = ((ManfredPassengerApplication)getActivity().getApplication()).getAnalyticsManager();
        binding.addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Card card = binding.cardLayout.confirm(new CardInputLayout.ConfirmationErrorHandler() {
                    @Override
                    public void onCardInputErrorClear(CardInputLayout view, EditText editText) {
                    }

                    @Override
                    public void onCardInputErrorCatched(CardInputLayout view, EditText editText, String error) {
                        editText.setError(error);
                        Log.d(TAG, "onCardInputErrorCatched: " + error);
                    }
                });
                if (card != null) {
                    binding.addCard.setEnabled(false);
                    final Order order = new Order(100, Currency.RUB, "vb_" + System.currentTimeMillis(), "cardVerification");
                    order.setLang(Order.Lang.ru);
                    Cloudipsp cloudipsp = new Cloudipsp(MERCHANT_ID, binding.addCardWebView);
                    order.setVerification(true);
                    order.setRequiredRecToken(true);
                    cloudipsp.pay(card, order, new Cloudipsp.PayCallback() {
                        @Override
                        public void onPaidProcessed(Receipt receipt) {
                            String infoMessage = "Paid " + receipt.status.name() + "\nPaymentId:" + receipt.paymentId + ", token " + receipt.recToken +
                                    " ,type: " + receipt.cardType;
                            //Toast.makeText(getActivity(), infoMessage, Toast.LENGTH_LONG).show();
                            //Log.d(TAG, "onPaidProcessed: " + infoMessage);
                            if (receipt.status.name().equals("approved")) {
                                sendCardToServer(receipt);
                            } else {
                                Toast.makeText(getContext(), "Ошибка добавления карты: " + receipt.status.name(), Toast.LENGTH_LONG).show();
                            }
                            analyticsManager.cardAdded(new Date(),receipt.maskedCard.substring(receipt.maskedCard.length()-4));

                        }

                        @Override
                        public void onPaidFailure(Cloudipsp.Exception e) {
                            Toast.makeText(getActivity(), "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                            analyticsManager.addingCardError(e.getMessage(),"");
                            //Log.d(TAG, "onPaidFailure: " + e.getMessage());
                        }
                    });
                }
            }
        });
        return binding.getRoot();
    }

    private void sendCardToServer(Receipt receipt) {
        WebSocketCardRepository manfredCardRepository =
                new WebSocketCardRepository(((ManfredPassengerApplication)getActivity().getApplication()).getNetworkManager(),
                        ((ManfredPassengerApplication)getActivity().getApplication()).getTokenManager());
        manfredCardRepository.sendNewCard(new CreditCardForServer(receipt.maskedCard, receipt.recToken, receipt.cardType.ordinal()),
                new NetworkResponseCallback<CreditCard>() {
                    @Override
                    public void allOk(CreditCard response) {
                        ((ManfredPassengerApplication)getActivity().getApplication()).getManfredCreditCardManager().refreshCards();
                        ((ManfredPassengerApplication)getActivity().getApplication()).getAppStatusManager().skipNeedingCard();
                        Objects.requireNonNull(getActivity()).finish();
                    }

                    @Override
                    public void error(ManfredNetworkError error) {
                        switch (error.getType()) {
                            case SERVER_UNAVAILABLE:
                                Toast.makeText(getContext(), "Сервер недоступен", Toast.LENGTH_LONG).show();
                                break;
                            case TOKEN_ERROR:
                                Toast.makeText(getContext(), error.getText(), Toast.LENGTH_LONG).show();
                                break;
                            case OTHER:
                                Toast.makeText(getContext(), error.getText(), Toast.LENGTH_LONG).show();
                                break;
                        }
                        binding.addCard.setEnabled(true);
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        View view = getView();
        if(view!=null) {
            //EditText cardNumber = view.findViewById(R.id.edit_card_number);
            binding.editCardNumber.requestFocus();
            binding.editCardNumber.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(getActivity()!=null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.showSoftInput(binding.editCardNumber, 0);
                        }
                    }
                }
            },200);
        }
    }

}
