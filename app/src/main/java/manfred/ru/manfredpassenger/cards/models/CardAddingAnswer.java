package manfred.ru.manfredpassenger.cards.models;

import android.support.annotation.Nullable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import manfred.ru.manfredpassenger.api.services.models.Cards;
import manfred.ru.manfredpassenger.cards.managers.CreditCard;

@Generated("com.robohorse.robopojogenerator")
public class CardAddingAnswer {

    @SerializedName("transaction_id")
	private final int transactionId;

    @SerializedName("pa_req")
	private final String paReq;

    @SerializedName("auth_required")
	private final boolean authRequired;

    @SerializedName("auth_url")
	private final String authUrl;

    @SerializedName("card")
	@Nullable private final CreditCard card;

    public CardAddingAnswer(int transactionId, String paReq, boolean authRequired, String authUrl, @Nullable CreditCard card) {
		this.transactionId = transactionId;
		this.paReq = paReq;
		this.authRequired = authRequired;
		this.authUrl = authUrl;
        this.card = card;
    }

	public int getTransactionId(){
		return transactionId;
	}

	public String getPaReq(){
		return paReq;
	}

	public boolean isAuthRequired(){
		return authRequired;
	}

	public String getAuthUrl(){
		return authUrl;
	}

    @Nullable
    public CreditCard getCard() {
        return card;
    }

	@Override
	public String toString(){
		return
				"Response{" +
						"transaction_id = '" + transactionId + '\'' +
						",pa_req = '" + paReq + '\'' +
						",auth_required = '" + authRequired + '\'' +
						",auth_url = '" + authUrl + '\'' +
						"}";
	}
}