package manfred.ru.manfredpassenger.cards.managers;

import com.google.gson.annotations.SerializedName;

import manfred.ru.manfredpassenger.common.models.PaymentSystem;

public class CreditCard {
    /* Типы банковских карт */
    public static final int CARD_TYPE_UNKNOWN = 0;
    public static final int CARD_TYPE_VISA = 1;
    public static final int CARD_TYPE_MASTERCARD = 2;
    public static final int CARD_TYPE_MAESTRO = 3;
    public static final int CARD_TYPE_GOOGLEPAY = 100;
    public static final int CARD_TYPE_APPLEPAY = 101;
    public static final int CARD_TYPE_CASH = 102;

    @SerializedName("card_number")
    private final String cardNumber;

    @SerializedName("active")
    private final boolean active;

    @SerializedName("id")
    private final int id;

    @SerializedName("payment_system")
    private final PaymentSystem paymentSystem;

    @Override
    public String toString() {
        return "CreditCard{" +
                "cardNumber='" + cardNumber + '\'' +
                ", active=" + active +
                ", id=" + id +
                ", cardType=" + cardType +
                '}';
    }

    /*
        CardTypeUnknown =0
        CardTypeVisa = 1
        CardTypeMastercard = 2
        CardTypeMaestro= 3
         */
    @SerializedName("card_type")
    private final int cardType;

    public CreditCard(String cardNumber, boolean active, int id, int cardType, PaymentSystem paymentSystem) {
        this.cardNumber = cardNumber;
        this.active = active;
        this.id = id;
        this.cardType = cardType;
        this.paymentSystem = paymentSystem;
    }

    public CreditCard(CreditCard creditCard, boolean isActive) {
        this.cardNumber = creditCard.getCardNumber();
        this.active = isActive;
        this.id = creditCard.getId();
        this.cardType = creditCard.getCardType();
        this.paymentSystem = creditCard.getPaymentSystem();

    }

    public String getCardNumber() {
        return cardNumber;
    }

    public boolean isActive() {
        return active;
    }

    public int getId() {
        return id;
    }

    public int getCardType() {
        return cardType;
    }

    public PaymentSystem getPaymentSystem() {
        return paymentSystem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreditCard that = (CreditCard) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}